import 'package:get/get.dart';
import 'package:karyo/karyo.dart';

class LoginController extends GetxController {
  final localUser = Rxn<User>();

  CommonAPIClient client = new CommonAPIClient();

  void hello() async {
    var token = await SharedPreferenceUtil.get(SharedPreferenceUtil.KEY_TOKEN);
    if (token != null && token != '') {
      GlobalSettings.setToken(token);
      GlobalSettings.setUranusToken(token);
      var user = await client.getUser(token);
      if (user != null) {
        localUser.value = user;
      }
    }
  }
}
