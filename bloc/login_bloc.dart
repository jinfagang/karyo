import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:karyo/karyo.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc implements BlocBase {
  BehaviorSubject<User> _userController = BehaviorSubject<User>();
  Stream<User> get user => _userController.stream;

  final userRx = User(
          userAcc: '',
          userAddr: '',
          userNickName: '',
          userAvatarUrl: '',
          userBirth: '',
          userCity: '',
          userSign: '')
      .obs;
    
  StreamController<bool> _syncDoneController =
      StreamController<bool>.broadcast();
  Stream<bool> get syncDone => _syncDoneController.stream;

  StreamController<File> _pickedImgFile = StreamController<File>.broadcast();
  Stream<File> get pickedImgFile => _pickedImgFile.stream;

  StreamController<bool> _forceSyncDoneController =
      StreamController<bool>.broadcast();
  Stream<bool> get forceSyncDone => _forceSyncDoneController.stream;

  StreamController<ManaUser> _manaUserController =
      StreamController<ManaUser>.broadcast();
  Stream<ManaUser> get manaUser => _manaUserController.stream;

  bool isLogin = false;
  CommonAPIClient client = new CommonAPIClient();
  String? _token;

  File? selectedAvatarFile;

  LoginBloc() {
    hello();
    scheduleTimeout(3 * 1000);
  }

  Timer scheduleTimeout([int milliseconds = 10000]) =>
      Timer(Duration(milliseconds: milliseconds), handleTimeout);

  void handleTimeout() {
    _syncDoneController.add(true);
    _syncDoneController.add(true);
  }

  void setSelectedAvatarFile(File f) {
    _pickedImgFile.add(f);
    selectedAvatarFile = f;
  }

  Stream<User> getUserStream() {
    return _userController.stream;
  }

  void hello() async {
    // read token from local
    // and say hello to server to get latest user information
    var token = await SharedPreferenceUtil.get(SharedPreferenceUtil.KEY_TOKEN);
    kLog('!!!!! token: $token');
    if (token != null && token != '') {
      _token = token;
      GlobalSettings.setToken(token);
      GlobalSettings.setUranusToken(token);
      // request for user using token
      var user = await client.getUser(token);
      if (user != null) {
        userRx.value = user;
        _userController.sink.add(user);
        // forceSync();
        _syncDoneController.add(true);
      }
      _syncDoneController.add(true);
    }

    var _t =
        await SharedPreferenceUtil.get(SharedPreferenceUtil.KEY_TOKEN_MANA);
    // get MANA user
    if (_t != null) {
      var _manaUser = await client.getMANAUser(_t);
      if (_manaUser != null) {
        _manaUserController.add(_manaUser);
      }
    }
    _syncDoneController.add(true);
  }

  void helloGetUser() async {
    var token = await SharedPreferenceUtil.get(SharedPreferenceUtil.KEY_TOKEN);
    _token = token;
    // request for user using token
    var user = await client.getUser(token!);
    if (user != null) {
      _userController.sink.add(user);
      userRx.value = user;
    }
  }

  void forceSync() async {}
  // login logic, and the request login status
  // when APP starts
  Future<bool> login(name, password, context, {doPop = true}) async {
    if (name != '' && password != '') {
      var res = await client.login(name, password, context);
      if (kDebugMode) {
        print('login result from client? $res');
      }
      if (res != null) {
        // 登录成功
        print('登录成功');
        isLogin = true;
        GlobalSettings.setUserAddr(res.userAddr);
        _userController.sink.add(res);
        hello();
        if (doPop) {
          Navigator.of(context).pop();
          _syncDoneController.add(true);
          _forceSyncDoneController.add(true);
        }
        return true;
      } else {
        print('登录失败');
        return false;
      }
    } else {
      showToast(context, '用户名或者密码不能为空', COLOR_PRIMARY);
      return false;
    }
  }

  void addUser(User user) {
    _userController.sink.add(user);
  }

  Future<bool> loginPure(name, password) async {
    if (name != '' && password != '') {
      var res = await client.login(name, password, null);
      if (kDebugMode) {
        debugPrint('login result from client? $res');
      }
      if (res != null) {
        // 登录成功
        debugPrint('登录成功');
        isLogin = true;
        GlobalSettings.setUserAddr(res.userAddr);
        _userController.sink.add(res);
        userRx.value = res;
        hello();
        return true;
      } else {
        debugPrint('登录失败');
        return false;
      }
    } else {
      return false;
    }
  }

  Future<bool> register(name, password, context) async {
    if (name != '' && password != '') {
      var res = await client.register(name, password, context);
      if (res != null) {
        // 登录成功
        print('登录成功');
        isLogin = true;
        _userController.sink.add(res);
        userRx.value = res;
        return true;
      } else {
        print('登录失败');
        return false;
      }
    } else {
      showToast(context, '用户名或者密码不能为空', COLOR_PRIMARY);
      return false;
    }
  }

  Future<bool> registerWithInviteCode(
      name, password, inviteCode, context) async {
    if (name != '' && password != '') {
      var res = await client.registerWithInviteCode(
          name, password, inviteCode, context);
      if (res != null) {
        kLog('登录成功');
        isLogin = true;
        _userController.sink.add(res);
        userRx.value = res;
        return true;
      } else {
        kLog('登录失败');
        return false;
      }
    } else {
      showToast(context, '用户名或密码或邀请码均不能为空', COLOR_PRIMARY);
      return false;
    }
  }

  Future<bool> updateUser(User newUser, BuildContext context) async {
    // update user info
    var _u = await client.updateUserInfo(newUser, _token!, context);
    if (_u != null) {
      _userController.sink.add(_u);
      userRx.value = _u;
      // showToast(context, "更新个人信息成功");
      return true;
    }
    // showToast(context, "更新个人信息失败");
    return false;
  }

  @override
  void dispose() {
    _userController.close();
    _syncDoneController.close();
    _forceSyncDoneController.close();
    _manaUserController.close();
  }
}
