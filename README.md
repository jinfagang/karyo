# Karyo



karyo is the core lib of my all APPs. It bridge the gap between different apps which using same components. The apps include Daybreak, Colibri, DragonRuler etc.

The common part would consist of these parts:

- [ ] app utils;
- [ ] color utils;
- [ ] time utils;
- [ ] apis;
- [ ] bloc;
- [ ] common widgets;

Once a components had an update, all apps using that would update too.



## Copyright

All rights reserved by Lucas Jin. Codes released under GPL license.

