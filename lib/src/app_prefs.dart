import 'package:shared_preferences/shared_preferences.dart';

// All possible settings in MANA apps
// such as Daybreak, Colibri, Mana etc.

class SharedPreferenceUtil {
  static const String KEY_ACCOUNT = "account";
  static const String KEY_ACCOUNT_ADDR = "user_addr";
  static const String KEY_USER_AVATAR_URL = "user_avata_url";
  static const String KEY_USER_NICK_NAME = "user_nick_name";
  static const String KEY_USER_SIGN = "user_sign";
  static const String KEY_TOKEN = "token";
  static const String KEY_THEME_INDEX = "themeIndex";

  static const String KEY_CLIENT_LAST_SYNC_ID = "client_last_sync_id";
  static const String KEY_SERVER_LAST_SYNC_ID = "server_last_sync_id";

  static const String KEY_CLIENT_LAST_SYNC_ID_PROJECT =
      "client_last_sync_id_project";
  static const String KEY_SERVER_LAST_SYNC_ID_PROJECT =
      "server_last_sync_id_project";

  static const String KEY_SETTING_FORCE_DARK = "is_force_dark";
  static const String KEY_THEME_COLOR = "theme_color";
  static const String KEY_USE_BLUR = "use_blur";
  static const String KEY_INTRO_READ = "intro_read";
  static const String KEY_PRE_VERSION = "pre_version";

  static const String KEY_VIP_PLAN = "vip_pan";
  static const String KEY_VIP_START_TIME = "vip_start_time";

  static const String KEY_SETTING_USE_IOS = "using_ios_bk";
  static const String KEY_SETTING_USE_GOD_MODE = "using_god_mode";
  static const String KEY_SETTING_USE_WAVE_PG = "using_wave_progress";
  static const String KEY_SETTING_CHANGE_LINE_COMPLETE = "change_line_complete";
  static const String KEY_SETTING_WENLI_BK = "KEY_SETTING_WENLI_BK";
  static const String KEY_SETTING_SIM_MODE = "sim_mode";
  // 开启仪式感字体
  static const String KEY_SETTING_YISHIGAN = "yishigan";
  static const String KEY_FONT_SCALE_LEVEL = "fontScaleLevel2";
  static const String KEY_TAB_LABEL = "tabLabelShown2";

  static const String KEY_SETTING_SORT_BY = "sort_by";
  static const String KEY_GOD_LAYOUT = "god_layout";
  static const String KEY_TODAY_RUNED = "KEY_TODAY_RUNED";

  static const String KEY_IS_LISTSHOW_PROJ = "KEY_IS_LISTSHOW_PROJ";

  static const String KEY_TOKEN_MANA = "token_mana";
  static const String KEY_TOKEN_URANUS = "token_uranus";

  static const String KEY_OPENAI_USAGE = "oepnai_usage";
  static const String KEY_OPENAI_KEY = "oepnai_key";

  // settings for neso
  static const String KEY_SETTING_IS_ENTER_SEND = "isEnterSend";
  static const String KEY_SETTING_IS_SHOW_INCOME_LABEL = "isShowIncomingLabel";
  static const String KEY_SETTING_IS_SHOW_ME_AVATAR = "isShowMeAvatar";

  static Future save(String key, String value) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString(key, value);
  }

  static Future<String?> get(String key) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    return sp.getString(key);
  }

  static Future saveBool(String key, bool value) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setBool(key, value);
  }

  static Future<bool> getBool(String key, {bool v = false}) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    return sp.getBool(key) ?? v;
  }

  static Future saveInt(String key, int value) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setInt(key, value);
  }

  static Future<int?> getInt(String key) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    return sp.getInt(key);
  }

  static Future saveDouble(String key, double value) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setDouble(key, value);
  }

  static Future<double?> getDouble(String key) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    return sp.getDouble(key);
  }
}
