import 'dart:async';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';


class SettingsBloc implements BlocBase {
  BehaviorSubject<String> _appVersion = BehaviorSubject<String>();

  Stream<String> get appVersion => _appVersion.stream;

  BehaviorSubject<double> _fontScaleValue = BehaviorSubject<double>();

  Stream<double> get fontScaleValue => _fontScaleValue.stream;

  BehaviorSubject<bool> _forceDark = BehaviorSubject<bool>();

  Stream<bool> get forceDark => _forceDark.stream;

  BehaviorSubject<bool> _usingIOSBackground = BehaviorSubject<bool>();

  Stream<bool> get usingIOSBackground => _usingIOSBackground.stream;

  BehaviorSubject<bool> _usingBlur = BehaviorSubject<bool>();

  Stream<bool> get usingBlur => _usingBlur.stream;

  BehaviorSubject<bool> _usingTabLabel = BehaviorSubject<bool>();

  Stream<bool> get usingTabLabel => _usingTabLabel.stream;

  BehaviorSubject<bool> _usingSimMode = BehaviorSubject<bool>();

  Stream<bool> get usingSimMode => _usingSimMode.stream;

  BehaviorSubject<bool> _usingGodMode = BehaviorSubject<bool>();

  Stream<bool> get usingGodMode => _usingGodMode.stream;

  BehaviorSubject<bool> _usingYishi = BehaviorSubject<bool>();

  Stream<bool> get usingYishi => _usingYishi.stream;

  BehaviorSubject<bool> _usingChangeLineSubmit = BehaviorSubject<bool>();

  Stream<bool> get usingChangeLineSubmit => _usingChangeLineSubmit.stream;

  BehaviorSubject<bool> _usingWaveProgress = BehaviorSubject<bool>();

  Stream<bool> get usingWaveProgress => _usingWaveProgress.stream;

  BehaviorSubject<bool> _noteGridView = BehaviorSubject<bool>();

  Stream<bool> get noteGridView => _noteGridView.stream;

  BehaviorSubject<bool> _noteMarkdown = BehaviorSubject<bool>();

  Stream<bool> get noteMarkDown => _noteMarkdown.stream;

  BehaviorSubject<bool> _personalStatic = BehaviorSubject<bool>();

  Stream<bool> get personalStatic => _personalStatic.stream;

  BehaviorSubject<int> _savedColor = BehaviorSubject<int>();

  Stream<int> get savedColor => _savedColor.stream;

  BehaviorSubject<int> _savedThemeIndex = BehaviorSubject<int>();

  Stream<int> get themeIndex => _savedThemeIndex.stream;

  BehaviorSubject<int> _gLayoutNum = BehaviorSubject<int>();

  Stream<int> get gLayoutNum => _gLayoutNum.stream;

  BehaviorSubject<VIPStatus> _vipStatusControl = BehaviorSubject<VIPStatus>();

  Stream<VIPStatus> get vipStatus => _vipStatusControl.stream;

  BehaviorSubject<Map<String, int>> _uranusStat =
  BehaviorSubject<Map<String, int>>();

  Stream<Map<String, int>> get uranusStat => _uranusStat.stream;

  VIPStatus vipInfo = VIPStatus.NO_VIP;

  AboutBloc() {
    _getAppName();
    // found settings from shared
    _initSettings();
  }

  void _initSettings() async {
    var _fd = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_FORCE_DARK);
    var _cs = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_CHANGE_LINE_COMPLETE);
    var _c =
    await SharedPreferenceUtil.getInt(SharedPreferenceUtil.KEY_THEME_COLOR);
    if (_c == null) {
      _c = COLOR_PRIMARY.value;
    }
    var _ti =
    await SharedPreferenceUtil.getInt(SharedPreferenceUtil.KEY_THEME_INDEX);
    if (_ti == null) {
      _ti = 0;
    }
    _savedThemeIndex.add(_ti);
    var _ios = await GlobalSettings.getUsingIOSBackground();
    var _sm = await GlobalSettings.getUsingSimMode();
    var _ys = await GlobalSettings.getUsingYishi();
    var _gm = await GlobalSettings.getUsingGodMode();
    var _ln = await GlobalSettings.getGodLayoutNum();
    var _ub = await GlobalSettings.getUsingBlur();
    var _ut = await GlobalSettings.getTabLabel();
    _usingIOSBackground.add(_ios);
    _usingBlur.add(_ub);
    _usingChangeLineSubmit.add(_cs);
    _usingSimMode.add(_sm ?? false);
    _usingYishi.add(_ys ?? false);
    _savedColor.add(_c);
    _forceDark.add(_fd);
    _usingGodMode.add(_gm);
    _gLayoutNum.add(_ln);
    _usingTabLabel.add(_ut);

    debugPrint("------- force dark mode?? $_fd");

    var _vipPlan =
    await SharedPreferenceUtil.getInt(SharedPreferenceUtil.KEY_VIP_PLAN);
    var _vipStartTime = await SharedPreferenceUtil.getInt(
        SharedPreferenceUtil.KEY_VIP_START_TIME);
    VIPStatus _vipStatus;
    if (_vipPlan == null) {
      _vipStatus = VIPStatus.NO_VIP;
    } else if (IsVipValid(
        _vipPlan, DateTime.fromMillisecondsSinceEpoch(_vipStartTime!))) {
      _vipStatus = VIPStatus.VIP_OK;
    } else {
      _vipStatus = VIPStatus.VIP_EXPIRED;
    }
    vipInfo = _vipStatus;
    _vipStatusControl.add(_vipStatus);
  }

  void _getAppName() async {
    // judge if Mobile or Windows or something
    if (isOnMobile()) {
      var _a = await getAppVersion();
      _appVersion.add(_a);
    } else {
      var _appVer = await getAppVersion();
      var _appBuildNum = await getBuildNumber();
      // _appVersion.add('PC版本号: ${PC_VERSION}, BuildNumber: ${_appBuildNum}');
      _appVersion.add('PC版本号: ${_appVer}, BuildNumber: ${_appBuildNum}');
    }
  }

  void shareWechat() {
    // shareToWeChat(WeChatShareWebPageModel("http://db.tsuai.cn",
    //     title: "一个小而美的todo APP来啦！", scene: WeChatScene.TIMELINE));
  }

  void setThemeColor(int v) {
    _savedColor.add(v);
    COLOR_PRIMARY = Color(v);
    SharedPreferenceUtil.saveInt(SharedPreferenceUtil.KEY_THEME_COLOR, v);
  }

  void setUsingIOSBackground(bool v) {
    _usingIOSBackground.add(v);
    GlobalSettings.setUsingIOSBackground(v);
  }

  void setUsingBlur(bool v) {
    _usingBlur.add(v);
    GlobalSettings.setUsingBlur(v);
  }

  void setUsingSimMode(bool v) {
    _usingSimMode.add(v);
    GlobalSettings.setUsingSimMode(v);
  }

  void setUsingYishi(bool v) {
    _usingYishi.add(v);
    GlobalSettings.setUsingYishi(v);
  }

  void setChangeLineSubmit(bool v) {
    _usingChangeLineSubmit.add(v);
    GlobalSettings.setUsingChangeLineSubmit(v);
  }

  void setUsingWaveProgress(bool v) {
    _usingWaveProgress.add(v);
    GlobalSettings.setUsingWaveProgress(v);
  }

  void setForceDark(v) {
    _forceDark.add(v);
    GlobalSettings.setForceDark(v);
  }

  void setFontScaleValue(l) {
    _fontScaleValue.add(l);
  }

  void setGodMode(bool v) {
    _usingGodMode.add(v);
    GlobalSettings.setUsingGodMode(v);
  }

  void setGodModeLayoutNum(int v) {
    _gLayoutNum.add(v);
    GlobalSettings.setGodLayoutNum(v);
  }

  void setNoteGridView(v) {
    _noteGridView.add(v);
  }

  void setNoteMarkDown(v) {
    _noteMarkdown.add(v);
  }

  void setTabLabel(bool v) {
    _usingTabLabel.add(v);
    GlobalSettings.setTabLabel(v);
  }

  void setThemeIndex(int i) {
    _savedThemeIndex.add(i);
    GlobalSettings.setThemeIndex(i);
  }

  @override
  void dispose() {
    _appVersion.close();
    _forceDark.close();
    _savedColor.close();
    _vipStatusControl.close();
    _usingWaveProgress.close();
    _usingChangeLineSubmit.close();
    _usingSimMode.close();
    _usingYishi.close();
    _fontScaleValue.close();
    _noteGridView.close();
    _noteMarkdown.close();
    _uranusStat.close();
    _usingGodMode.close();
    _gLayoutNum.close();
    _usingTabLabel.close();
    _savedThemeIndex.close();
  }
}
