import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:karyo/karyo.dart';

import 'package:flutter/material.dart';

class SettingsItem extends StatelessWidget {
  final IconData icons;
  final IconStyle? iconStyle;
  final String title;
  final TextStyle? titleStyle;
  final String? subtitle;
  final TextStyle? subtitleStyle;
  final Widget? trailing;
  final VoidCallback? onTap;
  final Color? backgroundColor;

  SettingsItem(
      {required this.icons,
      this.iconStyle,
      required this.title,
      this.titleStyle,
      this.subtitle,
      this.subtitleStyle,
      this.backgroundColor,
      this.trailing,
      this.onTap});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(15),
      child: InkWell(
        borderRadius: BorderRadius.circular(20),
        splashColor: tClrTertiaryContainer(context),
        onTap: onTap,
        child: ListTile(
          // onTap: onTap,
          // contentPadding: EdgeInsets.all(0),
          dense: true,
          // visualDensity: VisualDensity.compact,
          leading: (iconStyle != null && iconStyle!.withBackground!)
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: iconStyle!.backgroundColor,
                        borderRadius:
                            BorderRadius.circular(iconStyle!.borderRadius!),
                      ),
                      padding: EdgeInsets.all(2),
                      child: Icon(
                        icons,
                        size: SettingsScreenUtils.settingsGroupIconSize,
                        color: iconStyle!.iconsColor,
                      ),
                    ),
                  ],
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(2),
                      child: Icon(
                        icons,
                        size: SettingsScreenUtils.settingsGroupIconSize,
                      ),
                    ),
                  ],
                ),
          title: Text(
            title,
            style: titleStyle ??
                NORMAL_TXT_STYLE.copyWith(
                    color: tClrOnPrimaryContainer(context)),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          subtitle: (subtitle != null)
              ? Text(
                  subtitle!,
                  style: subtitleStyle ?? SMALL_TXT_STYLE,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                )
              : null,
          trailing: (trailing != null)
              ? trailing
              : Icon(
                  FluentIcons.chevron_right_12_filled,
                  size: 18,
                ),
        ),
      ),
    );
  }
}
