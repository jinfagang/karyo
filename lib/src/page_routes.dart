import 'package:animations/animations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyMaterialPageRoute extends MaterialPageRoute {
  final int duration;
  MyMaterialPageRoute({required WidgetBuilder builder, this.duration = 200})
      : super(builder: builder);

  @override
  Duration get transitionDuration => Duration(milliseconds: duration);
}

class MyCupertinoPageRoute extends CupertinoPageRoute {
  final int duration;
  final int reverseDuration;

  MyCupertinoPageRoute(
      {required WidgetBuilder builder,
      this.duration = 300,
      this.reverseDuration = 300})
      : super(builder: builder);

  @override
  Duration get transitionDuration => Duration(milliseconds: duration);

  @override
  Duration get reverseTransitionDuration =>
      Duration(milliseconds: reverseDuration);
}

class SharedAxisPageRoute extends PageRouteBuilder {
  SharedAxisPageRoute(
      {required Widget page,
      SharedAxisTransitionType transitionType =
          SharedAxisTransitionType.horizontal})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation primaryAnimation,
            Animation secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> primaryAnimation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) {
            return SharedAxisTransition(
              animation: primaryAnimation,
              secondaryAnimation: secondaryAnimation,
              transitionType: transitionType,
              child: child,
            );
          },
        );
}

class FadeThroughPageRoute extends PageRouteBuilder {
  FadeThroughPageRoute(
      {required Widget page,
      SharedAxisTransitionType transitionType =
          SharedAxisTransitionType.vertical})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation primaryAnimation,
            Animation secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> primaryAnimation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) {
            return FadeThroughTransition(
              animation: primaryAnimation,
              secondaryAnimation: secondaryAnimation,
              child: child,
            );
          },
        );
}
