import 'dart:async';

import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';
import 'package:sheet/sheet.dart';

class SnapSheet extends StatefulWidget {
  final Widget? child;
  const SnapSheet({super.key, this.child});

  @override
  State<SnapSheet> createState() => _SnapSheetState();
}

class _SnapSheetState extends State<SnapSheet> {
  final SheetController controller = SheetController();

  @override
  void initState() {
    Future<void>.delayed(const Duration(milliseconds: 400), animateSheet);

    super.initState();
  }

  void animateSheet() {
    controller.relativeAnimateTo(
      0.3,
      duration: const Duration(milliseconds: 400),
      curve: Curves.easeOut,
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Sheet(
        elevation: 8,
        physics: const SnapSheetPhysics(
          stops: <double>[0.1, 0.5, 0.8],
          parent: BouncingSheetPhysics(),
        ),
        controller: controller,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(22), topRight: Radius.circular(22))),
        child: Column(children: [
          g8,
          Container(
            width: 22,
            height: 8,
            decoration: BoxDecoration(
                color: Colors.grey.withAlpha(100),
                borderRadius: BorderRadius.circular(10)),
            child: Container(),
          ),
          widget.child ?? const Text('Default Content'),
        ]));
  }
}
