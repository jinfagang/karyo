import 'models/user.dart';

bool IsVipValid(int vipPlan, DateTime vipStartTime) {
  if (vipPlan == 1) {
    // half year plan -> 1 year plan
    var shouldEndTime = vipStartTime.add(Duration(days: 30 * 12));
    if (shouldEndTime.isAfter(DateTime.now())) {
      //should time not arrive yet
      return true;
    } else {
      return false;
    }
  } else if (vipPlan == 2) {
    // one year plan -> 2 year plan
    var shouldEndTime = vipStartTime.add(Duration(days: 365 * 2));
    if (shouldEndTime.isAfter(DateTime.now())) {
      //should time not arrive yet
      return true;
    } else {
      return false;
    }
  } else if (vipPlan == 3) {
    // forever plan
    return true;
  } else {
    return false;
  }
}

bool IsUserVIP(User user) {
  if (user.vipPlan != null) {
    return IsVipValid(user.vipPlan!, user.vipStartTime!);
  }
  return false;
}
