import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';
import './text_field_container.dart';

class RoundedInputField extends StatelessWidget {
  final String? hintText;
  final IconData? icon;
  final ValueChanged<String>? onChanged;
  final TextEditingController? controller;
  const RoundedInputField(
      {Key? key,
      this.hintText,
      this.icon = Icons.person,
      this.onChanged,
      this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        controller: controller,
        onChanged: onChanged,
        cursorColor: tClrOnTertiaryContainer(context),
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: tClrOnTertiaryContainer(context),
            size: 16,
          ),
          hintStyle:
              NORMAL_TXT_STYLE.copyWith(color: Colors.grey.withAlpha(80)),
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}
