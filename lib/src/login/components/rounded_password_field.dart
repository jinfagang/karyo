import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';
import 'text_field_container.dart';

class RoundedPasswordField extends StatefulWidget {
  final ValueChanged<String>? onChanged;
  final TextEditingController? controller;
  const RoundedPasswordField({Key? key, this.onChanged, this.controller})
      : super(key: key);

  @override
  _RoundedPasswordFieldState createState() => _RoundedPasswordFieldState();
}

class _RoundedPasswordFieldState extends State<RoundedPasswordField> {
  bool _obscure = true;

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        controller: widget.controller,
        obscureText: _obscure,
        onChanged: widget.onChanged,
        cursorColor: COLOR_PRIMARY,
        decoration: InputDecoration(
          hintText: "密码",
          icon: Icon(
            Icons.lock,
            color: tClrOnTertiaryContainer(context),
            size: 15,
          ),
          hintStyle:
              NORMAL_TXT_STYLE.copyWith(color: Colors.grey.withAlpha(80)),
          suffixIcon: IconButton(
              icon: Icon(
                Icons.visibility,
                color: tClrOnTertiaryContainer(context),
              ),
              onPressed: () {
                setState(() {
                  _obscure = !_obscure;
                });
              }),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
