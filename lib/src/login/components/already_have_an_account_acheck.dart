import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class AlreadyHaveAnAccountCheck extends StatelessWidget {
  final bool login;
  final Function()? press;
  const AlreadyHaveAnAccountCheck({
    Key? key,
    this.login = true,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          login ? "还没有账号吗? " : "已经有帐号了吗? ",
          style: NORMAL_S_TXT_STYLE.copyWith(color: Colors.grey.shade400),
        ),
        GestureDetector(
          onTap: press,
          child: Text(
            login ? "注册" : "登录",
            style: NORMAL_S_TXT_STYLE.copyWith(
                color: tClrOnTertiaryContainer(context),
                fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }
}
