import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class RoundedButton extends StatelessWidget {
  final String? text;
  final Function()? press;
  final Color? color, textColor;
  const RoundedButton({
    Key? key,
    this.text,
    this.press,
    this.color,
    this.textColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Theme(
        data: ThemeData(
          shadowColor: Colors.grey.withAlpha(10),
        ),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: this.color ?? COLOR_PRIMARY, // background
            foregroundColor: Colors.white,
            elevation: 12,
            // shape:
            //     BeveledRectangleBorder(borderRadius: BorderRadius.circular(8)),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            shadowColor: COLOR_PRIMARY.withAlpha(60),
            minimumSize: Size(148, 60),
          ),
          onPressed: press,
          child: Text(
            text!,
            style: NORMAL_BOLD_TXT_STYLE.copyWith(color: textColor),
          ),
        ));
  }
}
