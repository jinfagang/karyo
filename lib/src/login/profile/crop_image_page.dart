import 'dart:io';

import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';
import 'package:image_cropper/image_cropper.dart';

Future<String> cropPickedImage(File originalFile, BuildContext context) async {
  // final crop = cropKey.currentState
  CroppedFile? croppedFile = await ImageCropper().cropImage(
    sourcePath: originalFile.path,
    aspectRatio: const CropAspectRatio(ratioX: 0.5, ratioY: 0.5),
    uiSettings: [
      AndroidUiSettings(
          toolbarTitle: '图片裁剪',
          toolbarColor: tClrOnPrimaryContainer(context),
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.square,
          lockAspectRatio: false),
      IOSUiSettings(
        title: '图片裁剪',
      ),
      WebUiSettings(
        context: context,
      ),
    ],
  );

  debugPrint('----- $croppedFile');
  if (croppedFile != null) {
    debugPrint('---- got croppedfile: ${croppedFile.path}');
    // loginBloc.setSelectedAvatarFile(croppedFile.path);
    return croppedFile.path;
  } else {
    return originalFile.path;
  }
}
