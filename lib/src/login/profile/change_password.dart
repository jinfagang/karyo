import 'package:flash/flash.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:karyo/karyo.dart';
import 'package:shadcn_ui/shadcn_ui.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage({super.key});

  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  final TextEditingController _oldPasswordController = TextEditingController();
  final TextEditingController _newPasswordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  final _formKey = GlobalKey<FormState>();
  late LoginBloc _loginBloc;
  final isNotSame = false.obs;

  void _handleChangePassword() async {
    isNotSame.value = false;
    if (_oldPasswordController.text.isEmpty ||
        _newPasswordController.text.isEmpty ||
        _confirmPasswordController.text.isEmpty) {
      showSuccessMessage('请输入完整信息');
    } else if (_newPasswordController.text != _confirmPasswordController.text) {
      showSuccessMessage('新密码两次输入不一致');
      isNotSame.value = true;
    } else {
      var res = await _loginBloc.client.changeUserPwd(
          _oldPasswordController.text,
          _newPasswordController.text,
          GlobalSettings.token!);
      if (res != null) {
        openMyWechatDialog(
            context, '修改成功🎉', '密码修改成功, 请在【我的】页面退出登录后重新登录，否则将无法收发消息', () {
          // update local credentials
          Navigator.pop(context);
          Navigator.pop(context);
          Navigator.pop(context);
        }, barrierDismissible: false);
      }
    }
  }

  @override
  void initState() {
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldWrapperAdapt(
      child: _buildMain(),
    );
  }

  Widget _buildMain() {
    return Scaffold(
      appBar: AppBar(
        leading: buildBackButton3(context),
        title: buildAppBarTitle2('修改密码', context),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              EnhancedTextField(
                controller: _oldPasswordController,
                showCounter: false,
                obscureText: true,
                enableBackground: true,
                hintText: '旧密码',
              ),
              g16,
              Obx(
                () => EnhancedTextField(
                  controller: _newPasswordController,
                  showCounter: false,
                  obscureText: !isNotSame.value,
                  enableBackground: true,
                  hintText: '新密码',
                ),
              ),
              g16,
              Obx(
                () => EnhancedTextField(
                  controller: _confirmPasswordController,
                  showCounter: false,
                  obscureText: !isNotSame.value,
                  enableBackground: true,
                  hintText: '二次确认新密码',
                ),
              ),
              g16,
              g16,
              SizedBox(
                width: double.infinity,
                child: EnhancedButton(
                    onPressed: _handleChangePassword, title: '确认修改'),
              ),
              g32,
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: BoxBlockSim(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        backgroundColor: tClrPrimary(context).withAlpha(30),
                        child: Row(
                          children: [
                            Icon(
                              CupertinoIcons.bell,
                              color: tClrPrimary(context),
                              size: 18,
                            ),
                            g8,
                            Expanded(
                              child: Text(
                                '修改密码后，请在【我的】页面退出登录后重新登录，否则将无法收发消息',
                                style: NORMAL_TXT_STYLE.copyWith(
                                    color: tClrPrimary(context)),
                              ),
                            ),
                          ],
                        )),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
