// view or edit profile here

import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:karyo/karyo.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:karyo/src/login/profile/change_password.dart';

import 'crop_image_page.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  static final GlobalKey<FormState> _nameKey = GlobalKey<FormState>();
  static final GlobalKey<FormState> _editPasswdKey = GlobalKey<FormState>();
  static final GlobalKey<FormState> _genderKey = GlobalKey<FormState>();
  static final GlobalKey<FormState> _descriptionlKey = GlobalKey<FormState>();
  static final GlobalKey<FormState> _hobbiesKey = GlobalKey<FormState>();
  static final GlobalKey<FormState> _maneKey = GlobalKey<FormState>();
  static final GlobalKey<FormState> _datetimeKey = GlobalKey<FormState>();

  static final FocusNode _nameNode = FocusNode();
  static final FocusNode _descriptionNode = FocusNode();

  final TextEditingController _nickNameController = new TextEditingController();
  final TextEditingController _signController = new TextEditingController();

  String _selectedGender = '女';
  String? _nickNameInput;
  String? _signInput;
  bool _autoValidate = false;
  final picker = ImagePicker();
  LoginBloc loginBloc = new LoginBloc();

  Future getImage(BuildContext context, LoginBloc bloc) async {
    await picker
        .pickImage(source: ImageSource.gallery, maxWidth: 450)
        .then((value) {
      cropImage(File(value!.path), context, bloc);
    });
    // _pickedImgFile.add(File(image.path));
    // selectedAvatarFile = File(image.path);
  }

  void cropImage(
      File originalImage, BuildContext context, LoginBloc bloc) async {
    var croppedFilePath = await cropPickedImage(originalImage, context);
    loginBloc.setSelectedAvatarFile(File(croppedFilePath));
  }

  @override
  void initState() {
    print('will profile init call many times?');
    loginBloc.helloGetUser();
    loginBloc.getUserStream().listen((u) {
      _selectedGender = getGenderStr(u.userGender!);
    });
    super.initState();
  }

  CupertinoNavigationBar _appBar(BuildContext context) {
    return CupertinoNavigationBar(
        backgroundColor: Colors.transparent,
        border: Border(bottom: BorderSide(color: Colors.transparent)),
        automaticallyImplyLeading: true,
        // leading: CupertinoButton(
        //   padding: EdgeInsets.all(0),
        //   child: Text(
        //     '取消',
        //     style: NORMAL_BOLD_TXT_STYLE,
        //   ),
        //   onPressed: () {
        //     Navigator.pop(context, null);
        //   },
        // ),
        middle: Text('更新个人资料', style: NORMAL_BOLD_TXT_STYLE),
        trailing: CupertinoButton(
          padding: EdgeInsets.all(0),
          child: Text(
            '取消',
            style: NORMAL_BOLD_TXT_STYLE,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ));
  }

  @override
  Widget build(BuildContext context) {
    // LoginBloc loginBloc = BlocProvider.of(context);
    return ScaffoldWrapperAdapt(
      child: _buildMain(),
    );
  }

  Widget _buildMain() {
    return MyScaffold(
        // navigationBar: _appBar(context),
        appBar: AppBar(
          leading: buildBackButton3(context),
          title: buildAppBarTitle2("更改资料", context),
          surfaceTintColor: Colors.transparent,
          backgroundColor: Colors.transparent,
        ),
        body: ListView(
          children: [
            SizedBox(
              height: 12,
            ),
            Center(
              child: BouncingWidget(
                child: StreamBuilder(
                  stream: loginBloc.pickedImgFile,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return PhysicalModel(
                          color: Colors.grey.withAlpha(60),
                          borderRadius: BorderRadius.circular(48),
                          elevation: 10,
                          child: CircleAvatar(
                            radius: 48,
                            backgroundColor:
                                GlobalSettings.vipStatus == VIPStatus.VIP_OK
                                    ? ColorBook.yellowVIP
                                    : Colors.transparent,
                            // backgroundColor:Colors.transparent,
                            child: CircleAvatar(
                              backgroundImage: FileImage(snapshot.data as File),
                              radius: 45.0,
                            ),
                          ));
                    } else {
                      return StreamBuilder(
                          stream: loginBloc.user,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              User u = snapshot.data! as User;
                              return PhysicalModel(
                                  color: Colors.grey.withAlpha(1),
                                  borderRadius: BorderRadius.circular(48),
                                  elevation: 8,
                                  child: CircleAvatar(
                                    radius: 48,
                                    backgroundColor: GlobalSettings.vipStatus ==
                                            VIPStatus.VIP_OK
                                        ? ColorBook.yellowVIP
                                        : Colors.transparent,
                                    // backgroundColor:Colors.transparent,
                                    child: CircleAvatar(
                                      backgroundImage: u.userAvatarImage,
                                      radius: 45.0,
                                    ),
                                  ));
                            } else {
                              return PhysicalModel(
                                  color: Colors.grey.withAlpha(1),
                                  borderRadius: BorderRadius.circular(48),
                                  elevation: 8,
                                  child: CircleAvatar(
                                    radius: 48,
                                    backgroundColor: GlobalSettings.vipStatus ==
                                            VIPStatus.VIP_OK
                                        ? ColorBook.yellowVIP
                                        : Colors.transparent,
                                    // backgroundColor:Colors.transparent,
                                    child: const CircleAvatar(
                                      backgroundImage:
                                          CachedNetworkImageProvider(
                                              gAvatarDefaultMale),
                                      radius: 45.0,
                                    ),
                                  ));
                            }
                          });
                    }
                  },
                ),
                onPressed: () {
                  // Navigator.push(
                  //     context,
                  //     CupertinoPageRoute<bool>(
                  //       builder: (context) => WelcomeScreen(),
                  //     ));
                  if (isOnMobile()) {
                    getImage(context, loginBloc);
                  } else {
                    showSuccessMessage('头像仅支持手机端修改，在关于我们中获取手机端下载链接');
                  }
                },
              ),
            ),
            StreamBuilder(
              stream: loginBloc.user,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  User user = snapshot.data as User;
                  if (_nickNameInput != null) {
                    _nickNameController.text = _nickNameInput!;
                  } else {
                    _nickNameController.text = user.userNickName;
                  }
                  if (_signInput != null) {
                    _signController.text = _signInput!;
                  } else {
                    _signController.text = user.userSign;
                  }

                  // _nickNameController.selection = TextSelection.fromPosition(
                  //   TextPosition(offset: _nickNameController.text.length),
                  // );
                  // _signController.selection = TextSelection.fromPosition(
                  //   TextPosition(offset: _signController.text.length),
                  // );

                  return Theme(
                    data: Theme.of(context).copyWith(
                      dividerColor: Colors.grey.withAlpha(20),
                      cardTheme: CardTheme(
                        shadowColor: Colors.grey.withAlpha(30),
                        elevation: 14,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(22)),
                      ),
                      primaryTextTheme: TextTheme(
                        titleLarge: TextStyle(
                            color: Colors.lightBlue[50]), // app header text
                      ),
                      inputDecorationTheme: InputDecorationTheme(
                        labelStyle:
                            TextStyle(color: COLOR_PRIMARY), // style for labels
                      ),
                    ),
                    child: Column(children: [
                      const SizedBox(height: 58),
                      buildRoundedListTile(
                          context,
                          Text(
                            "昵称:",
                            style: NORMAL_BOLD_TXT_STYLE,
                          ),
                          TextField(
                              decoration: InputDecoration(
                                  contentPadding:
                                      const EdgeInsets.fromLTRB(4, 0, 0, 0),
                                  isCollapsed: true,
                                  alignLabelWithHint: true,
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                      color: Colors.grey.withAlpha(70)),
                                  hintText: "你的昵称"),
                              cursorHeight: 22.0,
                              cursorRadius: const Radius.circular(4),
                              cursorColor: COLOR_PRIMARY,
                              controller: _nickNameController,
                              onChanged: (v) {
                                _nickNameInput = v;
                                _nickNameController.text = v;
                              },
                              textInputAction: TextInputAction.next,
                              onEditingComplete: () {
                                FocusScope.of(context).unfocus();
                              },
                              style: NORMAL_BOLD_TXT_STYLE),
                          Container(),
                          Icon(
                            FontAwesomeIcons.chevronRight,
                            color: Colors.grey.withAlpha(80),
                            size: 16,
                          ),
                          true,
                          false,
                          () {}),
                      buildRoundedListTile(
                          context,
                          Text(
                            "账号:",
                            style: NORMAL_BOLD_TXT_STYLE,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                user.userAcc,
                                style: NORMAL_TXT_STYLE.copyWith(
                                    color: Colors.grey),
                              ),
                            ],
                          ),
                          Container(),
                          Icon(
                            FontAwesomeIcons.chevronRight,
                            color: Colors.grey.withAlpha(80),
                            size: 16,
                          ),
                          false,
                          false,
                          () {}),
                      buildRoundedListTile(
                          context,
                          Text(
                            "性别:",
                            style: NORMAL_BOLD_TXT_STYLE,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text('${_selectedGender}'),
                            ],
                          ),
                          Container(),
                          Icon(
                            FontAwesomeIcons.chevronRight,
                            color: Colors.grey.withAlpha(80),
                            size: 16,
                          ),
                          false,
                          false, () {
                        YYDialog().build(context)
                          ..width = 280
                          ..borderRadius = 4.0
                          ..backgroundColor =
                              PlatformTheme.getMainBkColorSection(context)
                          ..listViewOfListTile(
                              items: [
                                ListTileItem(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12, vertical: 0),
                                  leading: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                          width: 20,
                                          height: 20,
                                          child: Image.asset(
                                              "assets/images/ic_man.png"),
                                        ),
                                      ]),
                                  text: "男",
                                  // color: Colors.grey,
                                  fontSize: 20.0,
                                ),
                                ListTileItem(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12, vertical: 0),
                                  leading: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                          width: 20,
                                          height: 20,
                                          child: Image.asset(
                                              "assets/images/ic_woman.png"),
                                        )
                                      ]),
                                  text: "女",
                                  // color: Colors.grey,
                                  fontSize: 20.0,
                                ),
                                ListTileItem(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12, vertical: 0),
                                  leading: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                          width: 20,
                                          height: 20,
                                          child: Image.asset(
                                              "assets/images/ic_bot.png"),
                                        )
                                      ]),
                                  text: "机器人",
                                  // color: Colors.grey,
                                  fontSize: 20.0,
                                ),
                              ],
                              onClickItemListener: (index) {
                                debugPrint("[profile update gender] $index");
                                debugPrint(_selectedGender);
                                switch (index) {
                                  case 0:
                                    setState(() {
                                      _selectedGender = '男';
                                    });
                                    break;
                                  case 1:
                                    setState(() {
                                      _selectedGender = '女';
                                    });
                                    break;
                                  case 2:
                                    setState(() {
                                      _selectedGender = '机器人';
                                    });
                                    break;
                                  default:
                                    setState(() {
                                      _selectedGender = '女';
                                    });
                                    break;
                                }
                              })
                          ..show();
                      }),
                      // buildRoundedListTile(
                      //     context,
                      //     Text(
                      //       "密码:",
                      //       style: NORMAL_BOLD_TXT_STYLE,
                      //     ),
                      //     CupertinoTextField(
                      //       decoration: BoxDecoration(
                      //           color: Colors.transparent,
                      //           borderRadius: BorderRadius.circular(12)),
                      //       cursorHeight: 22.0,
                      //       cursorRadius: Radius.circular(4),
                      //       cursorColor: COLOR_PRIMARY,
                      //       placeholder: user.userPwd,
                      //       style: NORMAL_BOLD_TXT_STYLE,
                      //       // enabled: false,
                      //       obscureText: true,
                      //       readOnly: false,
                      //       // controller: _,
                      //     ),
                      //     Container(),
                      //     Icon(
                      //       FontAwesomeIcons.chevronRight,
                      //       color: Colors.grey.withAlpha(80),
                      //       size: 16,
                      //     ),
                      //     false,
                      //     false, () {
                      //   showSuccessMessage('密码暂时不支持修改');
                      // }),
                      buildRoundedListTileDescBox(
                          context,
                          Text(
                            "个人介绍:",
                            style: NORMAL_BOLD_TXT_STYLE,
                          ),
                          SizedBox(
                            height: 80,
                            child: TextField(
                              cursorHeight: 22.0,
                              cursorRadius: const Radius.circular(4),
                              cursorColor: COLOR_PRIMARY,
                              decoration: InputDecoration(
                                  contentPadding:
                                      const EdgeInsets.fromLTRB(4, 0, 0, 0),
                                  isCollapsed: true,
                                  alignLabelWithHint: true,
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                      color: Colors.grey.withAlpha(70)),
                                  hintText: "简单介绍一下自己吧~"),
                              style:
                                  NORMAL_TXT_STYLE.copyWith(color: Colors.grey),
                              controller: _signController,
                              maxLength: 80,
                              textAlign: TextAlign.start,
                              textAlignVertical: TextAlignVertical.top,
                              textInputAction: TextInputAction.done,
                              onChanged: (v) {
                                _signInput = v;
                                _signController.text = v;
                              },
                            ),
                          ),
                          Container(),
                          false,
                          true,
                          () {}),

                      Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          HoverableButton(
                            child: Text(
                              '修改密码?',
                              style: NORMAL_S_TXT_STYLE.copyWith(
                                  decoration: TextDecoration.underline,
                                  decorationColor: Colors.grey.withAlpha(100),
                                  color: Colors.grey.withAlpha(100)),
                            ),
                            onPressed: () {
                              // to change password page.
                              if (isOnMobile()) {
                                GoTo(
                                    context,
                                    BlocProvider(
                                        bloc: loginBloc,
                                        child: const ChangePasswordPage()));
                              } else {
                                showGeneralDialogMy(
                                    context,
                                    BlocProvider(
                                        bloc: loginBloc,
                                        child: const ChangePasswordPage()));
                              }
                            },
                          ),
                        ],
                      )
                    ]),
                  );
                } else {
                  return Center(
                    child: Container(
                      // width: 30,
                      padding: const EdgeInsets.all(32),
                      child: const CircularProgressIndicator(
                          // color: COLOR_PRIMARY,
                          ),
                    ),
                  );
                }
              },
            ),
            g16,
            StreamBuilder(
              stream: loginBloc.user,
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Padding(
                    padding: const EdgeInsets.all(16),
                    child: TextButton(
                        onPressed: () {
                          showSuccessMessage('未登录无法修改');
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 12),
                          child: Text(
                            "更新信息",
                            style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                color: Colors.white),
                          ),
                        )),
                  );
                } else {
                  User _u = snapshot.data as User;
                  return Padding(
                    padding: const EdgeInsets.all(16),
                    child: RoundedButton(
                        color: tClrPrimary(context),
                        press: () {
                          if (_nickNameController.text == '') {
                            showSuccessMessage("昵称不能为空");
                          } else if (_nickNameController.text.length > 10) {
                            showSuccessMessage("昵称长度不能大于十个字符");
                          } else {
                            debugPrint(
                                "UserNickName? :: ${_nickNameController.text}");
                            _u.userNickName = _nickNameController.text;
                            _u.userSign = _signController.text;
                            var _g = '0';
                            switch (_selectedGender) {
                              case '男':
                                _g = '0';
                                break;
                              case '女':
                                _g = '1';
                                break;
                              default:
                                _g = '2';
                            }
                            _u.userGender = _g;
                            if (loginBloc.selectedAvatarFile != null) {
                              _u.selectedImgAvatar =
                                  loginBloc.selectedAvatarFile;
                              debugPrint(
                                  'selectd avatar: ${loginBloc.selectedAvatarFile}');
                            }
                            debugPrint(
                                'selectd avatar: ${loginBloc.selectedAvatarFile}');
                            if (GlobalSettings.userCityLocFull != "") {
                              _u.userCity = GlobalSettings.userCityLocFull;
                            }
                            loginBloc.updateUser(_u, context).then((value) {
                              showSuccessMessage("更新成功，重启app生效");
                              // update cache
                              removeUrlFromCache(_u.userAvatarUrl ?? '');
                              loginBloc.hello();
                              Navigator.pop(context);
                            });
                          }
                        },
                        text: '更新信息'),
                  );
                }
              },
            )
          ],
        ));
  }

  Widget buildRoundedListTile(
      BuildContext context,
      Widget leading,
      Widget title,
      Widget subtitle,
      Widget tailing,
      bool roundTop,
      bool roundBottom,
      onTap,
      {onTapDown}) {
    BorderRadius radius = BorderRadius.zero;
    double _radius = 16;
    if (roundTop) {
      radius = BorderRadius.only(
          topLeft: Radius.circular(_radius),
          topRight: Radius.circular(_radius));
    }
    if (roundBottom) {
      radius = BorderRadius.only(
          bottomLeft: Radius.circular(_radius),
          bottomRight: Radius.circular(_radius));
    }

    if (roundBottom && roundTop) {
      radius = BorderRadius.only(
          topLeft: Radius.circular(_radius),
          topRight: Radius.circular(_radius),
          bottomLeft: Radius.circular(_radius),
          bottomRight: Radius.circular(_radius));
    }
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Card(
        elevation: 0,
        margin: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(borderRadius: radius),
        // color: Colors.grey.withAlpha(10),
        color: isDarkModeOnContext(context,
                isForcedDark: GlobalSettings.isForceDark)
            ? PlatformTheme.getMainBkColorSection2(context)
            : Colors.white,
        shadowColor: Colors.grey.withAlpha(10),
        child: InkWell(
            borderRadius: radius,
            onTap: onTap,
            onTapDown: onTapDown,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 12),
              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                const SizedBox(
                  width: 12,
                ),
                leading,
                const SizedBox(
                  width: 18,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      title,
                      const SizedBox(height: 2),
                      Wrap(
                        children: [subtitle],
                      )
                    ],
                  ),
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.end,
                //   children: <Widget>[, SizedBox(width: 8)],
                // ),
                SizedBox(
                  height: 32,
                  child: tailing,
                ),
                SizedBox(width: 8),
              ]),
            )),
      ),
    );
  }

  Widget buildRoundedListTileDescBox(BuildContext context, Widget title,
      Widget subtitle, Widget tailing, bool roundTop, bool roundBottom, onTap,
      {onTapDown}) {
    BorderRadius radius = BorderRadius.zero;
    double _radius = 16;
    if (roundTop) {
      radius = BorderRadius.only(
          topLeft: Radius.circular(_radius),
          topRight: Radius.circular(_radius));
    }
    if (roundBottom) {
      radius = BorderRadius.only(
          bottomLeft: Radius.circular(_radius),
          bottomRight: Radius.circular(_radius));
    }

    if (roundBottom && roundTop) {
      radius = BorderRadius.only(
          topLeft: Radius.circular(_radius),
          topRight: Radius.circular(_radius),
          bottomLeft: Radius.circular(_radius),
          bottomRight: Radius.circular(_radius));
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Card(
        elevation: 0,
        margin: const EdgeInsets.all(0),
        shape: RoundedRectangleBorder(borderRadius: radius),
        // color: Colors.grey.withAlpha(10),
        color: isDarkModeOnContext(context)
            ? PlatformTheme.getMainBkColorSection2(context)
            : Colors.white,
        shadowColor: Colors.grey.withAlpha(10),
        child: InkWell(
            borderRadius: radius,
            onTap: onTap,
            onTapDown: onTapDown,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 12),
              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                const SizedBox(
                  width: 12,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      title,
                      const SizedBox(height: 8),
                      Wrap(
                        children: [subtitle],
                      )
                    ],
                  ),
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.end,
                //   children: <Widget>[, SizedBox(width: 8)],
                // ),
                SizedBox(
                  height: 32,
                  child: tailing,
                ),
                const SizedBox(width: 8),
              ]),
            )),
      ),
    );
  }

// CardSettingsText _buildCardSettingsTextName(User user) {
//   return CardSettingsText(
//     fieldPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
//     key: _nameKey,
//     label: '昵称',
//     hintText: '昵称可以随意修改',
//     style: NORMAL_TXT_STYLE,
//     autovalidate: _autoValidate,
//     inputAction: TextInputAction.next,
//     inputActionNode: _descriptionNode,
//     controller: _nickNameController,
//     validator: (value) {
//       if (value == null || value.isEmpty) return 'Name is required.';
//       return null;
//     },
//     onSaved: (value) {
//       // how to gather those updates?
//     },
//     onChanged: (value) {},
//   );
// }

// CardSettingsText _buildCardSettingsTextDisabled(User user) {
//   return CardSettingsText(
//     label: 'Daybreak账号',
//     hintText: user.userAcc,
//     enabled: false,
//   );
// }

// CardSettingsText _buildCardSettingsEditPassword(User user) {
//   return CardSettingsPassword(
//     key: _editPasswdKey,
//     label: '密码',
//     hintText: '修改密码',
//     // autovalidate: _autoValidate,
//     inputAction: TextInputAction.next,
//     inputActionNode: _descriptionNode,
//     validator: (value) {
//       if (value == null || value.isEmpty) return 'Name is required.';
//       return null;
//     },
//     onSaved: (value) => _ponyModel.name = value,
//     onChanged: (value) {},
//   );
// }

// CardSettingsDateTimePicker _buildCardSettingsDateTimePickerBirthday(
//     User user) {
//   return CardSettingsDateTimePicker(
//     key: _datetimeKey,
//     icon: Icon(Icons.card_giftcard, color: Colors.yellow[700]),
//     label: '生日',
//     initialValue: DateTime.parse(user.userBirth),
//     onSaved: (value) => _ponyModel.showDateTime =
//         updateJustDate(value, _ponyModel.showDateTime),
//     onChanged: (value) {},
//   );
// }

// CardSettingsColorPicker _buildCardSettingsColorPickerMane() {
//   return CardSettingsColorPicker(
//     key: _maneKey,
//     label: '对外展示主题色',
//     initialValue: intelligentCast<Color>(_ponyModel.maneColor),
//     // autovalidate: _autoValidate,
//     pickerType: CardSettingsColorPickerType.material,
//     validator: (value) {
//       if (value.computeLuminance() > .7) return 'This color is too light.';
//       return null;
//     },
//     onSaved: (value) => _ponyModel.maneColor = colorToString(value),
//     onChanged: (value) {},
//   );
// }

// CardSettingsCheckboxPicker _buildCardSettingsCheckboxPickerHobbies() {
//   return CardSettingsCheckboxPicker(
//     key: _hobbiesKey,
//     label: '我的标签',
//     initialValues: _ponyModel.hobbies,
//     options: allHobbies,
//     // autovalidate: _autoValidate,
//     validator: (List<String> value) {
//       if (value == null || value.isEmpty)
//         return 'You must pick at least one hobby.';

//       return null;
//     },
//     onSaved: (value) => _ponyModel.hobbies = value,
//     onChanged: (value) {},
//   );
// }

// CardSettingsParagraph _buildCardSettingsParagraphDescription(User user) {
//   return CardSettingsParagraph(
//     key: _descriptionlKey,
//     label: '个性签名',
//     maxLength: 30,
//     controller: _signController,
//     numberOfLines: 2,
//     focusNode: _descriptionNode,
//     onSaved: (value) => _ponyModel.description = value,
//     onChanged: (value) {},
//   );
// }

// CardSettingsRadioPicker _buildCardSettingsRadioPickerGender(User user) {
//   return CardSettingsRadioPicker(
//     key: _genderKey,
//     label: '性别',
//     initialValue: user.userGender,
//     hintText: '女',
//     // autovalidate: _autoValidate,
//     options: <String>['男', '女', '保密'],
//     validator: (String value) {
//       if (value == null || value.isEmpty) return 'You must pick a gender.';
//       return null;
//     },
//     onSaved: (value) {
//       print('one save.');
//     },
//     onChanged: (value) {
//       print('onChange.');
//       _selectedGender = value;
//     },
//   );
// }
}
