import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class Background extends StatelessWidget {
  final Widget? child;
  const Background({
    Key? key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        // width: double.infinity,
        width: size.width,
        height: size.height,
        child: isOnMobile()
            ? Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  // Positioned(
                  //   top: -24,
                  //   left: -20,
                  //   child: Image.asset(
                  //     "assets/images/main_top.png",
                  //     width: size.width * 0.35,
                  //   ),
                  // ),
                  // Positioned(
                  //   bottom: -20,
                  //   right: -20,
                  //   child: Image.asset(
                  //     "assets/images/login_bottom.png",
                  //     width: size.width * 0.4,
                  //   ),
                  // ),
                  child!,
                ],
              )
            : Center(
                child: Padding(
                  padding: EdgeInsets.all(32),
                  child: child,
                ),
              ));
  }
}
