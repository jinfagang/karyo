import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:karyo/karyo.dart';
import 'package:karyo/src/login/components/already_have_an_account_acheck.dart';
import 'package:karyo/src/login/components/rounded_input_field.dart';
import 'package:karyo/src/login/components/rounded_password_field.dart';
import 'background.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key? key, this.onLoginCallBack}) : super(key: key);

  final Function()? onLoginCallBack;

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _pwdController = new TextEditingController();
  late FToast fToast;

  void _login(bloc, name, pwd, context) async {
    var res =
        await bloc.login(_nameController.text, _pwdController.text, context);
    if (kDebugMode) {
      print('login res: $res');
    }
    if (res) {
      Widget toast = getToastWidget('登录成功！欢迎回来');

      fToast.showToast(
        child: toast,
        gravity: ToastGravity.BOTTOM,
        toastDuration: Duration(seconds: 2),
      );
      widget.onLoginCallBack!();
    } else {
      showSuccessMessage("登录失败！确认密码是否正确");
    }
  }

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  Widget getToastWidget(String msg) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Colors.greenAccent,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text(msg),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    LoginBloc loginBloc = BlocProvider.of(context);
    // LoginBloc loginBloc = LoginBloc();

    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: const SystemUiOverlayStyle(
          // statusBarColor: Colors.transparent,
          // statusBarColor: Theme.of(context).dialogBackgroundColor,
          // systemNavigationBarColor: Theme.of(context).scaffoldBackgroundColor,
          systemNavigationBarColor: Colors.transparent,
        ),
        child: Scaffold(
            extendBody: true,
            body: Stack(children: [
              Background(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "欢迎回到你的小宇宙",
                        style: NORMAL_BOLD_TXT_STYLE.copyWith(
                            color: tClrOnTertiaryContainer(context),
                            fontSize: 28),
                      ),
                      SizedBox(height: size.height * 0.03),
                      SvgPicture.asset(
                        "assets/illustrations/undraw_My_universe_re_txot.svg",
                        height: size.height * 0.25,
                      ),
                      SizedBox(height: size.height * 0.03),
                      RoundedInputField(
                        hintText: "手机号/微信号",
                        onChanged: (value) {},
                        controller: _nameController,
                      ),
                      RoundedPasswordField(
                        onChanged: (value) {},
                        controller: _pwdController,
                      ),
                      SizedBox(height: size.height * 0.03),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.82,
                        child: EnhancedButton(
                          backgroundColor: tClrOnTertiaryContainer(context),
                          title: "登录",
                          onPressed: () {
                            _login(loginBloc, _nameController.text,
                                _pwdController.text, context);
                          },
                        ),
                      ),
                      SizedBox(height: size.height * 0.03),
                      AlreadyHaveAnAccountCheck(
                        press: () {
                          GoTo(
                              context,
                              BlocProvider(
                                  bloc: loginBloc,
                                  child: const SignUpScreenFt()));
                        },
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                left: 16,
                top: 38,
                child:
                    buildBackButton(context, tClrOnTertiaryContainer(context)),
              ),
            ])));
  }
}
