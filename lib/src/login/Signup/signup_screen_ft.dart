import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:karyo/karyo.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../components/already_have_an_account_acheck.dart';
import '../components/rounded_input_field.dart';
import '../components/rounded_password_field.dart';
import '../../uis/gradient_text.dart';
import 'package:flutter_animate/flutter_animate.dart';

class SignUpScreenFt extends StatefulWidget {
  const SignUpScreenFt({super.key});

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreenFt> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _nickNameController = TextEditingController();
  final TextEditingController _pwdController = TextEditingController();
  final TextEditingController _inviteCodeController = TextEditingController();
  late FToast fToast;
  bool isNeedInvite = true;

  bool isHaveName = false;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  Widget getToastWidget(String msg) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Colors.greenAccent,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Icon(Icons.check),
          const SizedBox(
            width: 12.0,
          ),
          Text(msg),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    LoginBloc loginBloc = BlocProvider.of(context);

    return Scaffold(
        appBar: AppBar(
          leading: buildBackButton3(context),
          backgroundColor: Colors.transparent,
        ),
        body:
            !isHaveName ? _buildNickName() : _buildSigUpMain(size, loginBloc));
  }

  Widget _buildNickName() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        g32,
        const Text(
          '😍',
          style: TextStyle(fontSize: 45),
        ),
        g8,
        Text(
          '取一个可爱的昵称',
          style: NORMAL_BOLD_TXT_STYLE,
        ),
        g32,
        Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 300),
            child: EnhancedTextField(
              showCounter: false,
              enableBackground: true,
              controller: _nickNameController,
              hintText: '一个有趣的昵称更容易吸引他人哦',
            ),
          ),
        ),
        g64,
        SizedBox(
          width: 100,
          child: HoverableButton(
            minWidth: 44,
            height: 44,
            borderRadius: BorderRadius.circular(14),
            accentColor: Colors.grey.withAlpha(20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '继续',
                  style: NORMAL_BOLD_TXT_STYLE,
                ),
                g8,
                const Icon(
                  CupertinoIcons.arrow_right_circle_fill,
                  size: 22,
                ),
              ],
            ),
            onPressed: () {
              if (_nickNameController.text.trim() != "") {
                // to
                if (_nickNameController.text.length > 12) {
                  showSuccessMessage('昵称长度不能超过12个字符');
                } else {
                  setState(() {
                    isHaveName = true;
                  });
                }
              } else {
                showSuccessMessage("请输入有意义昵称，不能为空或全空格");
              }
            },
          ),
        )
      ],
    );
  }

  Widget _buildSigUpMain(size, LoginBloc loginBloc) {
    return BackgroundSignUp(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            g32,
            g32,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${_nickNameController.text}, AI",
                  style: NORMAL_BOLD_TXT_STYLE.copyWith(
                      color: tClrOnTertiaryContainer(context), fontSize: 38),
                  textAlign: TextAlign.start,
                )
                    .animate()
                    .fadeIn(duration: 900.ms, delay: 300.ms)
                    .shimmer(
                        blendMode: BlendMode.srcOver, color: Colors.white12)
                    .move(
                        begin: const Offset(-32, 0), curve: Curves.easeOutQuad),
                sbh8,
                Text(
                  "Connected with Real World",
                  style: NORMAL_TXT_STYLE.copyWith(
                      color: tClrOnTertiaryContainer(context), fontSize: 22),
                )
                    .animate()
                    .fadeIn(duration: 900.ms, delay: 500.ms)
                    .shimmer(
                        blendMode: BlendMode.srcOver, color: Colors.white12)
                    .move(
                        begin: const Offset(-32, 0), curve: Curves.easeOutQuad),
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: Text(
                //     '将以 ${_nickNameControllerxt} 注册，请设置您的登陆手机号和密码',
                //     style: SMALL_TXT_STYLE,
                //   ),
                // ),
              ],
            ),
            g8,
            g32,
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              child: Column(
                children: [
                  EnhancedTextField(
                    enableBackground: true,
                    keyboardType: TextInputType.phone,
                    maxLength: 15,
                    showCounter: false,
                    hintText: "手机号",
                    onChanged: (value) {},
                    controller: _nameController,
                  ),
                  g16,
                  EnhancedTextField(
                    onChanged: (value) {},
                    controller: _pwdController,
                    obscureText: true,
                    maxLength: 15,
                    hintText: '设置密码(无要求)',
                    showCounter: false,
                    enableBackground: true,
                  ),
                  g16,
                  EnhancedTextField(
                    hintText: "邀请码(非必需)",
                    showCounter: false,
                    enableBackground: true,
                    onChanged: (value) {},
                    controller: _inviteCodeController,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 64),
              child: Text(
                "注意: 我们采用后验证注册机制, 免验证码叨扰, 请填写真实手机号, 否则后期认证可能无法通过",
                style: SMALL_TXT_STYLE.copyWith(color: Colors.grey.shade400),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: size.height * 0.02),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.82,
              child: EnhancedButton(
                backgroundColor: tClrPrimary(context).withAlpha(100),
                color:
                    tClrPrimary(context).withAlpha(100).computeLuminance() > 0.5
                        ? Colors.white
                        : Colors.black,
                title: "注册",
                onPressed: () async {
                  if (_pwdController.text.length > 16) {
                    showSuccessMessage("密码长度不能超过16位");
                  } else {
                    bool res = false;
                    if (isNeedInvite) {
                      res = await loginBloc.register(
                          _nameController.text,
                          _nickNameController.text,
                          _pwdController.text,
                          context);
                    } else {
                      res = await loginBloc.registerWithInviteCode(
                          _nameController.text,
                          _nickNameController.text,
                          _pwdController.text,
                          _inviteCodeController.text,
                          context);
                    }

                    if (res) {
                      SharedPreferenceUtil.save(
                          'account_name', _nameController.text);
                      SharedPreferenceUtil.save(
                          'account_password', _pwdController.text);
                      loginBloc.justTypedAcc = _nameController.text;
                      loginBloc.justTypedPwd = _pwdController.text;
                      Widget toast = Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 24.0, vertical: 12.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25.0),
                          color: Colors.greenAccent,
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Icon(Icons.check),
                            const SizedBox(
                              width: 12.0,
                            ),
                            Text("注册成功，登录用户名: ${_nameController.text}"),
                          ],
                        ),
                      );

                      fToast.showToast(
                        child: toast,
                        gravity: ToastGravity.BOTTOM,
                        toastDuration: const Duration(seconds: 2),
                      );
                    } else {
                      Widget toast = getToastWidget('注册失败, 请检查用户名或邀请码');
                      fToast.showToast(
                        child: toast,
                        gravity: ToastGravity.BOTTOM,
                        toastDuration: const Duration(seconds: 2),
                      );
                    }
                  }
                },
              ),
            ),
            if (isNeedInvite)
              const SizedBox(
                height: 12,
              ),
            if (isNeedInvite)
              SizedBox(
                  width: MediaQuery.of(context).size.width * 0.82,
                  child: EnhancedButton(
                      backgroundColor: Colors.grey.shade500,
                      title: "获取邀请码",
                      onPressed: () async {
                        Widget toast = getToastWidget('产品内测中，加入小镇获取邀请码');
                        fToast.showToast(
                          child: toast,
                          gravity: ToastGravity.BOTTOM,
                          toastDuration: const Duration(seconds: 2),
                        );
                        launchURL("https://wx.hlcode.cn/?id=NKZZRQo");
                      })),
            const SizedBox(height: 32),
            AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                GoTo(
                    context,
                    BlocProvider(
                      bloc: loginBloc,
                      child: LoginScreen(),
                    ));

                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) {
                //       return BlocProvider(
                //         bloc: loginBloc,
                //         child: LoginScreen(),
                //       );
                //     },
                //   ),
                // );
              },
            ),
            OrDivider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RawMaterialButton(
                    padding: const EdgeInsets.all(8),
                    shape: const CircleBorder(),
                    fillColor: isDarkModeOnContext(context)
                        ? Colors.grey.shade900
                        : Colors.grey.shade100,
                    elevation: 0,
                    child: Icon(
                      FontAwesomeIcons.weixin,
                      color: tClrPrimary(context).withAlpha(100),
                      size: 18,
                    ),
                    onPressed: () {
                      showSuccessMessage("微信登录即将开放");
                    }),
                const SizedBox(
                  width: 10,
                ),
                RawMaterialButton(
                    padding: const EdgeInsets.all(8),
                    shape: const CircleBorder(),
                    fillColor: isDarkModeOnContext(context)
                        ? Colors.grey.shade900
                        : Colors.grey.shade100,
                    elevation: 0,
                    child: Icon(
                      FontAwesomeIcons.qq,
                      color: tClrPrimary(context).withAlpha(100),
                      size: 18,
                    ),
                    onPressed: () {
                      showSuccessMessage("QQ登录即将开放");
                    }),
                const SizedBox(
                  width: 10,
                ),
                RawMaterialButton(
                    padding: const EdgeInsets.all(8),
                    shape: const CircleBorder(),
                    fillColor: isDarkModeOnContext(context)
                        ? Colors.grey.shade900
                        : Colors.grey.shade100,
                    elevation: 0,
                    child: Icon(
                      FontAwesomeIcons.weibo,
                      color: tClrPrimary(context).withAlpha(100),
                      size: 18,
                    ),
                    onPressed: () {
                      showSuccessMessage("微博登录即将开放");
                    }),
              ],
            ),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
