import 'dart:async';

import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:karyo/karyo.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:random_name_generator/random_name_generator.dart';

import '../components/already_have_an_account_acheck.dart';
import '../components/rounded_input_field.dart';
import '../components/rounded_password_field.dart';
import '../../uis/gradient_text.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _pwdController = new TextEditingController();
  TextEditingController _inviteCodeController = new TextEditingController();
  late FToast fToast;
  bool isNeedInvite = true;
  var randomNames = RandomNames(Zone.china);

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  Widget getToastWidget(String msg) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Colors.greenAccent,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text(msg),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    LoginBloc loginBloc = BlocProvider.of(context);

    return Scaffold(
        body: Stack(children: [
      BackgroundSignUp(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 88),
              GradientText(
                "Welcome!\n幕雨\n开启AI社交纪元",
                style: NORMAL_BOLD_TXT_STYLE.copyWith(
                    color: tClrOnTertiaryContainer(context), fontSize: 38),
                colors: [
                  Colors.pink,
                  Colors.redAccent,
                  Colors.pinkAccent,
                  Colors.blueAccent,
                ],
              ),
              sbh8,
              Text(
                "👏👏👏👏👏👏",
                style: NORMAL_BOLD_TXT_STYLE.copyWith(
                    color: tClrOnTertiaryContainer(context), fontSize: 36),
              ),
              SizedBox(height: size.height * 0.05),
              // SvgPicture.asset(
              //   "assets/illustrations/undraw_Progress_indicator_re_4o4n.svg",
              //   height: size.height * 0.2,
              // ),
              SizedBox(height: size.height * 0.05),
              RoundedInputField(
                hintText: "手机号/微信号(唯一标识)",
                onChanged: (value) {},
                controller: _nameController,
              ),
              RoundedPasswordField(
                onChanged: (value) {},
                controller: _pwdController,
              ),
              RoundedInputField(
                hintText: "邀请码",
                icon: FluentIcons.ticket_diagonal_16_filled,
                onChanged: (value) {},
                controller: _inviteCodeController,
              ),
              const SizedBox(
                height: 12,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 64),
                child: Text(
                  "注意: 我们采用后验证注册机制, 免验证码叨扰, 请填写真实手机号, 否则后期认证可能无法通过",
                  style: SMALL_TXT_STYLE.copyWith(color: Colors.grey.shade400),
                ),
              ),
              SizedBox(height: size.height * 0.02),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.82,
                child: EnhancedButton(
                  backgroundColor: tClrTertiaryContainer(context),
                  title: "注册",
                  onPressed: () async {
                    if (_pwdController.text.length > 16) {
                      showSuccessMessage("密码长度不能超过16位");
                    } else {
                      bool res = false;
                      if (isNeedInvite) {
                        res = await loginBloc.register(
                            _nameController.text,
                            'usr${randomNames.fullName()}',
                            _pwdController.text,
                            context);
                      } else {
                        res = await loginBloc.registerWithInviteCode(
                            _nameController.text,
                            'usr${randomNames.fullName()}',
                            _pwdController.text,
                            _inviteCodeController.text,
                            context);
                      }

                      if (res) {
                        Widget toast = Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 24.0, vertical: 12.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            color: Colors.greenAccent,
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Icon(Icons.check),
                              SizedBox(
                                width: 12.0,
                              ),
                              Text("注册成功，登录用户名: ${_nameController.text}"),
                            ],
                          ),
                        );

                        fToast.showToast(
                          child: toast,
                          gravity: ToastGravity.BOTTOM,
                          toastDuration: Duration(seconds: 2),
                        );
                      } else {
                        Widget toast = getToastWidget('注册失败, 请检查用户名或邀请码');
                        fToast.showToast(
                          child: toast,
                          gravity: ToastGravity.BOTTOM,
                          toastDuration: Duration(seconds: 2),
                        );
                      }
                    }
                  },
                ),
              ),
              if (isNeedInvite)
                SizedBox(
                  height: 12,
                ),
              if (isNeedInvite)
                SizedBox(
                    width: MediaQuery.of(context).size.width * 0.82,
                    child: EnhancedButton(
                        backgroundColor: Colors.grey.shade500,
                        title: "获取邀请码",
                        onPressed: () async {
                          Widget toast = getToastWidget('产品内测中，加入小镇获取邀请码');
                          fToast.showToast(
                            child: toast,
                            gravity: ToastGravity.BOTTOM,
                            toastDuration: Duration(seconds: 2),
                          );
                          launchURL("https://wx.hlcode.cn/?id=NKZZRQo");
                        })),
              SizedBox(height: 32),
              AlreadyHaveAnAccountCheck(
                login: false,
                press: () {
                  GoTo(
                      context,
                      BlocProvider(
                        bloc: loginBloc,
                        child: LoginScreen(),
                      ));

                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (context) {
                  //       return BlocProvider(
                  //         bloc: loginBloc,
                  //         child: LoginScreen(),
                  //       );
                  //     },
                  //   ),
                  // );
                },
              ),
              OrDivider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RawMaterialButton(
                      padding: EdgeInsets.all(8),
                      shape: CircleBorder(),
                      fillColor: isDarkModeOnContext(context)
                          ? Colors.grey.shade900
                          : Colors.grey.shade100,
                      elevation: 0,
                      child: Icon(
                        FontAwesomeIcons.weixin,
                        color: Colors.green,
                        size: 18,
                      ),
                      onPressed: () {
                        showSuccessMessage("微信登录即将开放");
                      }),
                  SizedBox(
                    width: 10,
                  ),
                  RawMaterialButton(
                      padding: EdgeInsets.all(8),
                      shape: CircleBorder(),
                      fillColor: isDarkModeOnContext(context)
                          ? Colors.grey.shade900
                          : Colors.grey.shade100,
                      elevation: 0,
                      child: Icon(
                        FontAwesomeIcons.qq,
                        color: Colors.blueAccent,
                        size: 18,
                      ),
                      onPressed: () {
                        showSuccessMessage("QQ登录即将开放");
                      }),
                  SizedBox(
                    width: 10,
                  ),
                  RawMaterialButton(
                      padding: EdgeInsets.all(8),
                      shape: CircleBorder(),
                      fillColor: isDarkModeOnContext(context)
                          ? Colors.grey.shade900
                          : Colors.grey.shade100,
                      elevation: 0,
                      child: Icon(
                        FontAwesomeIcons.weibo,
                        color: Colors.amber,
                        size: 18,
                      ),
                      onPressed: () {
                        showSuccessMessage("微博登录即将开放");
                      }),
                ],
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
      Positioned(
        left: 16,
        top: 38,
        child: buildBackButton(context, tClrOnTertiaryContainer(context)),
      ),
    ]));
  }
}

class BackgroundSignUp extends StatelessWidget {
  final Widget child;
  const BackgroundSignUp({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      // Here i can use size.width but use double.infinity because both work as a same
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          // isOnMobile()
          //     ? Positioned(
          //         top: 0,
          //         left: 0,
          //         child: Image.asset(
          //           "assets/images/signup_top.png",
          //           width: size.width * 0.35,
          //         ),
          //       )
          //     : Container(),
          // isOnMobile()
          //     ? Positioned(
          //         bottom: -30,
          //         left: -30,
          //         child: Image.asset(
          //           "assets/images/main_bottom.png",
          //           width: size.width * 0.25,
          //         ),
          //       )
          //     : Container(),
          child,
        ],
      ),
    );
  }
}

class OrDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: size.height * 0.02),
      width: size.width * 0.8,
      child: Row(
        children: <Widget>[
          buildDivider(context),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text("OR", style: SMALL_TXT_STYLE),
          ),
          buildDivider(context),
        ],
      ),
    );
  }

  Expanded buildDivider(context) {
    return Expanded(
      child: Divider(
        color: isDarkModeOnContext(context)
            ? Colors.grey.shade900
            : Color.fromARGB(255, 237, 237, 237),
        height: 1.5,
      ),
    );
  }
}
