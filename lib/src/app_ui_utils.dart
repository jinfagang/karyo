import 'dart:math';

import 'package:flash/flash.dart';
import 'package:flash/flash_helper.dart';
import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluwx/fluwx.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:iconsax_flutter/iconsax_flutter.dart';
import 'package:karyo/karyo.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:gap/gap.dart';

/// A custom Path to paint stars.
Path drawStar(Size size) {
  // Method to convert degree to radians
  double degToRad(double deg) => deg * (pi / 180.0);

  const numberOfPoints = 5;
  final halfWidth = size.width / 2;
  final externalRadius = halfWidth;
  final internalRadius = halfWidth / 2.5;
  final degreesPerStep = degToRad(360 / numberOfPoints);
  final halfDegreesPerStep = degreesPerStep / 2;
  final path = Path();
  final fullAngle = degToRad(360);
  path.moveTo(size.width, halfWidth);

  for (double step = 0; step < fullAngle; step += degreesPerStep) {
    path.lineTo(halfWidth + externalRadius * cos(step),
        halfWidth + externalRadius * sin(step));
    path.lineTo(halfWidth + internalRadius * cos(step + halfDegreesPerStep),
        halfWidth + internalRadius * sin(step + halfDegreesPerStep));
  }
  path.close();
  return path;
}

Widget getGenderIconUser(User contact) {
  double size = 15.0;
  if (contact.userGender == '0') {
    return Icon(
      Icons.male,
      color: Colors.blue,
      size: size,
    );
  } else if (contact.userGender == '1') {
    return Icon(
      Icons.female,
      color: Colors.pink,
      size: size,
    );
  } else if (contact.userGender == '2') {
    return const SizedBox();
  } else {
    return Icon(
      Icons.female,
      color: Colors.pink,
      size: size,
    );
  }
}

String getGenderStr(String g) {
  switch (g) {
    case '0':
      {
        return "男";
      }
    case '1':
      {
        return '女';
      }
    default:
      {
        return "机器人";
      }
  }
}

Widget getGenderIcon(Contact contact) {
  double gS = 15;
  if (contact.userGender == '0') {
    return SizedBox(
      width: gS,
      height: gS,
      child: Image.asset("assets/images/ic_man.png"),
    );
  } else if (contact.userGender == '1') {
    return SizedBox(
      width: gS,
      height: gS,
      child: Image.asset("assets/images/ic_woman.png"),
    );
  } else if (contact.userGender == '2') {
    return SizedBox(
      width: gS,
      height: gS,
      child: Image.asset("assets/images/ic_bot.png"),
    );
  } else {
    return SizedBox(
      width: gS,
      height: gS,
      child: Image.asset("assets/images/ic_woman.png"),
    );
  }
}

Widget getGenderIconByGender(String g) {
  double gS = 15;
  if (g == '0') {
    return SizedBox(
      width: gS,
      height: gS,
      child: Image.asset("assets/images/ic_man.png"),
    );
  } else if (g == '1') {
    return SizedBox(
      width: gS,
      height: gS,
      child: Image.asset("assets/images/ic_woman.png"),
    );
  } else if (g == '2') {
    return const SizedBox();
  } else {
    return SizedBox(
      width: gS,
      height: gS,
      child: Image.asset("assets/images/ic_woman.png"),
    );
  }
}

class MessageInCenterWidget extends StatelessWidget {
  final String _message;

  MessageInCenterWidget(this._message);

  @override
  Widget build(BuildContext context) {
    final _pics = ['assets/images/beach.svg', 'assets/images/beach2.svg'];
    return Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        // Icon(
        //   FlutterIcons.beach_mco,
        //   size: 77,
        //   color: Colors.grey.withAlpha(60),
        // ),
        SvgPicture.asset(
          _pics[Random().nextInt(2)],
          height: 200,
        ),
        SizedBox(
          height: 20,
        ),
        Text(_message,
            style: TextStyle(
                fontSize: FONT_MEDIUM, color: Colors.grey.withAlpha(60)))
      ]),
    );
  }
}

Widget buildMyDialog(BuildContext context, Widget mainContent,
    {onCancel, onConfirm, double height = 200}) {
  return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0)), //this right here
      child: Container(
        height: height,
        width: 300,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              mainContent,
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 60.0,
                    child: HoverableButton(
                      onPressed: onCancel == null
                          ? () => Navigator.pop(context)
                          : onCancel,
                      accentColor: Colors.grey.withAlpha(100),
                      child: Text(
                        "取消",
                        style: NORMAL_BOLD_TXT_STYLE.copyWith(
                            color: COLOR_PRIMARY),
                      ),
                      // color: const Color(0xFF1BC0C5),
                    ),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  SizedBox(
                    width: 60.0,
                    child: HoverableButton(
                      onPressed: onConfirm == null
                          ? () => Navigator.pop(context)
                          : onConfirm,
                      accentColor: COLOR_PRIMARY.withAlpha(100),
                      child: Text(
                        "知道了",
                        style: NORMAL_BOLD_TXT_STYLE.copyWith(
                            color: COLOR_PRIMARY),
                      ),
                      // color: const Color(0xFF1BC0C5),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ));
}

void showFlashToastMessage(BuildContext context, String message,
    {Duration? dur}) {
  context.showFlash<bool>(
    barrierDismissible: true,
    duration: dur ?? const Duration(seconds: 1),
    barrierCurve: Curves.decelerate,
    builder: (context, controller) => FlashBar(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20))),
      controller: controller,
      backgroundColor: tClrSecondaryContainer(context),
      forwardAnimationCurve: Curves.bounceIn,
      reverseAnimationCurve: Curves.bounceOut,
      position: FlashPosition.bottom,
      icon: Icon(
        FluentIcons.sparkle_20_filled,
        color: Colors.yellow,
      ),
      title: Text(
        "小提示",
        style: NORMAL_BOLD_TXT_STYLE,
      ),
      content: Text(
        message,
        style: NORMAL_S_TXT_STYLE,
      ),
    ),
  );
}

Widget buildCommentRaw(
    String userAvatarUrl, String userName, bool isVIP, String content) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircleAvatar(
              backgroundImage: NetworkImage(userAvatarUrl),
              radius: 13,
            ),
            SizedBox(
              width: 8,
            ),
            Text(
              userName,
              style: NORMAL_BOLD_TXT_STYLE,
            ),
            SizedBox(
              width: 8,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 6, vertical: 1),
                  decoration: BoxDecoration(
                    color: ColorBook.orangeVIP,
                    borderRadius: BorderRadius.circular(8),
                    gradient: LinearGradient(
                      colors: [
                        ColorBook.yellowLight,
                        ColorBook.yellowVIP.withAlpha(90)
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                  ),
                  child: Row(
                    children: [
                      Container(
                        width: 20,
                        height: 20,
                        child: SvgPicture.asset("assets/images/vip.svg"),
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        '永久会员',
                        style:
                            TextStyle(color: ColorBook.orangeVIP, fontSize: 9),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: 8,
        ),
        Row(
          children: [
            Expanded(
              child: Text(
                content,
                style: NORMAL_S_TXT_STYLE,
              ),
            )
          ],
        )
      ],
    ),
  );
}

Widget buildCustomTab(
    BuildContext context, String text, IconData? icon, bool isSelected) {
  return Container(
    // decoration: BoxDecoration(
    //   borderRadius: BorderRadius.circular(10),
    //   color: isSelected ? tClrPrimaryContainer(context) : Colors.grey.withAlpha(80),
    // ),
    child: SizedBox(
      // height: 20,
      width: 90,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 4),
            child: Text(
              text,
              style: NORMAL_BOLD_TXT_STYLE,
            ),
          ),
          if (icon != null)
            Padding(
              padding: const EdgeInsets.only(left: 4),
              child: Icon(
                icon,
                color: tClrPrimary(context),
                size: 18,
              ),
            ),
        ],
      ),
    ),
  );
}

double? getAppBarHeight() {
  return !isOnMobile() ? 40 : null;
}

PreferredSizeWidget? getAppBarBottom(BuildContext context) {
  return !isOnMobile()
      ? PreferredSize(
          preferredSize: const Size.fromHeight(1.0),
          child: Container(
            // decoration: BoxDecoration(gradient: LinearGradient()),
            color: isDarkModeOnContext(context)
                ? Colors.grey.shade800
                : Colors.grey.shade100,
            height: 1.0,
          ),
        )
      : null;
}

List<BoxShadow> getBoxShadow(BuildContext context,
    {double blurRadius = 25, double spreadRadius = 2}) {
  return <BoxShadow>[
    BoxShadow(
        color: isOnMobile()
            ? isDarkModeOnContext(context,
                    isForcedDark: GlobalSettings.isForceDark)
                ? const Color.fromARGB(255, 25, 25, 25)
                : Colors.grey.withAlpha(20)
            : isDarkModeOnContext(context,
                    isForcedDark: GlobalSettings.isForceDark)
                ? const Color.fromARGB(255, 25, 25, 25)
                : Colors.grey.withAlpha(70),
        blurRadius: blurRadius,
        spreadRadius: spreadRadius),
  ];
}

Widget buildCloseButton(BuildContext context) {
  return GestureDetector(
    onTap: () => Navigator.of(context).pop(),
    child: Padding(
      padding: const EdgeInsets.only(right: 16),
      child: Container(
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.09),
          shape: BoxShape.circle,
        ),
        child: Icon(
          Icons.close,
          size: 18,
          color: Colors.grey.withAlpha(100),
        ),
      ),
    ),
  );
}

Widget buildBackButtonCircle(BuildContext context, Function()? onTap,
    {IconData iconData = Icons.arrow_back}) {
  return GestureDetector(
    onTap: onTap,
    child: Padding(
      padding: const EdgeInsets.only(left: 16),
      child: Container(
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
          color: Colors.black.withOpacity(0.1),
          shape: BoxShape.circle,
        ),
        child: Icon(
          iconData,
          size: 24,
          color: Colors.black54,
        ),
      ),
    ),
  );
}

class AppShareRootWidget extends StatelessWidget {
  final String appName;
  final String appLogoUrl;
  final String appDownloadUrl;
  final String shareContent;

  const AppShareRootWidget(
      {super.key,
      required this.appName,
      required this.appLogoUrl,
      required this.appDownloadUrl,
      required this.shareContent});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 200),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  CachedNetworkImageEnhanced(
                    imageUrl: appLogoUrl,
                    width: 21,
                    height: 21,
                  ),
                  g4,
                  Expanded(
                    child: Text(appName,
                        textAlign: TextAlign.left,
                        style: NORMAL_BOLD_TXT_STYLE.copyWith(fontSize: 10)),
                  ),
                ],
              ),
            ),
            ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 200),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        shareContent,
                        style: SMALL_TXT_STYLE,
                      ),
                    ),
                  ],
                ))
          ],
        ),
        QrCodeWidget(
          data: appDownloadUrl,
          size: 30,
          embeddedImageSize: const Size(30, 30),
        )
      ],
    );
  }
}

Widget buildRRectAvatar(String imgUrl,
    {double boderRaidus = 8,
    double size = 34,
    bool isCached = true,
    String failImgUrl = gAvatarDefaultMale}) {
  // return Container(
  //   width: size,
  //   height: size,
  //   decoration: BoxDecoration(
  //       borderRadius: BorderRadius.circular(boderRaidus),
  //       image: DecorationImage(
  //         image: isCached
  //             ? CachedNetworkImageProviderEnhanced(
  //                 imgUrl,

  //               )
  //             : NetworkImage(imgUrl) as ImageProvider,
  //         fit: BoxFit.cover,
  //       )),
  // );
  return ClipRRect(
    borderRadius: BorderRadius.circular(boderRaidus),
    child: CachedNetworkImageEnhanced(
      imageUrl: imgUrl,
      width: size,
      height: size,
      fit: BoxFit.cover,
      errorWidget: (BuildContext, String, Object) {
        return Image.network(
          failImgUrl,
          fit: BoxFit.cover,
        );
      },
    ),
  );
}

Widget buildRedDots(
    {double size = 14, Color color = Colors.red, Widget? child}) {
  return Container(
    width: size,
    height: size,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      color: color,
    ),
    child: Center(child: child ?? const SizedBox()),
  );
}

Widget buildChipsLabel(String c, Color color,
    {IconData? icon, double radius = 4, double fontSize = 9}) {
  return Container(
      padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 3),
      decoration: BoxDecoration(
          color: color.withAlpha(60),
          borderRadius: BorderRadius.all(Radius.circular(radius))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          if (icon != null)
            Icon(
              icon,
              color: color,
              size: 12,
            ),
          if (icon != null) g2,
          Text(c,
              style: SMALL_TXT_STYLE.copyWith(
                  fontSize: fontSize, color: darken(color)))
        ],
      ));
}

Widget buildChipsLabelGradient(String c, Color color,
    {IconData? icon,
    double radius = 4,
    double fontSize = 9,
    EdgeInsets padding = const EdgeInsets.symmetric(horizontal: 4, vertical: 3),
    int alpha = 40}) {
  return Container(
      padding: padding,
      decoration: BoxDecoration(
          gradient:
              LinearGradient(colors: getGradientColors(color.withAlpha(alpha))),
          borderRadius: BorderRadius.all(Radius.circular(radius))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          if (icon != null)
            Icon(
              icon,
              color: color,
              size: 12,
            ),
          if (icon != null) g2,
          Text(c,
              style: SMALL_TXT_STYLE.copyWith(
                  fontSize: fontSize, color: darken(color)))
        ],
      ));
}

Widget buildChipsLableTight(String c, Color color,
    {IconData? icon,
    double radius = 4,
    double fontSize = 9,
    EdgeInsets padding =
        const EdgeInsets.symmetric(horizontal: 2, vertical: 1)}) {
  return Container(
      // height: fontSize + 5,
      padding: padding,
      decoration: BoxDecoration(
          color: color.withAlpha(20),
          borderRadius: BorderRadius.all(Radius.circular(radius))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          if (icon != null)
            Icon(
              icon,
              color: color,
              size: 12,
            ),
          if (icon != null) g2,
          Center(
            child: Text(c,
                textAlign: TextAlign.center,
                style:
                    SMALL_TXT_STYLE.copyWith(color: color, fontSize: fontSize)),
          )
        ],
      ));
}

Widget buildChipsLableTightExpanded(String c, Color color,
    {IconData? icon, double radius = 4}) {
  return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      decoration: BoxDecoration(
          color: color.withAlpha(5),
          borderRadius: BorderRadius.all(Radius.circular(radius))),
      child: Column(
        children: [
          Row(
            children: [
              if (icon != null)
                Icon(
                  icon,
                  color: color,
                  size: 10,
                ),
              if (icon != null) g4,
              if (icon != null)
                Text(
                  'AI总结',
                  style: NORMAL_S_BOLD_TXT_STYLE.copyWith(color: color),
                ),
            ],
          ),
          if (icon != null) g2,
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: MyMarkdownAutoWrap(
                data: c,
                selectable: true,
                breakNum: -1,
                color: color.withOpacity(0.7),
              ))
            ],
          ),
        ],
      ));
}

Widget buildDragBar({double width = 22, double height = 8}) {
  return Container(
    width: width,
    height: height,
    decoration: BoxDecoration(
        color: Colors.grey.withAlpha(100),
        borderRadius: BorderRadius.circular(10)),
    child: Container(),
  );
}

List<BoxShadow> buildBoxShadowLight() {
  return [
    const BoxShadow(offset: Offset(2, 2), blurRadius: 4, spreadRadius: 2)
  ];
}

List<BoxShadow> buildBoxShadowHeavy(Color color) {
  return [
    BoxShadow(
        offset: const Offset(2, 2),
        blurRadius: 4,
        spreadRadius: 2,
        color: color)
  ];
}

Widget sizedBoxW(double w) {
  return SizedBox(width: w);
}

Widget sizedBoxH(double h) {
  return SizedBox(
    height: h,
  );
}

class SizeHelper {
  static var screenH = 100.h;
  static var screenW = 100.w;
}

const sbw16 = SizedBox(width: 16);
const sbw24 = SizedBox(width: 24);
const sbw32 = SizedBox(width: 32);
const sbw8 = SizedBox(width: 8);
const sbw6 = SizedBox(width: 6);
const sbw4 = SizedBox(width: 4);
const sbw2 = SizedBox(width: 2);
const sbh16 = SizedBox(height: 16);
const sbh32 = SizedBox(height: 32);
const sbh8 = SizedBox(height: 8);
const sbh4 = SizedBox(height: 4);
const g2 = Gap(2);
const g4 = Gap(4);
const g6 = Gap(6);
const g8 = Gap(8);
const g16 = Gap(16);
const g32 = Gap(32);
const g64 = Gap(64);
const sg2 = SliverToBoxAdapter(child: Gap(2));
const sg4 = SliverToBoxAdapter(child: Gap(4));
const sg6 = SliverToBoxAdapter(child: Gap(6));
const sg8 = SliverToBoxAdapter(child: Gap(8));
const sg16 = SliverToBoxAdapter(child: Gap(16));
const sg32 = SliverToBoxAdapter(child: Gap(32));
const sg64 = SliverToBoxAdapter(child: Gap(64));
