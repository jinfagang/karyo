import 'package:chinese_font_library/chinese_font_library.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

const double PADDING_TINY = 2.0;
const double PADDING_VERY_SMALL = 4.0;
const double PADDING_SMALL = 8.0;
const double PADDING_MEDIUM = 16.0;
const double PADDING_LARGE = 24.0;
const double PADDING_VERY_LARGE = 32.0;

const double FONT_VERY_SMALL = 4.0;
const double FONT_SMALL = 8.0;
const double FONT_MEDIUM = 16.0;
const double FONT_LARGE = 24.0;
const double FONT_VERY_LARGE = 32.0;

const List<String> gIllus = [
  'assets/illustrations/ff_girl_swimming_monochromatic.svg',
  'assets/illustrations/ff_currency_monochromatic.svg',
  'assets/illustrations/ff_flying_kite_monochromatic.svg',
  'assets/illustrations/ff_lemonade_monochromatic.svg',
  'assets/illustrations/ff_relaxing_monochromatic.svg',
  'assets/illustrations/ff_surfing_monochromatic.svg',
  'assets/illustrations/ff_volleyball_monochromatic.svg'
];

//For Task Row
// const String FONT_NAME = "Huawenxihei";
const String FONT_NAME = "NotoSansSC";
// const String FONT_NAME = "GoogleProductFont";
// const String FONT_NAME = "Junya";
// const String FONT_NAME = "Msyh";
// const String FONT_NAME = "OpenSansEmoji";
const String FALL_BACK_FONT_NAME = "NotoSansSC";
const String FALL_BACK_FONT_NAME_EMOJI = "NotoSansSC";
const String FONT_LOMBOK = "Lombok";
// const String FONT_FANGSONG = "Fangsong";
const String FONT_FANGSONG = "Henggu";
const String FONT_FOR_NUMBER = "Din";
const String FONT_JUNYA = "Junya";
// const String FONT_CODE = "Code";
const String FONT_CODE = "RobotoMono";
const String FONT_MINI_FANGSONG = "FangSong";

const double FONT_SIZE_TITLE = 15.0;
const double FONT_SIZE_YISHI = 19.0;
const double FONT_SIZE_LABEL = 14.0;
const double FONT_SIZE_DATE = 12.0;

const String TWITTER_URL = "https://twitter.com/burhanrashid52";
const String FACEBOOK_URL = "https://www.facebook.com/Bursid";
const String GITHUB_URL = "https://github.com/jinfagang";
const String PROJECT_URL = "http://db.manaai.cn";
const String ISSUE_URL = "https://github.com/jinfagang/daybreak_flutter/issues";
const String README_URL = "https://github.com/jinfagang/daybreak_flutter";
const String EMAIL_URL = "http://manaai.cn";
const List<String> ZODIAC_12 = [
  '🐭',
  '🐂',
  '🐯',
  '🐰',
  '🐲',
  '🐍',
  '🐎',
  '🐑',
  '🐒',
  '🐔',
  '🐶',
  '🐷'
];

const List<String> COMMON_EMOJIS = [
  '🍯',
  '🍋',
  '🎊',
  '😙',
  '🦷',
  '🧠‍‍‍',
  '⛑',
  '🌝',
  '🌳️',
  '☄️️',
  '☃️',
  '🥬',
  '🧅',
  '🍯',
  '🧉',
  '🥂️️',
  '🏹',
  '🤼‍♀️',
  '🏋️',
  '🤣',
  '🙂',
  '🥰',
  '🤩',
  '🤪',
  '🤑',
  '🤗',
  '🤭',
  '🤫',
  '💄',
  '💍',
  '💼',
  '🤔',
  '🤐',
  '🤨',
  '🙄',
  '😬',
  '🤥',
  '🤤',
  '🤒',
  '🤕',
  '🤢',
  '🤮',
  '🤧',
  '🥵',
  '🥶',
  '🥴',
  '😵',
  '🤯',
  '🤠',
  '🥳',
  '🤓',
  '🧐',
  '🙁',
  '🥺',
  '😤',
  '🤬',
  '👿',
  '💀',
  '💩',
  '🤡',
  '👹',
  '👺',
  '👻',
  '👽',
  '👾',
  '🤖',
  '💋',
  '👋',
  '🤚',
  '🖖',
  '🤞',
  '🤟',
  '🤘',
  '🤙',
  '🖕',
  '👊',
  '🤛',
  '🤜',
  '👏',
  '🙌',
  '👐',
  '🤲',
  '🤝',
  '🙏',
  '💅',
  '🤳',
  '💪',
  '🦵',
  '🦶',
  '👃',
  '🧠',
  '🦷',
  '🦴',
  '👀',
  '👅',
  '👄',
  '👶',
  '🧒',
  '👦',
  '🤴',
  '👸',
  '👲',
  '🧕',
  '🤵',
  '👰',
  '🤰',
  '🤱',
  '👼',
  '🎅',
  '🤶',
  '💃',
  '🕺',
  '🧘',
  '👭',
  '👫',
  '👬',
  '💏',
  '💑',
  '👪',
  '👤',
  '👥',
  '👣',
  '🧳',
  '🌂',
  '🧵',
  '🧶',
  '🥽',
  '🥼',
  '👔',
  '👕',
  '👖',
  '🧣',
  '🧤',
  '🧥',
  '🧦',
  '👗',
  '👘',
  '👙',
  '👚',
  '👛',
  '👜',
  '👝',
  '🎒',
  '👞',
  '👟',
  '🥾',
  '🥿',
  '👠',
  '👡',
  '👢',
  '👑',
  '👒',
  '🎩',
  '🎓',
  '🧢',
];

// Colors
// const Color COLOR_PRIMARY = const Color(0xff3F4DE4);
// Color COLOR_PRIMARY = const Color(0xff272727);
// Color COLOR_PRIMARY = const Color(0xffffa190);
// Color COLOR_PRIMARY = const Color(0xffE83039);
// Color COLOR_PRIMARY = const Color(0xffE83039);
// Color COLOR_PRIMARY = const Color(0xffb4004e);
Color COLOR_PRIMARY = Color.fromARGB(255, 168, 59, 223);
// Color COLOR_PRIMARY = const Color(0xffb4004e);
const Color COLOR_PRIMARY_DARK = const Color(0xffb4004e);
// const Color COLOR_ACCENT = const Color(0xee5160FE);
const Color COLOR_ACCENT = const Color(0xfff50766);
// const Color COLOR_ACCENT2 = const Color(0xff480FF5);
const Color COLOR_ACCENT2 = const Color(0xffcc2f4e);
const Color COLOR_ACCENT3 = const Color(0xff000000);
const Color COLOR_MAIN_BK = Colors.white;

double FONT_SIZE_BASE = 14;

double NORMAL_TITLE_FONT_SIZE = 18;
double NORMAL_FONT_SIZE = 15;
double NORMAL_S_FONT_SIZE = 12;
double HERO_FONT_SIZE = 24;
double HERO_S_FONT_SIZE = 20;
double SMALL_FONT_SIZE = 10;
double EX_SMALL_FONT_SIZE = 8;

// default text Styles
TextStyle NORMAL_TXT_STYLE = TextStyle(
    fontSize: NORMAL_FONT_SIZE,
    // color: Color(0xff2d3645),
    fontFamily: FONT_NAME,
    // letterSpacing: 1,
    fontFamilyFallback: const [
      FALL_BACK_FONT_NAME,
      FALL_BACK_FONT_NAME_EMOJI
    ]).useSystemChineseFont();
TextStyle NORMAL_S_TXT_STYLE = TextStyle(
    fontSize: NORMAL_S_FONT_SIZE,
    fontFamily: FONT_NAME,
    fontFamilyFallback: const [
      FALL_BACK_FONT_NAME,
      FALL_BACK_FONT_NAME_EMOJI
    ]).useSystemChineseFont();
TextStyle NORMAL_S_BOLD_TXT_STYLE = TextStyle(
    fontSize: NORMAL_S_FONT_SIZE,
    fontFamily: FONT_NAME,
    fontWeight: FontWeight.bold,
    letterSpacing: 1,
    fontFamilyFallback: const [
      FALL_BACK_FONT_NAME,
      FALL_BACK_FONT_NAME_EMOJI
    ]).useSystemChineseFont();
TextStyle TITLE_TXT_STYLE = TextStyle(
        fontSize: NORMAL_TITLE_FONT_SIZE,
        fontFamily: FONT_NAME,
        fontFamilyFallback: [FALL_BACK_FONT_NAME, FALL_BACK_FONT_NAME_EMOJI])
    .useSystemChineseFont();
TextStyle TITLE_TXT_BOLD_STYLE = TextStyle(
    fontSize: NORMAL_TITLE_FONT_SIZE,
    fontWeight: FontWeight.bold,
    fontFamily: FONT_NAME,
    fontFamilyFallback: const [
      FALL_BACK_FONT_NAME,
      FALL_BACK_FONT_NAME_EMOJI
    ]).useSystemChineseFont();
TextStyle HERO_TITLE_TXT_STYLE = TextStyle(
    fontSize: HERO_FONT_SIZE,
    fontFamily: FONT_NAME,
    fontFamilyFallback: const [
      FALL_BACK_FONT_NAME,
      FALL_BACK_FONT_NAME_EMOJI
    ]).useSystemChineseFont();
TextStyle HERO_TITLE_TXT_BOLD_STYLE = TextStyle(
    fontSize: HERO_FONT_SIZE,
    fontWeight: FontWeight.bold,
    fontFamily: FONT_NAME,
    fontFamilyFallback: const [
      FALL_BACK_FONT_NAME,
      FALL_BACK_FONT_NAME_EMOJI
    ]).useSystemChineseFont();
TextStyle HERO_S_TITLE_TXT_BOLD_STYLE = TextStyle(
    fontSize: HERO_S_FONT_SIZE,
    fontWeight: FontWeight.bold,
    fontFamily: FONT_NAME,
    fontFamilyFallback: const [
      FALL_BACK_FONT_NAME,
      FALL_BACK_FONT_NAME_EMOJI
    ]).useSystemChineseFont();
TextStyle NORMAL_BOLD_TXT_STYLE = NORMAL_TXT_STYLE
    .copyWith(
      fontWeight: FontWeight.bold,
    )
    .useSystemChineseFont();

TextStyle SMALL_TXT_STYLE = TextStyle(
        fontSize: SMALL_FONT_SIZE,
        color: PlatformTheme.grey100,
        fontFamily: FONT_NAME,
        fontFamilyFallback: [FALL_BACK_FONT_NAME, FALL_BACK_FONT_NAME_EMOJI])
    .useSystemChineseFont();
TextStyle SMALL_LG_TXT_STYLE = TextStyle(
        fontSize: 12,
        color: PlatformTheme.grey100,
        fontFamily: FONT_NAME,
        fontFamilyFallback: [FALL_BACK_FONT_NAME, FALL_BACK_FONT_NAME_EMOJI])
    .useSystemChineseFont();
TextStyle EX_SMALL_TXT_STYLE = SMALL_TXT_STYLE
    .copyWith(
      fontSize: EX_SMALL_FONT_SIZE,
    )
    .useSystemChineseFont();
TextStyle SMALL_BOLD_TXT_STYLE = SMALL_TXT_STYLE
    .copyWith(
      fontWeight: FontWeight.bold,
    )
    .useSystemChineseFont();

const gDefaultAvatarUrl =
    '${API.BASE_URL_HTTPS}/uploads/gerg/pictures/2024/08/15_22_7_42_81723730844_.pic.jpg';

const gAvatarDefaultMale =
    '${API.BASE_URL_HTTPS}/uploads/gerg/pictures/2024/08/29_12_58_39_user.jpg';
const gAvatarDefaultFemale =
    '${API.BASE_URL_HTTPS}/uploads/gerg/pictures/2024/08/29_13_2_49_202408291302979.png';
