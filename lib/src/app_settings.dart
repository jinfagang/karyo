import 'package:flutter/material.dart';

import 'app_enums.dart';
import 'app_prefs.dart';
import 'app_utils.dart';
import 'logic_util.dart';
import 'models/user.dart';

/////////////// settings //////////////////
class GlobalSettings {
  static VIPStatus vipStatus = VIPStatus.NO_VIP;
  static bool isUsingIOSBackground = false;
  static bool isUsingWaveProgress = false;
  static bool isUsingChangeLineSubmit = false;
  static bool isUsingWenliBk = false;
  static bool isUsingSimMode = false;
  static bool isUsingGodMode = false;
  static bool isUsingYishi = false;
  static bool isUsingBlur = false;
  static bool isForceDark = false;
  static bool isTabLabel = true;
  static bool isAppFirstRun = false;
  static User? user;
  static String? token;
  static String? userAddr;
  static String? userAvatarUrl;
  static int? sortBy;
  static int? lnum;
  static double? fontScaleLevel;
  static bool isListShowProj = false;

  static String? token_mana;
  static String? token_uranus;
  static int themeIndex = 1;

  static bool isEnterSend = false;
  static bool isShowIncomingLabel = false;
  static bool isShowMeAvatar = false;

  static bool isAppInBackground = false;
  static String userCityLocFull = "";
  static String userCityLoc = "";
  static int appID = 0;

  // using get_storage
  static bool useChatAug = true;
  static String? desktopInitMode = 'default';

  static init() async {
    getUsingIOSBackground();
    getVIPStatus();
    getUsingChangeLineSubmit();
    getUsingSimMode();
    getToken();
    getUserAddr();
    getUsingYishi();
    getFontScaleLevel();
    getUsingGodMode();
    getGodLayoutNum();
    getUserAavatarUrl();
    getSortBy();
    getUsingWenliBk();
    getListShowProj();
    getDesktopInitMode();
    await getUsingBlur();
    await getForceDark();
    await getIsShowIncomingLabel();
    await getIsEnterSend();
    await getIsShowMeAvatar();

    // add for dragon ruler
    await getMANAToken();
    await getUranusToken();

    await getTabLabel();
    await getThemeIndex();
  }

  static Future<int> getThemeIndex() async {
    var v =
        await SharedPreferenceUtil.getInt(SharedPreferenceUtil.KEY_THEME_INDEX);
    debugPrint("themeIndex from sharePref: $v");
    themeIndex = v ?? themeIndex;
    return themeIndex;
  }

  static setMANAToken(String v) {
    token = v;
    SharedPreferenceUtil.save(SharedPreferenceUtil.KEY_TOKEN_MANA, v);
  }

  static setDesktopInitMode(String v) {
    desktopInitMode = v;
    SharedPreferenceUtil.save('desktopInitMode', v);
  }

  static setEnterSend(bool v) {
    isEnterSend = v;
    SharedPreferenceUtil.saveBool(
        SharedPreferenceUtil.KEY_SETTING_IS_ENTER_SEND, v);
  }

  static setShowIncomingLabel(bool v) {
    isShowIncomingLabel = v;
    SharedPreferenceUtil.saveBool(
        SharedPreferenceUtil.KEY_SETTING_IS_SHOW_INCOME_LABEL, v);
  }

  static setShowMeAvatar(bool v) {
    isShowMeAvatar = v;
    SharedPreferenceUtil.saveBool(
        SharedPreferenceUtil.KEY_SETTING_IS_SHOW_ME_AVATAR, v);
  }

  static getMANAToken() async {
    token_mana =
        await SharedPreferenceUtil.get(SharedPreferenceUtil.KEY_TOKEN_MANA);
  }

  static getDesktopInitMode() async {
    desktopInitMode =
        await SharedPreferenceUtil.get('desktopInitMode') ?? 'default';
  }

  static setForceDark(bool v) {
    isForceDark = v;
    SharedPreferenceUtil.saveBool(
        SharedPreferenceUtil.KEY_SETTING_FORCE_DARK, v);
  }

  static getTabLabel() async {
    isTabLabel =
        await SharedPreferenceUtil.getBool(SharedPreferenceUtil.KEY_TAB_LABEL);
    return isTabLabel;
  }

  static getForceDark() async {
    isForceDark = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_FORCE_DARK);
  }

  static setUranusToken(String v) {
    token = v;
    SharedPreferenceUtil.save(SharedPreferenceUtil.KEY_TOKEN_URANUS, v);
  }

  static getUranusToken() async {
    token_uranus =
        await SharedPreferenceUtil.get(SharedPreferenceUtil.KEY_TOKEN_URANUS);
  }

  static updateFontSize() {
    // if (fontScaleLevel! > 3.0) {
    //   fontScaleLevel *= 0.4;
    // } else if (fontScaleLevel! > 2.0) {
    //   fontScaleLevel *= 0.5;
    // } else if (fontScaleLevel! > 1.0) {
    //   fontScaleLevel *= 0.7;
    // }
    // print('font scale is: $fontScaleLevel');
  }

  static setFontScaleLevel(double l) {
    SharedPreferenceUtil.saveDouble(
        SharedPreferenceUtil.KEY_FONT_SCALE_LEVEL, l);
    fontScaleLevel = l;
  }

  static getFontScaleLevel() async {
    fontScaleLevel = await SharedPreferenceUtil.getDouble(
        SharedPreferenceUtil.KEY_FONT_SCALE_LEVEL);
    double _dS = 1.0;
    if (!isOnMobile()) {
      _dS = 1.6;
      // not on mobile, default Scale bigger
    }
    fontScaleLevel = fontScaleLevel == null ? _dS : fontScaleLevel;
  }

  static setSortBy(int v) {
    SharedPreferenceUtil.saveInt(SharedPreferenceUtil.KEY_SETTING_SORT_BY, v);
  }

  static getSortBy() async {
    sortBy = await SharedPreferenceUtil.getInt(
        SharedPreferenceUtil.KEY_SETTING_SORT_BY);
    sortBy = sortBy ?? SORT_BY.BY_DUE_DATE.index;
  }

  static setListShowProj(bool v) {
    SharedPreferenceUtil.saveBool(SharedPreferenceUtil.KEY_IS_LISTSHOW_PROJ, v);
  }

  static getListShowProj() async {
    var v = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_IS_LISTSHOW_PROJ);
    isListShowProj = v;
  }

  static setUserAddr(String ua) {
    SharedPreferenceUtil.save(SharedPreferenceUtil.KEY_ACCOUNT_ADDR, ua);
    userAddr = ua;
  }

  static getUserAddr() async {
    userAddr =
        await SharedPreferenceUtil.get(SharedPreferenceUtil.KEY_ACCOUNT_ADDR);
    return userAddr;
  }

  static setUserAavatarUrl(String ua) {
    SharedPreferenceUtil.save(SharedPreferenceUtil.KEY_USER_AVATAR_URL, ua);
    userAvatarUrl = ua;
  }

  static getUserAavatarUrl() async {
    userAvatarUrl = await SharedPreferenceUtil.get(
        SharedPreferenceUtil.KEY_USER_AVATAR_URL);
    return userAvatarUrl;
  }

  static getGodLayoutNum() async {
    lnum =
        await SharedPreferenceUtil.getInt(SharedPreferenceUtil.KEY_GOD_LAYOUT);
    lnum = lnum ?? 2;
    return lnum;
  }

  static setGodLayoutNum(int v) {
    lnum = v;
    SharedPreferenceUtil.saveInt(SharedPreferenceUtil.KEY_GOD_LAYOUT, v);
  }

  static setUser(User u) {
    user = u;
  }

  static getUser() {
    return user;
  }

  static setVIPStatus(int s) {
    vipStatus = VIPStatus.values[s];
  }

  static getVIPStatus() async {
    var _vipPlan =
        await SharedPreferenceUtil.getInt(SharedPreferenceUtil.KEY_VIP_PLAN);
    var _vipStartTime = await SharedPreferenceUtil.getInt(
        SharedPreferenceUtil.KEY_VIP_START_TIME);
    if (_vipPlan == null) {
      vipStatus = VIPStatus.NO_VIP;
    } else if (IsVipValid(
        _vipPlan, DateTime.fromMillisecondsSinceEpoch(_vipStartTime!))) {
      vipStatus = VIPStatus.VIP_OK;
    } else {
      vipStatus = VIPStatus.VIP_EXPIRED;
    }
  }

  static setUsingIOSBackground(bool v) {
    isUsingIOSBackground = v;
    SharedPreferenceUtil.saveBool(SharedPreferenceUtil.KEY_SETTING_USE_IOS, v);
  }

  static setUsingBlur(bool v) {
    isUsingBlur = v;
    SharedPreferenceUtil.saveBool(SharedPreferenceUtil.KEY_USE_BLUR, v);
  }

  static setUsingGodMode(bool v) {
    isUsingGodMode = v;
    SharedPreferenceUtil.saveBool(
        SharedPreferenceUtil.KEY_SETTING_USE_GOD_MODE, v);
  }

  static getUsingGodMode() async {
    isUsingGodMode = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_USE_GOD_MODE);
    return isUsingGodMode;
  }

  static setUsingYishi(bool v) {
    isUsingYishi = v;
    SharedPreferenceUtil.saveBool(SharedPreferenceUtil.KEY_SETTING_YISHIGAN, v);
  }

  static setTabLabel(bool v) {
    isTabLabel = v;
    SharedPreferenceUtil.saveBool(SharedPreferenceUtil.KEY_TAB_LABEL, v);
  }

  static setThemeIndex(int v) {
    // isTabLabel = v;
    SharedPreferenceUtil.saveInt(SharedPreferenceUtil.KEY_THEME_INDEX, v);
  }

  static setToken(String v) {
    token = v;
    SharedPreferenceUtil.save(SharedPreferenceUtil.KEY_TOKEN, v);
  }

  static getToken() async {
    token = await SharedPreferenceUtil.get(SharedPreferenceUtil.KEY_TOKEN);
    return token;
  }

  static setUsingChangeLineSubmit(bool v) {
    isUsingChangeLineSubmit = v;
    SharedPreferenceUtil.saveBool(
        SharedPreferenceUtil.KEY_SETTING_CHANGE_LINE_COMPLETE, v);
  }

  static getUsingChangeLineSubmit() async {
    isUsingChangeLineSubmit = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_CHANGE_LINE_COMPLETE);
    return isUsingChangeLineSubmit;
  }

  static setUsingWenliBk(bool v) {
    isUsingWenliBk = v;
    SharedPreferenceUtil.saveBool(SharedPreferenceUtil.KEY_SETTING_WENLI_BK, v);
  }

  static getUsingWenliBk() async {
    isUsingWenliBk = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_WENLI_BK);
    return isUsingWenliBk;
  }

  static getUsingYishi() async {
    isUsingYishi = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_YISHIGAN);
    return isUsingYishi;
  }

  static getUsingSimMode() async {
    isUsingSimMode = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_SIM_MODE);
    return isUsingSimMode;
  }

  static getUsingIOSBackground() async {
    isUsingIOSBackground = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_USE_IOS);
    return isUsingIOSBackground;
  }

  static getIsEnterSend() async {
    isEnterSend = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_IS_ENTER_SEND);
    return isEnterSend;
  }

  static getIsShowIncomingLabel() async {
    isShowIncomingLabel = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_IS_SHOW_INCOME_LABEL);
    return isShowIncomingLabel;
  }

  static getIsShowMeAvatar() async {
    isShowMeAvatar = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_IS_SHOW_ME_AVATAR);
    return isShowMeAvatar;
  }

  static getUsingBlur() async {
    isUsingBlur =
        await SharedPreferenceUtil.getBool(SharedPreferenceUtil.KEY_USE_BLUR);
    return isUsingBlur;
  }

  static setUsingWaveProgress(bool v) {
    isUsingWaveProgress = v;
    SharedPreferenceUtil.saveBool(
        SharedPreferenceUtil.KEY_SETTING_USE_WAVE_PG, v);
  }

  static setUsingSimMode(bool v) {
    isUsingSimMode = v;
    SharedPreferenceUtil.saveBool(SharedPreferenceUtil.KEY_SETTING_SIM_MODE, v);
  }

  static getUsingWaveProgress() async {
    isUsingWaveProgress = await SharedPreferenceUtil.getBool(
        SharedPreferenceUtil.KEY_SETTING_USE_WAVE_PG);
    return isUsingWaveProgress;
  }
}
