import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class GradientIcon extends StatelessWidget {
  GradientIcon(this.icon,
      {this.size,
      this.color,
      this.gradient,
      this.withBk = false,
      this.padding});

  final IconData icon;
  final double? size;
  final Gradient? gradient;
  final Color? color;
  final bool withBk;
  final EdgeInsets? padding;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding ?? EdgeInsets.all(2),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: withBk ? (color ?? Colors.pink).withAlpha(40) : null),
      child: ShaderMask(
        child: SizedBox(
          width: size ?? 22 * 1.2,
          height: size ?? 22 * 1.2,
          child: Icon(
            icon,
            size: size,
            color: Colors.white,
          ),
        ),
        shaderCallback: (Rect bounds) {
          final Rect rect = Rect.fromLTRB(0, 0, size ?? 22, size ?? 22);
          return (gradient ??
                  LinearGradient(
                      colors: getGradientColors(color ?? Colors.pink)))
              .createShader(rect);
        },
      ),
    );
  }
}
