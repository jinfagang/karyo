import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_highlighter/flutter_highlighter.dart';
import 'package:flutter_highlighter/themes/atom-one-dark.dart';
import 'package:flutter_highlighter/themes/atom-one-light.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:get/get.dart';
import 'package:karyo/karyo.dart';
import 'package:karyo/src/app_utils.dart';
import 'package:markdown/markdown.dart' as md;
import 'dart:ui' as ui;

// import 'package:google_fonts/google_fonts.dart';

class CodeElementBuilder extends MarkdownElementBuilder {
  String stripTrailingNewlines(String text) {
    return text.replaceAll(RegExp(r'\n+$'), '');
  }

  @override
  Widget? visitElementAfterWithContext(BuildContext context, md.Element element,
      TextStyle? preferredStyle, TextStyle? parentStyle) {
    var language = '';

    if (element.attributes['class'] != null) {
      String lg = element.attributes['class'] as String;
      language = lg.substring(9);
    }
    return ClipRRect(
      borderRadius: BorderRadius.circular(6),
      child: Stack(
        fit: StackFit.passthrough,
        children: [
          HighlightView(
            // The original code to be highlighted
            stripTrailingNewlines(element.textContent),
            // Specify language
            // It is recommended to give it a value for performance
            language: language,
            // Specify highlight theme
            // All available themes are listed in `themes` folder
            theme: isDarkModeOnContext(context)
                ? atomOneDarkTheme
                : atomOneLightTheme,
            // Specify padding
            padding: const EdgeInsets.all(4),
            // Specify text style
            textStyle: preferredStyle!
                .copyWith(fontSize: 12, backgroundColor: Colors.transparent),
          ),
          if (language != '')
            Align(
              alignment: Alignment.topRight,
              child: Padding(
                padding: const EdgeInsets.only(right: 4, top: 4),
                child: TextButton(
                  style: getZeroBtnStyle(),
                  onPressed: () {
                    CopyToClipboard(element.textContent);
                  },
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        language,
                        style: SMALL_TXT_STYLE,
                      ),
                      g2,
                      Icon(
                        CupertinoIcons.doc_on_clipboard_fill,
                        size: 14,
                        color: Colors.grey.withAlpha(70),
                      ),
                    ],
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }
}

class CachedNetworkImageBuilder extends MarkdownElementBuilder {
  @override
  Widget visitElementAfter(md.Element element, TextStyle? preferredStyle) {
    final String? imageUrl = element.attributes['src'];
    if (imageUrl != null) {
      return CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => const CircularProgressIndicator(),
        errorWidget: (context, url, error) => const Icon(CupertinoIcons.photo),
      );
    }
    return const Text('Image not found');
  }
}

class NewlineSyntax extends md.InlineSyntax {
  NewlineSyntax() : super(r'(\n\s*)');

  @override
  bool onMatch(md.InlineParser parser, Match match) {
    final newlineCount = '\n'.allMatches(match[0]!).length;
    kLog("fuck?? ==> $newlineCount");
    parser.addNode(md.Text('\n' * newlineCount));
    return true;
  }
}

class MyMarkdownAutoWrap extends StatefulWidget {
  final bool selectable;
  final Color color;
  final String data;
  final int breakNum;
  final Color? moreBtnColor;
  final Function(String, String?, String)? onTapLink;
  const MyMarkdownAutoWrap({
    super.key,
    this.data = '',
    this.selectable = true,
    this.color = Colors.black,
    this.breakNum = 600,
    this.moreBtnColor = Colors.transparent,
    this.onTapLink,
  });

  @override
  State<MyMarkdownAutoWrap> createState() => _MyMarkdownAutoWrapState();
}

class _MyMarkdownAutoWrapState extends State<MyMarkdownAutoWrap> {
  final isExpanded = false.obs;

  @override
  Widget build(BuildContext context) {
    return getMainPart();
  }

  Widget getMainPart() {
    return widget.breakNum < 0
        ? _normalMarkdown(widget.data)
        : Stack(
            alignment: Alignment.center,
            children: [
              Obx(
                () => (!isExpanded.value &&
                        widget.data.length > widget.breakNum)
                    ? Padding(
                        padding: const EdgeInsets.only(bottom: 16),
                        child: _normalMarkdown(
                            '${widget.data.substring(0, widget.breakNum)}......'))
                    : const SizedBox(
                        width: 0,
                      ),
              ),
              Obx(
                () =>
                    (isExpanded.value && widget.data.length > widget.breakNum ||
                            widget.data.length <= widget.breakNum)
                        ? _normalMarkdown(widget.data)
                        : const SizedBox(
                            width: 0,
                          ),
              ),
              if (widget.data.length > widget.breakNum)
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: TextButton(
                    onPressed: () {
                      isExpanded.value = !isExpanded.value;
                    },
                    child: Center(
                      child: Obx(
                        () => Text(
                          isExpanded.value ? '收起' : '更多',
                          style: NORMAL_BOLD_TXT_STYLE.copyWith(
                              color: tClrSecondary(context)),
                        ),
                      ),
                    ),
                  ),
                ),
            ],
          );
  }

  Widget _buildMarkdownBody(String data) {
    return MarkdownBody(
      data: data,
      shrinkWrap: true,
      styleSheet: getMarkdownStyleSheet(widget.color),
      builders: {
        'code': CodeElementBuilder(),
        "img": CachedNetworkImageBuilder(),
      },
      onTapLink: widget.onTapLink,
      // styleSheetTheme: MarkdownStyleSheetBaseTheme.cupertino,
    );
  }

  Widget _normalMarkdown(String data) {
    return Theme(
      data: ThemeData(
        textSelectionTheme: TextSelectionThemeData(
          selectionColor: tClrSecondary(context),
        ),
      ),
      child: isOnMobile()
          ? _buildMarkdownBody(data)
          : SelectionArea(child: _buildMarkdownBody(data)),
    );
  }

  MarkdownStyleSheet getMarkdownStyleSheet(Color txtColor) {
    return MarkdownStyleSheet(
        p: TextStyle(
          fontSize: 16,
          color: txtColor,
          fontWeight: FontWeight.w500,
        ),
        h1: TextStyle(
            fontSize: 16, fontWeight: FontWeight.bold, color: txtColor),
        h2: TextStyle(
            fontSize: 16, fontWeight: FontWeight.w700, color: txtColor),
        h3: TextStyle(
            fontSize: 16, fontWeight: FontWeight.w600, color: txtColor),
        h4: TextStyle(
            fontSize: 16, fontWeight: FontWeight.w600, color: txtColor),
        h5: TextStyle(
            fontSize: 16, fontWeight: FontWeight.w600, color: txtColor),
        listBullet: TextStyle(fontSize: 16, color: txtColor),
        listIndent: 24.0,
        listBulletPadding: const EdgeInsets.only(left: 0),
        blockquote: TextStyle(fontSize: 16, color: txtColor),
        codeblockDecoration: BoxDecoration(
            color: const Color.fromARGB(255, 35, 35, 35),
            borderRadius: BorderRadius.circular(10)),
        // code: TextStyle(
        //   // backgroundColor: theme.cardTheme.color ?? theme.cardColor,
        //   backgroundColor: Colors.transparent,
        //   fontFamily: 'monospace',
        //   fontSize: 14,
        // ),
        horizontalRuleDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(45),
          border: Border(
            top: BorderSide(
              color: Colors.grey.withAlpha(70),
              width: 1.0,
            ),
          ),
        ),
        blockquoteDecoration: BoxDecoration(
            color: Colors.grey.withAlpha(30),
            borderRadius: BorderRadius.circular(10)));
  }
}
