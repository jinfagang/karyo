import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';

class ImageGallery extends StatelessWidget {
  const ImageGallery({
    super.key,
    this.imageHeaders,
    required this.images,
    required this.onClosePressed,
    this.options = const ImageGalleryOptions(),
    required this.pageController,
  });

  final Map<String, String>? imageHeaders;
  final List<String> images;
  final VoidCallback onClosePressed;
  final ImageGalleryOptions options;
  final PageController pageController;

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: () async {
          onClosePressed();
          return false;
        },
        child: Dismissible(
          key: const Key('photo_view_gallery'),
          direction: DismissDirection.down,
          onDismissed: (direction) => onClosePressed(),
          child: SizedBox(
            height: double.infinity,
            child: Stack(
              children: [
                PhotoViewGallery.builder(
                  builder: (BuildContext context, int index) =>
                      PhotoViewGalleryPageOptions(
                    imageProvider: CachedNetworkImageProvider(images[index]),
                    minScale: options.minScale,
                    maxScale: options.maxScale,
                  ),
                  itemCount: images.length,
                  loadingBuilder: (context, event) =>
                      _imageGalleryLoadingBuilder(event),
                  pageController: pageController,
                  scrollPhysics: const ClampingScrollPhysics(),
                ),
                Positioned.directional(
                  end: 16,
                  textDirection: Directionality.of(context),
                  top: 56,
                  child: CloseButton(
                    color: Colors.grey.shade100,
                    onPressed: onClosePressed,
                  ),
                ),
              ],
            ),
          ),
        ),
      );

  Widget _imageGalleryLoadingBuilder(ImageChunkEvent? event) => Center(
        child: SizedBox(
          width: 20,
          height: 20,
          child: CircularProgressIndicator(
            value: event == null || event.expectedTotalBytes == null
                ? 0
                : event.cumulativeBytesLoaded / event.expectedTotalBytes!,
          ),
        ),
      );
}

class ImageGalleryOptions {
  const ImageGalleryOptions({
    this.maxScale,
    this.minScale,
  });

  /// See [PhotoViewGalleryPageOptions.maxScale].
  final dynamic maxScale;

  /// See [PhotoViewGalleryPageOptions.minScale].
  final dynamic minScale;
}
