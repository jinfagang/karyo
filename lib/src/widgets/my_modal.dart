import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hyper_effects/hyper_effects.dart';
import 'package:karyo/karyo.dart';
import 'package:karyo/src/app_utils.dart';
import 'package:karyo/src/widgets/enchanced_button.dart';

import 'dart:ui';
import 'package:flutter/material.dart';

class BottomSheetBox extends StatelessWidget {
  final Widget child;
  const BottomSheetBox({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 24),
      child: SafeArea(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 400),
          child: Stack(
            children: [
              Positioned.fill(
                child: ClipRRect(
                    borderRadius: const BorderRadius.vertical(
                      top: Radius.circular(55),
                      bottom: Radius.circular(55),
                    ),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 22, sigmaY: 22),
                      child: Container(
                        // width: double.infinity,
                        // height: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.vertical(
                            top: Radius.circular(44),
                            bottom: Radius.circular(44),
                          ),
                          boxShadow: getBoxShadow(context, spreadRadius: 12),
                          color: isDarkModeOnContext(context)
                              ? PlatformTheme.getMainBkColor(context)
                                  .withAlpha(190)
                              : Colors.white.withAlpha(200),
                        ),
                        padding: const EdgeInsets.only(
                            top: 0, left: 8, right: 8, bottom: 8),
                      ),
                    )),
              ),
              child,
            ],
          ),
        ),
      ),
    );
  }
}

Future openModalBottomSheet(
  BuildContext context,
  Widget Function(BuildContext context) builder, {
  bool useSafeArea = false,
  isScrollControlled = true,
  double heightFactor = 0.5,
  String? title,
  bool disableEvent = false,
  bool disableCompleteEvent = false,
  bool disableInitEvent = false,
}) {
  if (!disableEvent && !disableInitEvent) {
    // GlobalEvent().emit('hideBottomNavigatorBar');
  }

  return showModalBottomSheet(
    context: context,
    useSafeArea: useSafeArea,
    isScrollControlled: isScrollControlled,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(45),
      ),
    ),
    elevation: 0,
    backgroundColor: Colors.transparent,
    barrierColor: Colors.transparent,
    showDragHandle: false,
    useRootNavigator: true,
    builder: (context) {
      return BottomSheetBox(
        child: FractionallySizedBox(
          heightFactor: heightFactor,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              buildBottomSheetTopBar(),
              if (title != null)
                Text(
                  title,
                  style: const TextStyle(fontSize: 16),
                ),
              if (title != null) const SizedBox(height: 10),
              Expanded(
                child: builder(context),
              ),
            ],
          ),
        ),
      );
    },
  ).whenComplete(() {
    if (!disableEvent && !disableCompleteEvent) {
      // GlobalEvent().emit('showBottomNavigatorBar');
    }
  });
}

openConfirmDialog(
  BuildContext context,
  String? message,
  Function() onConfirm, {
  Widget? title,
  Function()? onCancel,
  bool danger = false,
  String? confirmText,
  String? confirmText2,
  Color? color,
  Color? color2,
  String? cancelText,
  Function()? onConfirm2,
  bool? doPop = true,
  Widget? actionWidget,
}) {
  HapticFeedback.lightImpact();
  final btnH = 60.0;
  final radius = BorderRadius.circular(22);
  showModalBottomSheet(
    context: context,
    elevation: 0,
    backgroundColor: Colors.transparent,
    // barrierColor: Colors.grey.withOpacity(0.2),
    barrierColor: Colors.transparent,
    builder: (context) {
      return BottomSheetBox(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            buildBottomSheetTopBar(),
            if (message != null) const SizedBox(height: 4),
            if (title != null) title,
            if (title != null) const SizedBox(height: 6),
            if (message != null)
              Text(
                message,
                style: NORMAL_BOLD_TXT_STYLE,
                textAlign: TextAlign.center,
              ),
            const SizedBox(height: 32),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 44),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  actionWidget ?? const SizedBox(),
                  if (actionWidget != null) g8,
                  EnhancedButton(
                    title: confirmText ?? "确定",
                    radius: radius,
                    height: btnH,
                    onPressed: () {
                      onConfirm();
                      if (onConfirm2 == null && doPop!) {
                        Navigator.pop(context);
                      }
                    },
                    // size: const ButtonSize.full(),
                    color: danger
                        ? const Color.fromARGB(255, 255, 17, 0)
                        : tClrOnTertiaryContainer(context),
                    backgroundColor: color ?? tClrTertiaryContainer(context),
                  ),
                  g8,
                  if (confirmText2 != null)
                    EnhancedButton(
                      height: btnH,
                      title: confirmText2,
                      radius: radius,

                      onPressed: () {
                        if (onConfirm2 != null) onConfirm2();
                        Navigator.pop(context);
                      },
                      // size: const ButtonSize.full(),
                      color: tClrPrimary(context),
                      backgroundColor: color2 ?? tClrPrimary(context),
                    ),
                  if (confirmText2 != null) const SizedBox(height: 16),
                  EnhancedButton(
                      title: cancelText ?? "取消",
                      height: btnH,
                      radius: radius,
                      backgroundColor: isDarkModeOnContext(context)
                          ? Colors.grey.shade700
                          : Colors.grey.shade200,
                      // color: customColors.dialogDefaultTextColor?.withAlpha(150),
                      onPressed: () {
                        if (onCancel != null) onCancel();
                        Navigator.pop(context);
                      },
                      // size: const ButtonSize.full(),
                      color: Colors.black26),
                  const SizedBox(height: 16),
                ],
              ),
            ),
          ],
        ),
      );
    },
  ).whenComplete(() {});
}

Center buildBottomSheetTopBar() {
  return Center(
    child: FractionallySizedBox(
      widthFactor: 0.08,
      child: Container(
        margin: const EdgeInsets.only(top: 10, bottom: 12),
        height: 10,
        decoration: BoxDecoration(
          color: Colors.grey.withAlpha(40),
          borderRadius: BorderRadius.circular(6),
        ),
      ),
    ),
  );
}
