import 'dart:collection';
import 'package:flutter/material.dart';

import 'popover.dart';

typedef EntryMap = LinkedHashMap<PopoverDesktopState, OverlayEntryContext>;

class RootOverlayEntry {
  final EntryMap _entries = EntryMap();
  RootOverlayEntry();

  void addEntry(
    BuildContext context,
    PopoverDesktopState newState,
    OverlayEntry entry,
    bool asBarrier,
  ) {
    _entries[newState] = OverlayEntryContext(entry, newState, asBarrier);
    Overlay.of(context).insert(entry);
  }

  bool contains(PopoverDesktopState oldState) {
    return _entries.containsKey(oldState);
  }

  void removeEntry(PopoverDesktopState oldState) {
    if (_entries.isEmpty) return;

    final removedEntry = _entries.remove(oldState);
    removedEntry?.overlayEntry.remove();
  }

  bool get isEmpty => _entries.isEmpty;

  bool get isNotEmpty => _entries.isNotEmpty;

  bool hasEntry() {
    return _entries.isNotEmpty;
  }

  PopoverDesktopState? popEntry() {
    if (_entries.isEmpty) return null;

    final lastEntry = _entries.values.last;
    _entries.remove(lastEntry.popoverState);
    lastEntry.overlayEntry.remove();
    lastEntry.popoverState.widget.onClose?.call();

    if (lastEntry.asBarrier) {
      return lastEntry.popoverState;
    } else {
      return popEntry();
    }
  }
}

class OverlayEntryContext {
  final bool asBarrier;
  final PopoverDesktopState popoverState;
  final OverlayEntry overlayEntry;

  OverlayEntryContext(
    this.overlayEntry,
    this.popoverState,
    this.asBarrier,
  );
}

class PopoverMask extends StatelessWidget {
  final void Function() onTap;
  final Decoration? decoration;

  const PopoverMask({super.key, required this.onTap, this.decoration});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: decoration,
      ),
    );
  }
}
