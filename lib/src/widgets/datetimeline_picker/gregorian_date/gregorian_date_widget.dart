/// ***
/// This class consists of the DateWidget that is used in the ListView.builder
///
/// Author: Vivek Kaushik <me@vivekkasuhik.com>
/// github: https://github.com/iamvivekkaushik/
/// ***

import 'package:karyo/karyo.dart';

import '../gestures/tap.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class GregorianDateWidget extends StatelessWidget {
  final double? width;
  final DateTime date;
  final TextStyle? monthTextStyle, dayTextStyle, dateTextStyle;
  final Color selectionColor;
  final DateSelectionCallback? onDateSelected;
  final String? locale;

  GregorianDateWidget({
    required this.date,
    required this.monthTextStyle,
    required this.dayTextStyle,
    required this.dateTextStyle,
    required this.selectionColor,
    this.width,
    this.onDateSelected,
    this.locale,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      customBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0))),
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(DateFormat("MMM", locale).format(date).toUpperCase(), // Month
                style: _isToday(date)
                    ? monthTextStyle!.copyWith(color: Colors.red)
                    : monthTextStyle!.copyWith(
                        color: PlatformTheme.getMainBkColorSectionReverse(
                            context))),
            Container(
              width: width,
              margin: const EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(8.0)),
                color: _isToday(date) ? Colors.red : selectionColor,
              ),
              child: Center(
                child: Text(date.day.toString(), // Date
                    style: _isToday(date)
                        ? dateTextStyle!.copyWith(color: Colors.white)
                        : dateTextStyle!.copyWith(
                            color: PlatformTheme.getMainBkColorSectionReverse(
                                context))),
              ),
            ),
            Text(DateFormat("E", locale).format(date).toUpperCase(), // WeekDay
                style: _isToday(date)
                    ? dayTextStyle!.copyWith(color: Colors.red)
                    : dayTextStyle!.copyWith(
                        color: PlatformTheme.getMainBkColorSectionReverse(
                            context)))
          ],
        ),
      ),
      onTap: () {
        onDateSelected?.call(this.date);
      },
    );
  }

  bool _isToday(DateTime date) {
    final now = DateTime.now();
    return date.year == now.year &&
        date.month == now.month &&
        date.day == now.day;
  }
}
