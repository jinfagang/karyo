import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';
import 'package:qr_flutter/qr_flutter.dart';

///二维码生成
class QrCodeWidget extends StatelessWidget {
  final String data;
  final double? size;
  final ImageProvider? embeddedImage;
  final Size? embeddedImageSize;

  const QrCodeWidget({
    super.key,
    required this.data,
    this.size,
    this.embeddedImage,
    this.embeddedImageSize,
  });

  @override
  Widget build(BuildContext context) {
    return QrImageView(
      data: data,
      padding: const EdgeInsets.all(1),
      version: QrVersions.auto,
      
      size: size,
      embeddedImage: embeddedImage,
      embeddedImageStyle: QrEmbeddedImageStyle(
        size: embeddedImageSize,
      ),
      // foregroundColor: Theme.of(context).textTheme.titleLarge!.color,
      // backgroundColor: PlatformTheme.getMainBkColorSectionReverse(context),
      // backgroundColor: Colors.white,
      backgroundColor: Colors.transparent,
      eyeStyle: QrEyeStyle(
        color: PlatformTheme.getMainBkColorSectionReverse(context),
        eyeShape: QrEyeShape.square,
      ),
      dataModuleStyle: QrDataModuleStyle(
        color: PlatformTheme.getMainBkColorSectionReverse(context),
        dataModuleShape: QrDataModuleShape.square
      ),
    );
  }
}
