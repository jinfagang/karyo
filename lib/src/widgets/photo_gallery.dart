import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'image_gallery.dart';

enum DisposeLevel { high, medium, low }

const _kRouteDuration = Duration(milliseconds: 300);

class PhotoGalleryWrapper extends StatefulWidget {
  const PhotoGalleryWrapper({
    super.key,
    required this.images,
    required this.onClosePressed,
    this.options = const ImageGalleryOptions(),
    this.pageController,
    this.disposeLevel = DisposeLevel.low,
    this.backgroundColor = Colors.black,
  });

  final Color backgroundColor;

  /// Images to show in the gallery.
  final List<dynamic> images;

  /// Triggered when the gallery is swiped down or closed via the icon.
  final VoidCallback onClosePressed;

  /// Customisation options for the gallery.
  final ImageGalleryOptions options;

  /// Page controller for the image pages.
  final PageController? pageController;
  final DisposeLevel? disposeLevel;

  @override
  State<PhotoGalleryWrapper> createState() => _PhotoGalleryWrapperState();
}

class _PhotoGalleryWrapperState extends State<PhotoGalleryWrapper> {
  // late int currentIndex = widget.initialIndex;

  double? _initialPositionY = 0;
  double? _currentPositionY = 0;
  double _positionYDelta = 0;
  double _opacity = 1;
  double _disposeLimit = 150;
  Duration _animationDuration = Duration.zero;

  late PageController pageController;
  @override
  void initState() {
    super.initState();
    if (widget.pageController == null) {
      pageController = PageController();
    } else {
      pageController = widget.pageController!;
    }
    setDisposeLevel();
  }

  @override
  Widget build(BuildContext context) {
    final horizontalPosition = 0 + max(_positionYDelta, -_positionYDelta) / 15;

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        // color: widget.backgroundColor.withOpacity(_opacity),
        color: Colors.transparent,
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
        child: Stack(
          children: [
            AnimatedPositioned(
              duration: _animationDuration,
              curve: Curves.fastOutSlowIn,
              top: 0 + _positionYDelta,
              bottom: 0 - _positionYDelta,
              left: horizontalPosition,
              right: horizontalPosition,
              child: KeymotionGestureDetector(
                onStart: (details) => _dragStart(details),
                onUpdate: (details) => _dragUpdate(details),
                onEnd: (details) => _dragEnd(details),
                onLongPress: () {
                  debugPrint('kkkkk');
                },
                onTap: () {
                  debugPrint('kkkkk');
                },
                child: GestureDetector(
                  child: PhotoViewGallery.builder(
                    backgroundDecoration: BoxDecoration(
                      color: widget.backgroundColor.withOpacity(_opacity),
                    ),
                    builder: (BuildContext context, int index) =>
                        PhotoViewGalleryPageOptions(
                      imageProvider: CachedNetworkImageProvider(
                        widget.images[index],
                      ),
                      minScale: widget.options.minScale,
                      maxScale: widget.options.maxScale,
                      onTapDown: (context, details, controllerValue) {
                        Navigator.pop(context);
                      },
                      heroAttributes:
                          PhotoViewHeroAttributes(tag: widget.images[index]),
                    ),
                    itemCount: widget.images.length,
                    loadingBuilder: (context, event) =>
                        _imageGalleryLoadingBuilder(event),
                    pageController: pageController,
                    scrollPhysics: const BouncingScrollPhysics(),
                  ),
                ),
              ),
            ),
            Positioned.directional(
              end: 16,
              textDirection: Directionality.of(context),
              top: 56,
              child: CloseButton(
                color: Colors.grey.shade100,
                onPressed: widget.onClosePressed,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void setOpacity() {
    final tmp = _positionYDelta < 0
        ? 1 - ((_positionYDelta / 1000) * -1)
        : 1 - (_positionYDelta / 1000);

    if (tmp > 1) {
      _opacity = 1;
    } else if (tmp < 0) {
      _opacity = 0;
    } else {
      _opacity = tmp * 0.01;
    }

    if (_positionYDelta > _disposeLimit || _positionYDelta < -_disposeLimit) {
      _opacity = 0;
    }
  }

  setDisposeLevel() {
    if (widget.disposeLevel == DisposeLevel.high) {
      _disposeLimit = 300;
    } else if (widget.disposeLevel == DisposeLevel.medium) {
      _disposeLimit = 200;
    } else {
      _disposeLimit = 100;
    }
  }

  void _dragUpdate(DragUpdateDetails details) {
    setState(() {
      _currentPositionY = details.globalPosition.dy;
      _positionYDelta = _currentPositionY! - _initialPositionY!;
      setOpacity();
    });
  }

  void _dragStart(DragStartDetails details) {
    setState(() {
      _initialPositionY = details.globalPosition.dy;
    });
  }

  _dragEnd(DragEndDetails details) {
    if (_positionYDelta > _disposeLimit || _positionYDelta < -_disposeLimit) {
      Navigator.of(context).pop();
    } else {
      setState(() {
        _animationDuration = _kRouteDuration;
        _opacity = 1;
        _positionYDelta = 0;
      });

      Future.delayed(_animationDuration).then((_) {
        setState(() {
          _animationDuration = Duration.zero;
        });
      });
    }
  }

  Widget _imageGalleryLoadingBuilder(ImageChunkEvent? event) => Center(
        child: SizedBox(
          width: 20,
          height: 20,
          child: CircularProgressIndicator(
            value: event == null || event.expectedTotalBytes == null
                ? 0
                : event.cumulativeBytesLoaded / event.expectedTotalBytes!,
          ),
        ),
      );
}

class KeymotionGestureDetector extends StatelessWidget {
  const KeymotionGestureDetector({
    super.key,
    required this.child,
    this.onUpdate,
    this.onEnd,
    this.onStart,
    this.onLongPress,
    this.onTap,
  });

  final Widget child;
  final GestureDragUpdateCallback? onUpdate;
  final GestureDragEndCallback? onEnd;
  final GestureDragStartCallback? onStart;
  final Function()? onLongPress;
  final Function()? onTap;

  @override
  // ignore: prefer_expression_function_bodies
  Widget build(BuildContext context) {
    return RawGestureDetector(
      gestures: <Type, GestureRecognizerFactory>{
        VerticalDragGestureRecognizer:
            GestureRecognizerFactoryWithHandlers<VerticalDragGestureRecognizer>(
          () => VerticalDragGestureRecognizer()
            ..onStart = onStart
            ..onUpdate = onUpdate
            ..onEnd = onEnd,
          (instance) {},
        ),
        // DoubleTapGestureRecognizer: GestureRecognizerFactoryWithHandlers<DoubleTapGestureRecognizer>(
        //   () => DoubleTapGestureRecognizer()..onDoubleTap = onDoubleTap,
        //   (instance) {},
        // ),
        // LongPressGestureRecognizer:
        //     GestureRecognizerFactoryWithHandlers<LongPressGestureRecognizer>(
        //   () => LongPressGestureRecognizer()..onLongPress = onLongPress,
        //   (instance) {},
        // ),
        // TapGestureRecognizer:
        //     GestureRecognizerFactoryWithHandlers<TapGestureRecognizer>(
        //   () => TapGestureRecognizer()..onTap = onTap,
        //   (instance) {},
        // ),
      },
      child: child,
    );
  }
}
