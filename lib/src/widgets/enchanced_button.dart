import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class EnhancedButton extends StatelessWidget {
  final String title;
  final Color? backgroundColor;
  final Color? color;
  final double? height;
  final double? width;
  final double? fontSize;
  final bool enableGradient;
  final Widget? icon;
  final BorderRadius? radius;
  final Function() onPressed;

  const EnhancedButton({
    super.key,
    required this.title,
    this.backgroundColor,
    this.color,
    this.height,
    this.width,
    this.fontSize,
    required this.onPressed,
    this.enableGradient = false,
    this.radius,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    const r = 20.0;
    return Material(
      color: Colors.transparent,
      child: InkWell(
          // borderRadius: radius ?? BorderRadius.circular(10),
          borderRadius: radius ?? BorderRadius.circular(r),
          onTap: onPressed,
          child: Ink(
            decoration: BoxDecoration(
              gradient: backgroundColor != null && backgroundColor!.alpha == 0
                  ? null
                  : LinearGradient(
                      colors: getGradientColors(
                          backgroundColor ?? tClrTertiaryContainer(context),
                          delta: 10,
                          alpha: 160)),
              borderRadius: radius ?? BorderRadius.circular(r),
            ),
            child: SizedBox(
              height: height ?? 44,
              width: width ?? double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  if (icon != null) icon!,
                  if (icon != null && title != "") const SizedBox(width: 5),
                  if (title != "")
                    Text(title,
                        textAlign: TextAlign.center,
                        style: NORMAL_TXT_STYLE.copyWith(
                            fontWeight: FontWeight.bold,
                            fontSize: fontSize,
                            color: color ?? Colors.white)),
                ],
              ),
            ),
          )),
    );
  }
}

class EnhancedButtonComplex extends StatelessWidget {
  final String title;
  final Function() onPressed;
  final Function()? onLongPressed;
  final ButtonSize size;
  final Widget? icon;
  final Color? backgroundColor;
  final Color? color;
  final BorderRadius? radius;
  final AxisDirection? direction;
  final EdgeInsets? padding;
  final bool isLight;
  final bool isPureBackground;
  final TextStyle? textStyle;
  final double gradientDelta;

  const EnhancedButtonComplex(
      {super.key,
      required this.title,
      required this.onPressed,
      this.onLongPressed,
      this.size = const ButtonSize.small(),
      this.backgroundColor,
      this.color,
      this.icon,
      this.radius,
      this.padding,
      this.isLight = true,
      this.isPureBackground = false,
      this.textStyle,
      this.gradientDelta = 20,
      this.direction = AxisDirection.right});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
          borderRadius: radius ?? BorderRadius.circular(10),
          onTap: onPressed,
          onLongPress: onLongPressed,
          child: Ink(
              padding: EdgeInsets.zero,
              decoration: BoxDecoration(
                color: isPureBackground
                    ? (backgroundColor ?? Colors.green).withAlpha(40)
                    : null,
                gradient: isPureBackground
                    ? (isLight
                        ? LinearGradient(
                            colors: getGradientColors(
                                lighten(
                                  backgroundColor ?? Colors.green,
                                ).withAlpha(50),
                                delta: gradientDelta))
                        : LinearGradient(
                            colors: getGradientColors(
                                lighten(
                                  backgroundColor ?? Colors.green,
                                ),
                                delta: gradientDelta)))
                    : null,
                borderRadius: radius ?? BorderRadius.circular(10),
              ),
              child: direction == AxisDirection.right
                  ? Padding(
                      padding: padding ??
                          const EdgeInsets.symmetric(
                              vertical: 24, horizontal: 20),
                      child: Row(
                        children: [
                          icon ?? Container(),
                          g16,
                          Text(title,
                              textAlign: TextAlign.center,
                              style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                  color: isLight
                                      ? backgroundColor
                                      : color ?? Colors.white)),
                        ],
                      ),
                    )
                  : direction == AxisDirection.left
                      ? Padding(
                          padding: padding ??
                              const EdgeInsets.symmetric(
                                  vertical: 24, horizontal: 20),
                          child: Row(
                            children: [
                              g8,
                              Text(title,
                                  textAlign: TextAlign.center,
                                  style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                      color: isLight
                                          ? backgroundColor
                                          : color ?? Colors.white)),
                              g8,
                              icon ?? Container(),
                            ],
                          ),
                        )
                      : Padding(
                          padding: padding ??
                              const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              icon ?? Container(),
                              if (title != "") g4,
                              if (title != "")
                                Text(title,
                                    textAlign: TextAlign.center,
                                    style: textStyle ??
                                        NORMAL_BOLD_TXT_STYLE.copyWith(
                                            color: color ?? Colors.white)),
                            ],
                          ),
                        ))),
    );
  }
}

class ButtonSize {
  final double width;
  final double height;
  final double fontSize;

  const ButtonSize({
    required this.width,
    required this.height,
    required this.fontSize,
  });

  const ButtonSize.full()
      : width = double.infinity,
        height = 42,
        fontSize = 16;

  const ButtonSize.small()
      : width = 80,
        height = 35,
        fontSize = 14;
}

ButtonStyle getZeroBtnStyle({
  Color foregroundColor = Colors.deepOrange,
  EdgeInsets? padding,
  Color? backgroundColor,
  double? radius = 10,
}) {
  return TextButton.styleFrom(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(radius!),
      ),
      minimumSize: Size.zero,
      padding: padding ?? EdgeInsets.zero,
      foregroundColor: foregroundColor.withAlpha(150),
      backgroundColor: backgroundColor,
      iconColor: foregroundColor,
      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
      alignment: Alignment.centerLeft);
}
