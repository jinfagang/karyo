import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:karyo/karyo.dart';

class GradientScaffold extends StatefulWidget {
  final PreferredSizeWidget? appBar;
  final Widget child;
  final Widget? floatingActionButton;
  final bool isGradient;
  final bool isBlur;
  final bool animateBackground;
  final Color? backgroundColor;
  final Gradient? gradient;
  final BottomNavigationBar? bottomNavigationBar;
  final FloatingActionButtonLocation? floatingActionButtonLocation;
  final bool? extendBody;
  const GradientScaffold(
      {super.key,
      required this.child,
      this.appBar,
      this.floatingActionButton,
      this.animateBackground = false,
      this.backgroundColor,
      this.gradient,
      this.isBlur = false,
      this.extendBody,
      this.bottomNavigationBar,
      this.floatingActionButtonLocation,
      this.isGradient = true});

  @override
  State<GradientScaffold> createState() => _GradientScaffoldState();
}

class _GradientScaffoldState extends State<GradientScaffold> {
  Color _getDefaultColor() {
    return isOnWindows()
        ? Colors.transparent
        : PlatformTheme.getMainBkColor(context);
  }

  Widget _buildMain() {
    return Scaffold(
        appBar: widget.appBar,
        backgroundColor: widget.backgroundColor ?? _getDefaultColor(),
        extendBodyBehindAppBar: true,
        extendBody: widget.extendBody ?? true,
        floatingActionButtonLocation: widget.floatingActionButtonLocation,
        floatingActionButton: widget.floatingActionButton,
        bottomNavigationBar: widget.bottomNavigationBar,
        body: widget.isBlur
            ? Stack(
                children: [
                  ClipRect(
                    child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaX: 22, sigmaY: 22),
                        child: _buildGradientContainer(null)),
                  ),
                  widget.child,
                ],
              )
            : _buildGradientContainer(widget.child));
  }

  Widget _buildGradientContainer(Widget? child) {
    return Container(
      padding: EdgeInsets.only(
          top: isOnMobile()
              ? MediaQueryData.fromView(View.of(context)).padding.top +
                  kToolbarHeight
              : 40),
      decoration: BoxDecoration(
          color: isOnMobile()
              ? (isDarkModeOnContext(context) ? PlatformTheme.subBlack : null)
              : Colors.transparent,
          gradient: isDarkModeOnContext(context) ||
                  !isOnMobile() ||
                  !widget.isGradient
              ? null
              : widget.gradient ??
                  LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [
                        isOnMobile() ? 0.18 : 0.25,
                        isOnMobile() ? 0.58 : 0.45,
                        0.7,
                      ],
                      colors: [
                        Color.fromARGB(255, 234, 239, 249),
                        Color.fromARGB(155, 234, 239, 249),
                        // Color.fromARGB(255, 255, 244, 243),
                        Colors.white,
                      ])),
      child: child ??
          const SizedBox(
            height: double.infinity,
            width: double.infinity,
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (widget.animateBackground) {
      return BackgroundShapes(child: _buildMain());
    }
    return _buildMain();
  }
}

class BackgroundShapes extends StatefulWidget {
  const BackgroundShapes({
    super.key,
    required this.child,
  });

  final Widget child;

  @override
  State<BackgroundShapes> createState() => _BackgroundShapesState();
}

class _BackgroundShapesState extends State<BackgroundShapes>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 10),
    );
    _animation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_controller);
    _controller.repeat(reverse: true);
    super.initState();
  }

  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  Widget build(BuildContext context) {
    return Material(
      // color: Colors.blue,
      child: Stack(
        children: [
          AnimatedBuilder(
            animation: _animation,
            builder: (context, child) {
              return CustomPaint(
                painter: BackgroundPainter(_animation),
                child: Container(),
              );
            },
          ),
          // BackdropFilter(
          //   filter: ImageFilter.blur(sigmaX: 60, sigmaY: 60),
          //   child: Container(
          //     color: Colors.black.withOpacity(0.1),
          //   ),
          // ),
          widget.child,
        ],
      ),
    );
  }

  @override
  void dispose() {
    _controller.removeStatusListener((status) {});
    _controller.dispose();
    super.dispose();
  }
}

class BackgroundPainter extends CustomPainter {
  final Animation<double> animation;

  const BackgroundPainter(this.animation);

  Offset getOffset(Path path) {
    final pms = path.computeMetrics(forceClosed: false).elementAt(0);
    final length = pms.length;
    final offset = pms.getTangentForOffset(length * animation.value)!.position;
    return offset;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.maskFilter = const MaskFilter.blur(
      BlurStyle.normal,
      90,
    );
    drawShape1(canvas, size, paint, Colors.orange);
    drawShape2(canvas, size, paint, Colors.purple.withAlpha(210));
    drawShape3(canvas, size, paint, Colors.blue);
    drawShape4(canvas, size, paint, Colors.deepOrange);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return oldDelegate != this;
  }

  void drawShape1(
    Canvas canvas,
    Size size,
    Paint paint,
    Color color,
  ) {
    paint.color = color;
    Path path = Path();

    path.moveTo(size.width, 0);
    path.quadraticBezierTo(
      size.width / 2,
      size.height / 2,
      -100,
      size.height / 4,
    );

    final offset = getOffset(path);
    canvas.drawCircle(offset, 150, paint);
  }

  void drawShape2(
    Canvas canvas,
    Size size,
    Paint paint,
    Color color,
  ) {
    paint.color = color;
    Path path = Path();

    path.moveTo(size.width, size.height);
    path.quadraticBezierTo(
      size.width / 2,
      size.height / 2,
      size.width * 0.9,
      size.height * 0.9,
    );

    final offset = getOffset(path);
    canvas.drawCircle(offset, 250, paint);
  }

  void drawShape3(
    Canvas canvas,
    Size size,
    Paint paint,
    Color color,
  ) {
    paint.color = color;
    Path path = Path();

    path.moveTo(0, 0);
    path.quadraticBezierTo(
      0,
      size.height,
      size.width / 3,
      size.height / 3,
    );

    final offset = getOffset(path);
    canvas.drawCircle(offset, 250, paint);
  }

  void drawShape4(
    Canvas canvas,
    Size size,
    Paint paint,
    Color color,
  ) {
    paint.color = color;
    Path path = Path();

    path.moveTo(0, size.height / 2);
    path.quadraticBezierTo(
      0,
      size.height,
      size.width / 3,
      size.height / 3,
    );

    final offset = getOffset(path);
    canvas.drawCircle(offset, 250, paint);
  }
}

class MyCupertinoScaffold extends StatefulWidget {
  const MyCupertinoScaffold(
      {super.key,
      required this.child,
      this.mainColor,
      this.appBar,
      this.previousTitle,
      this.title});
  final Widget child;
  final Color? mainColor;
  final String? previousTitle;
  final String? title;
  final ObstructingPreferredSizeWidget? appBar;

  @override
  State<MyCupertinoScaffold> createState() => _MyCupertinoScaffoldState();
}

class _MyCupertinoScaffoldState extends State<MyCupertinoScaffold> {
  @override
  Widget build(BuildContext context) {
    return CupertinoTheme(
        data: CupertinoThemeData(
            scaffoldBackgroundColor:
                // PlatformTheme.getMainBkColorSection(context),
                PlatformTheme.getMainBkColor(context),
            // barBackgroundColor: PlatformTheme.getMainBkColorSection(context),
            barBackgroundColor: PlatformTheme.getMainBkColor(context),
            primaryColor: widget.mainColor ?? Colors.amber),
        child: CupertinoPageScaffold(
          // backgroundColor: PlatformTheme.getMainBkColor(context),
          // backgroundColor: Colors.red,
          navigationBar: widget.appBar ??
              CupertinoNavigationBar(
                border: const Border(),
                previousPageTitle: widget.previousTitle ?? '',
                middle: Text(
                  widget.title ?? '',
                  style: NORMAL_BOLD_TXT_STYLE.copyWith(
                      color: widget.mainColor ?? Colors.amber),
                ),
                trailing: isOnMobile()
                    ? null
                    : Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          buildDesktopCrossBtn(context, null),
                        ],
                      ),
              ),
          child: widget.child,
        ));
  }
}

class MyLargeTitleCupertinoScaffold extends StatefulWidget {
  const MyLargeTitleCupertinoScaffold({
    super.key,
    required this.child,
    this.mainColor,
  });
  final Widget child;
  final Color? mainColor;

  @override
  State<MyLargeTitleCupertinoScaffold> createState() =>
      _MyLargeTitleCupertinoScaffoldState();
}

class _MyLargeTitleCupertinoScaffoldState
    extends State<MyLargeTitleCupertinoScaffold> {
  @override
  Widget build(BuildContext context) {
    return CupertinoTheme(
        data: CupertinoThemeData(
            scaffoldBackgroundColor: PlatformTheme.getMainBkColor(context),
            barBackgroundColor: PlatformTheme.getMainBkColor(context),
            primaryColor: widget.mainColor ?? Colors.amber),
        child: CupertinoPageScaffold(
          child: widget.child,
        ));
  }
}

class CustomSliverPersistentHeaderDelegate
    extends SliverPersistentHeaderDelegate {
  final double minHeight;
  final double maxHeight;
  final Widget child;

  CustomSliverPersistentHeaderDelegate({
    required this.minHeight,
    required this.maxHeight,
    required this.child,
  });

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => maxHeight;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}

class MyScaffold extends StatefulWidget {
  const MyScaffold({
    super.key,
    required this.body,
    this.backgroundColor,
    this.appBar,
    this.floatingActionButton,
  });
  final Widget body;
  final Color? backgroundColor;
  final PreferredSizeWidget? appBar;
  final FloatingActionButton? floatingActionButton;

  @override
  State<MyScaffold> createState() => _MyScaffoldState();
}

class _MyScaffoldState extends State<MyScaffold> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: widget.backgroundColor ??
          PlatformTheme.getMainBkColorSection(context),
      appBar: widget.appBar ??
          AppBar(
            automaticallyImplyLeading: false,
            leading: buildBackButton3(context),
            backgroundColor: Colors.transparent,
            surfaceTintColor: Colors.transparent,
          ),
      body: widget.body,
      floatingActionButton: widget.floatingActionButton,
    );
  }
}
