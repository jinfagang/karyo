import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class GradientBox extends StatelessWidget {
  final Widget child;
  final Color? backgroundColor;
  final Color? color;
  final double? height;
  final double? width;
  final double? fontSize;
  final bool enableGradient;
  final Widget? icon;
  final BorderRadius? radius;
  final Function() onPressed;
  final Function()? onLongPressed;

  const GradientBox({
    super.key,
    required this.child,
    this.backgroundColor,
    this.color,
    this.height,
    this.width,
    this.fontSize,
    required this.onPressed,
    this.onLongPressed,
    this.enableGradient = false,
    this.radius,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
          borderRadius: radius ?? BorderRadius.circular(10),
          onTap: onPressed,
          onLongPress: onLongPressed,
          child: Ink(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: getGradientColors(backgroundColor ?? Colors.green,
                        delta: 18)),
                borderRadius: radius ?? BorderRadius.circular(20),
              ),
              child: SizedBox(
                height: height,
                width: width,
                child: child,
              ))),
    );
  }
}
