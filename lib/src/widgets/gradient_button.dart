import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class MyElevatedButton extends StatelessWidget {
  final BorderRadiusGeometry? borderRadius;
  final double? width;
  final double height;
  final Gradient? gradient;
  final VoidCallback? onPressed;
  final Widget child;
  final ButtonStyle? style;
  final bool isShadow;

  const MyElevatedButton(
      {Key? key,
      required this.onPressed,
      required this.child,
      this.borderRadius,
      this.width,
      this.height = 44.0,
      this.isShadow = false,
      this.gradient,
      this.style})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final borderRadius = this.borderRadius ?? BorderRadius.circular(12);
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        gradient: gradient,
        color: gradient == null ? tClrPrimaryContainer(context) : null,
        borderRadius: borderRadius,
        boxShadow: isShadow
            ? [
                BoxShadow(
                  offset: Offset(0, 1),
                  blurRadius: 1,
                  spreadRadius: 1,
                  color: Colors.grey.withAlpha(30),
                ),
              ]
            : null,
      ),
      child: ElevatedButton(
        onPressed: onPressed,
        style: style ??
            ElevatedButton.styleFrom(
              backgroundColor: Colors.transparent,
              shadowColor: Colors.transparent,
              shape: RoundedRectangleBorder(borderRadius: borderRadius),
            ),
        child: child,
      ),
    );
  }
}
