import 'package:flutter/material.dart';
import 'package:flutter_emoji_mart/flutter_emoji_mart.dart';
import 'package:karyo/karyo.dart';

class FlowyEmojiHeader extends StatelessWidget {
  const FlowyEmojiHeader({
    super.key,
    required this.category,
  });

  final Category category;

  @override
  Widget build(BuildContext context) {
    if (!isOnMobile()) {
      return Container(
        height: 22,
        // color: Theme.of(context).cardColor,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 4.0),
          child: Text(
            category.id,
            style: NORMAL_S_BOLD_TXT_STYLE.copyWith(
              color: Theme.of(context).hintColor,
            ),
          ),
        ),
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 40,
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            color: Theme.of(context).cardColor,
            child: Padding(
              padding: const EdgeInsets.only(
                top: 14.0,
                bottom: 4.0,
              ),
              child: Text(category.id),
            ),
          ),
          const Divider(
            height: 1,
            thickness: 1,
          ),
        ],
      );
    }
  }
}
