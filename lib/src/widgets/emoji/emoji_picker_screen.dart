
import 'package:flutter/material.dart';


class MobileEmojiPickerScreen extends StatelessWidget {
  const MobileEmojiPickerScreen({super.key, this.title});

  final String? title;

  static const routeName = '/emoji_picker';
  static const pageTitle = 'title';

  @override
  Widget build(BuildContext context) {
    return IconPickerPage(
      title: title,
      onSelected: (result) {
        context.pop<EmojiPickerResult>(result);
      },
    );
  }
}
