import 'package:custom_sliding_segmented_control/custom_sliding_segmented_control.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class MySegmentBtn extends StatelessWidget {
  final Map<int, Widget> children;
  final int initialValue;
  final Function(int)? onValueChanged;
  const MySegmentBtn(
      {super.key,
      required this.children,
      this.initialValue = 0,
      this.onValueChanged});

  @override
  Widget build(BuildContext context) {
    return CustomSlidingSegmentedControl<int>(
      customSegmentSettings: CustomSegmentSettings(
          hoverColor: Colors.transparent, splashColor: Colors.transparent),
      initialValue: initialValue,
      children: children,
      decoration: BoxDecoration(
        color: isDarkModeOnContext(context)
            ? Colors.black.withAlpha(40)
            : Colors.grey.withAlpha(10),
        borderRadius: BorderRadius.circular(12),
      ),
      thumbDecoration: BoxDecoration(
        color: PlatformTheme.getMainBkColorSection5(context),
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withAlpha(20),
            blurRadius: 4.0,
            spreadRadius: 1.0,
            offset: const Offset(
              0.0,
              2.0,
            ),
          ),
        ],
      ),
      duration: const Duration(milliseconds: 100),
      curve: Curves.decelerate,
      onValueChanged: onValueChanged!,
    );
  }
}
