import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';
import 'package:settings_ui/settings_ui.dart';

class MySettingsList extends StatelessWidget {
  final EdgeInsets? contentPadding;
  final ScrollPhysics? physics;
  final bool? shrinkWrap;
  final List<AbstractSettingsSection> sections;
  const MySettingsList(
      {super.key,
      this.contentPadding,
      this.physics,
      this.shrinkWrap,
      required this.sections});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: SettingsList(
          physics: physics,
          shrinkWrap: shrinkWrap ?? false,
          contentPadding: contentPadding,
          applicationType: ApplicationType.cupertino,
          brightness:
              isDarkModeOnContext(context) ? Brightness.dark : Brightness.light,
          //   brightness: Brightness.dark,
          lightTheme: SettingsThemeData(
              dividerColor: Colors.grey.withAlpha(20),
              settingsSectionBackground:
                  isOnMobile() ? Colors.white : Colors.transparent,
              settingsListBackground:
                  isOnMobile() ? Colors.white : Colors.transparent),
          darkTheme: SettingsThemeData(
              dividerColor: Colors.grey.withAlpha(20),
              settingsSectionBackground:
                  isOnMobile() ? null : Colors.transparent,
              settingsListBackground: isOnMobile()
                  ? PlatformTheme.getMainBkColor(context)
                  : Colors.transparent),
          platform: DevicePlatform.iOS,
          sections: sections),
    );
  }
}
