import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class ProgressOverviewWidget extends StatelessWidget {
  const ProgressOverviewWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final now = DateTime.now();

    // Calculate year progress.
    final startOfYear = DateTime(now.year, 1, 1);
    final endOfYear = DateTime(now.year + 1, 1, 1);
    final yearProgress = now.difference(startOfYear).inSeconds /
        endOfYear.difference(startOfYear).inSeconds;

    // Calculate month progress.
    final startOfMonth = DateTime(now.year, now.month, 1);
    final endOfMonth = (now.month < 12)
        ? DateTime(now.year, now.month + 1, 1)
        : DateTime(now.year + 1, 1, 1);
    final monthProgress = now.difference(startOfMonth).inSeconds /
        endOfMonth.difference(startOfMonth).inSeconds;

    // Calculate week progress (assuming week starts on Monday).
    final startOfDay = DateTime(now.year, now.month, now.day);
    final startOfWeek =
        startOfDay.subtract(Duration(days: now.weekday - 1)); // Monday
    final endOfWeek = startOfWeek.add(const Duration(days: 7));
    final weekProgress = now.difference(startOfWeek).inSeconds /
        endOfWeek.difference(startOfWeek).inSeconds;

    // Calculate day progress.
    final endOfDay = startOfDay.add(const Duration(days: 1));
    final dayProgress = now.difference(startOfDay).inSeconds /
        endOfDay.difference(startOfDay).inSeconds;

    return Container(
      margin: const EdgeInsets.all(16),
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
          color: PlatformTheme.getMainBkColorSection4(context),
          borderRadius: BorderRadius.circular(12),
          // A subtle shadow for a modern look.
          boxShadow: getBoxShadow(context)),
      child: Row(
        children: [
          // Left part: wave animation for year progress.
          TweenAnimationBuilder<double>(
              tween: Tween<double>(begin: 0, end: 1 - yearProgress),
              duration: const Duration(seconds: 1),
              builder: (context, value, child) {
                return getProgressBarWave(
                    progress: value,
                    color: yearProgress > 0.8 ? Colors.red : Colors.lightGreen);
              }),
          g32,
          g8,
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AnimatedProgressBarRow(
                  progress: monthProgress,
                  label: "本月已过",
                ),
                g16,
                AnimatedProgressBarRow(
                  progress: weekProgress,
                  label: "本周已过",
                ),
                g16,
                AnimatedProgressBarRow(
                  progress: dayProgress,
                  label: "今天已过",
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class AnimatedProgressBarRow extends StatelessWidget {
  final double progress;
  final String label;

  const AnimatedProgressBarRow({
    Key? key,
    required this.progress,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TweenAnimationBuilder<double>(
      tween: Tween<double>(begin: 0, end: progress),
      duration: const Duration(seconds: 1),
      builder: (context, value, child) {
        final percentageText = (value * 100).toStringAsFixed(1) + "%";

        return Row(
          children: [
            // Progress bar.
            Expanded(
              child: Container(
                height: 6,
                decoration: BoxDecoration(
                  color: Colors.grey.withAlpha(50),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: FractionallySizedBox(
                  alignment: Alignment.centerLeft,
                  widthFactor: value,
                  child: Container(
                    decoration: BoxDecoration(
                      color: tClrPrimary(context),
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(width: 8),
            // Label.
            Text(
              "$label $percentageText",
              style: SMALL_TXT_STYLE.copyWith(
                  color: tClrPrimary(context), fontFamily: getFontMonospace()),
            ),
          ],
        );
      },
    );
  }
}
