import 'package:flutter/material.dart';

class SplitColumnLayout extends StatefulWidget {
  final Widget leftChild;
  final Widget rightChild;
  final double
      initialLeftRatio; // Initial ratio for the left child (0.0 to 1.0)
  final double minLeftRatio; // Minimum ratio for the left child
  final double minRightRatio; // Minimum ratio for the right child

  const SplitColumnLayout({
    Key? key,
    required this.leftChild,
    required this.rightChild,
    this.initialLeftRatio = 0.5,
    this.minLeftRatio = 0.1,
    this.minRightRatio = 0.1,
  })  : assert(initialLeftRatio >= 0.0 && initialLeftRatio <= 1.0),
        assert(minLeftRatio >= 0.0 && minLeftRatio <= 1.0),
        assert(minRightRatio >= 0.0 && minRightRatio <= 1.0),
        super(key: key);

  @override
  _SplitColumnLayoutState createState() => _SplitColumnLayoutState();
}

class _SplitColumnLayoutState extends State<SplitColumnLayout> {
  late double _leftWidthRatio;
  bool _isHovered = false; // 是否鼠标悬停
  bool _isVisible = false; // 分隔条是否可见

  @override
  void initState() {
    super.initState();
    _leftWidthRatio = widget.initialLeftRatio;
  }

  void _onEnter(PointerEvent details) {
    setState(() {
      _isHovered = true;
      _isVisible = true; // 鼠标进入时立即显示
    });
  }

  void _onExit(PointerEvent details) {
    setState(() {
      _isHovered = false;
    });
    // 延迟 1 秒后隐藏
    Future.delayed(Duration(seconds: 1), () {
      if (!_isHovered) {
        setState(() {
          _isVisible = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Row(
          children: [
            // 左侧内容
            Expanded(
              flex: (_leftWidthRatio * 100).toInt(),
              child: Container(
                constraints: BoxConstraints(
                  minWidth: constraints.maxWidth * widget.minLeftRatio,
                ),
                child: widget.leftChild,
              ),
            ),
            // 分隔条
            MouseRegion(
              onEnter: _onEnter,
              onExit: _onExit,
              child: GestureDetector(
                onPanUpdate: (details) {
                  setState(() {
                    final newRatio = _leftWidthRatio +
                        details.delta.dx / constraints.maxWidth;
                    _leftWidthRatio = newRatio.clamp(
                      widget.minLeftRatio,
                      1.0 - widget.minRightRatio,
                    );
                  });
                },
                child: AnimatedOpacity(
                  opacity: _isVisible ? 0.6 : 0.1, // 透明度动画
                  duration: const Duration(milliseconds: 300),
                  child: SizedBox(
                    width: 3,
                    child: Container(
                      width: _isVisible ? 3 : 1, // 宽度动态变化
                      color: Colors.grey.withAlpha(100),
                    ),
                  ),
                ),
              ),
            ),
            // 右侧内容
            Expanded(
              flex: ((1 - _leftWidthRatio) * 100).toInt(),
              child: Container(
                constraints: BoxConstraints(
                  minWidth: constraints.maxWidth * widget.minRightRatio,
                ),
                child: widget.rightChild,
              ),
            ),
          ],
        );
      },
    );
  }
}
