import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

import 'package:soft_edge_blur/soft_edge_blur.dart';

class GradientBlurNavBar extends StatelessWidget {
  final Widget child; // 子组件，支持传入任何Widget
  final double? blurIntensity; // 模糊强度，默认10
  final double? height; // 模糊强度，默认10
  final double? heightFactor; // 控制模糊高度分布，默认0.4
  final Alignment? alignment;

  // 构造函数
  const GradientBlurNavBar({
    Key? key,
    required this.child,
    this.blurIntensity = 24.0,
    this.height = 200,
    this.heightFactor = 1.0,
    this.alignment = Alignment.center,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double navbarHeight = height ?? 89;
    final double blurHeight = navbarHeight * (heightFactor ?? 1.0);

    return Stack(
      children: [
        // 多层模糊效果
        ClipRect(
          child: Stack(
            children: [
              // 强模糊层
              // BackdropFilter(
              //   filter: ImageFilter.blur(
              //       sigmaX: blurIntensity!, sigmaY: blurIntensity!),
              //   child: Container(
              //     height: blurHeight * 0.4,
              //     color: PlatformTheme.getMainBkColor(context).withOpacity(0.5),
              //   ),
              // ),
              // // 中等模糊层
              // Positioned(
              //   top: blurHeight * 0.4,
              //   child: BackdropFilter(
              //     filter: ImageFilter.blur(
              //         sigmaX: blurIntensity! - 5, sigmaY: blurIntensity! - 5),
              //     child: Container(
              //       height: blurHeight * 0.3,
              //       width: MediaQuery.of(context).size.width,
              //       color:
              //           PlatformTheme.getMainBkColor(context).withOpacity(0.5),
              //     ),
              //   ),
              // ),
              // // 轻微模糊层
              BackdropFilter(
                filter: ImageFilter.blur(
                    sigmaX: blurIntensity! - 8, sigmaY: blurIntensity! - 8),
                child: Container(
                  height: blurHeight,
                  width: MediaQuery.of(context).size.width,
                  color: PlatformTheme.getMainBkColor(context).withOpacity(0.4),
                ),
              ),
            ],
          ),
        ),

        // 渐变透明遮罩层
        // Positioned.fill(
        //   child: Container(
        //     height: navbarHeight,
        //     decoration: BoxDecoration(
        //       gradient: LinearGradient(
        //         begin: Alignment.topCenter,
        //         end: Alignment.bottomCenter,
        //         colors: [
        //           PlatformTheme.getMainBkColor(context).withOpacity(0.6),
        //           PlatformTheme.getMainBkColor(context).withOpacity(0.4),
        //           PlatformTheme.getMainBkColor(context).withOpacity(0.2),
        //           PlatformTheme.getMainBkColor(context).withOpacity(0.1),
        //           PlatformTheme.getMainBkColor(context).withOpacity(0.06),
        //           PlatformTheme.getMainBkColor(context).withOpacity(0.02),
        //           Colors.transparent,
        //         ],
        //         stops: [0.0, 0.4, 0.7, 0.86, 0.9, 0.97, 1.0],
        //       ),
        //     ),
        //   ),
        // ),

        Positioned.fill(
            child: Align(
          alignment: alignment!,
          child: child,
        )),
      ],
    );
  }
}

enum EdgeOrientation {
  top,
  bottom,
  left,
  right,
}

class SoftEdgeBlurWidget extends StatelessWidget {
  final Widget child;
  final EdgeOrientation orientation;
  final Color? tintColor;
  final double? size;

  const SoftEdgeBlurWidget({
    Key? key,
    required this.child,
    this.orientation = EdgeOrientation.top,
    this.tintColor,
    this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SoftEdgeBlur(
      edges: [
        EdgeBlur(
          type: getEdgeType(),
          size: size ?? 180,
          sigma: 40,
          tintColor: tintColor ??
              PlatformTheme.getMainBkColor(context).withOpacity(0.8),
          controlPoints: [
            ControlPoint(
              position: 0.3,
              type: ControlPointType.visible,
            ),
            ControlPoint(
              position: 0.5,
              type: ControlPointType.visible,
            ),
            ControlPoint(
              position: 1.0,
              type: ControlPointType.transparent,
            ),
          ],
        ),
      ],
      child: child,
    );
  }

  EdgeType getEdgeType() {
    switch (orientation) {
      case EdgeOrientation.top:
        return EdgeType.topEdge;
      case EdgeOrientation.bottom:
        return EdgeType.bottomEdge;
      case EdgeOrientation.left:
        return EdgeType.leftEdge;
      case EdgeOrientation.right:
        return EdgeType.rightEdge;
    }
  }
}
