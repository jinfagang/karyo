import 'dart:io';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class CupertinoPickerButton extends StatefulWidget {
  final bool isDate; // Set true for date picker, false for time picker
  final Function(DateTime)? onSelect;
  final ValueNotifier<DateTime>? selectedDateTime;

  const CupertinoPickerButton(
      {super.key, required this.isDate, this.onSelect, this.selectedDateTime});

  @override
  _CupertinoPickerButtonState createState() => _CupertinoPickerButtonState();
}

class _CupertinoPickerButtonState extends State<CupertinoPickerButton> {
  DateTime selectedDateTime = DateTime.now();
  bool isPickerVisible = false;

  @override
  void initState() {
    super.initState();
    if (widget.selectedDateTime != null) {
      widget.selectedDateTime!.addListener(_updateDisplay);
    }
  }

  @override
  void dispose() {
    if (widget.selectedDateTime != null) {
      widget.selectedDateTime!.removeListener(_updateDisplay);
    }
    super.dispose();
  }

  void _updateDisplay() {
    setState(() {
      selectedDateTime = widget.selectedDateTime!.value;
    });
  }

  void _showPicker(BuildContext context) {
    setState(() {
      isPickerVisible = true;
    });

    showCupertinoModalPopup(
      filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
      context: context,
      builder: (_) => SizedBox(
        height: 300,
        child: Container(
          color: PlatformTheme.getMainBkColor(context),
          child: Column(
            children: [
              SizedBox(
                  height: 200,
                  child: CupertinoDatePicker(
                    initialDateTime: selectedDateTime,
                    mode: widget.isDate
                        ? CupertinoDatePickerMode.date
                        : CupertinoDatePickerMode.time,
                    use24hFormat: true,
                    showDayOfWeek: true,
                    onDateTimeChanged: (dateTime) {
                      setState(() {
                        selectedDateTime = dateTime;
                      });

                      if (widget.onSelect != null) {
                        widget.onSelect!(selectedDateTime);
                      }
                    },
                  )),
              CupertinoButton(
                child: const Text('Done'),
                onPressed: () {
                  setState(() {
                    isPickerVisible = false;
                  });
                  Navigator.pop(context);
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BoxBlockSim(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4.0),
      backgroundColor: isDarkModeOnContext(context)
          ? PlatformTheme.getMainBkColorSection5(context)
          : Colors.grey.withAlpha(10),
      borderRadius: 8,
      border: Border.all(color: Colors.grey.withAlpha(10), width: 1),
      child: Text(
        widget.isDate
            ? '${selectedDateTime.year == DateTime.now().year ? "" : selectedDateTime.year} ${selectedDateTime.month.toString().padLeft(2, '0')}月${selectedDateTime.day.toString().padLeft(2, '0')}号 ${getWeekDayStr(selectedDateTime.millisecondsSinceEpoch)}'
            : '${selectedDateTime.hour.toString().padLeft(2, '0')}:${selectedDateTime.minute.toString().padLeft(2, '0')}',
        style: TextStyle(
            color: isPickerVisible
                ? Colors.red
                : PlatformTheme.getMainBkColorSectionReverse(context),
            fontSize: 14),
      ),
      onTap: () {
        _showPicker(context);
      },
    );
  }
}
