import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:karyo/karyo.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class CustomCupertinoDateTimePicker extends StatefulWidget {
  final DateTime? initialDateTime;
  final Function(DateTime selectedDateTime)? onDateTimeSelected;

  const CustomCupertinoDateTimePicker(
      {Key? key, this.initialDateTime, this.onDateTimeSelected})
      : super(key: key);

  @override
  _CustomCupertinoDateTimePickerState createState() =>
      _CustomCupertinoDateTimePickerState();
}

class _CustomCupertinoDateTimePickerState
    extends State<CustomCupertinoDateTimePicker> {
  late ValueNotifier<DateTime> selectedDate;
  late ValueNotifier<DateTime> selectedTime;

  @override
  void initState() {
    super.initState();
    final now = DateTime.now();
    selectedDate = ValueNotifier<DateTime>(widget.initialDateTime ?? now);
    selectedTime = ValueNotifier<DateTime>(widget.initialDateTime ?? now);
  }

  void _notifySelection() {
    final selectedDateTime = DateTime(
      selectedDate.value.year,
      selectedDate.value.month,
      selectedDate.value.day,
      selectedTime.value.hour,
      selectedTime.value.minute,
    );
    widget.onDateTimeSelected?.call(selectedDateTime); // 调用回调
  }

  void _updateDate(DateTime newDate) {
    selectedDate.value = newDate;
    _notifySelection();
  }

  void _updateTime(DateTime newTime) {
    selectedTime.value = newTime;
    _notifySelection();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 165),
      child: Container(
        padding:
            const EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 32),
        height: 200,
        decoration: BoxDecoration(
          color: PlatformTheme.getMainBkColorSection(context),
          borderRadius: BorderRadius.circular(44.0),
          boxShadow: const [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 12.0,
              offset: Offset(0, 4),
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            // Left Side: Date Selector
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CupertinoPickerButton(
                    isDate: true,
                    onSelect: _updateDate,
                    selectedDateTime: selectedDate,
                  ),
                  const SizedBox(height: 8.0),
                  _buildDateOptions(),
                ],
              ),
            ),
            // Right Side: Time Selector
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CupertinoPickerButton(
                    isDate: false,
                    onSelect: _updateTime,
                    selectedDateTime: selectedTime,
                  ),
                  const SizedBox(height: 8.0),
                  _buildTimeOptions(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDateOptions() {
    final now = DateTime.now();
    final dateOptions = {
      '今天': now.add(const Duration(days: 0)),
      '明天': now.add(const Duration(days: 1)),
      '后天': now.add(const Duration(days: 2)),
      '大后天': now.add(const Duration(days: 3)),
      '下周一': now.add(Duration(days: (8 - now.weekday) % 7)),
    };

    return Wrap(
      alignment: WrapAlignment.start,
      children: dateOptions.keys.map((label) {
        return _optionButton(
          label,
          () => _updateDate(dateOptions[label]!),
        );
      }).toList(),
    );
  }

  Widget _buildTimeOptions() {
    final timeLabels = {
      '十点': DateTime(selectedTime.value.year, selectedTime.value.month,
          selectedTime.value.day, 10),
      '中午': DateTime(selectedTime.value.year, selectedTime.value.month,
          selectedTime.value.day, 12),
      '下午': DateTime(selectedTime.value.year, selectedTime.value.month,
          selectedTime.value.day, 15),
      '晚上': DateTime(selectedTime.value.year, selectedTime.value.month,
          selectedTime.value.day, 19),
    };

    return Wrap(
      children: timeLabels.keys.map((label) {
        return _optionButton(
          label,
          () => _updateTime(timeLabels[label]!),
        );
      }).toList(),
    );
  }

  Widget _optionButton(String text, VoidCallback onPressed) {
    return SizedBox(
      width: 66,
      child: HoverableButton(
        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
        borderRadius: BorderRadius.circular(8.0),
        accentColor: Colors.grey.withAlpha(20),
        onPressed: onPressed,
        child: Text(text, style: NORMAL_S_TXT_STYLE),
      ),
    );
  }
}

// datetime picker slots

class DateTimePickerDialogSlot extends StatefulWidget {
  final DateTime? initialDateTime;
  final Function(DateTime selectedDateTime)? onDateTimeSave;
  final bool showTime;
  final bool showQuickDate;
  final String buttonText;
  const DateTimePickerDialogSlot(
      {Key? key,
      this.initialDateTime,
      this.onDateTimeSave,
      this.showTime = true,
      this.showQuickDate = true,
      this.buttonText = '保存'})
      : super(key: key);

  @override
  _DateTimePickerDialogState createState() => _DateTimePickerDialogState();
}

class _DateTimePickerDialogState extends State<DateTimePickerDialogSlot> {
  late Rx<DateTime> selectedDate;
  late Rx<DateTime> selectedTime;
  late DateTime originDateTime;
  DateRangePickerController dateRangePickerController =
      DateRangePickerController();
  @override
  void initState() {
    super.initState();
    final now = DateTime.now();
    selectedDate = (widget.initialDateTime ?? now).obs;
    selectedTime = (widget.initialDateTime ?? now).obs;
    originDateTime = widget.initialDateTime ?? now;
    dateRangePickerController.selectedDate = originDateTime;
  }

  void _onSaveDateTime() {
    final selectedDateTime = DateTime(
      selectedDate.value.year,
      selectedDate.value.month,
      selectedDate.value.day,
      selectedTime.value.hour,
      selectedTime.value.minute,
    );

    // check if time changed or not
    if (selectedDateTime.difference(originDateTime).inMinutes != 0) {
      widget.onDateTimeSave?.call(selectedDateTime); // 调用回调
    }
  }

  void _updateDate(DateTime newDate) {
    HapticFeedback.lightImpact();
    selectedDate.value = newDate;
  }

  void _updateTime(DateTime newTime) {
    HapticFeedback.lightImpact();

    selectedTime.value = newTime;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        decoration: BoxDecoration(
          color: PlatformTheme.getMainBkColor(context),
          borderRadius: BorderRadius.circular(26),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withValues(alpha: 0.2),
              blurRadius: 20,
              offset: const Offset(0, 10),
            ),
          ],
        ),
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 420),
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                _buildDatePicker(),
                g8,
                Row(
                  children: [
                    if (widget.showQuickDate)
                      Expanded(flex: 3, child: _buildDateQuickPicker()),
                    if (widget.showTime)
                      Expanded(flex: 2, child: _buildTimePicker()),
                  ],
                ),
                if (widget.showTime) g16,
                if (widget.showTime)
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: _buildQuickTimeSelection(),
                      )
                    ],
                  ),
                g8,
                _buildConfirmBtn()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildConfirmBtn() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: HoverableButton(
            height: 38,
            onPressed: () {
              Navigator.pop(context);
              _onSaveDateTime();
            },
            accentColor: tClrPrimary(context),
            child: Text(
              widget.buttonText,
              style:
                  NORMAL_BOLD_TXT_STYLE.copyWith(color: tClrOnPrimary(context)),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildDatePicker() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ConstrainedBox(
          constraints: const BoxConstraints(maxHeight: 220),
          child: Obx(
            () => SfDateRangePicker(
              controller: dateRangePickerController,
              initialDisplayDate: selectedDate.value,
              headerHeight: 30,
              onSelectionChanged: _onSelectionChanged,
              selectionMode: DateRangePickerSelectionMode.single,
              backgroundColor: Colors.transparent,
              todayHighlightColor: tClrPrimary(context),
              viewSpacing: 0,
              showTodayButton: false,
              headerStyle: const DateRangePickerHeaderStyle(
                  backgroundColor: Colors.transparent),
            ),
          ),
        ),
      ],
    );
  }

  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    kLog("==> selected dateimte? : ${args.value}");
    _updateDate(args.value as DateTime);
  }

  Widget _buildChip({required String label, required VoidCallback onTap}) {
    return SizedBox(
      width: 55,
      height: 32,
      child: HoverableButton(
        accentColor: Colors.grey.withAlpha(30),
        onPressed: onTap,
        child: Text(
          label,
          style: NORMAL_S_TXT_STYLE,
        ),
      ),
    );
  }

  TimeOfDay dateTimeToTimeOfDay(DateTime dateTime) {
    return TimeOfDay(hour: dateTime.hour, minute: dateTime.minute);
  }

  DateTime timeOfDayToDateTime(TimeOfDay timeOfDay, {DateTime? date}) {
    final now = date ?? DateTime.now();
    return DateTime(
        now.year, now.month, now.day, timeOfDay.hour, timeOfDay.minute);
  }

  Widget _buildTimePicker() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        isOnMobile()
            ? Column(
                children: [
                  SizedBox(
                      width: 70,
                      height: 44,
                      child: HoverableButton(
                        accentColor: tClrPrimary(context),
                        borderRadius: BorderRadius.circular(22),
                        onPressed: () async {
                          final picked = await showTimePicker(
                            context: context,
                            initialTime:
                                dateTimeToTimeOfDay(selectedTime.value),
                          );
                          if (picked != null) {
                            _updateTime(timeOfDayToDateTime(picked));
                          }
                        },
                        child: Obx(
                          () => Text(
                            '${selectedTime.value.hour}'.padLeft(2, '0'),
                            style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                fontSize: 17, color: tClrOnPrimary(context)),
                          ),
                        ),
                      )),
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        buildRedDots(size: 4, color: tClrPrimary(context)),
                        g4,
                        buildRedDots(size: 4, color: tClrPrimary(context))
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 70,
                    height: 44,
                    child: HoverableButton(
                      accentColor: tClrTertiary(context),
                      borderRadius: BorderRadius.circular(22),
                      onPressed: () async {
                        final picked = await showTimePicker(
                          context: context,
                          initialTime: dateTimeToTimeOfDay(selectedTime.value),
                        );
                        if (picked != null) {
                          _updateTime(timeOfDayToDateTime(picked));
                        }
                      },
                      child: Obx(
                        () => Text(
                          '${selectedTime.value.minute}'.padLeft(2, '0'),
                          style: NORMAL_BOLD_TXT_STYLE.copyWith(
                              fontSize: 17, color: tClrOnTertiary(context)),
                        ),
                      ),
                    ),
                  )
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                      width: 70,
                      height: 44,
                      child: HoverableButton(
                        accentColor: tClrPrimary(context),
                        borderRadius: BorderRadius.circular(22),
                        onPressed: () async {
                          final picked = await showTimePicker(
                            context: context,
                            initialTime:
                                dateTimeToTimeOfDay(selectedTime.value),
                          );
                          if (picked != null) {
                            _updateTime(timeOfDayToDateTime(picked));
                          }
                        },
                        child: Obx(
                          () => Text(
                            '${selectedTime.value.hour}'.padLeft(2, '0'),
                            style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                fontSize: 17, color: tClrOnPrimary(context)),
                          ),
                        ),
                      )),
                  Text(
                    ':',
                    style: HERO_S_TITLE_TXT_BOLD_STYLE,
                  ),
                  SizedBox(
                    width: 70,
                    height: 44,
                    child: HoverableButton(
                      accentColor: tClrTertiary(context),
                      borderRadius: BorderRadius.circular(22),
                      onPressed: () async {
                        final picked = await showTimePicker(
                          context: context,
                          initialTime: dateTimeToTimeOfDay(selectedTime.value),
                        );
                        if (picked != null) {
                          _updateTime(timeOfDayToDateTime(picked));
                        }
                      },
                      child: Obx(
                        () => Text(
                          '${selectedTime.value.minute}'.padLeft(2, '0'),
                          style: NORMAL_BOLD_TXT_STYLE.copyWith(
                              fontSize: 17, color: tClrOnTertiary(context)),
                        ),
                      ),
                    ),
                  )
                ],
              ),
      ],
    );
  }

  Widget _buildDateQuickPicker() {
    return Wrap(
      spacing: 4,
      children: [
        SizedBox(
          width: 70,
          height: 40,
          child: HoverableButton(
            accentColor: Colors.grey.withAlpha(20),
            onPressed: () async {
              DateTime n = DateTime.now();
              DateTime nd = n.add(const Duration(days: 1));
              _updateDate(nd);
              dateRangePickerController.selectedDate = nd;
              kLog("==> tomorror: ${nd}");
            },
            child: Text(
              '明天',
              style: NORMAL_BOLD_TXT_STYLE,
            ),
          ),
        ),
        SizedBox(
          width: 70,
          height: 40,
          child: HoverableButton(
            accentColor: Colors.grey.withAlpha(20),
            onPressed: () async {
              DateTime n = DateTime.now();
              DateTime nd = n.add(const Duration(days: 2));
              _updateDate(nd);
              dateRangePickerController.selectedDate = nd;
            },
            child: Text(
              '后天',
              style: NORMAL_BOLD_TXT_STYLE,
            ),
          ),
        ),
        SizedBox(
          width: 70,
          height: 40,
          child: HoverableButton(
            accentColor: Colors.grey.withAlpha(20),
            onPressed: () async {
              DateTime n = DateTime.now();

              DateTime nd = n.add(const Duration(days: 3));
              _updateDate(nd);
              dateRangePickerController.selectedDate = nd;
            },
            child: Text(
              '大后天',
              style: NORMAL_BOLD_TXT_STYLE,
            ),
          ),
        ),
        if (DateTime.now().weekday != 6)
          SizedBox(
            width: 70,
            height: 40,
            child: HoverableButton(
              accentColor: Colors.grey.withAlpha(20),
              onPressed: () async {
                DateTime n = DateTime.now();

                final int daysUntilSaturday = DateTime.saturday - n.weekday;
                final DateTime nextSaturday =
                    n.add(Duration(days: daysUntilSaturday));
                _updateDate(nextSaturday);
                dateRangePickerController.selectedDate = nextSaturday;
              },
              child: Text(
                '周六',
                style: NORMAL_BOLD_TXT_STYLE,
              ),
            ),
          ),
        if (DateTime.now().weekday != 7)
          SizedBox(
            width: 70,
            height: 40,
            child: HoverableButton(
              accentColor: Colors.grey.withAlpha(20),
              onPressed: () async {
                DateTime n = DateTime.now();

                final int daysUntilSunday = DateTime.sunday - n.weekday;
                final DateTime nextSunday =
                    n.add(Duration(days: daysUntilSunday));
                _updateDate(nextSunday);
                dateRangePickerController.selectedDate = nextSunday;
              },
              child: Text(
                '周日',
                style: NORMAL_BOLD_TXT_STYLE,
              ),
            ),
          ),
        SizedBox(
          width: 70,
          height: 40,
          child: HoverableButton(
            accentColor: Colors.grey.withAlpha(20),
            onPressed: () async {
              DateTime n = DateTime.now();

              final int daysUntilSaturday = DateTime.saturday - n.weekday;
              final DateTime nextSaturday =
                  n.add(Duration(days: daysUntilSaturday + 7));
              _updateDate(nextSaturday);
              dateRangePickerController.selectedDate = nextSaturday;
            },
            child: Text(
              '下周六',
              style: NORMAL_BOLD_TXT_STYLE,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildQuickTimeSelection() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey.withAlpha(20),
                      borderRadius: BorderRadius.circular(12)),
                  child: Wrap(
                    spacing: 4,
                    children: ['08:30', '09:30', '11:00', '12:00']
                        .map((time) => _buildChip(
                              label: time,
                              onTap: () {
                                final parts = time.split(':');
                                _updateTime(timeOfDayToDateTime(TimeOfDay(
                                    hour: int.parse(parts[0]),
                                    minute: int.parse(parts[1]))));
                              },
                            ))
                        .toList(),
                  ),
                ),
              ),
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.grey.withAlpha(20),
                    borderRadius: BorderRadius.circular(12)),
                child: Wrap(
                  spacing: 4,
                  children: ['14:00', '15:00', '17:30', '18:00']
                      .map((time) => _buildChip(
                            label: time,
                            onTap: () {
                              final parts = time.split(':');
                              _updateTime(timeOfDayToDateTime(TimeOfDay(
                                  hour: int.parse(parts[0]),
                                  minute: int.parse(parts[1]))));
                            },
                          ))
                      .toList(),
                ),
              ),
            ))
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(CupertinoIcons.moon_circle_fill,
                color: Colors.grey.withAlpha(70)),
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey.withAlpha(20),
                      borderRadius: BorderRadius.circular(12)),
                  child: Wrap(
                    spacing: 4,
                    children: ['19:00', '20:00', '21:00', '22:00']
                        .map((time) => _buildChip(
                              label: time,
                              onTap: () {
                                final parts = time.split(':');
                                _updateTime(timeOfDayToDateTime(TimeOfDay(
                                    hour: int.parse(parts[0]),
                                    minute: int.parse(parts[1]))));
                              },
                            ))
                        .toList(),
                  ),
                )),
          ],
        )
      ],
    );
  }
}
