import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';
import 'package:soft_edge_blur/soft_edge_blur.dart';

class SoftEdgeBlurImage extends StatelessWidget {
  final CachedNetworkImageProvider imageProvider;
  const SoftEdgeBlurImage({super.key, required this.imageProvider});

  @override
  Widget build(BuildContext context) {
    return SoftEdgeBlur(
      edges: [
        EdgeBlur(
          type: EdgeType.bottomEdge,
          size: 70,
          sigma: 40,
          tintColor: tClrPrimary(context).withAlpha(1),
          controlPoints: [
            ControlPoint(
              position: 0.7,
              type: ControlPointType.visible,
            ),
            ControlPoint(
              position: 1,
              type: ControlPointType.transparent,
            )
          ],
        )
      ],
      child: Image(
        image: imageProvider,
        fit: BoxFit.cover,
      ),
    );
  }
}
