import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class ColumnBlock extends StatelessWidget {
  final List<Widget> children;
  final double? innerPadding;
  final Color? backgroundColor;
  final BoxBorder? border;
  final EdgeInsets? padding;
  final EdgeInsets? margin;
  final double? borderRadius;
  final bool showDivider;
  final MainAxisAlignment? mainAxisAlignment;
  final CrossAxisAlignment? crossAxisAlignment;

  const ColumnBlock({
    super.key,
    required this.children,
    this.innerPadding,
    this.backgroundColor = Colors.white,
    this.border,
    this.padding,
    this.margin,
    this.borderRadius = 12,
    this.showDivider = true,
    this.crossAxisAlignment = CrossAxisAlignment.start,
    this.mainAxisAlignment = MainAxisAlignment.start,
  });

  @override
  Widget build(BuildContext context) {
    if (children.isEmpty) {
      return Container();
    }

    var items = <Widget>[];
    for (var i = 0; i < children.length; i++) {
      items.add(children[i]);
      if (i < children.length - 1 && showDivider) {
        items.add(Container(
          padding: EdgeInsets.symmetric(vertical: innerPadding ?? 0),
          child: Divider(
            color: isDarkModeOnContext(context)
                ? Colors.grey.shade900
                : Colors.grey.shade200,
            height: 1,
          ),
        ));
      }
    }

    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: PlatformTheme.getSectionColor(context),
        border: border,
        borderRadius: BorderRadius.circular(borderRadius!),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withAlpha(10),
            offset: const Offset(0, 3),
            blurRadius: 10,
          ),
          BoxShadow(
            color: Colors.grey.withAlpha(10),
            offset: const Offset(-3, 0),
            blurRadius: 10,
          ),
        ],
      ),
      padding:
          padding ?? const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      margin: margin ?? const EdgeInsets.only(bottom: 10, left: 5, right: 5),
      child: Column(
        crossAxisAlignment: crossAxisAlignment!,
        mainAxisAlignment: mainAxisAlignment!,
        mainAxisSize: MainAxisSize.min,
        children: items,
      ),
    );
  }
}

class BoxBlock extends StatelessWidget {
  final Widget child;
  final double? innerPadding;
  final Color? backgroundColor;
  final BoxBorder? border;
  final EdgeInsets? padding;
  final EdgeInsets? margin;
  final double? borderRadius;
  final bool showDivider;
  final bool enableShadow;
  final bool enableGradient;
  final bool enableTransparent;
  final BoxShadow? boxShadow;
  final int? alpha;
  final Function()? onTap;

  const BoxBlock({
    super.key,
    required this.child,
    this.innerPadding,
    this.backgroundColor,
    this.border,
    this.padding,
    this.margin,
    this.borderRadius = 12,
    this.showDivider = true,
    this.enableShadow = true,
    this.enableGradient = true,
    this.enableTransparent = false,
    this.boxShadow,
    this.onTap,
    this.alpha = 255,
  });

  @override
  Widget build(BuildContext context) {
    return enableGradient
        ? Stack(
            children: [
              enableTransparent
                  ? SizedBox(
                      child: child,
                    )
                  : InkWell(
                      onTap: onTap,
                      borderRadius: BorderRadius.circular(borderRadius!),
                      child: Ink(
                          decoration: BoxDecoration(
                            color: backgroundColor ??
                                PlatformTheme.getSectionColor(context)
                                    .withAlpha(alpha!),
                            border: border,
                            borderRadius: BorderRadius.circular(borderRadius!),
                            boxShadow: enableShadow
                                ? [
                                    boxShadow ??
                                        BoxShadow(
                                            color: isDarkModeOnContext(context)
                                                ? const Color.fromARGB(
                                                    68, 27, 27, 27)
                                                : const Color.fromARGB(
                                                    159, 245, 245, 245),
                                            blurRadius: 9,
                                            spreadRadius: 5),
                                  ]
                                : null,
                          ),
                          padding: padding ??
                              const EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 5),
                          child: child),
                    ),
              if (enableGradient)
                Positioned.fill(
                  child: DecoratedBox(
                    // width: double.infinity,
                    decoration: BoxDecoration(
                      border: border,
                      borderRadius: BorderRadius.circular(borderRadius!),
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            isDarkModeOnContext(context)
                                ? Colors.black.withAlpha(10)
                                : Colors.white.withAlpha(10),
                            isDarkModeOnContext(context)
                                ? Colors.black.withAlpha(90)
                                : Colors.white.withAlpha(90)
                          ]),
                    ),
                  ),
                ),
            ],
          )
        : InkWell(
            onTap: onTap,
            borderRadius: BorderRadius.circular(borderRadius!),
            child: Ink(
                decoration: BoxDecoration(
                  color: enableTransparent
                      ? Colors.transparent
                      : backgroundColor ??
                          PlatformTheme.getSectionColor(context)
                              .withAlpha(alpha!),
                  border: border,
                  borderRadius: BorderRadius.circular(borderRadius!),
                  boxShadow: enableShadow && !enableTransparent
                      ? [
                          boxShadow ??
                              BoxShadow(
                                  color: isDarkModeOnContext(context)
                                      ? const Color.fromARGB(68, 27, 27, 27)
                                      : const Color.fromARGB(
                                          159, 245, 245, 245),
                                  blurRadius: 9,
                                  spreadRadius: 5),
                        ]
                      : null,
                ),
                padding: enableTransparent
                    ? EdgeInsets.zero
                    : padding ??
                        const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                child: child),
          );
  }
}

class BoxBlockSim extends StatelessWidget {
  final Widget child;
  final double? innerPadding;
  final Color? backgroundColor;
  final List<Color>? backgroundColors;
  final BoxBorder? border;
  final LinearGradient? borderGradient;
  final LinearGradient? backgroundGradient;
  final EdgeInsets? padding;
  final EdgeInsets? margin;
  final double? borderRadius;
  final BorderRadius? borderRadiusAll;
  final bool showDivider;
  final bool enableShadow;
  final bool enableGradient;
  final bool enableTransparent;
  final bool enableInk;
  final BoxShadow? boxShadow;
  final int? alpha;
  final Function()? onTap;
  final Function()? onSecondaryTap;
  final Function(bool)? onHover;

  const BoxBlockSim({
    super.key,
    required this.child,
    this.innerPadding,
    this.backgroundColor,
    this.backgroundColors,
    this.border,
    this.borderGradient,
    this.backgroundGradient,
    this.padding,
    this.margin,
    this.borderRadius = 12,
    this.borderRadiusAll,
    this.showDivider = true,
    this.enableShadow = true,
    this.enableGradient = false,
    this.enableTransparent = false,
    this.enableInk = true,
    this.boxShadow,
    this.onTap,
    this.alpha = 255,
    this.onSecondaryTap,
    this.onHover,
  });

  @override
  Widget build(BuildContext context) {
    return enableInk
        ? InkWell(
            onTap: onTap,
            onSecondaryTap: onSecondaryTap,
            onHover: onHover,
            borderRadius:
                borderRadiusAll ?? BorderRadius.circular(borderRadius!),
            child: Ink(
                decoration: BoxDecoration(
                  color: backgroundColor ??
                      PlatformTheme.getMainBkColor(context).withAlpha(alpha!),
                  // color: Colors.red,
                  // border: border,
                  borderRadius:
                      borderRadiusAll ?? BorderRadius.circular(borderRadius!),
                  gradient: backgroundGradient,
                ),
                child: borderGradient != null
                    ? ClipRRect(
                        borderRadius: borderRadiusAll ??
                            BorderRadius.circular(borderRadius!),
                        child: DecoratedBox(
                            decoration: BoxDecoration(gradient: borderGradient),
                            child: Padding(
                              padding: const EdgeInsets.all(1.0),
                              child: _buildContainer(context),
                            )))
                    : _buildContainer(context)))
        : _buildContainer(context);
  }

  Widget _buildContainer(BuildContext context) {
    return backgroundColors != null
        ? ClipRRect(
            borderRadius: BorderRadius.circular(borderRadius!),
            child: CustomPaint(
                painter: BubblePainter(
                  scrollable: Scrollable.of(context),
                  bubbleContext: context,
                  colors: backgroundColors ??
                      [
                        const Color(0xFF19B7FF),
                        const Color(0xFF491CCB),
                      ],
                ),
                child: Stack(
                  children: <Widget>[
                    _buildCore(context),
                    // Text('aaaaa')
                  ],
                )),
          )
        : Container(
            margin: margin,
            decoration: BoxDecoration(
              color: backgroundColor ??
                  PlatformTheme.getMainBkColor(context).withAlpha(alpha!),
              // color: backgroundColor,
              // color: Colors.red,
              border: border,
              borderRadius:
                  borderRadiusAll ?? BorderRadius.circular(borderRadius!),
              gradient: backgroundGradient,
              boxShadow: enableShadow
                  ? [
                      boxShadow ??
                          BoxShadow(
                              color: isDarkModeOnContext(context)
                                  ? const Color.fromARGB(68, 27, 27, 27)
                                  : Colors.grey.withAlpha(10),
                              blurRadius: 13,
                              spreadRadius: 4,
                              offset: const Offset(-1, -1)),
                    ]
                  : null,
            ),
            child: _buildCore(context),
          );
  }

  Widget _buildCore(BuildContext context) {
    return Padding(
      padding:
          padding ?? const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
      child: child,
    );
  }
}

class BoxBlockContainer extends StatelessWidget {
  final Widget child;
  final EdgeInsets? padding;
  final EdgeInsets? margin;
  final double? borderRadius;
  final bool enableBorder;
  final Color? backgroundColor;
  final Function()? onTap;
  final Function()? onSecondaryTap;
  final Function(bool)? onHover;

  const BoxBlockContainer({
    super.key,
    required this.child,
    this.padding,
    this.margin,
    this.borderRadius = 12,
    this.onTap,
    this.onSecondaryTap,
    this.enableBorder = true,
    this.backgroundColor,
    this.onHover,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onTap,
        onSecondaryTap: onSecondaryTap,
        onHover: onHover,
        borderRadius: BorderRadius.circular(borderRadius!),
        child: Ink(
            decoration: BoxDecoration(
              color: backgroundColor ?? PlatformTheme.getMainBkColor(context),
            ),
            child: _buildContainer(context)));
  }

  Widget _buildContainer(BuildContext context) {
    return Container(
      margin: margin,
      decoration: BoxDecoration(
        color: backgroundColor ?? PlatformTheme.getMainBkColor(context),
        border:
            enableBorder ? Border.all(color: Colors.grey.withAlpha(20)) : null,
        borderRadius: BorderRadius.circular(borderRadius!),
        boxShadow: [
          BoxShadow(
              color: isDarkModeOnContext(context)
                  ? const Color.fromARGB(68, 27, 27, 27)
                  : Colors.grey.withAlpha(10),
              blurRadius: 13,
              spreadRadius: 4,
              offset: const Offset(-1, -1)),
        ],
      ),
      child: _buildCore(context),
    );
  }

  Widget _buildCore(BuildContext context) {
    return Padding(
      padding:
          padding ?? const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
      child: child,
    );
  }
}

class DashedDivider extends StatelessWidget {
  final double height;
  final Color color;
  final double dashWidth;
  final double dashGap;

  const DashedDivider({
    Key? key,
    this.height = 1,
    this.color = Colors.grey,
    this.dashWidth = 5,
    this.dashGap = 3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      size: Size(double.infinity, height),
      painter: _DashedLinePainter(
        color: color,
        dashWidth: dashWidth,
        dashGap: dashGap,
        height: height,
      ),
    );
  }
}

class _DashedLinePainter extends CustomPainter {
  final double dashWidth;
  final double dashGap;
  final double height;
  final Color color;

  _DashedLinePainter({
    required this.dashWidth,
    required this.dashGap,
    required this.height,
    required this.color,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = color
      ..strokeWidth = height;

    double startX = 0;
    while (startX < size.width) {
      canvas.drawLine(
        Offset(startX, 0),
        Offset(startX + dashWidth, 0),
        paint,
      );
      startX += dashWidth + dashGap;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
