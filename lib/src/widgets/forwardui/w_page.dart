import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class ScaffoldWrapperAdapt extends StatelessWidget {
  final Widget? child;
  const ScaffoldWrapperAdapt({super.key, this.child});

  @override
  Widget build(BuildContext context) {
    return !isOnMobile()
        ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 120, vertical: 45),
            child: Container(
              decoration: BoxDecoration(
                color: PlatformTheme.getMainBkColor(context),
                borderRadius: BorderRadius.circular(26),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    blurRadius: 20,
                    offset: const Offset(0, 10),
                  ),
                ],
              ),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(12), child: child),
            ),
          )
        : child ?? const SizedBox();
  }
}

class NiceNavigationRail extends StatefulWidget {
  final List<Widget> children; // List of widget content for each tab
  final Function(int)
      onDestinationSelected; // Callback when a destination is selected
  final int selectedIndex; // Currently selected index

  const NiceNavigationRail({
    super.key,
    required this.children,
    required this.onDestinationSelected,
    this.selectedIndex = 0,
  });

  @override
  _NiceNavigationRailState createState() => _NiceNavigationRailState();
}

class _NiceNavigationRailState extends State<NiceNavigationRail> {
  late int _selectedIndex;

  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.selectedIndex;
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        colorScheme: Theme.of(context).colorScheme.copyWith(
              primary:
                  Colors.transparent, // Set the primary color to transparent
            ),
      ),
      child: NavigationRail(
        minWidth: 150,
        indicatorColor: Colors.transparent,
        extended: false,
        backgroundColor: Colors.transparent,
        selectedIndex: _selectedIndex,
        labelType: NavigationRailLabelType.none,
        onDestinationSelected: (int index) {
          setState(() {
            _selectedIndex = index;
          });
          widget.onDestinationSelected(index); // Trigger callback
        },
        destinations: List.generate(
          widget.children.length,
          (index) => _buildCustomDestination(widget.children[index], index),
        ),
      ),
    );
  }

  NavigationRailDestination _buildCustomDestination(Widget child, int index) {
    return NavigationRailDestination(
      icon: InkWell(
        onTap: () {
          setState(() {
            _selectedIndex = index;
          });
          widget.onDestinationSelected(index); // Trigger callback
        },
        borderRadius: BorderRadius.circular(8), // Shape of the click effect
        hoverColor: tClrPrimary(context).withOpacity(0.04),
        child: Container(
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
            decoration: BoxDecoration(
              color: _selectedIndex == index
                  ? Colors.grey.withAlpha(30)
                  : Colors.transparent,
              borderRadius: BorderRadius.circular(8), // Rounded rectangle
            ),
            child: child),
      ),
      label: const SizedBox.shrink(), // Hide the label below the icon
    );
  }
}
