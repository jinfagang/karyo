import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';
import 'package:shadcn_ui/shadcn_ui.dart';

class MySelect<T> extends StatelessWidget {
  final List<T> data;
  final T? initialValue;
  final Widget? placeholder;
  final Widget Function(T value)? optionBuilder;
  final Widget Function(BuildContext context, T value)? selectedOptionBuilder;
  final void Function(T? value) onChanged;

  const MySelect({
    super.key,
    required this.data,
    this.placeholder,
    this.initialValue,
    this.optionBuilder,
    this.selectedOptionBuilder,
    required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return ShadTheme(
      data: ShadThemeData(
        popoverTheme: ShadPopoverTheme(
          decoration: ShadDecoration(
              border: ShadBorder.all(
                  color: Colors.transparent, radius: BorderRadius.circular(17)),
              secondaryBorder: ShadBorder.all(
                padding: const EdgeInsets.all(4),
                width: 0,
              ),
              secondaryFocusedBorder: ShadBorder.all(
                width: 0,
                color: Colors.transparent,
                radius: BorderRadius.circular(0),
                padding: const EdgeInsets.all(0),
              ),
              color: PlatformTheme.getMainBkColorSection4(context)),
          shadows: getBoxShadow(context),
        ),
        selectTheme: const ShadSelectTheme(decoration: ShadDecoration.none),
        // orderPolicy: const WidgetOrderPolicy.reverse(),
        decoration: ShadDecoration(
          border: ShadBorder.all(color: Colors.transparent),
        ),
        disableSecondaryBorder: true,
        brightness: Theme.of(context).brightness,
        colorScheme: isDarkModeOnContext(context)
            ? ShadNeutralColorScheme.dark(
                background: PlatformTheme.getMainBkColorSection4(context))
            : ShadNeutralColorScheme.light(
                background: PlatformTheme.getMainBkColorSection4(context)),
      ),
      child: ShadSelect<T>(
        trailing: const SizedBox(),
        padding: EdgeInsets.zero,
        shrinkWrap: true,
        placeholder: placeholder,
        showScrollToBottomChevron: false,
        showScrollToTopChevron: false,
        decoration:
            ShadDecoration(border: ShadBorder.all(color: Colors.transparent)),
        initialValue: initialValue,
        options: [
          ...data.map(
            (e) => ShadOption(
                radius: BorderRadius.circular(13),
                orderPolicy: const OrderPolicy.reverse(),
                value: e,
                // selectedIcon: Icon(
                //   CupertinoIcons.check_mark,
                //   color: tClrPrimary(context),
                // ),
                child: optionBuilder!(e)),
          ),
        ],
        selectedOptionBuilder: (context, value) =>
            selectedOptionBuilder!(context, value),
        onChanged: onChanged,
      ),
    );
  }
}
