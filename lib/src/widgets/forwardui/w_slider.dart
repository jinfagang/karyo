import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class SegmentedSlider extends StatelessWidget {
  final List<int> options;
  final int selectedValue;
  final ValueChanged<int> onChanged;

  const SegmentedSlider({
    required this.options,
    required this.selectedValue,
    required this.onChanged,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        // Stack(
        //   alignment: Alignment.center,
        //   children: [
        //     SliderTheme(
        //       data: SliderThemeData(
        //         activeTrackColor: tClrPrimary(context),
        //         inactiveTrackColor: Colors.grey.withAlpha(40),
        //         trackHeight: 12.0,
        //         thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12.0),
        //         overlayShape: RoundSliderOverlayShape(overlayRadius: 0.0),
        //         thumbColor: Theme.of(context).colorScheme.primary,
        //         inactiveTickMarkColor: Colors.transparent,
        //         activeTickMarkColor: Colors.transparent,
        //         overlayColor:
        //             Theme.of(context).colorScheme.primary.withOpacity(0.2),
        //         trackShape: CustomTrackShape(), // Custom track shape
        //       ),
        //       child: Slider(
        //         value: options.indexOf(selectedValue).toDouble(),
        //         min: 0,
        //         max: (options.length - 1).toDouble(),
        //         divisions: options.length - 1,
        //         onChanged: (newValue) {
        //           onChanged(options[newValue.round()]);
        //         },
        //       ),
        //     ),
        //     Positioned.fill(
        //       child: Row(
        //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //         children: options.map((option) {
        //           final isSelected = option == selectedValue;
        //           return Column(
        //             mainAxisSize: MainAxisSize.min,
        //             children: [
        //               // Text(
        //               //   "$option",
        //               //   textAlign: TextAlign.center,
        //               //   style: TextStyle(
        //               //       fontSize: 14.0,
        //               //       fontWeight: isSelected
        //               //           ? FontWeight.bold
        //               //           : FontWeight.normal,
        //               //       color: isSelected
        //               //           ? tClrOnPrimary(context)
        //               //           : tClrSecondary(context).withAlpha(60)),
        //               // ),
        //               // SizedBox(height: 4.0),
        //               // CircleAvatar(
        //               //   radius: 4.0,
        //               //   backgroundColor: isSelected
        //               //       ? Theme.of(context).colorScheme.primary
        //               //       : Theme.of(context)
        //               //           .colorScheme
        //               //           .onSurface
        //               //           .withOpacity(0.6),
        //               // ),
        //             ],
        //           );
        //         }).toList(),
        //       ),
        //     ),
        //   ],
        // ),

        SliderTheme(
          data: SliderTheme.of(context).copyWith(
            trackHeight: 12,
            thumbShape: SliderComponentShape.noThumb,
            inactiveTickMarkColor: Colors.transparent,
            activeTickMarkColor: Colors.transparent,
          ),
          child: Slider(
            value: selectedValue.toDouble(),
            secondaryTrackValue: selectedValue.toDouble(),
            max: options.last.toDouble(),
            min: options.first.toDouble(),
            divisions: options.length,
            label: selectedValue.toString(),
            onChanged: (value) {
              // onSelectionChanged(value.toInt());
              onChanged(value.round());
            },
          ),
        )
      ],
    );
  }
}

/// Custom track shape to ensure no padding around the slider track.
class CustomTrackShape extends RoundedRectSliderTrackShape {
  @override
  Rect getPreferredRect({
    required RenderBox parentBox,
    Offset offset = Offset.zero,
    required SliderThemeData sliderTheme,
    bool isEnabled = false,
    bool isDiscrete = false,
  }) {
    final double trackHeight = sliderTheme.trackHeight ?? 6.0;
    final double trackLeft = offset.dx;
    final double trackTop =
        offset.dy + (parentBox.size.height - trackHeight) / 2;
    final double trackWidth = parentBox.size.width;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }
}
