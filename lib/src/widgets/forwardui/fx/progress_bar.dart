// progress_loader.dart
import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'package:karyo/karyo.dart';

class DynamicWaveProgressStrategy implements ProgressAnimationStrategy {
  // Configuration for wave animation
  final Duration waveDuration;
  final bool autoAnimate;
  final Curve waveCurve;

  /// Creates a wave progress strategy
  /// [waveDuration] controls how fast the waves move
  /// [autoAnimate] determines if waves should animate automatically
  /// [waveCurve] defines the wave motion curve
  const DynamicWaveProgressStrategy({
    this.waveDuration = const Duration(milliseconds: 2000),
    this.autoAnimate = true,
    this.waveCurve = Curves.linear,
  });

  @override
  Widget buildProgressWidget({
    required double progress,
    required Animation<double> animation,
    required BuildContext context,
    required ProgressStyle style,
  }) {
    return _WaveProgressWidget(
      progress: progress,
      style: style,
      waveDuration: waveDuration,
      autoAnimate: autoAnimate,
      waveCurve: waveCurve,
    );
  }
}

/// Internal widget to handle wave animation state
class _WaveProgressWidget extends StatefulWidget {
  final double progress;
  final ProgressStyle style;
  final Duration waveDuration;
  final bool autoAnimate;
  final Curve waveCurve;

  const _WaveProgressWidget({
    Key? key,
    required this.progress,
    required this.style,
    required this.waveDuration,
    required this.autoAnimate,
    required this.waveCurve,
  }) : super(key: key);

  @override
  State<_WaveProgressWidget> createState() => _WaveProgressWidgetState();
}

class _WaveProgressWidgetState extends State<_WaveProgressWidget>
    with SingleTickerProviderStateMixin {
  late final AnimationController _waveController;
  late final Animation<double> _waveAnimation;

  @override
  void initState() {
    super.initState();

    // Initialize wave animation controller
    _waveController = AnimationController(
      vsync: this,
      duration: widget.waveDuration,
    );

    // Create curved animation for wave movement
    _waveAnimation = CurvedAnimation(
      parent: _waveController,
      curve: widget.waveCurve,
    );

    // Start animation if auto-animate is enabled and progress < 100%
    if (widget.autoAnimate && widget.progress < 1.0) {
      _waveController.repeat();
    }
  }

  @override
  void didUpdateWidget(_WaveProgressWidget oldWidget) {
    super.didUpdateWidget(oldWidget);

    // Handle animation state based on progress
    if (widget.progress >= 1.0) {
      _waveController.stop();
    } else if (widget.autoAnimate && !_waveController.isAnimating) {
      _waveController.repeat();
    }

    // Update animation duration if changed
    if (widget.waveDuration != oldWidget.waveDuration) {
      _waveController.duration = widget.waveDuration;
    }
  }

  @override
  void dispose() {
    _waveController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.style.width,
      height: widget.style.height,
      padding: widget.style.padding,
      decoration: BoxDecoration(
        color: widget.style.primaryColor,
        borderRadius: widget.style.borderRadius ?? BorderRadius.circular(8),
      ),
      child: ClipRRect(
        borderRadius: widget.style.borderRadius != null
            ? widget.style.borderRadius! * 0.8
            : BorderRadius.circular(8),
        child: CustomPaint(
          painter: WaveProgressPainter(
            progress: widget.progress,
            color: widget.style.primaryColor,
            animation: _waveAnimation,
          ),
        ),
      ),
    );
  }
}

/// Custom painter for wave progress effect with dynamic wave animation
class WaveProgressPainter extends CustomPainter {
  final double progress;
  final Color color;
  final Animation<double> animation;

  // Constants for wave configuration
  static const double _twoPi = 2 * math.pi;
  static const int _waveCount =
      2; // Number of overlapping waves for more natural effect

  WaveProgressPainter({
    required this.progress,
    required this.color,
    required this.animation,
  }) : super(repaint: animation);

  @override
  void paint(Canvas canvas, Size size) {
    // Calculate wave parameters based on progress
    final maxWaveHeight = size.height * 0.1; // Maximum height of waves at 0%
    final currentWaveHeight =
        maxWaveHeight * (1 - progress); // Waves reduce as progress increases

    final paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;

    // Calculate the base water level
    final waterLevel = size.height * (1 - progress);

    // Create path for the wave
    final path = Path();
    path.moveTo(0, size.height); // Start from bottom-left

    // Width of one complete wave cycle
    final waveWidth = size.width / 2 + 10;

    // Draw multiple overlapping waves
    for (var i = 0; i < _waveCount; i++) {
      final wavePhase = i * math.pi / 2; // Phase difference between waves
      final opacity = i == 0 ? 1.0 : 0.5; // Second wave is more transparent

      paint.color = color.withOpacity(opacity);

      final wavePath = Path();
      wavePath.moveTo(0, size.height);

      // Draw wave points
      for (double x = 0; x <= size.width; x++) {
        final relativeX = x / waveWidth;
        final normalizedProgress = math.min(1.0, math.max(0.0, progress));

        // Wave function with dynamic amplitude and frequency
        final y = waterLevel +
            currentWaveHeight *
                math.sin((relativeX * _twoPi) +
                    (animation.value * 2 * _twoPi) +
                    wavePhase) *
                (1 -
                    normalizedProgress *
                        0.7); // Reduce wave height as progress increases

        wavePath.lineTo(x, y);
      }

      // Complete the wave path
      wavePath.lineTo(size.width, size.height);
      wavePath.lineTo(0, size.height);
      wavePath.close();

      // Draw the wave
      canvas.drawPath(wavePath, paint);
    }

    // Add a gentle gradient overlay for depth effect
    final gradientPaint = Paint()
      ..shader = LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [
          color.withOpacity(0.1),
          color.withOpacity(0.3),
        ],
        stops: const [0.0, 1.0],
      ).createShader(
          Rect.fromLTWH(0, waterLevel, size.width, size.height - waterLevel));

    canvas.drawRect(
      Rect.fromLTWH(0, waterLevel, size.width, size.height - waterLevel),
      gradientPaint,
    );
  }

  @override
  bool shouldRepaint(WaveProgressPainter oldDelegate) {
    return oldDelegate.progress != progress ||
        oldDelegate.color != color ||
        oldDelegate.animation != animation;
  }
}

/// Abstract class defining the contract for progress animation strategies
abstract class ProgressAnimationStrategy {
  Widget buildProgressWidget({
    required double progress,
    required Animation<double> animation,
    required BuildContext context,
    required ProgressStyle style,
  });
}

/// Style configuration for progress loaders
class ProgressStyle {
  final Color primaryColor;
  final Color backgroundColor;
  final double width;
  final double height;
  final EdgeInsets padding;
  final BorderRadius? borderRadius;
  final List<Color>? gradientColors;
  final Curve curve;
  final Duration animationDuration;

  const ProgressStyle({
    this.primaryColor = Colors.blue,
    this.backgroundColor = Colors.grey,
    this.width = 200,
    this.height = 20,
    this.padding = EdgeInsets.zero,
    this.borderRadius,
    this.gradientColors,
    this.curve = Curves.easeInOut,
    this.animationDuration = const Duration(milliseconds: 300),
  });

  ProgressStyle copyWith({
    Color? primaryColor,
    Color? backgroundColor,
    double? width,
    double? height,
    EdgeInsets? padding,
    BorderRadius? borderRadius,
    List<Color>? gradientColors,
    Curve? curve,
    Duration? animationDuration,
  }) {
    return ProgressStyle(
      primaryColor: primaryColor ?? this.primaryColor,
      backgroundColor: backgroundColor ?? this.backgroundColor,
      width: width ?? this.width,
      height: height ?? this.height,
      padding: padding ?? this.padding,
      borderRadius: borderRadius ?? this.borderRadius,
      gradientColors: gradientColors ?? this.gradientColors,
      curve: curve ?? this.curve,
      animationDuration: animationDuration ?? this.animationDuration,
    );
  }
}

/// Main progress loader widget that uses a strategy pattern
class ProgressLoader extends StatefulWidget {
  final double progress;
  final ProgressAnimationStrategy strategy;
  final ProgressStyle style;
  final ValueChanged<double>? onProgressUpdate;

  const ProgressLoader({
    Key? key,
    required this.progress,
    required this.strategy,
    this.style = const ProgressStyle(),
    this.onProgressUpdate,
  }) : super(key: key);

  @override
  State<ProgressLoader> createState() => _ProgressLoaderState();
}

class _ProgressLoaderState extends State<ProgressLoader>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: widget.style.animationDuration,
    );
    _animation = Tween<double>(
      begin: 0,
      end: widget.progress,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: widget.style.curve,
    ))
      ..addListener(() {
        if (widget.onProgressUpdate != null) {
          widget.onProgressUpdate!(_animation.value);
        }
      });
    _controller.forward();
  }

  @override
  void didUpdateWidget(ProgressLoader oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.progress != widget.progress) {
      _animation = Tween<double>(
        begin: oldWidget.progress,
        end: widget.progress,
      ).animate(CurvedAnimation(
        parent: _controller,
        curve: widget.style.curve,
      ));
      _controller.forward(from: 0);
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animation,
      builder: (context, child) {
        return widget.strategy.buildProgressWidget(
          progress: _animation.value,
          animation: _animation,
          context: context,
          style: widget.style,
        );
      },
    );
  }
}

Widget getProgressBarWave({Color color = Colors.green, double progress = 0.5}) {
  final percentageText = ((1 - progress) * 100).toStringAsFixed(1) + "%";

  return Column(
    children: [
      ProgressLoader(
        progress: progress,
        strategy: const DynamicWaveProgressStrategy(
          waveDuration: Duration(milliseconds: 3500),
          autoAnimate: true,
          waveCurve: Curves.linear,
        ),
        style: ProgressStyle(
            height: 80,
            width: 38,
            borderRadius: BorderRadius.circular(15),
            primaryColor: color.withAlpha(40),
            backgroundColor: Colors.grey,
            padding: const EdgeInsets.all(2)),
      ),
      // g4,
      // Text(
      //   '$percentageText',
      //   style: SMALL_TXT_STYLE.copyWith(
      //       color: color, fontFamily: getFontMonospace()),
      // )
    ],
  );
}
