import 'dart:async';

import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

void showYYDialog(String msg, BuildContext context,
    {Color color = Colors.green}) {
  var d = YYDialog().build(context)
    ..width = 120
    // ..height = 120
    ..duration = Duration(milliseconds: 300)
    ..backgroundColor = PlatformTheme.getMainBkColorDialog(context)
    ..borderRadius = 10.0
    ..animatedFunc = (child, animation) {
      return ScaleTransition(
        child: child,
        scale: Tween(begin: 0.0, end: 1.0).animate(animation),
      );
    }
    ..gravity = Gravity.spaceEvenly
    ..widget(Center(
      child: CupertinoButton(
          child: Text(
            msg,
            style: NORMAL_BOLD_TXT_STYLE.copyWith(color: color),
            textAlign: TextAlign.center,
          ),
          onPressed: () {}),
    ));
  d.show();
  Timer(const Duration(seconds: 2), () {
    d.dismiss();
  });
}

void showWechatDialog(String msg, BuildContext context) {
  var d = YYDialog().build(context)
    ..width = 340
    // ..height = 120
    ..duration = const Duration(milliseconds: 300)
    ..borderRadius = 10.0
    ..animatedFunc = (child, animation) {
      return ScaleTransition(
        scale: Tween(begin: 0.0, end: 1.0).animate(animation),
        child: child,
      );
    }
    ..backgroundColor = Colors.grey.shade900
    ..gravity = Gravity.spaceEvenly
    ..widget(
      Center(
          child: Column(
        children: [
          const Icon(
            FluentIcons.checkmark_12_filled,
            color: Colors.white,
            size: 44,
          ),
          g8,
          Text(
            msg,
            style: NORMAL_BOLD_TXT_STYLE.copyWith(color: Colors.white),
            textAlign: TextAlign.center,
          ),
        ],
      )),
    );
  d.show();
  Timer(const Duration(seconds: 1), () {
    d.dismiss();
  });
}

void showYYDialogFullScreen(String msg, BuildContext context,
    {Color color = Colors.green}) {
  var d = YYDialog().build(context)
    ..width = MediaQuery.of(context).size.width * 3 / 4
    // ..height = 400
    ..duration = Duration(milliseconds: 200)
    ..backgroundColor = PlatformTheme.getSectionColor(context)
    ..borderRadius = 0.0
    ..animatedFunc = (child, animation) {
      return ScaleTransition(
        child: child,
        scale: Tween(begin: 0.0, end: 1.0).animate(animation),
      );
    }
    ..text(color: Colors.black)
    ..barrierColor = PlatformTheme.getSectionColor(context)
    ..gravity = Gravity.spaceEvenly
    ..barrierDismissible = true
    ..widget(Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Center(
        child: CupertinoButton(
            child: Text(
              msg,
              style: NORMAL_TXT_STYLE.copyWith(fontSize: 46),
              textAlign: TextAlign.center,
            ),
            onPressed: () {}),
      ),
    ]));
  d.show();
  // Timer(Duration(seconds: 2), () {
  //   d.dismiss();
  // });
}
