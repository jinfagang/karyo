import 'package:bot_toast/bot_toast.dart';
import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:karyo/karyo.dart';

showSuccessMessageEnhanced(
  BuildContext context,
  Object message, {
  Duration duration = const Duration(seconds: 2),
}) {
  showErrorMessage(message.toString(), duration: duration);
}

showErrorMessageEnhanced(
  BuildContext context,
  Object message, {
  Duration duration = const Duration(seconds: 2),
}) {
  showErrorMessage(message.toString(), duration: duration);
}

showErrorMessage(String message,
    {Duration duration = const Duration(seconds: 3)}) {
  BotToast.showCustomText(
    clickClose: false,
    crossPage: true,
    duration: duration,
    toastBuilder: (_) => Align(
      alignment: const Alignment(0, -0.8),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 6),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: GlobalSettings.isForceDark || isDarkModeOnNoContext()
                ? Colors.grey.shade900
                : Colors.white,
            boxShadow: [
              BoxShadow(
                  blurRadius: 8,
                  spreadRadius: 6,
                  color: GlobalSettings.isForceDark || isDarkModeOnNoContext()
                      ? Colors.grey.withAlpha(10)
                      : Colors.grey.withAlpha(20))
            ]),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Flexible(
                child: ConstrainedBox(
              constraints: BoxConstraints(maxWidth: SizeHelper.screenW * 0.8),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 6),
                child: Text(
                  message,
                  style: NORMAL_BOLD_TXT_STYLE.copyWith(color: Colors.red),
                  textAlign: TextAlign.center,
                ),
              ),
            )),
          ],
        ),
      ),
    ),
  );
}

showSuccessMessage(String message,
    {Duration duration = const Duration(seconds: 3), Color? color}) {
  BotToast.showCustomText(
    clickClose: false,
    crossPage: true,
    duration: duration,
    toastBuilder: (_) => Align(
      alignment: const Alignment(0, -0.8),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 6),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: color ??
                (GlobalSettings.isForceDark || isDarkModeOnNoContext()
                    ? Colors.grey.shade900
                    : Colors.white),
            boxShadow: [
              BoxShadow(
                  blurRadius: 8,
                  spreadRadius: 6,
                  color: GlobalSettings.isForceDark || isDarkModeOnNoContext()
                      ? Colors.grey.withAlpha(10)
                      : Colors.grey.withAlpha(20))
            ]),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Flexible(
                child: ConstrainedBox(
              constraints: BoxConstraints(maxWidth: SizeHelper.screenW * 0.8),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 6),
                child: Text(
                  message,
                  style: color != null
                      ? NORMAL_BOLD_TXT_STYLE.copyWith(
                          color: color.computeLuminance() < 0.5
                              ? Colors.white
                              : Colors.black)
                      : NORMAL_BOLD_TXT_STYLE,
                  textAlign: TextAlign.center,
                ),
              ),
            )),
          ],
        ),
      ),
    ),
  );
}

showWechatSuccessMessage(String message,
    {Duration duration = const Duration(seconds: 3)}) {
  BotToast.showCustomLoading(
    clickClose: true,
    crossPage: true,
    duration: duration,
    toastBuilder: (_) => Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 23),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.grey.shade800,
        // boxShadow: [
        //   BoxShadow(
        //       blurRadius: 8,
        //       spreadRadius: 6,
        //       color: Colors.grey.withAlpha(30))
        // ]
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const Icon(
            Icons.check,
            color: Colors.white,
            size: 44,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Text(
              message,
              style: NORMAL_TXT_STYLE.copyWith(color: Colors.white),
            ),
          ),
        ],
      ),
    ),
  );
}

showGeneralDialogMy(BuildContext context, Widget child,
    {bool useRootNavigator = true}) {
  showGeneralDialog(
    useRootNavigator: useRootNavigator,
    context: context,
    barrierDismissible: true,
    barrierLabel: '345t5',
    transitionDuration: const Duration(milliseconds: 300),
    barrierColor: PlatformTheme.getMainBkColor(context).withAlpha(190),
    pageBuilder: (context, animation1, animation2) {
      return Center(
        child: child,
      );
      // return child;
    },
    transitionBuilder: (context, animation, secondaryAnimation, child) {
      // return ScaleTransition(
      //   scale: CurvedAnimation(
      //     parent: animation,
      //     curve: Curves.easeOutBack,
      //   ),
      //   child: child,
      // );
      return FadeTransition(
        opacity: animation,
        child: ScaleTransition(
          scale: Tween<double>(begin: 0.9, end: 1.0).animate(
            CurvedAnimation(parent: animation, curve: Curves.easeOutBack),
          ),
          child: child,
        ),
      );
    },
  );
}

showGeneralDialogMyWrapped(BuildContext context, Widget child) {
  showGeneralDialog(
    context: context,
    barrierDismissible: true,
    barrierLabel: '345t5',
    transitionDuration: const Duration(milliseconds: 300),
    barrierColor: PlatformTheme.getMainBkColor(context).withAlpha(190),
    pageBuilder: (context, animation1, animation2) {
      return Dialog(
        child: Container(
          decoration: BoxDecoration(
            color: PlatformTheme.getMainBkColor(context),
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withValues(alpha: 0.2),
                blurRadius: 20,
                offset: const Offset(0, 10),
              ),
            ],
          ),
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 420),
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: child,
            ),
          ),
        ),
      );
    },
    transitionBuilder: (context, animation, secondaryAnimation, child) {
      return FadeTransition(
        opacity: animation,
        child: ScaleTransition(
          scale: Tween<double>(begin: 0.9, end: 1.0).animate(
            CurvedAnimation(parent: animation, curve: Curves.easeOutBack),
          ),
          child: child,
        ),
      );
    },
  );
}

Future<void> showPopupDialog({
  required BuildContext context,
  required Widget child,
  Duration transitionDuration = const Duration(milliseconds: 300),
  Color barrierColor =
      const Color(0x80000000), // Default semi-transparent black
  bool barrierDismissible = true,
  String barrierLabel = 'popupDialog',
}) {
  return showGeneralDialog(
    context: context,
    barrierDismissible: barrierDismissible,
    barrierLabel: barrierLabel,
    transitionDuration: transitionDuration,
    barrierColor: barrierColor,
    pageBuilder: (context, animation, secondaryAnimation) {
      return child;
    },
    transitionBuilder: (context, animation, secondaryAnimation, child) {
      // Custom animation: fade and scale
      return FadeTransition(
        opacity: animation,
        child: ScaleTransition(
          scale: Tween<double>(begin: 0.9, end: 1.0).animate(
            CurvedAnimation(parent: animation, curve: Curves.easeOutBack),
          ),
          child: child,
        ),
      );
    },
  );
}
