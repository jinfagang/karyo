import 'dart:math';
import 'package:flutter/material.dart';
import 'package:random_color/random_color.dart';

var priorityColor = [Colors.red, Colors.orange, Colors.yellow, Colors.white];

class ColorPalette {
  final String colorName;
  final int colorValue;

  ColorPalette(this.colorName, this.colorValue);
}

class ColorPaletteHex {
  final String colorName;
  final String hexValue;

  ColorPaletteHex(this.colorName, this.hexValue);
}

class RandomColorGen {
  static final Random _random = new Random();
  static final RandomColor _randomColor = RandomColor();

  /// Returns a random color.
  static Color next() {
    return new Color(0xFF000000 + _random.nextInt(0x00FFFFFF));
  }

  static Color nextColor() {
    return _randomColor.randomColor(colorBrightness: ColorBrightness.light);
  }

  static Color nextColorDark() {
    return _randomColor.randomColor(colorBrightness: ColorBrightness.dark);
  }

  static ColorPalette nextPalette() {
    return new ColorPalette("random",
        _randomColor.randomColor(colorBrightness: ColorBrightness.light).value);
  }
}

var colorsPalettes = <ColorPalette>[
  ColorPalette("Red", 0xffffa190),
  ColorPalette("Pink", Colors.pink.value),
  ColorPalette("Purple", Colors.purple.value),
  ColorPalette("Deep Purple", Colors.deepPurple.value),
  ColorPalette("Indigo", Colors.indigo.value),
  ColorPalette("Blue", Colors.blue.value),
  ColorPalette("Lightblue", Colors.lightBlue.value),
  ColorPalette("Cyan", Colors.cyan.value),
  ColorPalette("Teal", Colors.teal.value),
  ColorPalette("Green", Colors.green.value),
  ColorPalette("Lightgreen", Colors.lightGreen.value),
  ColorPalette("Lime", Colors.lime.value),
  ColorPalette("Yellow", Colors.yellow.value),
  ColorPalette("Amber", Colors.amber.value),
  ColorPalette("Orange", 0xffffa95f),
  ColorPalette("Deeporange", Colors.deepOrange.value),
  ColorPalette("Brown", Colors.brown.value),
  // ColorPalette("Black", Colors.black.value),
  ColorPalette("WindRed", 0xffffa190),
  ColorPalette("WindRedLight", 0xffffeeee),
  ColorPalette("WindGreen", 0xff61d2a1),
  ColorPalette("WindGreenLight", 0xff9df3c4),
  ColorPalette("White", 0xffffffff),
];

var colorsPalettesLight = <ColorPalette>[
  ColorPalette(
    "白色",
    Colors.pink.value,
  ),
  ColorPalette(
    "淡粉色",
    Color(0xFFF28C82).value,
  ),
  ColorPalette(
    "橙色",
    Color(0xFFFABD03).value,
  ),
  ColorPalette(
    "淡黄色",
    Color(0xFFFFF476).value,
  ),
  ColorPalette(
    "浅黄色",
    Color(0xFFCDFF90).value,
  ),
  ColorPalette(
    "淡蓝色",
    Color(0xFFA7FEEB).value,
  ),
  ColorPalette(
    "白蓝色",
    Color(0xFFCBF0F8).value,
  ),
  ColorPalette(
    "云蓝",
    Color(0xFFAFCBFA).value,
  ),
  ColorPalette(
    "淡紫色",
    Color(0xFFD7AEFC).value,
  ),
  ColorPalette(
    "Red",
    Color(0xFFFDCFE9).value,
  ),
  ColorPalette(
    "淡红色",
    Color(0xFFE6C9A9).value,
  ),
  ColorPalette(
    "淡白色",
    Color(0xFFE9EAEE).value,
  ),
  ColorPalette("Red", 0xffffa190),
  ColorPalette("Pink", Colors.pink.value),
  ColorPalette("Purple", Colors.purple.value),
  ColorPalette("Deep Purple", Colors.deepPurple.value),
  ColorPalette("Indigo", Colors.indigo.value),
  ColorPalette("Blue", Colors.blue.value),
  ColorPalette("Lightblue", Colors.lightBlue.value),
  ColorPalette("Cyan", Colors.cyan.value),
  ColorPalette("Teal", Colors.teal.value),
  ColorPalette("Green", Colors.green.value),
  ColorPalette("Lightgreen", Colors.lightGreen.value),
  ColorPalette("Lime", Colors.lime.value),
  ColorPalette("Yellow", Colors.yellow.value),
  ColorPalette("Amber", Colors.amber.value),
  ColorPalette("Orange", 0xffffa95f),
  ColorPalette("Deeporange", Colors.deepOrange.value),
  ColorPalette("Brown", Colors.brown.value),
  // ColorPalette("Black", Colors.black.value),
  ColorPalette("WindRed", 0xffffa190),
  ColorPalette("WindRedLight", 0xffffeeee),
  ColorPalette("WindGreen", 0xff61d2a1),
  ColorPalette("WindGreenLight", 0xff9df3c4),
  ColorPalette("White", 0xffffffff),
];

class ColorBook {
  static const Color red = Color(0xffffa190);
  static const Color redDeep = Color(0xffd90000);
  static const Color redDeepLighter = Color(0xffe64d27);
  static const Color lightGreen = Color(0xff9df3c4);
  static const Color green = Color(0xff61d2a1);
  static const Color lightRed = Color(0xffffeeee);

  static const Color yellowVIPLight = Color(0xffEDCC99);
  static const Color yellowVIP = Color(0xffE5BE81);

  static const Color yellowLight = Color(0xffe0c85e);
  static const Color yellowLightPlus = Color(0xffe8d066);

  static const Color brownVIPLight = Color(0xffEDCC99);
  static const Color brownVIP = Color(0xff634f17);

  static const Color orangeVIPLight = Color(0xffff8e59);
  static const Color orangeVIP = Color(0xffff8a1c);
  static const Color pink = Color(0xffec407a);
  static const Color greenMaerDai = Color(0xff009c8f);
  static const Color redGugong = Color(0xffba1436);
  static const Color orangeEnergy = Color(0xffff6836);
  static const Color blueNavy = Color(0xff443f69);
  static const Color greenYandai = Color(0xff295c49);
  static const Color blueShanhu = Color(0xff2b9ecc);
  static const Color deepBlue = Color(0xff2a43fa);
  static const Color purpleNight = Color(0xff9415b0);
  static const Color greyNapolun = Color(0xff303030);
  static const Color black = Color(0xff000000);

  // 玫瑰灰
  static const Color Redmeiguihui = Color(0xff4b2e2b);
  // 鹅血石红
  static const Color Redexueshi = Color(0xffab372f);
  // 淡栗综
  static const Color Browndanli = Color(0xff673424);
  // 珠母灰
  static const Color GreyZhumu = Color(0xff64483d);
  // 鹦鹉绿
  static const Color GreenYingwu = Color(0xff5bae23);
  // 荷叶绿
  static const Color GreenHeye = Color(0xff1a6840);
  // 秋波蓝
  static const Color Blueqiubo = Color(0xff8abcd1);

  static const Color lightBrown = Color(0xffCC9999);
  static const Color blueGrey = Color(0xff666699);
  static const Color lightPink = Color(0xffFF6666);
  static const Color lightPurple = Color(0xff993399);
  static const Color softPurple = Color(0xff9999CC);

  static const Color glassColor = Color(0x55EEE5E9);
  static const Color borderColor = Color(0xFFFFFFFF);

  // Wechat link color
  static const Color wechatLink = Color(0xff576b95);
}

// ranges from 0.0 to 1.0

Color darken(Color color, [double amount = .1]) {
  assert(amount >= 0 && amount <= 1);
  final hsl = HSLColor.fromColor(color);
  final hslDark = hsl.withLightness((hsl.lightness - amount).clamp(0.0, 1.0));

  return hslDark.toColor();
}

Color lighten(Color color, [double amount = .1]) {
  assert(amount >= 0 && amount <= 1);
  final hsl = HSLColor.fromColor(color);
  final hslLight = hsl.withLightness((hsl.lightness + amount).clamp(0.0, 1.0));
  return hslLight.toColor();
}

Color lightenColor(Color color, double amount) {
  assert(
      amount >= 0.0 && amount <= 1.0, 'Amount should be between 0.0 and 1.0');
  final double opacity = color.opacity;
  final int red = color.red + ((255 - color.red) * amount).round();
  final int green = color.green + ((255 - color.green) * amount).round();
  final int blue = color.blue + ((255 - color.blue) * amount).round();
  return Color.fromRGBO(red, green, blue, opacity);
}

List<Color> getGradientColors(Color baseColor,
    {double delta = 10.0, int alpha = -1, bool lighten = false}) {
  HSLColor hslColor = HSLColor.fromColor(baseColor);
  HSLColor lighterColor = hslColor.withHue((hslColor.hue + delta) % 360.0);
  HSLColor darkerColor = hslColor.withHue((hslColor.hue - delta) % 360.0);
  if (lighten) {
    return [
      lightenColor(darkerColor.toColor(), 0.5),
      lightenColor(baseColor, 0.5),
      lightenColor(lighterColor.toColor(), 0.5),
    ];
  }
  return [
    alpha > 0 ? darkerColor.toColor().withAlpha(alpha) : darkerColor.toColor(),
    alpha > 0 ? baseColor.withAlpha(alpha) : baseColor,
    alpha > 0 ? lighterColor.toColor().withAlpha(alpha) : lighterColor.toColor()
  ];
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class SpecialColorTransparent extends Color {
  const SpecialColorTransparent() : super(0x00000000);

  @override
  int get alpha => 0xFF;
}

String colorToHex(Color color) =>
    color.value.toRadixString(16).toUpperCase().padLeft(8, '0');
final colorBooks = <ColorPaletteHex>[
  // 红系
  ColorPaletteHex("朱砂绛", "#C62828"), // 传统矿物颜料色
  ColorPaletteHex("胭脂醉", "#D50000"), // 唐代女子妆容红
  ColorPaletteHex("枫丹白露", "#B71C1C"), // 法国宫殿装饰红
  ColorPaletteHex("茜色染", "#E53935"), // 古代植物染料红

  // 橙系
  ColorPaletteHex("金乌曜", "#FF6D00"), // 神话太阳神鸟色
  ColorPaletteHex("琥珀光", "#FFAB00"), // 树脂化石暖橙色

  // 黄系
  ColorPaletteHex("缃叶初", "#FDD835"), // 桑叶嫩黄色
  ColorPaletteHex("琉璃黄", "#FFEB3B"), // 古代玻璃器皿色

  // 绿系
  ColorPaletteHex("松花酿", "#43A047"), // 松花粉青绿色
  ColorPaletteHex("碧潭影", "#00796B"), // 深潭倒影墨绿
  ColorPaletteHex("翡翠冷", "#1B5E20"), // 缅甸翡翠深绿
  ColorPaletteHex("竹月青", "#689F38"), // 月光竹影色

  // 蓝系
  ColorPaletteHex("青花瓷", "#2962FF"), // 元明青花钴蓝色
  ColorPaletteHex("沧海泪", "#01579B"), // 深海神秘蓝
  ColorPaletteHex("天霁蓝", "#4FC3F7"), // 雨后晴空蓝
  ColorPaletteHex("珐琅彩", "#0091EA"), // 景泰蓝釉色

  // 紫系
  ColorPaletteHex("紫气东来", "#7B1FA2"), // 道家祥瑞紫色
  ColorPaletteHex("暮山紫", "#6A1B9A"), // 王勃《滕王阁序》色
  ColorPaletteHex("丁香结", "#BA68C8"), // 李商隐诗意象

  // 中性色
  ColorPaletteHex("玄墨", "#212121"), // 徽墨漆黑
  ColorPaletteHex("素雪", "#F5F5F5"), // 未染丝绸白
  ColorPaletteHex("鸦青", "#37474F"), // 乌鸦羽毛青灰

  // 特殊色系
  ColorPaletteHex("敦煌赭", "#BF360C"), // 壁画矿物赭石
  ColorPaletteHex("釉里红", "#D32F2F"), // 元代瓷器釉色
  ColorPaletteHex("孔雀翎", "#009688"), // 孔雀尾羽蓝绿
  ColorPaletteHex("玛瑙红", "#C2185B"), // 宝石矿物色
  ColorPaletteHex("蟹壳青", "#795548"), // 宋代瓷器釉色

  // 现代流行色
  ColorPaletteHex("赛博紫", "#7C4DFF"), // 霓虹灯效紫
  ColorPaletteHex("极光绿", "#64DD17"), // 北极光荧光绿
  ColorPaletteHex("钴晶蓝", "#304FFE"), // 未来科技蓝
  ColorPaletteHex("珊瑚橘", "#FF5252"), // Pantone年度色

  // 中间过渡色
  ColorPaletteHex("藕荷浅", "#EDE7F6"), // 浅灰紫过渡色
  ColorPaletteHex("秋香褐", "#8D6E63"), // 黄褐过渡色
  ColorPaletteHex("月白灰", "#E0E0E0"), // 冷调浅灰
  ColorPaletteHex("黛蓝染", "#283593"), // 深蓝过渡紫

  // 文化意象扩展
  ColorPaletteHex("敦煌飞天", "#FFA000"), // 壁画飘带橙
  ColorPaletteHex("千里江山", "#558B2F"), // 王希孟画卷青
  ColorPaletteHex("钧窑紫霞", "#8E24AA"), // 宋代窑变釉色
  ColorPaletteHex("和田玉白", "#E0F2F1"), // 羊脂玉暖白
  ColorPaletteHex("青铜饕餮", "#616161"), // 商周青铜器色

  // 自然意象
  ColorPaletteHex("火山熔岩", "#DD2C00"), // 炽热岩浆红
  ColorPaletteHex("极地冰蓝", "#84FFFF"), // 冰川透蓝色
  ColorPaletteHex("沙漠黄昏", "#FF8A65"), // 撒哈拉落日色
  ColorPaletteHex("雨林秘境", "#2E7D32"), // 热带植被绿

  // 金属色系
  ColorPaletteHex("陨铁灰", "#455A64"), // 太空金属灰
  ColorPaletteHex("秘银", "#E0E0E0"), // 奇幻金属银
  ColorPaletteHex("赤铜", "#D84315"), // 氧化铜红色
  ColorPaletteHex("钛金", "#CFD8DC"), // 现代合金色

  // 补充色
  ColorPaletteHex("妃色", "#F48FB1"), // 古代浅妃红
  ColorPaletteHex("天水碧", "#80DEEA"), // 南唐宫廷色
  ColorPaletteHex("胭脂水", "#F06292"), // 清代瓷器釉色
  ColorPaletteHex("苍葭", "#9CCC65") // 《诗经》芦苇绿
];
