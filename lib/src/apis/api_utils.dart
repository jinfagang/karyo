import 'dart:convert';

import 'package:http/http.dart' as http;

Future<dynamic> getJsonDataFromUrl(String url) async {
  final response = await http.get(
    Uri.parse(url),
    headers: {'Accept-Charset': 'utf-8'},
  );

  if (response.statusCode == 200) {
    final jsonData = json.decode(utf8.decode(response.bodyBytes));
    return jsonData;
  } else {
    return null;
  }
}
