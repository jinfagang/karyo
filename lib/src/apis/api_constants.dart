class API {
  // uranus APIs
  // static const String DOMIN = "db.manaai.cn";
  // static const String DOMIN = "db2.manaai.cn";
  static const String DOMIN = "ark.manaai.cn";
  // static const String DOMIN = "192.168.18.145";
  // static const String DOMIN = "localhost";
  // static const String DOMIN = "192.168.100.153";
  // static const String DOMIN = "192.168.125.28";
  // static const String BASE_URL = "http://$DOMIN:9000";
  static const String BASE_URL = "http://$DOMIN";
  static const String BASE_URL_HTTPS = "https://$DOMIN";
  static const String MQTT_URL = DOMIN;
  static const String URANUS_URL_G = "https://g.manaai.cn";

  // New server can not open 9000, it will process by ningx to forward.
  static const String URANUS_URL = "https://$DOMIN";

  static const String WS_URL = "ws://$DOMIN/v1/ws";
  static const String WS2_URL = "ws://$DOMIN/v1/ws2";

  static const String MANA_HOST = "https://manaai.cn";

  static const String LOGIN_URL = "$URANUS_URL/api/v2/users_login";
  static const String USERS_URL = "$URANUS_URL/api/v2/users";
  static const String USERS_CHANGE_PWD_URL = "$URANUS_URL/api/v2/users_change_pwd";
  static const String USERS_DELETE_URL = "$URANUS_URL/api/v2/users_delete";
  // with invite code, diable for now
  static const String USERS2_URL = "$URANUS_URL/api/v2/users2";
  static const String USERS_EDIT = "$URANUS_URL/api/v2/users_edit";

  static const String FIND_USER_URL = "$URANUS_URL/api/v2/find_user";
  static const String FIND_USER_BY_ADDR =
      "$URANUS_URL/api/v2/find_user_by_addr";
  static const String USER_REGISTER_TMP =
      '$URANUS_URL/api/v2/users_register_tmp';

  static const String SET_URANUS_VIPS = "$URANUS_URL/api/v2/open_vip";
  static const String SET_URANUS_QUOTA = "$URANUS_URL/api/v2/add_quota";
  static const String GET_ALL_USERS_URANUS = "$URANUS_URL/api/v2/get_all_users";
  static const String apiUploadImage = "$URANUS_URL/api/v2/upload_image";

  // pgyer's API updates url
  // POST: _api_key: 81b426570d1330dba8bf6512ea830cbc	String, appKey: 17148e6acd9775d77da183dc549bc9ed, buildVersion, buildBuildVersion
  // curl -d "_api_key=81b426570d1330dba8bf6512ea830cbc&appKey=17148e6acd9775d77da183dc549bc9ed&buildBuildVersion=1" https://www.pgyer.com/apiv2/app/check
  static const String APP_UPDATE_CHECK =
      "https://www.pgyer.com/apiv2/app/check";
  static const String APP_CHECK_UPDATE_JSON_URL =
      "https://db.manaai.cn/allplatform_ver.json";
  static const String APP_CHECK_UPDATE_JSON_URL_Tianmu =
      "https://db2.manaai.cn/update_info_tianmu.json";
  static const String APP_CHECK_UPDATE_JSON_URL_Hydra =
      "https://db.manaai.cn/update_info_aichat.json";
  static const String FREECHAT_MUYU = "$URANUS_URL/update_freechat.json";

  // Tasks sync API
  static const String UPDATE_TASKS_URL =
      "$URANUS_URL/api/v2/daybreak_update_tasks";
  static const String UPDATE_PROJECT_URL =
      "$URANUS_URL/api/v2/daybreak_update_project";
  static const String LOAD_TASKS_UPDATE_URL =
      "$URANUS_URL/api/v2/daybreak_load_tasks_update";
  // update single tasks
  static const String GET_TASKS_UPDATE_URL =
      "$URANUS_URL/api/v2/daybreak_update_tasks";
  static const String DELETE_TASKS_URL =
      "$URANUS_URL/api/v2/daybreak_delete_tasks";
  static const String DELETE_PROJECTS_URL =
      "$URANUS_URL/api/v2/daybreak_delete_projects";
  static const String SYNC_TASKS_SIM_URL =
      "$URANUS_URL/api/v2/daybreak_sync_tasks_sim";
  static const String SYNC_PROJECTS_URL =
      "$URANUS_URL/api/v2/daybreak_sync_projects";
  static const String SYNC_PROJECTS_SIM_URL =
      "$URANUS_URL/api/v2/daybreak_sync_projects_sim";
  static const String SYNC_LABELS_URL =
      "$URANUS_URL/api/v2/daybreak_sync_labels";

  static const String GET_SHARED_TASKS_URL =
      "$URANUS_URL/api/v2/daybreak_get_tasks_shared";
  static const String SET_SHARED_TASKS_URL =
      "$URANUS_URL/api/v2/daybreak_set_tasks_shared";

  static const String ADD_TASKS_LOOK_OR_LIKE =
      "$URANUS_URL/api/v2/daybreak_tasks_look_or_like";

  static const String GET_TASKS_USERS =
      "$URANUS_URL/api/v2/daybreak_get_tasks_users";
  static const String ADD_USER_TO_TASK =
      "$URANUS_URL/api/v2/daybreak_add_user_to_tasks";
  static const String GET_SHARED_IDEAS_URL =
      "$URANUS_URL/api/v2/daybreak_get_ideas_shared";

  ///////////////// User related API /////////////////////
  static const String FIND_USER = "$URANUS_URL/api/v2/find_user";

  //////////// Tasks comments related API /////////////
  static const String COMMENT_TO_TASKS =
      "$URANUS_URL/api/v2/daybreak_comment_to_tasks";
  static const String GET_TASKS_COMMENTS =
      "$URANUS_URL/api/v2/daybreak_get_tasks_comments";
  //////////// taks comments end ///////////////////

  static const String URANUS_STATISTICS = "$URANUS_URL/api/v2/uranus_statistic";
  static const String DAYBREAK_GET_LEADERBOARD =
      "$URANUS_URL/api/v2/daybreak_get_leaderboard";

  ////////// API for create APIKey //////////////////
  static const String URANUS_APIKEY_CREATE = "$URANUS_URL/api/v2/apikey_new";
  static const String URANUS_APIKEY_GET = "$URANUS_URL/api/v2/apikey_get";
  static const String URANUS_APIKEY_REVOKE = "$URANUS_URL/api/v2/apikey_revoke";

  ///////////////////// Below APIS not using for now ///////////////////////
  static const String testContactsUrl = "$URANUS_URL/api/v2/test_contacts";
  static const String searchUserUrl = "$URANUS_URL/api/v2/users";
  static const String sessionsUrl = "$URANUS_URL/api/v2/sessions";
  static const String suggestUserUrl = "$URANUS_URL/api/v2/suggest_users";

  static const String globalSessionsUrl = "$URANUS_URL/api/v2/sessions";
  static const String globalTimelineUrl = "$URANUS_URL/api/v2/timeline";

  //////////// Colibri related API ///////////////////////
  static const String COLIBRI_GET_LOGIN_INFOS =
      "$URANUS_URL/api/v2/get_login_infos";

  static const String COLI_DEL_USER = "$URANUS_URL/api/v2/del_user";
  static const String COLI_GET_ALL_USERS = "$URANUS_URL/api/v2/get_all_users";
  static const String COLI_GET_FRIENDS = "$URANUS_URL/api/v2/friends";
  static const String COLI_BAND_USER = "$URANUS_URL/api/v2/band_user";
  static const String COLI_LIMIT_USER = "$URANUS_URL/api/v2/limit_user";

  static const String globalImagesUploadUrl =
      "http://$DOMIN/api/v1/image_upload_outside";
// vendor API
  static const String globalImageUploadSMSUrl = "https://sm.ms/api/upload";

  static const String globalTimeLineCommentUrl =
      "$URANUS_URL/api/v2/timeline_comment";
  static const String globalTimeLineCommentReplyUrl =
      "$URANUS_URL/api/v2/timeline_comment_reply";
  String globalTimeLineCommentLikeUrl =
      "$URANUS_URL/api/v2/timeline_comment_like";

// post or get friends, post: request or approval
  static const String globalFriendsUrl = "$URANUS_URL/api/v2/friends";
  static const String globalFriendsRequestUrl =
      "$URANUS_URL/api/v2/friends_requests";
  static const String globalIsFriendsUrl = "$URANUS_URL/api/v2/is_friends";

// Adding create group relate apis
  static const String globalCreateGroupUrl = "$URANUS_URL/api/v2/create_group";
  static const String globalGetAllGroupUrl = "$URANUS_URL/api/v2/all_groups";
  static const String globalGetGroupUrl = "$URANUS_URL/api/v2/group";
  static const String globalGetGroupAllMembersUrl =
      "$URANUS_URL/api/v2/get_group_subscribers";
  static const String globalAddMemberToGroupUrl =
      "$URANUS_URL/api/v2/add_subscriber_to_group";
  static const String globalAllOpenGroups =
      "$URANUS_URL/api/v2/all_open_groups";

  static const String getActivityUsers =
      "$URANUS_URL/api/v2/get_activity_users";

// urls for anonymous post and user anonymous info
// post, get, del
  static const String globalAnonymousPost = "$URANUS_URL/api/v1/anonymous";
  static const String globalAnonymousComments =
      "$URANUS_URL/api/v1/anonymous_comments";
  static const String globalAnonymousCommentsPost =
      "$URANUS_URL/api/v1/anonymous_comment";
  static const String globalAnonymousLike = "$URANUS_URL/api/v1/anonymous_like";
  static const String globalAnonymousUnLike =
      "$URANUS_URL/api/v1/anonymous_unlike";
  static const String globalAnonymousReplyComments =
      "$URANUS_URL/api/v1/anonymous_comment_reply";
  static const String globalUpdateUserAnonymousInfo =
      "$URANUS_URL/api/v1/update_user_anonymous_info";

  static const String gGetAllUsersUrl = "$URANUS_URL/api/v2/get_all_users";
  static const String gDelUserUrl = "$URANUS_URL/api/v2/del_user";
  static const String gBandUserUrl = "$URANUS_URL/api/v2/band_user";
  static const String gLimitUserUrl = "$URANUS_URL/api/v2/limit_user";
  static const String gTimelineUserAddrUrl =
      '$URANUS_URL/api/v2/timeline_user_addr';

  ////////////////////////// APIS for Nirvana Platform ////////////////////
  static const String MANA_USERS_GET = MANA_HOST + "/api/v2/get_all_users";
  static const String MANA_USERS_URL = MANA_HOST + "/api/v2/users";
  static const String MANA_USERS_LOGIN = MANA_HOST + "/api/v2/login";
  static const String URANUS_STATISTIC =
      URANUS_URL + "/api/v2/uranus_statistic";

  // set vip
  static const String SET_MANA_VIP = MANA_HOST + "/api/v2/set_user_manavip";
  static const String USER_GET_BY_ID = MANA_HOST + "/api/v2/get_user_by_id";
  static const String MANA_USERS_SUMMARY = MANA_HOST + "/api/v2/users_summary";
  // search user
  static const String SEARCH_MANA_USER =
      MANA_HOST + "/api/v2/search_user_email";
}
