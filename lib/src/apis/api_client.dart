import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:karyo/karyo.dart';
import 'dart:async';

class CommonAPIClient {
  var dio = CommonAPIClient.createDio();

  static Dio createDio() {
    var options = BaseOptions(
      baseUrl: API.URANUS_URL,
      connectTimeout: 10000,
      receiveTimeout: 100000,
      contentType: "json",
    );
    return Dio(options);
  }

  // MANA platform
  Future<ManaUser?> getMANAUser(String token) async {
    var _url = '${API.MANA_USERS_URL}?token=$token';
    http.Response response = await http.get(Uri.parse(_url));
    var rpJson = json.decode(response.body);
    debugPrint(rpJson);
    if (rpJson['status'] == 'success') {
      var _u = ManaUser.fromMap(rpJson['data']);
      return _u;
    } else {
      return null;
    }
  }

  Future<List<APIkeyModel>> getAllApiKeys(String token) async {
    var _url = '${API.URANUS_APIKEY_GET}?token=$token';
    http.Response response = await http.get(Uri.parse(_url));
    var rpJson = json.decode(response.body);
    debugPrint(rpJson.toString());
    List<APIkeyModel> apiKeys = [];

    if (rpJson['status'] == 'success') {
      List<dynamic> dataList = rpJson['data'];

      for (var data in dataList) {
        var apiKeyModel = APIkeyModel.fromJson(data);
        apiKeys.add(apiKeyModel);
      }
    }
    return apiKeys;
  }

  Future<List<APIkeyModel>> createApiKey(String token) async {
    var _url = '${API.URANUS_APIKEY_CREATE}?token=$token';
    http.Response response = await http.get(Uri.parse(_url));
    var rpJson = json.decode(response.body);
    debugPrint(rpJson.toString());
    List<APIkeyModel> apiKeys = [];

    if (rpJson['status'] == 'success') {
      List<dynamic> dataList = rpJson['data'];

      for (var data in dataList) {
        var apiKeyModel = APIkeyModel.fromJson(data);
        apiKeys.add(apiKeyModel);
      }
    }
    return apiKeys;
  }

  Future<ManaUser?> getManaUserByEmail(String e, String token) async {
    var _url = '${API.SEARCH_MANA_USER}?user_email=${e}&token=$token';
    http.Response response = await http.get(Uri.parse(_url));
    var rpJson = json.decode(response.body);
    debugPrint(rpJson);
    if (rpJson['status'] == 'success' && rpJson['data'].length > 0) {
      var _u = ManaUser.fromMap(rpJson['data']);
      return _u;
    } else {
      return null;
    }
  }

  Future<List<ManaUser>?> getAllUsersMana(
      String token, int pageNum, int perPage) async {
    http.Response response = await http.get(Uri.parse(
        '${API.MANA_USERS_GET}?page_num=${pageNum}&per_page=$perPage&token=$token'));
    var rp = utf8.decode(response.bodyBytes);
    var rpJson = json.decode(rp);

    if (rpJson['status'] == 'success') {
      var _allUsers = ManaUser.allFromResponse(rp);
      return _allUsers;
    } else {
      debugPrint('get mana all users fail----- $rpJson');
      return null;
    }
  }

  Future<bool> setUserManaVip(ManaUser user, String act, String token) async {
    http.Response response = await http.get(Uri.parse(
        '${API.SET_MANA_VIP}?act=${act}&user_id=${user.userID}&token=$token'));
    var rp = utf8.decode(response.bodyBytes);
    // get the response
    var rpJson = json.decode(rp);
    if (rpJson['status'] == 'success') {
      var msg = rpJson['message'];
      var isManaVIP = rpJson['data'];
      debugPrint(isManaVIP);
    } else {
      debugPrint(rpJson);
      var msg = rpJson['message'];
    }
    return true;
  }

  Future<List<User>?> getAllUsersUranus(
      String token, int pageNum, int perPage) async {
    http.Response response = await http.get(Uri.parse(
        '${API.GET_ALL_USERS_URANUS}?token=$token&page_num=$pageNum&per_page=$perPage'));
    var rp = utf8.decode(response.bodyBytes);
    var rpJson = json.decode(rp);
    if (rpJson['status'] == 'success') {
      var _allUsers = User.allFromResponse(response.body);
      debugPrint(_allUsers[0].toString());
      return _allUsers;
    } else {
      debugPrint(rpJson);
      return null;
    }
  }

  /// MANA end
  Future<User?> getUser(String token) async {
    http.Response response =
        await http.get(Uri.parse('${API.USERS_URL}?token=$token'));
    var rpJson = json.decode(response.body);
    if (rpJson['status'] == 'success') {
      var user = User.fromMap(rpJson['data']);
      return user;
    } else {
      return null;
    }
  }

  Future<User?> changeUserPwd(
      String oldPwd, String newPwd, String token) async {
    var formData = FormData.fromMap({
      "old_password": oldPwd,
      'new_password': newPwd,
    });
    Dio dio = Dio();
    Response response = await dio
        .post('${API.USERS_CHANGE_PWD_URL}?token=$token', data: formData);

    var rpJson = response.data;
    if (rpJson['status'] == 'success') {
      var user = User.fromMap(rpJson['data']);
      return user;
    } else {
      var msg = rpJson['msg'];
      showSuccessMessage('密码修改失败：$msg');
      return null;
    }
  }

  Future<User?> deleteUser(String targetUserAddr, String token) async {
    var formData = FormData.fromMap({
      "target_user_addr": targetUserAddr,
    });
    Dio dio = Dio();
    Response response =
        await dio.post('${API.USERS_DELETE_URL}?token=$token', data: formData);

    var rpJson = response.data;
    if (rpJson['status'] == 'success') {
      var user = User.fromMap(rpJson['data']);
      return user;
    } else {
      var msg = rpJson['msg'];
      showSuccessMessage('删除用户失败$msg');
      return null;
    }
  }

  // update user info
  Future<User?> updateUserInfo(
      User newUser, String token, BuildContext context) async {
    FormData formData;
    if (newUser.selectedImgAvatar != null) {
      formData = FormData.fromMap({
        "user_nick_name": newUser.userNickName,
        'user_sign': newUser.userSign,
        'user_city': newUser.userCity,
        'user_birth': newUser.userBirth,
        'user_gender': newUser.userGender,
        'user_avatar':
            await MultipartFile.fromFile(newUser.selectedImgAvatar!.path),
        'user_phone': '',
      });
      debugPrint('${newUser.selectedImgAvatar}');
    } else {
      formData = FormData.fromMap({
        "user_nick_name": newUser.userNickName,
        'user_sign': newUser.userSign,
        'user_city': newUser.userCity,
        'user_birth': newUser.userBirth,
        'user_gender': newUser.userGender,
        'user_phone': '',
      });
    }
    debugPrint(formData.toString());

    Dio dio = Dio();
    Response response =
        await dio.post('${API.USERS_EDIT}?token=$token', data: formData);
    var rpJson = response.data;
    if (rpJson['status'] == 'success') {
      debugPrint("${rpJson['data']}");
      debugPrint("${rpJson['data']['user_avatar_url']}");
      var _u = User.fromMap(rpJson['data']);
      // GlobalSettings.user =
      showSuccessMessage("修改成功，头像重启APP生效");
      return _u;
    } else {
      debugPrint('更新信息失败');
      showSuccessMessage("更新资料失败");
      return null;
    }
  }

  Future<User?> login(
      String username, String password, BuildContext? context) async {
    // call /api/v1/users_login api
    var body = {"user_acc": username, "user_password": password};
    var response = await http.post(Uri.parse(API.LOGIN_URL), body: body);
    var responseJson = json.decode(response.body);
    if (kDebugMode) {
      debugPrint('login response json: $responseJson');
    }
    if (responseJson["status"] == "success") {
      var token = responseJson["data"]["token"];
      var u = User.fromMap(responseJson['data']);
      // debugPrint(token);
      GlobalSettings.user = u;
      SharedPreferenceUtil.save(SharedPreferenceUtil.KEY_TOKEN, token);
      // showToast(context, "登录成功，欢迎 ${u.userNickName}", COLOR_PRIMARY);
      return u;
    } else {
      if (context != null) {
        showErrorMessage(
          '登录失败，$username $password ${responseJson['msg']}',
        );
      }
      return null;
    }
  }

// search user
  Future<User?> searchUser(String userAcc, String token) async {
    http.Response response = await http
        .get(Uri.parse('${API.FIND_USER}?token=$token&user_acc=$userAcc'));
    var rpJson = json.decode(response.body);
    if (rpJson['status'] == 'success') {
      var user = User.fromMap(rpJson['data']);
      return user;
    } else {
      return null;
    }
  }

  // register
  Future<User?> register(String username, String nickname, String password,
      BuildContext context) async {
    FormData _formData = new FormData.fromMap({
      "user_acc": username,
      "user_nick_name": nickname,
      'user_password': password,
    });

    Dio dio = Dio();
    Response response = await dio.post(API.USERS_URL, data: _formData);
    var rpJson = response.data;
    if (kDebugMode) {
      debugPrint("register response data: ${rpJson}");
    }
    if (rpJson['code'] == 200) {
      if (rpJson['status'] == 'success') {
        // login success, save values, and jump to home
        var token = rpJson['data']['token'];
        SharedPreferenceUtil.save(SharedPreferenceUtil.KEY_TOKEN, token);
        showSuccessMessage(
          "注册成功！你的登录帐号号为: $username",
        );
        if (kDebugMode) {
          debugPrint("注册成功！你的登录帐号号为: $username");
        }

        var user = login(username, password, context);
        return user;
      } else {
        showErrorMessage("注册失败");
      }
    } else if (rpJson['code'] == 409) {
      showErrorMessage("该帐号已经存在，请更换一个，如果这是您的帐号，请直接登录");
    } else {
      showErrorMessage("登录失败： ${rpJson['msg']}");
    }
    return null;
  }

  Future<User?> registerWithInviteCode(String username, String nickName,
      String password, String inviteCode, BuildContext context) async {
    FormData formData = FormData.fromMap({
      "user_acc": username,
      "user_nick_name": nickName,
      'user_password': password,
      "invite_code": inviteCode,
    });

    Dio dio = Dio();
    if (kDebugMode) {
      debugPrint("request register: ${API.USERS_URL}");
    }
    // Response response = await dio.post(API.USERS2_URL, data: _formData);
    Response response = await dio.post(API.USERS_URL, data: formData);
    var rpJson = response.data;
    if (rpJson['code'] == 200) {
      if (rpJson['status'] == 'success') {
        // login success, save values, and jump to home
        var token = rpJson['data']['token'];
        SharedPreferenceUtil.save(SharedPreferenceUtil.KEY_TOKEN, token);
        showSuccessMessage("注册成功！你的登录帐号号为: $username");
      } else {
        showErrorMessage("注册失败");
      }
    } else if (rpJson['code'] == 409) {
      showErrorMessage("该帐号已经存在，请更换一个，如果这是您的帐号，请直接登录");
    } else {
      showErrorMessage(
        "注册失败： ${rpJson['msg']}",
      );
    }
    return null;
  }

  Future<bool> registerNormal(String username, String password,
      String inviteCode, BuildContext context) async {
    FormData formData = FormData.fromMap({
      "user_acc": username,
      "user_nick_name": 'DB大玩家',
      'user_password': password,
      "invite_code": inviteCode,
    });

    Dio dio = Dio();
    if (kDebugMode) {
      debugPrint("request register: ${API.USERS_URL}");
    }
    // Response response = await dio.post(API.USERS2_URL, data: _formData);
    Response response = await dio.post(API.USERS_URL, data: formData);
    var rpJson = response.data;
    if (rpJson['code'] == 200) {
      if (rpJson['status'] == 'success') {
        // login success, save values, and jump to home
        var token = rpJson['data']['token'];
        SharedPreferenceUtil.save(SharedPreferenceUtil.KEY_TOKEN, token);
        showSuccessMessage(
          "注册成功！你的登录帐号号为: $username",
        );
        return true;
      } else {
        showErrorMessage(
          "注册失败",
        );
      }
    } else if (rpJson['code'] == 409) {
      showErrorMessage(
        "该帐号已经存在，请更换一个，如果这是您的帐号，请直接登录",
      );
    } else {
      showErrorMessage("注册失败： ${rpJson['msg']}");
    }
    return false;
  }

  static request(String url, Map<String, dynamic> params) async {
    Dio dio = new Dio(new BaseOptions(responseType: ResponseType.json));
    Response response;
    response = await dio.get(url, queryParameters: params);
    return response.data is String ? jsonDecode(response.data) : response.data;
  }

  // update file
  Future<Map<String, dynamic>> uploadFileFromPath(String endpointUrl,
      String filePath, Function(int count, int? total) onSendProgress) async {
    var formData = FormData.fromMap({
      'file': await MultipartFile.fromFile(filePath),
      // 'user_phone': '',
    });
    try {
      Response response = await dio.post(
        endpointUrl,
        data: formData,
        onSendProgress: onSendProgress,
      );
      return response.data;
    } catch (e) {
      return {
        "status": "error",
        "content": "got ${e.toString()} when get file."
      };
    }
    // debugdebugPrint(response.data.toString());
  }

  // upload image file to server
  Future<String> uploadImageFile({
    required File imageFile,
    required String appUsage,
    required String blurHash,
    String? uuid,
  }) async {
    try {
      String uploadUrl = API.apiUploadImage; // Replace with your upload URL
      Dio dio = Dio();

      FormData formData = FormData();

      // Add image file
      formData.files.add(MapEntry(
        'image_file',
        await MultipartFile.fromFile(
          imageFile.path,
        ),
      ));

      formData.fields.addAll([
        MapEntry('app_usage', appUsage),
        MapEntry('blur_hash', blurHash),
        MapEntry('token', GlobalSettings.token ?? ''),
      ]);

      if (uuid != null) {
        formData.fields.add(MapEntry('uuid', uuid));
      }

      // Send POST request
      Response response = await dio.post(
        uploadUrl,
        data: formData,
        options: Options(
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        ),
      );

      // Check response status
      if (response.statusCode == 200) {
        // Return image URL or file path from server
        kLog("[--- uplaoe imag success] ${response.data}");
        return response.data['data'];
      } else {
        kLog("[--- uplaoe imag eerror] ${response.data}");
        throw Exception('Failed to upload image');
      }
    } catch (e) {
      // Handle errors
      print('Error uploading image: $e');
      rethrow; // Rethrow the error for further handling
    }
  }
}
