import 'dart:math';
import 'dart:ui';

import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_displaymode/flutter_displaymode.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:karyo/karyo.dart';
import 'dart:io' show File, Platform;
import 'package:page_transition/page_transition.dart';

// import 'package:package_info/package_info.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sheet/route.dart' as sheet;
import 'package:sheet/sheet.dart' show SheetFit;
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/foundation.dart' show defaultTargetPlatform, kIsWeb;
import 'package:dio/dio.dart';
import 'package:file_saver/file_saver.dart';
import 'dart:io';
import 'package:page_transition/page_transition.dart';
import 'package:file_picker/file_picker.dart';
import 'package:path/path.dart' as p;

String getFontNameBySys() {
  if (isOnMobile()) {
    return FONT_NAME;
  } else {
    return FALL_BACK_FONT_NAME_EMOJI;
  }
}

buildAppBarTitle(
  String title,
  color,
  BuildContext context,
) {
  return Row(
    children: [
      Text(
        title,
        style: NORMAL_BOLD_TXT_STYLE.copyWith(color: color),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

buildAppBarTitle2(
  String title,
  BuildContext context,
) {
  return Text(title, style: NORMAL_BOLD_TXT_STYLE);
}

buildBackButton(BuildContext context, Color color, {Function()? onPressed}) {
  return HoverableButton(
    padding: const EdgeInsets.all(0),
    child: Icon(
      FluentIcons.chevron_left_24_filled,
      color: color,
      size: 26,
    ),
    onPressed: onPressed == null
        ? () {
            Navigator.pop(context);
          }
        : onPressed,
  );
}

Widget buildFlexibleBlurSpace({bool isBlur = false}) {
  if (isBlur) {
    return ClipRect(
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 16, sigmaY: 26),
        child: Container(color: Colors.transparent),
      ),
    );
  } else {
    return DecoratedBox(
        decoration: BoxDecoration(
            color: isDarkModeOnNoContext()
                ? PlatformTheme.darkSettingSectionColor
                : PlatformTheme.lightSettingSectionColor,
            boxShadow: <BoxShadow>[
          BoxShadow(
            offset: Offset(0, 2.0),
            color: isDarkModeOnNoContext()
                ? Colors.grey.shade900
                : Colors.grey.shade200,
            blurRadius: 3,
          )
        ]));
  }
}

Widget buildFlexibleGradientColor(BuildContext context) {
  return Container(
      decoration: BoxDecoration(
          color: isDarkModeOnContext(context) ? Colors.black : null,
          gradient: isDarkModeOnContext(context)
              ? null
              : LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [
                      isOnMobile() ? 0.45 : 0.35,
                      isOnMobile() ? 0.45 : 0.6,
                      0.75,
                    ],
                  colors: [
                      Color.fromARGB(255, 234, 239, 249),
                      Color.fromARGB(255, 255, 244, 243),
                      Colors.grey.shade100
                    ]),
          boxShadow: <BoxShadow>[]));
}

buildBackButton3(BuildContext context,
    {Color? color,
    Function()? onPressed,
    bool isBlur = false,
    bool isBk = false,
    Widget? rightWidget = null}) {
  color ??= tClrPrimary(context);

  if (!isOnMobile()) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        HoverableButton(
          padding: EdgeInsets.zero,
          minWidth: 35,
          borderRadius: BorderRadius.circular(8),
          accentColor: isBk ? color!.withAlpha(10) : Colors.transparent,
          onPressed: onPressed ??
              () {
                Navigator.pop(context);
              },
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                CupertinoIcons.chevron_back,
                color: color,
                size: 24,
              ),
              rightWidget ?? const SizedBox()
            ],
          ),
        ),
        g8,
      ],
    );
  } else {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        isBlur
            ? ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                  child: HoverableButton(
                    // padding: EdgeInsets.all(0),
                    minWidth: 22,
                    accentColor: color!.withAlpha(10),
                    onPressed: onPressed ??
                        () {
                          Navigator.pop(context);
                        },
                    child: Icon(
                      CupertinoIcons.chevron_back,
                      color: color,
                      size: 22,
                    ),
                  ),
                ))
            : HoverableButton(
                padding: const EdgeInsets.all(0),
                minWidth: 26,
                borderRadius: BorderRadius.circular(22),
                accentColor: isBk ? color!.withAlpha(10) : Colors.transparent,
                onPressed: onPressed ??
                    () {
                      Navigator.pop(context);
                    },
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      CupertinoIcons.chevron_back,
                      color: color,
                      // size: 26,
                    ),
                    rightWidget ?? const SizedBox(),
                  ],
                ),
              ),
        rightWidget != null ? g8 : const SizedBox(),
      ],
    );
  }
}

Widget buildDesktopCrossBtn(BuildContext context, Function? onPressed) {
  return HoverableButton(
      padding: const EdgeInsets.all(0),
      accentColor: Colors.transparent,
      onPressed: () async {
        HapticFeedback.lightImpact();
        Navigator.of(context).pop();
        if (onPressed != null) {
          await onPressed();
        }
      },
      borderRadius: BorderRadius.circular(8),
      child: SizedBox(
        height: 30,
        width: 30,
        child: Icon(
          CupertinoIcons.xmark,
          size: 17,
          color: Colors.grey.withAlpha(80),
        ),
      ));
}

buildBkBtnSim(BuildContext context, {Color color = Colors.amber}) {
  return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        TextButton(
          style: getZeroBtnStyle(foregroundColor: color),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                CupertinoIcons.chevron_back,
                color:
                    isDarkModeOnContext(context) ? Colors.white : Colors.black,
                size: 24,
              ),
            ],
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        )
      ]);
}

buildBackButtonCross(BuildContext context, color) {
  return IconButton(
    icon: Icon(
      FluentIcons.dismiss_circle_12_filled,
      color: color,
      size: 25,
    ),
    onPressed: () => Navigator.of(context).pop(),
  );
}

buildCrossButton(BuildContext context) {
  return GestureDetector(
    onTap: () => Navigator.of(context).pop(),
    child: Align(
      alignment: Alignment.topCenter,
      child: Container(
        margin: const EdgeInsets.only(top: 16, left: 8),
        padding: const EdgeInsets.all(6),
        decoration: BoxDecoration(
          color: PlatformTheme.getMainBkColorSection2(context),
          shape: BoxShape.circle,
        ),
        child: Icon(
          CupertinoIcons.clear,
          size: 18,
          color: Colors.grey.shade300,
        ),
      ),
    ),
  );
}

buildCrossButtonLeft(BuildContext context, {EdgeInsets? padding}) {
  return Row(
    mainAxisSize: MainAxisSize.min,
    children: [
      SizedBox(
        width: 22,
        height: 22,
        child: IconButton(
          style: getZeroBtnStyle(),
          onPressed: () => Navigator.of(context).pop(),
          icon: Align(
            alignment: Alignment.center,
            child: Icon(
              CupertinoIcons.clear,
              size: 18,
              color: Colors.grey.shade300,
            ),
          ),
        ),
      )
    ],
  );
}

showToast(BuildContext context, String msg, Color c,
    {Widget icon = const Icon(
      Icons.check,
      color: Colors.white,
    )}) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_SHORT,
    backgroundColor: c,
    textColor: Colors.white,
    timeInSecForIosWeb: 5,
  );
}

void showToastNoContext(String msg, Color c,
    {Widget icon = const Icon(
      Icons.check,
      color: Colors.white,
    )}) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_SHORT,
    backgroundColor: c,
    textColor: Colors.white,
    timeInSecForIosWeb: 5,
  );
}

showSnackbar(GlobalKey<ScaffoldState> scaffoldState, String message,
    {MaterialColor? materialColor}) {
  if (message.isEmpty) return;
  // Find the Scaffold in the Widget tree and use it to show a SnackBar
  // scaffoldState.currentState.showSnackBar(
  //     SnackBar(content: Text(message), backgroundColor: materialColor));
}

// Move to app widgets
Widget buildListTile(Widget leading, Widget title, Widget subtitle,
    Widget tailing, bool roundTop, bool roundBottom, onTap,
    {onTapDown,
    padding = const EdgeInsets.symmetric(horizontal: 6, vertical: 6)}) {
  BorderRadius radius = BorderRadius.zero;
  double _radius = 15;
  if (roundTop) {
    radius = BorderRadius.only(
        topLeft: Radius.circular(_radius), topRight: Radius.circular(_radius));
  }
  if (roundBottom) {
    radius = BorderRadius.only(
        bottomLeft: Radius.circular(_radius),
        bottomRight: Radius.circular(_radius));
  }

  if (roundBottom && roundTop) {
    radius = BorderRadius.only(
        topLeft: Radius.circular(_radius),
        topRight: Radius.circular(_radius),
        bottomLeft: Radius.circular(_radius),
        bottomRight: Radius.circular(_radius));
  }
  return Card(
    elevation: 9,
    shadowColor: Colors.grey.withAlpha(20),
    margin: const EdgeInsets.all(0),
    shape: RoundedRectangleBorder(
      borderRadius: radius,
    ),
    child: InkWell(
        borderRadius: radius,
        onTap: onTap,
        onTapDown: onTapDown,
        child: Padding(
          padding: padding,
          child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                g16,
                leading,
                g16,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[title, g2, subtitle],
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[tailing, g8],
                  ),
                )
              ]),
        )),
  );
}

Widget buildLDoubleListTile(
    Widget left, Widget right, bool roundTop, bool roundBottom, onTap1, onTap2,
    {onTapDown1, onTapDown2}) {
  BorderRadius radius = BorderRadius.zero;
  double _radius = 15;
  if (roundTop) {
    radius = BorderRadius.only(
        topLeft: Radius.circular(_radius), topRight: Radius.circular(_radius));
  }
  if (roundBottom) {
    radius = BorderRadius.only(
        bottomLeft: Radius.circular(_radius),
        bottomRight: Radius.circular(_radius));
  }

  if (roundBottom && roundTop) {
    radius = BorderRadius.only(
        topLeft: Radius.circular(_radius),
        topRight: Radius.circular(_radius),
        bottomLeft: Radius.circular(_radius),
        bottomRight: Radius.circular(_radius));
  }
  return Card(
    elevation: 9,
    shadowColor: Colors.grey.withAlpha(20),
    margin: EdgeInsets.all(0),
    shape: RoundedRectangleBorder(
      borderRadius: radius,
    ),
    child: Padding(
      padding: EdgeInsets.symmetric(horizontal: 6, vertical: 6),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Expanded(
          child: InkWell(
            borderRadius: radius,
            onTap: onTap1,
            onTapDown: onTapDown1,
            child: left,
          ),
        ),
        Expanded(
          child: InkWell(
            borderRadius: radius,
            onTap: onTap2,
            onTapDown: onTapDown2,
            child: right,
          ),
        )
      ]),
    ),
  );
}

Widget buildListTileNoShadow(BuildContext context, Widget leading, Widget title,
    Widget subtitle, Widget tailing, bool roundTop, bool roundBottom, onTap,
    {onTapDown}) {
  BorderRadius radius = BorderRadius.zero;
  double _radius = 8;
  if (roundTop) {
    radius = BorderRadius.only(
        topLeft: Radius.circular(_radius), topRight: Radius.circular(_radius));
  }
  if (roundBottom) {
    radius = BorderRadius.only(
        bottomLeft: Radius.circular(_radius),
        bottomRight: Radius.circular(_radius));
  }

  if (roundBottom && roundTop) {
    radius = BorderRadius.only(
        topLeft: Radius.circular(_radius),
        topRight: Radius.circular(_radius),
        bottomLeft: Radius.circular(_radius),
        bottomRight: Radius.circular(_radius));
  }
  return Card(
      elevation: 0,
      // shadowColor: Colors.grey.withAlpha(30),
      margin: const EdgeInsets.all(0),
      color: Theme.of(context).scaffoldBackgroundColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 6),
        child: InkWell(
            borderRadius: radius,
            onTap: onTap,
            onTapDown: onTapDown,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 6),
              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                g16,
                leading,
                g16,
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      title,
                      g2,
                      Wrap(
                        children: [subtitle],
                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[tailing, g8],
                ),
              ]),
            )),
      ));
}

Widget buildRoundedListTile(
    BuildContext context,
    Widget leading,
    Widget title,
    Widget subtitle,
    Widget tailing,
    bool roundTop,
    bool roundBottom,
    onTap,
    Color color,
    {onTapDown,
    padding = const EdgeInsets.symmetric(horizontal: 16)}) {
  BorderRadius radius = BorderRadius.zero;
  double _radius = 16;
  if (roundTop) {
    radius = BorderRadius.only(
        topLeft: Radius.circular(_radius), topRight: Radius.circular(_radius));
  }
  if (roundBottom) {
    radius = BorderRadius.only(
        bottomLeft: Radius.circular(_radius),
        bottomRight: Radius.circular(_radius));
  }

  if (roundBottom && roundTop) {
    radius = BorderRadius.only(
        topLeft: Radius.circular(_radius),
        topRight: Radius.circular(_radius),
        bottomLeft: Radius.circular(_radius),
        bottomRight: Radius.circular(_radius));
  }
  return Padding(
    padding: padding,
    child: Card(
      elevation: 0,
      margin: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(borderRadius: radius),
      // color: Colors.grey.withAlpha(10),
      color: color,
      // shadowColor: Colors.grey.withAlpha(20),
      child: InkWell(
          borderRadius: radius,
          onTap: onTap,
          onTapDown: onTapDown,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 12),
            child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              g16,
              leading,
              g16,
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    title,
                    g2,
                    Wrap(
                      children: [subtitle],
                    )
                  ],
                ),
              ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.end,
              //   children: <Widget>[, g8],
              // ),
              SizedBox(
                height: 32,
                child: tailing,
              ),
              g8,
            ]),
          )),
    ),
  );
}

Widget buildDivider() {
  return Padding(
    padding: EdgeInsets.symmetric(horizontal: 16),
    child: Container(
      color: Colors.grey.withAlpha(20),
      child: Divider(height: 0.2, color: Colors.grey.withAlpha(25), indent: 32),
    ),
  );
}

bool isAppInDarkMode(BuildContext context) {
  Brightness brightness = MediaQuery.platformBrightnessOf(context);
  return brightness == Brightness.dark;
}

void launchURL(String url) async {
  if (url.isEmpty) return;
  if (await canLaunchUrl(Uri.parse(url))) {
    if (Platform.isAndroid) {
      await launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication);
    } else {
      await launchUrl(Uri.parse(url));
    }
  } else {
    throw 'Could not launch $url';
  }
}

bool isOnMobile() {
  return Platform.isIOS || Platform.isAndroid;
}

bool isOniPhone() {
  return Platform.isIOS;
}

bool isOnAndroid() {
  return Platform.isAndroid;
}

bool isOnLinux() {
  return Platform.isLinux;
}

bool isOnWindows() {
  return Platform.isWindows;
}

bool isOnMacOS() {
  return Platform.isMacOS;
}

bool isDarwin() {
  return Platform.isMacOS || Platform.isIOS;
}

bool isWeb() {
  return Platform.isFuchsia;
}

bool isDarkModeOnGetX(BuildContext context) {
  // var brightness = MediaQuery.of(context).platformBrightness;
  // bool darkModeOn = brightness == Brightness.dark;
  return Get.isDarkMode;
  // return darkModeOn;
}

bool isDarkModeOnNoContext() {
  return SchedulerBinding.instance.platformDispatcher.platformBrightness ==
      Brightness.dark;
}

bool isDarkModeOnContext(BuildContext context, {bool isForcedDark = false}) {
  if (isOnMobile()) {
    // var brightness = MediaQuery.of(context).platformBrightness;
    var brightness = Theme.of(context).brightness;
    bool isDarkMode = brightness == Brightness.dark;
    return isDarkMode || isForcedDark || GlobalSettings.isForceDark;
  } else {
    // PC not support auto-mode, must change dark manually.
    return Theme.of(context).brightness == Brightness.dark;
  }
}

bool isDarkModeOnByTheme(BuildContext context) {
  var brightness = Theme.of(context).brightness;
  bool isDarkMode = brightness == Brightness.dark;
  return isDarkMode;
}

Future<DisplayMode> getCurrentMode() async {
  return await FlutterDisplayMode.active;
}

void goToSheet(BuildContext context, Widget page) {
  if (isOnMobile()) {
    Navigator.of(context).push(
      sheet.CupertinoSheetRoute<void>(
        // backgroundColor: Color.fromARGB(0, 0, 0, 0),
        backgroundColor: Colors.red,
        builder: (BuildContext context) => page,
        fit: SheetFit.loose,
      ),
    );
  } else {
    showGeneralDialogMy(context, page);
  }
}

void goToOrGeneralDialog(BuildContext context, Widget page,
    {bool useRootNavigator = true}) {
  if (isOnMobile()) {
    GoTo(context, page);
  } else {
    showGeneralDialogMy(context, page, useRootNavigator: useRootNavigator);
  }
}

void GoTo(BuildContext context, Widget page,
    {bool isCupertino = false, int duration = 450, int reverseDuration = 250}) {
  if (isCupertino || isOniPhone()) {
    GoToCupertino(context, page, duration, reverseDuration: reverseDuration);
  } else {
    GoToCupertino(context, page, duration, reverseDuration: reverseDuration);

    // Navigator.push(
    //   context,
    //   // MaterialWithModalsPageRoute(builder: (context) {
    //   //   return page;
    //   // }),
    //   PageTransition(
    //     type: PageTransitionType.rightToLeft,
    //     child: page,
    //     curve: Easing.linear,
    //     duration: const Duration(milliseconds: 190),
    //     reverseDuration: const Duration(milliseconds: 150),
    //   ),
    // );
    // PageRouteBuilder(
    //     transitionDuration: const Duration(milliseconds: 400),
    //     reverseTransitionDuration: const Duration(milliseconds: 400),
    //     pageBuilder: (context, animation, secondaryAnimation) => page,
    //     transitionsBuilder:
    //         (context, animation, secondaryAnimation, child) {
    //       return FadeThroughTransition(
    //         animation: animation,
    //         secondaryAnimation: secondaryAnimation,
    //         child: child,
    //       );
    //     }));
  }
}

void GoToLeftRight(BuildContext context, Widget page,
    {bool isCupertino = false}) {
  if (isCupertino) {
    GoToCupertino(context, page, 350);
  } else {
    Navigator.push(
      context,
      // MaterialWithModalsPageRoute(builder: (context) {
      //   return page;
      // }),
      PageTransition(
        type: PageTransitionType.rightToLeft,
        child: page,
      ),
    );
  }
}

void GoToFadeThrough(BuildContext context, Widget page,
    {bool isCupertino = false, int duration = 350}) {
  if (isCupertino) {
    GoToCupertino(context, page, duration);
  } else {
    Navigator.push(
        context,
        //   MaterialWithModalsPageRoute(builder: (context) {
        //     return page;
        //   }),
        // );
        PageRouteBuilder(
            transitionDuration: const Duration(milliseconds: 400),
            reverseTransitionDuration: const Duration(milliseconds: 400),
            pageBuilder: (context, animation, secondaryAnimation) => page,
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              return FadeThroughTransition(
                animation: animation,
                secondaryAnimation: secondaryAnimation,
                child: child,
              );
            }));
  }
}

void GoToFadeThroughTransparent(BuildContext context, Widget page,
    {bool isCupertino = false, int duration = 350}) {
  Navigator.push(
      context,
      PageRouteBuilder(
          opaque: false,
          transitionDuration: const Duration(milliseconds: 400),
          reverseTransitionDuration: const Duration(milliseconds: 400),
          pageBuilder: (context, animation, secondaryAnimation) => page,
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return FadeThroughTransition(
              animation: animation,
              secondaryAnimation: secondaryAnimation,
              child: child,
            );
          }));
}

void GoToCupertino(BuildContext context, Widget page, int duration,
    {int reverseDuration = 250}) {
  Navigator.push(
    context,
    MyCupertinoPageRoute(
      builder: (context) {
        return page;
      },
      // duration: 200,
      // reverseDuration: 150,
      duration: duration,
      reverseDuration: reverseDuration,
    ),
    // CupertinoPageRoute(
    //   builder: (context) {
    //     return page;
    //   },
    //   // duration: 280,
    // ),
    // duration: duration),
  );
}

void goToMaterial(BuildContext context, Widget page, int duration) {
  if (isOniPhone()) {
    GoToCupertino(context, page, duration);
  } else {
    Navigator.push(
      context,
      // MaterialWithModalsPageRoute(builder: (context) {
      //   return page;
      // }),
      PageTransition(
        type: PageTransitionType.rightToLeft,
        child: page,
      ),
    );
  }
}

void GoToPush(BuildContext context, Widget page,
    {bool isCupertino = false, int duration = 350}) {
  if (isCupertino || isOniPhone())
    GoToCupertino(context, page, duration);
  else {
    Navigator.push(
      context,
      // SharedAxisPageRoute(
      FadeThroughPageRoute(
        page: page,
      ),
    );
  }
}

Color tClrPrimary(BuildContext context) {
  return Theme.of(context).colorScheme.primary;
}

Color tClrOnPrimary(BuildContext context) {
  return Theme.of(context).colorScheme.onPrimary;
}

Color tClrPrimaryContainer(BuildContext context) {
  return Theme.of(context).colorScheme.primaryContainer;
}

Color tClrOnPrimaryContainer(BuildContext context) {
  return Theme.of(context).colorScheme.onPrimaryContainer;
}

Color tClrSecondaryContainer(BuildContext context) {
  return Theme.of(context).colorScheme.secondaryContainer;
}

Color tClrOnSecondaryContainer(BuildContext context) {
  return Theme.of(context).colorScheme.onSecondaryContainer;
}

Color tClrSecondary(BuildContext context) {
  return Theme.of(context).colorScheme.secondary;
}

Color tClrOnSecondary(BuildContext context) {
  return Theme.of(context).colorScheme.onSecondary;
}

Color tClrDialogBackground(BuildContext context) {
  return Theme.of(context).dialogBackgroundColor;
}

Color tClrScaffoldBackground(BuildContext context) {
  return Theme.of(context).scaffoldBackgroundColor;
}

Color tClrTertiary(BuildContext context) {
  return Theme.of(context).colorScheme.tertiary;
}

Color tClrOnTertiary(BuildContext context) {
  return Theme.of(context).colorScheme.onTertiary;
}

Color tClrTertiaryContainer(BuildContext context) {
  return Theme.of(context).colorScheme.tertiaryContainer;
}

Color tClrOnTertiaryContainer(BuildContext context) {
  return Theme.of(context).colorScheme.onTertiaryContainer;
}

void initDefaultRefreshRate() async {
  final DisplayMode m = await getCurrentMode();
  await FlutterDisplayMode.setHighRefreshRate();
  debugPrint('已经设置为默认刷新率: $m');
}

Future<String> getAppName() async {
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  return packageInfo.appName;
}

Future<String> getAppVersion() async {
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  return packageInfo.version;
}

Future<String> getBuildNumber() async {
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  return packageInfo.buildNumber;
}

Future<String?> getDeviceIdentifier() async {
  String? deviceIdentifier = "unknown";
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  if (Platform.isAndroid) {
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    deviceIdentifier = androidInfo.device;
  } else if (Platform.isIOS) {
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    deviceIdentifier = iosInfo.identifierForVendor!;
  } else if (kIsWeb) {
    // The web doesnt have a device UID, so use a combination fingerprint as an example
    WebBrowserInfo webInfo = await deviceInfo.webBrowserInfo;
    deviceIdentifier = webInfo.vendor! +
        webInfo.userAgent! +
        webInfo.hardwareConcurrency.toString();
  } else if (Platform.isLinux) {
    LinuxDeviceInfo linuxInfo = await deviceInfo.linuxInfo;
    deviceIdentifier = linuxInfo.machineId!;
  } else if (Platform.isWindows) {
    WindowsDeviceInfo windowsDeviceInfo = await deviceInfo.windowsInfo;
    deviceIdentifier = windowsDeviceInfo.computerName;
  } else if (Platform.isMacOS) {
    MacOsDeviceInfo macosDeviceInfo = await deviceInfo.macOsInfo;
    deviceIdentifier = macosDeviceInfo.computerName + macosDeviceInfo.model;
  }
  return deviceIdentifier;
}

Future<String> getDeviceName() async {
  String deviceIdentifier = "unknown";
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  if (Platform.isAndroid) {
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    deviceIdentifier = androidInfo.manufacturer + androidInfo.model;
  } else if (Platform.isIOS) {
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    deviceIdentifier = iosInfo.name + iosInfo.model;
  } else if (kIsWeb) {
    // The web doesnt have a device UID, so use a combination fingerprint as an example
    WebBrowserInfo webInfo = await deviceInfo.webBrowserInfo;
    deviceIdentifier = webInfo.vendor! +
        webInfo.userAgent! +
        webInfo.hardwareConcurrency.toString();
  } else if (Platform.isLinux) {
    LinuxDeviceInfo linuxInfo = await deviceInfo.linuxInfo;
    deviceIdentifier = linuxInfo.prettyName;
  } else if (Platform.isWindows) {
    WindowsDeviceInfo windowsDeviceInfo = await deviceInfo.windowsInfo;
    deviceIdentifier = windowsDeviceInfo.computerName;
  } else if (Platform.isMacOS) {
    MacOsDeviceInfo macosDeviceInfo = await deviceInfo.macOsInfo;
    deviceIdentifier = macosDeviceInfo.computerName + macosDeviceInfo.model;
  }
  return deviceIdentifier;
}

int getExtendedVersionNumber(String version) {
  if (Platform.isAndroid) {
    return int.parse(version);
  } else {
    List versionCells = version.split('.');
    versionCells = versionCells.map((i) => int.parse(i)).toList();
    return versionCells[0] * 100000 + versionCells[1] * 1000 + versionCells[2];
  }
}

Future<bool> IsAppFirstRun() async {
  var _v = await SharedPreferenceUtil.get(SharedPreferenceUtil.KEY_PRE_VERSION);
  if (isOnMobile()) {
    var _vv = await getBuildNumber();
    debugPrint('----- 本地的: $_v');
    debugPrint('----- 本地的 _vv: $_vv');
    bool _isFirstRun = false;

    if (_v == null) {
      _isFirstRun = true;
    } else {
      int v1Number = getExtendedVersionNumber(_v); // return 10020003
      int v2Number = getExtendedVersionNumber(_vv);
      debugPrint('------ v1n: ${v1Number}, v2n: ${v2Number}');
      if (v2Number > v1Number) {
        // if saved version older than now one, to intro
        _isFirstRun = true;
      }
    }
    SharedPreferenceUtil.save(SharedPreferenceUtil.KEY_PRE_VERSION, _vv);
    return _isFirstRun;
  } else {
    var _vv = await getBuildNumber();
    debugPrint('----- 本地的: $_v');
    debugPrint('----- 本地的 _vv: $_vv');
    bool _isFirstRun = false;

    if (_v == null || _v == '') {
      _isFirstRun = true;
    } else {
      int v1Number = getExtendedVersionNumber(_v); // return 10020003
      int v2Number = getExtendedVersionNumber(_vv);
      debugPrint('------ v1n: ${v1Number}, v2n: ${v2Number}');
      if (v2Number > v1Number) {
        // if saved version older than now one, to intro
        _isFirstRun = true;
      }
    }
    SharedPreferenceUtil.save(SharedPreferenceUtil.KEY_PRE_VERSION, _vv);
    return _isFirstRun;
  }
}

class PageRoutes {
  static const double kDefaultDuration = .35;
  static const Curve kDefaultEaseFwd = Curves.fastOutSlowIn;
  static const Curve kDefaultEaseReverse = Curves.easeOut;

  static Route<T> sharedAxis<T>(Widget page,
      [SharedAxisTransitionType type = SharedAxisTransitionType.scaled,
      double duration = kDefaultDuration]) {
    return PageRouteBuilder<T>(
      opaque: false,
      transitionDuration: Duration(milliseconds: (duration * 1000).round()),
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        return SharedAxisTransition(
          fillColor: Colors.transparent,
          child: child,
          animation: animation,
          secondaryAnimation: secondaryAnimation,
          transitionType: type,
        );
      },
    );
  }
}

Future<String> getDeviceClientUUID(String suffix) async {
  var deviceId = await getDeviceIdentifier();
  var deviceName = await getDeviceName();
  var _clientId =
      "${Platform.operatingSystem}_${deviceName}_${deviceId!}_$suffix";
  return _clientId;
}

String _getRandomString(int length) {
  const _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random(DateTime.now().millisecondsSinceEpoch);
  return String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
}

void CopyToClipboard(String text) {
  ClipboardData data = ClipboardData(text: text);
  Clipboard.setData(data);
}

Future<String?> getTextFromClipboard() async {
  var a = await Clipboard.getData('text/plain');
  if (a != null) {
    return a.text!;
  }
  return null;
}

void lightVibrate() {
  HapticFeedback.lightImpact();
}

Future<void> removeUrlFromCache(String photoUrl) async {
  await CachedNetworkImage.evictFromCache(photoUrl);
}

Color getFileTypeColor(String p) {
  p = p.replaceFirst('.', '');
  switch (p) {
    case 'pdf':
      return Colors.red;
    case 'txt':
      return Colors.blueGrey;
    case 'doc':
    case 'docx':
    case 'pages':
      return Colors.indigo;
    case 'excel':
    case 'keynote':
    case 'csv':
    case 'xlsx':
      return Colors.green;
    default:
      return Colors.purple;
  }
}

IconData getfileIconByExtension(String e) {
  e = e.replaceFirst('.', '');

  switch (e) {
    case 'png':
    case 'jpg':
    case 'jpeg':
      return FontAwesomeIcons.solidFileImage;
    case 'pdf':
      return FontAwesomeIcons.solidFilePdf;
    case 'txt':
      return FontAwesomeIcons.solidFileLines;
    case 'docs':
    case 'doc':
    case 'docx':
      return FontAwesomeIcons.solidFileWord;
    case 'excel':
    case 'xlsx':
    case 'numbers':
      return FontAwesomeIcons.solidFileExcel;
    case 'csv':
      return FontAwesomeIcons.fileCsv;
    case 'ppt':
    case 'pptx':
    case 'keynote':
      return FontAwesomeIcons.solidFilePowerpoint;
    case 'mp4':
    case 'mov':
      return FontAwesomeIcons.solidFileVideo;
    default:
      return Icons.file_copy;
  }
}

enum PlatformType { android, ios, web, macOS, windows, linux, unknown }

PlatformType getPlatform() {
  if (Platform.isAndroid) {
    return PlatformType.android;
  } else if (Platform.isIOS) {
    return PlatformType.ios;
  } else if (Platform.isMacOS) {
    return PlatformType.macOS;
  } else if (Platform.isWindows) {
    return PlatformType.windows;
  } else if (Platform.isLinux) {
    return PlatformType.linux;
  } else if (kIsWeb) {
    return PlatformType.web;
  } else {
    return PlatformType.unknown;
  }
}

String getFontMonospace() {
  String fontFamily;
  switch (defaultTargetPlatform) {
    case TargetPlatform.iOS:
      fontFamily = 'Menlo';
      break;
    case TargetPlatform.android:
      fontFamily = 'monospace';
      break;
    case TargetPlatform.windows:
      fontFamily = 'Consolas';
      break;
    case TargetPlatform.linux:
      // You can use 'monospace' to rely on the system's configuration,
      // or choose a specific font like 'DejaVu Sans Mono'.
      fontFamily = 'monospace';
      break;
    default:
      fontFamily = 'monospace';
  }
  return fontFamily;
}
