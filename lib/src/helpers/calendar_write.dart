import 'dart:io';

import 'package:device_calendar/device_calendar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:karyo/karyo.dart';
import 'package:flutter_timezone/flutter_timezone.dart';

class CalendarWrite {
  late final DeviceCalendarPlugin _deviceCalendarPlugin;
  String _timezone = 'Etc/UTC';
  List<Calendar>? _localCalendars;

  CalendarWrite._internal() {
    _deviceCalendarPlugin = DeviceCalendarPlugin();
  }

  // Static instance of the class
  static final CalendarWrite _instance = CalendarWrite._internal();

  // Factory constructor to return the singleton instance
  factory CalendarWrite() {
    return _instance;
  }

  Future<void> requestPermissions() async {
    final permissionsGranted = await _deviceCalendarPlugin.hasPermissions();
    if (!permissionsGranted.data!) {
      await _deviceCalendarPlugin.requestPermissions();
    }
  }

  Future<void> _retrieveCalendars() async {
    try {
      var permissionsGranted = await _deviceCalendarPlugin.hasPermissions();
      if (permissionsGranted.isSuccess &&
          (permissionsGranted.data == null ||
              permissionsGranted.data == false)) {
        permissionsGranted = await _deviceCalendarPlugin.requestPermissions();
        if (!permissionsGranted.isSuccess ||
            permissionsGranted.data == null ||
            permissionsGranted.data == false) {
          return;
        }
      }

      final calendarsResult = await _deviceCalendarPlugin.retrieveCalendars();
      if (calendarsResult.isSuccess) {
        _localCalendars = calendarsResult.data as List<Calendar>;
      } else {
        debugPrint('Failed to retrieve calendars: ${calendarsResult.errors}');
        _localCalendars = [];
      }
    } on PlatformException catch (e, s) {
      debugPrint('RETRIEVE_CALENDARS: $e, $s');
    }
  }

  Future<List<Calendar>?> getLocalCalendars() async {
    if (_localCalendars == null) {
      await _retrieveCalendars();
    }
    return _localCalendars;
  }

  Future<String> getCurrentTimezone() async {
    try {
      final String timezone = await FlutterTimezone.getLocalTimezone();
      return timezone;
    } catch (e) {
      print("Error getting timezone: $e");
      throw Exception("Failed to get timezone: $e");
    }
  }

  TZDateTime? _combineDateWithTime(
      TZDateTime? date, TimeOfDay? time, Event event) {
    if (date == null) return null;
    var currentLocation = timeZoneDatabase.locations[_timezone];

    final dateWithoutTime = TZDateTime.from(
        DateTime.parse(DateFormat('y-MM-dd 00:00:00').format(date)),
        currentLocation!);

    if (time == null) return dateWithoutTime;
    if (Platform.isAndroid && event.allDay == true) return dateWithoutTime;

    return dateWithoutTime
        .add(Duration(hours: time.hour, minutes: time.minute));
  }

  Future<bool> addEventToDefaultCalendar(
      {required String title,
      required String description,
      required DateTime startTime,
      DateTime? endTime}) async {
    final calendars = await getLocalCalendars();
    if (calendars == null || calendars.isEmpty) {
      debugPrint('No calendars available');
      return false;
    } else {
      kLog("calendars: $calendars");
    }
    return addEventToCalendar(
        calendarId: calendars[0].id!,
        title: title,
        description: description,
        startTime: startTime,
        endTime: endTime);
  }

  Future<bool> addEventToCalendar(
      {required String calendarId,
      required String title,
      required String description,
      required DateTime startTime,
      required DateTime? endTime}) async {
    await requestPermissions();

    final event = Event(calendarId)
      ..title = title
      ..description = description;

    _timezone = await getCurrentTimezone();
    var currentLocation = timeZoneDatabase.locations[_timezone];
    kLog('currentLocation: $currentLocation $_timezone');
    if (currentLocation != null) {
      var startDate0 = TZDateTime.from(startTime, currentLocation);
      event.start = startDate0;

      if (endTime != null) {
        var endTime0 = TZDateTime.from(endTime, currentLocation);
        event.end = endTime0;
      } else {
        endTime = startTime.add(const Duration(hours: 1));
        var endTime0 = TZDateTime.from(endTime, currentLocation);
        event.end = endTime0;
      }
    }

    if (startTime.difference(DateTime.now()).inMinutes > 10) {
      event.reminders = [Reminder(minutes: 10)];
    }
    kLog("==> add event: ${event.toJson()}");
    // {calendarId: 1, eventId: null, eventTitle: 5分钟之后叫我去上厕所, eventDescription: 5分钟之后叫我去上厕所, eventStartDate: 1722160939987, eventStartTimeZone: null, eventEndDate: 1722160939987, eventEndTimeZone: null, eventAllDay: false, eventLocation: null, eventURL: null, availability: BUSY, eventStatus: null}
    final createEventResult =
        await _deviceCalendarPlugin.createOrUpdateEvent(event);

    if (createEventResult?.isSuccess == true) {
      return true;
    } else {
      if (createEventResult?.errors != null) {
        for (var error in createEventResult!.errors) {
          debugPrint('Error Message Code: ${error.errorMessage}');
          debugPrint('Error Code: ${error.errorCode}');
        }
      } else {
        debugPrint('Failed to create event: Unknown error');
      }
      return false;
    }
  }
}
