bool isConsistsOfEmojisA(String input) {
  // Emoji 的 Unicode 范围
  const List<int> emojiRanges = [
    0x1F600, 0x1F64F, // Emoticons
    0x1F300, 0x1F5FF, // Miscellaneous Symbols and Pictographs
    0x1F680, 0x1F6FF, // Transport and Map Symbols
    0x2600, 0x26FF, // Miscellaneous Symbols
    0x2700, 0x27BF, // Dingbats
    0x1F900, 0x1F9FF, // Supplemental Symbols and Pictographs
    0x1F1E0, 0x1F1FF, // Flags (iOS)
  ];

  // 逐个检查字符是否在 Emoji 范围内
  for (int i = 0; i < input.length; i++) {
    final codeUnit = input.codeUnitAt(i);
    bool isEmoji = false;

    for (int j = 0; j < emojiRanges.length; j += 2) {
      if (codeUnit >= emojiRanges[j] && codeUnit <= emojiRanges[j + 1]) {
        isEmoji = true;
        break;
      }
    }

    if (!isEmoji) {
      return false;
    }
  }

  return true;
}
