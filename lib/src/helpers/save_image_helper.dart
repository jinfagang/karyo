import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:karyo/karyo.dart';

///保存图片到本地存储
class SaveImageHelper {
  ///已保存图片的路径
  String? _fileImage;
  Uint8List? _pngBytes;
  GlobalKey? _globalKey;
  bool? _darkModel;

  Future<String> getImagePath(String imageName) async {
    File fileImage = await PathHelper.getShareImage()
        .then((value) => File('$value/$imageName'));
    return fileImage.path;
  }

  ///保存图片
  Future<String?> saveImage(
      BuildContext context, GlobalKey globalKey, String imageName) async {
    ///key相等才有可能是同一图像
    _globalKey = globalKey;
    _fileImage = null;

    RenderRepaintBoundary boundary =
        globalKey.currentContext!.findRenderObject() as RenderRepaintBoundary;

    ///弹框宽度与屏幕宽度比值避免截图出来比预览更大
    ///分辨率通过获取设备的devicePixelRatio以达到清晰度良好
    var image = await boundary.toImage(
        pixelRatio: (MediaQuery.of(context).devicePixelRatio));

    ///转二进制
    ByteData byteData = (await image.toByteData(format: ImageByteFormat.png))!;

    ///图片数据
    // _pngBytes = byteData.buffer.asUint8List();

    final result = await saveBytesImageToLocal(byteData.buffer.asUint8List());
    // final result = await ImageGallerySaver.saveImage(
    //     byteData.buffer.asUint8List(),
    //     quality: 100,
    //     name: imageName);

    ///保存图片到系统图库
    return result.toString();
  }
}
