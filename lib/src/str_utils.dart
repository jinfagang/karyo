import 'package:intl/intl.dart';

formatNumber(dynamic myNumber) {
  String stringNumber = myNumber.toString();
  double doubleNumber = double.tryParse(stringNumber) ?? 0;
  NumberFormat numberFormat = new NumberFormat.compact(locale: 'en_US');
  return numberFormat.format(doubleNumber);
}

formattedTimeVoiceLen({required int timeInSecond}) {
  int sec = timeInSecond % 60;
  int min = (timeInSecond / 60).floor();
  String minute = min.toString().length <= 1 ? "" : "$min'";
  String second = sec.toString().length <= 1 ? "$sec" : "$sec";
  return "$minute$second''";
}

String generateUniqueId(String input, {int length = 8}) {
  if (input.isEmpty) return '';

  int hashCode = input.hashCode;
  String hashString =
      hashCode.toUnsigned(32).toRadixString(16); // Convert to hex string

  // If the hash string length is less than the desired length, pad it with zeros
  while (hashString.length < length) {
    hashString = '0' + hashString;
  }

  // Take only the first 'length' characters of the hash string
  return hashString.substring(0, length);
}
