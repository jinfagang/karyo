import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class PlatformTheme {
  final lightSettingsListBackground = Color.fromRGBO(242, 242, 247, 1);
  final darkSettingsListBackground = CupertinoColors.black;

  static final lightSettingSectionColor = CupertinoColors.white;
  // static final darkSettingSectionColor = Color(0x191919);
  // static final darkSettingSectionColor = Color(0xff202020);
  static final darkSettingSectionColor = Color.fromARGB(255, 22, 22, 22);

  static final lightSettingsTitleColor = Color.fromRGBO(109, 109, 114, 1);
  static final darkSettingsTitleColor = CupertinoColors.systemGrey;

  static final lightDividerColor = Color.fromARGB(255, 238, 238, 238);
  static final darkDividerColor = Color.fromARGB(255, 40, 40, 42);

  static final lightTrailingTextColor = Color.fromARGB(255, 138, 138, 142);
  static final darkTrailingTextColor = Color.fromARGB(255, 152, 152, 159);

  static final lightTileHighlightColor = Color.fromARGB(255, 209, 209, 214);
  static final darkTileHighlightColor = Color.fromARGB(255, 58, 58, 60);

  static final lightSettingsTileTextColor = CupertinoColors.black;
  static final darkSettingsTileTextColor = CupertinoColors.white;

  // static final bubbleGrey = Color(0xFFE8E8EE);
  static final bubbleGrey = Color.fromARGB(255, 248, 248, 248);
  static const coolGrey = Color(0xfff1f1f1);
  // static const coolGrey = Color(0xfff1f1f1);
  static const white = Color(0xffffffff);
  static final subBlack = Color.fromARGB(255, 13, 13, 13);
  static const subBlack1919 = Color(0xff191919);
  static const subBlack1111 = Color(0xff111111);
  static const subBlack1515 = Color.fromARGB(255, 17, 16, 17);

  static final subBlack2 = Color.fromARGB(255, 27, 22, 22);
  static final subBlack3 = Color.fromARGB(255, 20, 19, 19);
  static final subBlackLighter = Color.fromARGB(255, 25, 22, 25);
  static final subBlackLighterLighter = Color.fromARGB(255, 38, 37, 37);

  static final wechatGrey2 = Color(0xfff7f7f7);
  static final wechatGrey = Color.fromARGB(255, 247, 247, 247);

  static final wechatLinkColor = Color.fromARGB(255, 31, 30, 30);

  static final lightGrey = Color.fromARGB(255, 252, 250, 250);
  static final darkGrey = Color.fromARGB(255, 34, 34, 34);
  static final grey150 = Colors.grey.withAlpha(160);
  static final grey100 = Colors.grey.withAlpha(100);

  static Color getSectionColor(BuildContext context) {
    return isDarkModeOnContext(context)
        ? darkSettingSectionColor
        : lightSettingSectionColor;
  }

  static getMainBkColor(BuildContext context) {
    return isDarkModeOnContext(context)
        // ? subBlack1111
        ? subBlack1515
        : lightSettingSectionColor;
  }

  static getMainAppBarColor(BuildContext context) {
    return isDarkModeOnContext(context)
        ? subBlack1111
        : lightSettingSectionColor;
  }

  static Color getMainBkColorSection(BuildContext context,
      {bool reverse = false}) {
    return isDarkModeOnContext(context)
        ? darkSettingSectionColor
        // : lightSettingSectionColor;
        // : Theme.of(context).dialogBackgroundColor;
        : Theme.of(context).scaffoldBackgroundColor;
  }

  static Color getMainBkColorSection3(BuildContext context,
      {bool reverse = false}) {
    return isDarkModeOnContext(context)
        ? Colors.grey.shade700
        // : lightSettingSectionColor;
        // : Theme.of(context).dialogBackgroundColor;
        : Colors.grey.shade300;
  }

  static Color getMainBkColorSection2(BuildContext context,
      {bool reverse = false}) {
    return isDarkModeOnContext(context) ? subBlack2 : lightSettingSectionColor;
  }

  static Color getMainBkColorSection5(BuildContext context,
      {bool reverse = false}) {
    return isDarkModeOnContext(context)
        ? subBlackLighterLighter
        : lightSettingSectionColor;
  }

  static Color getMainBkColorSectionSubblack3(BuildContext context,
      {bool reverse = false}) {
    return isDarkModeOnContext(context) ? subBlack3 : Colors.white;
  }

  static Color getMainBkColorSection4(BuildContext context,
      {bool reverse = false}) {
    return isDarkModeOnContext(context) ? subBlackLighter : white;
  }

  static Color getMainBkColorSectionPrimary(BuildContext context,
      {bool reverse = false}) {
    return isDarkModeOnContext(context) ? tClrPrimaryContainer(context) : white;
  }

  static Color getMainBkColorDialog2(BuildContext context,
      {bool reverse = false}) {
    return isDarkModeOnContext(context)
        ? subBlack1111
        : Theme.of(context).dialogBackgroundColor;
  }

  static Color getMainBkColorDialog(BuildContext context,
      {bool reverse = false}) {
    return isDarkModeOnContext(context)
        ? Theme.of(context).cardColor
        : Theme.of(context).cardColor;
  }

  static Color getMainBkColorSectionReverse(BuildContext context,
      {bool reverse = false}) {
    return isDarkModeOnContext(context)
        ? lightSettingSectionColor
        : darkSettingSectionColor;
  }

  static Color getMainBkColorSectionReverseLight(BuildContext context,
      {bool reverse = false}) {
    return isDarkModeOnContext(context) ? darkGrey : lightGrey;
  }

  static getPureBkColorNoContext() {
    return isDarkModeOnNoContext() ? Colors.black : Colors.white;
  }

  static getPureBkColorReverse(BuildContext context) {
    return isDarkModeOnContext(context) ? Colors.white : Colors.black;
  }

  static getSectionTitleColor(BuildContext context) {
    return isDarkModeOnContext(context)
        ? darkSettingsTileTextColor
        : lightSettingsTileTextColor;
  }
}
