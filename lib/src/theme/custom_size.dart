import 'dart:io';

import 'package:flutter/material.dart';

class CustomSize {
  static const double appBarTitleSize = 16;
  static const double defaultHintTextSize = 14;
  static const double maxWindowSize = 800;
  static const double smallWindowSize = 500;

  static double get toolbarHeight {
    if (Platform.isMacOS) {
      return kToolbarHeight + 30;
    }

    return kToolbarHeight;
  }
}
