import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:karyo/src/apis/api_constants.dart';

class Contact {
  Contact(
      {required this.avatar,
      required this.name,
      required this.email,
      required this.location,
      required this.userAddr,
      required this.phone,
      this.sign,
      this.token,
      this.userGender,
      this.isMyFriend,
      this.targetAvatarImage,
      this.userAcc,
      this.userLevel,
      this.userAvatarUrl,
      this.userSign,
      this.clicked,
      this.authInfo,
      this.isGroupOwner});

  final String avatar;
  final String name;
  final String email;
  final String location;
  final String userAddr;
  final String phone;
  String? userGender;
  String? userAvatarUrl;
  String? userSign;
  String? userAcc;
  String? sign;
  String? token;
  String? authInfo;
  int? userLevel;
  bool? isMyFriend = false;
  bool? isGroupOwner = false;
  bool? clicked = false;

  ImageProvider? targetAvatarImage;

  static List<Contact> allFromResponse(String response) {
    var decodedJson = json.decode(response).cast<String, dynamic>();
    return decodedJson['data']
        .cast<Map<String, dynamic>>()
        .map((obj) => Contact.fromMap(obj))
        .toList()
        .cast<Contact>();
  }

  static Contact fromMap(Map map) {
    ImageProvider targetAvatarImage;
    var userAvatarUrl = map['user_avatar_url'];

    if (userAvatarUrl != null && userAvatarUrl != '') {
      // userAvatarImage = AdvancedNetworkImage(baseUrl + userAvatarUrl);
      targetAvatarImage = NetworkImage(API.URANUS_URL + userAvatarUrl);
    } else if (map['user_avatar'] != null && map['user_avatar'] != '') {
      Uint8List bytes = base64.decode(map['user_avatar']);
      targetAvatarImage = MemoryImage(bytes);
    } else {
      targetAvatarImage = const AssetImage('assets/images/blank_avatar.png');
    }

    var isOwner = false;
    if (map.containsKey('is_owner')) {
      isOwner = map['is_owner'];
    }

    if (map['user_auth_info'] == null || map['user_auth_info'] == '') {
      map['user_auth_info'] = '未设置认证信息';
    }
    return new Contact(
        avatar: map['user_avatar'],
        name: map['user_nick_name'],
        email: map['user_email'],
        location: map['user_city'],
        userAddr: map['user_addr'],
        userAcc: map['user_acc'],
        phone: map['user_phone'],
        userAvatarUrl: API.URANUS_URL + userAvatarUrl,
        sign: map['user_sign'],
        userLevel: map['user_level'],
        authInfo: map['user_auth_info'],
        userGender: map['user_gender'],
        isMyFriend: map['is_my_friend'],
        clicked: false,
        isGroupOwner: isOwner,
        targetAvatarImage: targetAvatarImage);
  }

  static Contact fromJson(dynamic map) {
    ImageProvider targetAvatarImage;
    var userAvatarUrl = map['user_avatar_url'];
    if (userAvatarUrl != null && userAvatarUrl != '') {
      targetAvatarImage = NetworkImage(API.URANUS_URL + userAvatarUrl);
    } else if (map['user_avatar'] != null && map['user_avatar'] != '') {
      Uint8List bytes = base64.decode(map['user_avatar']);
      targetAvatarImage = MemoryImage(bytes);
    } else {
      targetAvatarImage = const AssetImage('assets/images/default_pic.png');
    }

    if (map['user_auth_info'] == null || map['user_auth_info'] == '') {
      map['user_auth_info'] = '未设置认证信息';
    }

    return new Contact(
        avatar: map['user_avatar'],
        name: map['user_nick_name'],
        email: map['user_email'],
        location: map['user_city'],
        userAddr: map['user_addr'],
        userAcc: map['user_acc'],
        userLevel: map['user_level'],
        authInfo: map['user_auth_info'],
        userAvatarUrl: API.URANUS_URL + userAvatarUrl,
        phone: map['user_phone'],
        sign: map['user_sign'],
        isMyFriend: map['is_my_friend'],
        userGender: map['user_gender'],
        clicked: false,
        targetAvatarImage: targetAvatarImage);
  }
}
