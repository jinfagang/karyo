import 'dart:convert';

class BotCommands {
  String? cmd;
  String? desc;

  BotCommands({this.cmd, this.desc});

  BotCommands.fromJson(Map<String, dynamic> json) {
    cmd = json['cmd'];
    desc = json['desc'];
  }

  static List<BotCommands> allFromResponse(String response) {
    var decodedJson = json.decode(response);
    return decodedJson
        .cast<Map<String, dynamic>>()
        .map((obj) => BotCommands.fromJson(obj))
        .toList()
        .cast<BotCommands>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cmd'] = this.cmd;
    data['desc'] = this.desc;
    return data;
  }
}
