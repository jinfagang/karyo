class APIkeyModel {
  int id;
  String userAddr;
  String key;
  String name;
  String description;
  bool disabled;
  DateTime? expiresAt;
  int? usageCount;
  DateTime? lastUsedAt;
  String? createdAtIP;
  String lastUsedIP;
  DateTime? revokedAt;
  String revokedBy;

  APIkeyModel({
    required this.id,
    required this.userAddr,
    required this.key,
    required this.name,
    required this.description,
    required this.disabled,
    this.expiresAt,
    this.usageCount,
    this.lastUsedAt,
    this.createdAtIP,
    required this.lastUsedIP,
    this.revokedAt,
    required this.revokedBy,
  });

  factory APIkeyModel.fromJson(Map<String, dynamic> json) {
    return APIkeyModel(
      id: json['ID'],
      userAddr: json['user_addr'],
      key: json['key'],
      name: json['name'],
      description: json['description'],
      disabled: json['disabled'],
      usageCount: json['UsageCount'],
      createdAtIP: json['CreatedAtIP'],
      lastUsedIP: json['LastUsedIP'],
      revokedBy: json['RevokedBy'],
    );
  }
}
