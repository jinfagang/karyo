import 'dart:convert';

class HiMsg {
  String? token;
  String? userAddr;
  String? device;
  String? deviceName;
  String? deviceUuid;
  String? location;
  String? ua;
  bool? isOnline;
  int? upTime;
  int? lastSeenTime;

  HiMsg(
      {this.token,
      this.userAddr,
      this.device,
      this.deviceName,
      this.deviceUuid,
      this.location,
      this.ua,
      this.isOnline,
      this.upTime,
      this.lastSeenTime});

  HiMsg.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    userAddr = json['user_addr'];
    device = json['device'];
    deviceName = json['device_name'];
    deviceUuid = json['device_uuid'];
    location = json['location'];
    ua = json['ua'];
    isOnline = json['is_online'];
    upTime = json['up_time'];
    lastSeenTime = json['last_seen_time'] * 1000;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['user_addr'] = this.userAddr;
    data['device'] = this.device;
    data['device_name'] = this.deviceName;
    data['device_uuid'] = this.deviceUuid;
    data['location'] = this.location;
    data['ua'] = this.ua;
    data['is_online'] = this.isOnline;
    data['up_time'] = this.upTime;
    data['last_seen_time'] = this.lastSeenTime;
    return data;
  }

  static List<HiMsg>? allFromResponse(String response) {
    var decodedJson = json.decode(response);
    if (decodedJson['code'] == 403 || decodedJson['code'] == 400) {
      // token invalid or request invalid
      return null;
    } else {
      return decodedJson['data']
          .cast<Map<String, dynamic>>()
          .map((obj) => HiMsg.fromJson(obj))
          .toList()
          .cast<HiMsg>();
    }
  }
}
