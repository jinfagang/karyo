import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';
import '../apis/api_constants.dart';

class User {
  User(
      {required this.userAddr,
      required this.userNickName,
      required this.userCity,
      required this.userSign,
      required this.userBirth,
      required this.userAcc,
      required this.userAvatarUrl,
      this.userAvatar,
      this.userGender,
      this.userType,
      this.enableAnonymous,
      this.anonymousNickName,
      this.anonymousGender,
      this.anonymousSign,
      this.anonymousCity,
      this.userAvatarUrlShort,
      this.anonymousAvatarID,
      this.userAvatarImage,
      this.userLevel,
      this.loginType,
      this.accType,
      this.vipPlan,
      this.vipStartTime,
      this.vipPlanApp2,
      this.vipStartTimeApp2,
      this.userID,
      this.numBlinks,
      this.numNotes,
      this.numTasks,
      this.order,
      this.userPwd,
      this.userRegisterTime,
      this.chatQuota,
      this.usedQuota,
      this.totalUsedQuota,
      this.selectedImgAvatar});

  String userAddr;
  String userAcc;
  String userNickName;
  String userCity;
  String userSign;
  String userBirth;
  String? userAvatarUrl;
  String? userAvatarUrlShort;
  String? userAvatar;
  String? userPwd;

  bool? enableAnonymous;
  String? anonymousNickName;
  String? anonymousGender;
  String? anonymousCity;
  String? anonymousSign;
  String? anonymousAvatarID;

  int? userLevel;
  int? loginType;
  int? accType;

  // some statistics
  int? numTasks;
  int? numNotes;
  int? numBlinks;
  int? order;

  int? chatQuota;
  int? usedQuota;
  int? totalUsedQuota;

  ImageProvider? userAvatarImage;

  // if no vipStartTime means no vip
  int? vipPlan;
  DateTime? vipStartTime;

  int? vipPlanApp2;
  DateTime? vipStartTimeApp2;

  int? userType;
  //  same as userTYpe
  String? userGender;

  DateTime? userRegisterTime;
  int? userID;

  File? selectedImgAvatar;

  static List<User> allFromResponse(String response) {
    var decodedJson = json.decode(response).cast<String, dynamic>();
    return decodedJson['data']
        .cast<Map<String, dynamic>>()
        .map((obj) => User.fromMap(obj))
        .toList()
        .cast<User>();
  }

  Map<String, dynamic> toMap() {
    return {
      "user_addr": userAddr,
      "user_nick_name": userNickName,
      "user_avatar_url": userAvatarUrl,
      "user_acc": userAcc,
    };
  }

  static User fromMap(Map map) {
    // String uG = map['user_gender'];
    var userAvatar = map['user_avatar'];
    var userAvatarUrl = map['user_avatar_url'];
    if (userAvatar == null) {
      userAvatar = '';
    }

    // int userGender;
    // if (uG == '' || uG == null || uG == "null") {
    //   userGender = 0;
    // } else {
    //   userGender = int.parse(map['user_gender']);
    // }
    // String userGenderStr = '';
    // switch (userGender) {
    //   case 0:
    //     userGenderStr = '男';
    //     break;
    //   case 1:
    //     userGenderStr = '女';
    //     break;
    //   case 2:
    //     userGenderStr = '机器人';
    //     break;
    // }
    var userBirth;
    if (map['user_birth'] == '' || map['user_birth'] == null) {
      userBirth = '1998';
    } else {
      userBirth = map['user_birth'];
    }

    ImageProvider userAvatarImage;
    if (userAvatarUrl != null && userAvatarUrl != '') {
      // print('userAvatarUrl: ${userAvatarUrl}');
      // userAvatarImage =
      //     CachedNetworkImageProvider(API.BASE_URL + userAvatarUrl);
      userAvatarImage = NetworkImage(API.URANUS_URL + userAvatarUrl);
    } else if (userAvatar != null && userAvatar != '') {
      Uint8List bytes = base64.decode(userAvatar);
      userAvatarImage = MemoryImage(bytes);
    } else {
      userAvatarImage = const CachedNetworkImageProvider(gAvatarDefaultMale);
    }

    DateTime vipStartTime = DateTime.now();
    DateTime vipStartTimeApp2 = DateTime.now();
    int vipPlan = -1;
    int vipPlanApp2 = -1;
    // prepare for vip info
    if (map['vips'] != null) {
      // user have vip info
      for (var vipInfo in map['vips']) {
        if (vipInfo['vip_app'] == 'daybreak') {
          //
          // print('----------------- ${map['vip_start_time']}');
          vipStartTime = DateTime.parse(vipInfo['vip_start_time']).toLocal();
          vipPlan = vipInfo['vip_plan'];
        } else if (vipInfo['vip_app'] == 'jarvis') {
          vipStartTimeApp2 =
              DateTime.parse(vipInfo['vip_start_time']).toLocal();
          vipPlanApp2 = vipInfo['vip_plan'];
        }
      }
    }

    int _numTasks = 0;
    int _numBlinks = 0;
    int _numNotes = 0;
    if (map.containsKey('db_statistics')) {
      _numTasks = map['db_statistics']['tasks'];
      _numBlinks = map['db_statistics']['blinks'];
      _numNotes = map['db_statistics']['notes'];
    }

    DateTime _registerTime;
    if (map['user_register_time'] != null) {
      _registerTime = DateTime.parse(map['user_register_time']);
    } else {
      _registerTime = DateTime.now();
    }
    String _userPwd = '';
    if (map.keys.contains('user_pwd')) {
      _userPwd = map['user_pwd'];
    }
    // print('------------------------ ${map['user_pwd']}');
    return User(
      userAvatarImage: userAvatarImage,
      userAddr: map['user_addr'],
      userAvatar: userAvatar,
      userNickName: map['user_nick_name'],
      userAcc: map['user_acc'],
      userCity: map['user_city'],
      userSign: map['user_sign'],
      enableAnonymous: map['enable_anonymous'],
      userBirth: userBirth,
      userAvatarUrl: API.URANUS_URL + userAvatarUrl,
      userAvatarUrlShort: userAvatarUrl,
      anonymousNickName: map['anonymous_nick_name'],
      anonymousGender: map['anonymous_gender'],
      anonymousSign: map['anonymous_sign'],
      anonymousCity: map['anonymous_city'],
      anonymousAvatarID: map['anonymous_avatar_id'],
      userGender: map['user_gender'],
      userType: 0,
      userLevel: map['user_level'] ?? 1,
      loginType: map['login_type'],
      accType: map['account_type'],
      vipPlan: vipPlan,
      vipPlanApp2: vipPlanApp2,
      vipStartTime: vipStartTime,
      vipStartTimeApp2: vipStartTimeApp2,
      userRegisterTime: _registerTime,
      userID: map['user_id'],
      userPwd: _userPwd,
      numTasks: _numTasks,
      numBlinks: _numBlinks,
      numNotes: _numNotes,
      chatQuota: map['chat_quota'],
      usedQuota: map['used_quota'],
      totalUsedQuota: map['total_used_quota'],
    );
  }
}
