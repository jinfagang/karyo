// this defines a timeline post

import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:karyo/karyo.dart';

class Post {
  String? userNickName;
  String? userAvatar;
  String? userAvatarUrl;
  String? userAddr;

  String? userCity;
  String? userGender;
  String? postContent;
  List<String>? postImagesList;
  String? postTime;
  String? postDevice;

  String? content;
  List<String>? imgLists;
  int? numLikes;
  String? place;
  String? weather;
  String? mood;
  int? id;
  String? timeTamp;
  List<Comment>? comments = [];
  int? likesCount;
  int? commentsCount;
  bool? liked;
  bool? isPrivate;
  ImageProvider? postUserAvatarImage;

  Post(
      {required this.userNickName,
      this.userCity,
      this.userAvatar,
      this.userAvatarUrl,
      this.userGender,
      required this.userAddr,
      this.postTime,
      this.postDevice,
      this.commentsCount,
      this.content,
      this.imgLists,
      this.numLikes,
      this.place,
      this.weather,
      this.mood,
      this.liked,
      this.id,
      this.timeTamp,
      this.comments,
      this.postUserAvatarImage,
      this.isPrivate});

  static List<Post> allFromResponse(String response) {
    var decodedJson = json.decode(response);
    if (decodedJson['code'] == 403 || decodedJson['code'] == 400) {
      // token invalid or request invalid
      return [];
    } else {
      // print('Post, data: ${decodedJson['data']}');
      return decodedJson['data']
          .cast<Map<String, dynamic>>()
          .map((obj) => Post.fromMap(obj))
          .toList()
          .cast<Post>();
    }
  }

  static Post fromMap(Map map) {
    var lastTime = map['time'];
    // print("targetUser['user_gender']${targetUser['user_gender']}");
    // format last_time into human readable format
    // var t = TimelineUtil.formatA(
    //   DateTime.parse(lastTime).millisecondsSinceEpoch,
    //   languageCode: 'zh',
    //   short: true,
    // ).toString();
    var t = getTimelineTime(DateTime.parse(lastTime).millisecondsSinceEpoch);

    List<String> imgLists = [];
    if (map['img_lists'] != '') {
      imgLists = map['img_lists'].split(' ');
    }
    ImageProvider postUserAvatarImage;

    if (map['user_avatar_url'] != null && map['user_avatar_url'] != '') {
      // userAvatarImage = AdvancedNetworkImage(baseUrl + userAvatarUrl);
      postUserAvatarImage = NetworkImage(API.BASE_URL + map['user_avatar_url']);
    } else if (map['user_avatar'] != null && map['user_avatar'] != '') {
      Uint8List bytes = base64.decode(map['user_avatar']);
      postUserAvatarImage = MemoryImage(bytes);
    } else {
      postUserAvatarImage = const AssetImage('assets/images/blank_avatar.png');
    }

    return Post(
      content: map['content'],
      postTime: t,
      timeTamp: lastTime,
      userAvatar: map['user_avatar'],
      userNickName: map['user_nick_name'],
      userAddr: map['user_addr'],
      imgLists: imgLists,
      liked: false,
      numLikes: map['num_likes'],
      postUserAvatarImage: postUserAvatarImage,
      id: map["time_line_id"],
      isPrivate: map["is_private"] ?? true,
      comments:
          map['comments'] == null ? null : Comment.allFromList(map['comments']),
    );
  }
}

class Comment {
  String? commentContent;
  String? commentUserNickName;
  String? commentUserAddr;
  String? commentUserAvatarUrl;
  String? time;
  int? commentId;
  List<CommentReply>? commentReplies;

  bool? isReply;
  String? replyUserNickName;
  String? replyUserAddr;
  String? replyContent;
  String? targetUserNickName;

  Comment(
      {this.commentContent,
      this.commentUserNickName,
      this.commentUserAddr,
      this.commentUserAvatarUrl,
      this.commentId,
      this.commentReplies,
      this.isReply,
      this.time,
      this.replyContent,
      this.replyUserNickName,
      this.targetUserNickName,
      this.replyUserAddr});

  static List<Comment> allFromList(List<dynamic> allCommentsMap) {
    // convert comments map to list of comments
    // print('allComments: $allCommentsMap');
    if (allCommentsMap.isNotEmpty) {
      List<Comment> allComments = [];
      for (var c in allCommentsMap) {
        allComments.add(Comment.fromMap(c));

        if (c['comment_replies'] != null) {
          // print('Comment replies: ${c['comment_replies']}');
          // convert replies into comment
          var commentReplies = c['comment_replies'];
          for (var r in commentReplies) {
            Comment rC = Comment(
              isReply: true,
              replyContent: r['reply_content'],
              replyUserNickName: r['reply_user_nick_name'],
              replyUserAddr: r['reply_user_addr'],
              targetUserNickName: c['target_user_nick_name'],
            );
            allComments.add(rC);
          }
        }
      }
      return allComments;
    } else {
      return [];
    }
  }

  static Comment fromMap(Map map) {
    // print("map['comment_user_avatar_url']: ${map['comment_user_avatar_url']}");
    var t = map['time'];
    if (t == null) {
      t = '刚刚';
    } else {
      t = getFormattedDateTime(
          DateTime.parse(map['time']).millisecondsSinceEpoch);
    }

    return Comment(
        isReply: false,
        commentContent: map['comment_content'],
        commentId: map['comment_id'],
        commentUserNickName: map['comment_user_nick_name'],
        commentUserAvatarUrl: API.BASE_URL + map['comment_user_avatar_url'],
        commentUserAddr: map['comment_user_addr'],
        time: t,
        commentReplies: map['comment_replies'] == null
            ? null
            : CommentReply.allFromList(map['comment_replies']));
  }
}

class CommentReply {
  String? replyContent;
  String? replyUserNickName;
  String? replyUserAddr;
  String? time;
  int? replyId;

  CommentReply(
      {this.replyContent,
      this.replyUserNickName,
      this.replyUserAddr,
      this.replyId});

  static List<CommentReply> allFromList(List<dynamic> allCommentsMap) {
    // convert comments map to list of comments
    // print('allComments: $allCommentsMap');
    if (allCommentsMap.isNotEmpty) {
      List<CommentReply> allCommentReplies = [];
      for (var c in allCommentsMap) {
        allCommentReplies.add(CommentReply.fromMap(c));
      }
      return allCommentReplies;
    } else {
      return [];
    }
  }

  static CommentReply fromMap(Map map) {
    return CommentReply(
        replyContent: map['reply_content'],
        replyId: map['reply_id'],
        replyUserNickName: map['reply_user_nick_name']);
  }
}
