class AppUpdateInfo {
  String? buildBuildVersion;
  String? forceUpdateVersion;
  String? forceUpdateVersionNo;
  bool? needForceUpdate;
  // the download url maybe not same for iOS and Android
  String? downloadURL;
  bool? buildHaveNewVersion;
  String? buildVersionNo;
  String? buildVersion;
  String? buildShortcutUrl;
  String? buildUpdateDescription;

  AppUpdateInfo(
      {this.buildBuildVersion,
      this.forceUpdateVersion,
      this.forceUpdateVersionNo,
      this.needForceUpdate,
      this.downloadURL,
      this.buildHaveNewVersion,
      this.buildVersionNo,
      this.buildVersion,
      this.buildShortcutUrl,
      this.buildUpdateDescription});

  AppUpdateInfo.fromJson(Map<String, dynamic> json) {
    buildBuildVersion = json['buildBuildVersion'];
    forceUpdateVersion = json['forceUpdateVersion'];
    forceUpdateVersionNo = json['forceUpdateVersionNo'];
    needForceUpdate = json['needForceUpdate'];
    downloadURL = json['downloadURL'];
    buildHaveNewVersion = json['buildHaveNewVersion'];
    buildVersionNo = json['buildVersionNo'];
    buildVersion = json['buildVersion'];
    buildShortcutUrl = json['buildShortcutUrl'];
    buildUpdateDescription = json['buildUpdateDescription'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['buildBuildVersion'] = this.buildBuildVersion;
    data['forceUpdateVersion'] = this.forceUpdateVersion;
    data['forceUpdateVersionNo'] = this.forceUpdateVersionNo;
    data['needForceUpdate'] = this.needForceUpdate;
    data['downloadURL'] = this.downloadURL;
    data['buildHaveNewVersion'] = this.buildHaveNewVersion;
    data['buildVersionNo'] = this.buildVersionNo;
    data['buildVersion'] = this.buildVersion;
    data['buildShortcutUrl'] = this.buildShortcutUrl;
    data['buildUpdateDescription'] = this.buildUpdateDescription;
    return data;
  }
}
