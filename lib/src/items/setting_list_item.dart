import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:karyo/karyo.dart';

class SettingListItemStyle0 extends StatelessWidget {
  const SettingListItemStyle0(
      {super.key,
      required this.icon,
      required this.color,
      required this.title,
      this.trailing,
      this.padding,
      this.onTap});
  final IconData icon;
  final EdgeInsets? padding;
  final Color color;
  final String title;
  final Widget? trailing;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? EdgeInsets.zero,
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(18),
          onTap: onTap,
          child: Ink(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(18),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      DecoratedBox(
                        decoration: BoxDecoration(
                            color: color.withAlpha(80),
                            borderRadius: BorderRadius.circular(13)),
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: GradientIcon(
                            icon,
                            size: 20,
                            color: color,
                          ),
                        ),
                      ),
                      g8,
                      g4,
                      Text(
                        title,
                        style: NORMAL_BOLD_TXT_STYLE.copyWith(fontSize: 15),
                      ),
                    ],
                  ),
                  trailing ??
                      IconButton(
                          style: getZeroBtnStyle(),
                          onPressed: null,
                          icon: Icon(
                            CupertinoIcons.chevron_right,
                            color: Colors.grey.withAlpha(20),
                          ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class SettingListItemStyle1 extends StatelessWidget {
  const SettingListItemStyle1(
      {super.key,
      required this.icon,
      required this.color,
      required this.title,
      this.onTap});
  final IconData icon;
  final Color color;
  final String title;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    const double bd = 16;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(bd),
          onTap: onTap,
          child: Ink(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(bd),
                color: PlatformTheme.getMainBkColorSection2(context)),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      DecoratedBox(
                        decoration: BoxDecoration(
                            color: color.withAlpha(80),
                            borderRadius: BorderRadius.circular(bd)),
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: GradientIcon(
                            icon,
                            size: 23,
                            color: color,
                          ),
                        ),
                      ),
                      g8,
                      Text(
                        title,
                        style: NORMAL_BOLD_TXT_STYLE.copyWith(fontSize: 16),
                      ),
                    ],
                  ),
                  IconButton(
                      style: getZeroBtnStyle(),
                      onPressed: null,
                      icon: Icon(
                        FluentIcons.chevron_right_12_filled,
                        color: Colors.grey.withAlpha(50),
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
