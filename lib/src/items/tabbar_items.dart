import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

Widget getDefaultTabBar(
    List<Tab> tabs, TabController tabController, Color mainColor,
    {Function(int)? tabTapped}) {
  return TabBar(
      splashBorderRadius: BorderRadius.circular(28),
      onTap: tabTapped,
      controller: tabController,
      isScrollable: true,
      indicator: TCUnderlineTabIndicator(
          isRound: false, borderSide: BorderSide(width: 5, color: mainColor)),
      indicatorWeight: 4,
      indicatorPadding: const EdgeInsets.symmetric(vertical: 16),
      labelStyle: NORMAL_BOLD_TXT_STYLE.copyWith(fontSize: 18),
      unselectedLabelStyle: NORMAL_TXT_STYLE,
      tabAlignment: TabAlignment.start,
      labelColor: mainColor,
      dividerHeight: 0,
      tabs: tabs);
}

Widget getDefaultTabBarSmallFit(
    List<Tab> tabs, TabController tabController, Color mainColor,
    {Function(int)? tabTapped}) {
  return TabBar(
      splashBorderRadius: BorderRadius.circular(28),
      onTap: tabTapped,
      controller: tabController,
      isScrollable: true,
      indicator: TCUnderlineTabIndicator(
          isRound: true, borderSide: BorderSide(width: 5, color: mainColor)),
      indicatorWeight: 4,
      indicatorSize: TabBarIndicatorSize.label,
      indicatorPadding: const EdgeInsets.symmetric(vertical: 16),
      labelStyle: NORMAL_BOLD_TXT_STYLE.copyWith(fontSize: 12),
      unselectedLabelStyle: NORMAL_TXT_STYLE,
      tabAlignment: TabAlignment.start,
      labelColor: mainColor,
      dividerHeight: 0,
      tabs: tabs);
}

Widget getDefaultTabBarChiped(
    List<Tab> tabs, TabController tabController, BuildContext context,
    {Function(int)? tabTapped}) {
  return TabBar(
    splashBorderRadius: BorderRadius.circular(28),
    onTap: tabTapped,
    controller: tabController,
    isScrollable: true,
    indicator: BoxDecoration(
      color: tClrPrimary(context),
      borderRadius: BorderRadius.circular(13),
    ),
    indicatorSize: TabBarIndicatorSize.tab,
    indicatorPadding:
        const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2.0),
    indicatorWeight: 0,
    labelStyle: NORMAL_BOLD_TXT_STYLE.copyWith(fontSize: 12),
    unselectedLabelStyle: NORMAL_TXT_STYLE,
    // unselectedLabelColor: tClrPrimary(context),
    tabAlignment: TabAlignment.start,
    labelColor: tClrOnPrimary(context),
    dividerHeight: 0,
    tabs: tabs.map((tab) {
      return Container(
        height: 34,
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 0),
        child: tab,
      );
    }).toList(),
  );
}

class TCUnderlineTabIndicator extends Decoration {
  const TCUnderlineTabIndicator({
    this.borderSide = const BorderSide(width: 2.0, color: Colors.white),
    this.insets = EdgeInsets.zero,
    this.indicatorBottom = 0.0,
    this.indicatorWidth = 28,
    this.isRound = true,
  });
  final BorderSide borderSide;
  final EdgeInsetsGeometry insets;
  final double indicatorBottom; // 自定义指示条距离底部距离
  final double indicatorWidth; // 自定义指示条宽度
  final bool? isRound; // 自定义指示条是否是圆角

  @override
  Decoration? lerpFrom(Decoration? a, double t) {
    if (a is TCUnderlineTabIndicator) {
      return TCUnderlineTabIndicator(
        borderSide: BorderSide.lerp(a.borderSide, borderSide, t),
        insets: EdgeInsetsGeometry.lerp(a.insets, insets, t)!,
      );
    }
    return super.lerpFrom(a, t);
  }

  @override
  Decoration? lerpTo(Decoration? b, double t) {
    if (b is TCUnderlineTabIndicator) {
      return TCUnderlineTabIndicator(
        borderSide: BorderSide.lerp(borderSide, b.borderSide, t),
        insets: EdgeInsetsGeometry.lerp(insets, b.insets, t)!,
      );
    }
    return super.lerpTo(b, t);
  }

  @override
  BoxPainter createBoxPainter([VoidCallback? onChanged]) {
    return _UnderlinePainter(this, onChanged, isRound ?? false);
  }

  Rect _indicatorRectFor(Rect rect, TextDirection textDirection) {
    assert(rect != null);
    assert(textDirection != null);
    final Rect indicator = insets.resolve(textDirection).deflateRect(rect);

    //    return Rect.fromLTWH(
    //      indicator.left,
    //      indicator.bottom - borderSide.width,
    //      indicator.width,
    //      borderSide.width,
    //    );

    //取中间坐标
    double cw = (indicator.left + indicator.right) / 2;
    // ***************************这里可以自定义指示条的宽度和底部间距***************************
    Rect indictorRect = Rect.fromLTWH(
        cw - indicatorWidth / 2,
        indicator.bottom - borderSide.width - indicatorBottom,
        indicatorWidth,
        borderSide.width);
    return indictorRect;
  }

  @override
  Path getClipPath(Rect rect, TextDirection textDirection) {
    return Path()..addRect(_indicatorRectFor(rect, textDirection));
  }
}

class _UnderlinePainter extends BoxPainter {
  _UnderlinePainter(this.decoration, VoidCallback? onChanged, this.isRound)
      : assert(decoration != null),
        super(onChanged);

  final TCUnderlineTabIndicator decoration;
  bool isRound = false;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    assert(configuration != null);
    assert(configuration.size != null);
    final Rect rect = offset & configuration.size!;
    final TextDirection textDirection = configuration.textDirection!;
    final Rect indicator = decoration
        ._indicatorRectFor(rect, textDirection)
        .deflate(decoration.borderSide.width / 2.0);
    //***************************这里可以自定义指示条是否是圆角***************************
    final Paint paint = decoration.borderSide.toPaint()
      ..strokeCap = isRound ? StrokeCap.round : StrokeCap.square;
    canvas.drawLine(indicator.bottomLeft, indicator.bottomRight, paint);
  }
}

const double _kTextAndIconTabHeight = 72.0;

enum TabBarIndicatorSizeTC {
  tab,
  label,
}

class TCTab extends StatelessWidget implements PreferredSizeWidget {
  const TCTab({
    Key? key,
    this.text,
    this.icon,
    this.iconMargin = const EdgeInsets.only(bottom: 10.0),
    this.height,
    this.child,
    this.tabBarHeight = 44,
  })  : assert(text != null || child != null || icon != null),
        assert(text == null || child == null),
        super(key: key);

  final String? text;

  final Widget? child;

  final Widget? icon;

  final EdgeInsetsGeometry iconMargin;

  final double? height;

  final double tabBarHeight; // 自定义tab的高度

  Widget _buildLabelText() {
    return child ?? Text(text!, softWrap: false, overflow: TextOverflow.fade);
  }

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMaterial(context));

    final double calculatedHeight;
    final Widget label;
    if (icon == null) {
      calculatedHeight = tabBarHeight;
      label = _buildLabelText();
    } else if (text == null && child == null) {
      calculatedHeight = tabBarHeight;
      label = icon!;
    } else {
      calculatedHeight = _kTextAndIconTabHeight;
      label = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: iconMargin,
            child: icon,
          ),
          _buildLabelText(),
        ],
      );
    }

// ***********************tab的高度在这里定义****************************
    return SizedBox(
      height: height ?? calculatedHeight,
      child: Center(
        widthFactor: 1.0,
        child: label,
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('text', text, defaultValue: null));
    properties
        .add(DiagnosticsProperty<Widget>('icon', icon, defaultValue: null));
  }

  @override
  Size get preferredSize {
    if (height != null) {
      return Size.fromHeight(height!);
    } else if ((text != null || child != null) && icon != null) {
      return const Size.fromHeight(_kTextAndIconTabHeight);
    } else {
      return Size.fromHeight(tabBarHeight);
    }
  }
}

class CustomTabBaChip extends StatefulWidget {
  const CustomTabBaChip({
    Key? key,
    required List<String> tabBarItems,
    required List<Widget> tabViewItems,
    Color? selectedCardColor,
    Color? selectedTitleColor,
    Color? unSelectedCardColor,
    Color? unSelectedTitleColor,
    double? selectedTabElevation,
    Duration? duration,
    RoundedRectangleBorder? shape,
    double? tabBarItemExtend,
    EdgeInsets? padding,
    TabBarLocation? tabBarLocation,
    double? tabBarItemHeight,
    double? tabBarItemWidth,
    double? tabViewItemHeight,
    double? tabViewItemWidth,
    TextStyle? titleStyle,
  })  : _selectedCardColor = selectedCardColor,
        _unSelectedCardColor = unSelectedCardColor,
        _selectedTitleColor = selectedTitleColor,
        _unSelectedTitleColor = unSelectedTitleColor,
        _tabBarItems = tabBarItems,
        _tabViewItems = tabViewItems,
        _duration = duration,
        _shape = shape,
        _tabBarItemExtend = tabBarItemExtend,
        _padding = padding,
        _tabBarLocation = tabBarLocation,
        _selectedTabElevation = selectedTabElevation,
        _tabBarItemHeight = tabBarItemHeight,
        _tabBarItemWidth = tabBarItemWidth,
        _tabViewItemHeight = tabViewItemHeight,
        _tabViewItemWidth = tabViewItemWidth,
        _titleStyle = titleStyle,
        super(key: key);

  final Color? _selectedCardColor;
  final Color? _unSelectedCardColor;
  final Color? _selectedTitleColor;
  final Color? _unSelectedTitleColor;

  final double? _selectedTabElevation;
  final double? _tabBarItemHeight;
  final double? _tabBarItemWidth;
  final double? _tabViewItemHeight;
  final double? _tabViewItemWidth;
  final TextStyle? _titleStyle;

  final List<String> _tabBarItems;
  final List<Widget> _tabViewItems;
  final Duration? _duration;
  final RoundedRectangleBorder? _shape;
  final double? _tabBarItemExtend;
  final EdgeInsets? _padding;
  final TabBarLocation? _tabBarLocation;

  @override
  State<CustomTabBaChip> createState() => _CustomTabBarState();
}

enum TabBarLocation {
  top,
  bottom,
}

class _CustomTabBarState extends State<CustomTabBaChip> {
  int _selectedIndex = 0;
  late final PageController _pageController;

  void _changeSelectedIndex(int index) {
    setState(() {
      _selectedIndex = index;
      _pageController.animateToPage(_selectedIndex,
          duration: widget._duration ?? const Duration(milliseconds: 200),
          curve: Curves.ease);
    });
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  Widget build(BuildContext context) {
    // return Column(
    //   children: [
    //     widget._tabBarLocation == TabBarLocation.top ||
    //             widget._tabBarLocation == null
    //         ? _TabBarItems(context)
    //         : const SizedBox.shrink(),
    //     _TabViewItems(),
    //     widget._tabBarLocation == TabBarLocation.bottom
    //         ? _TabBarItems(context)
    //         : const SizedBox.shrink(),
    //   ],
    // );

    return Stack(
      children: [
        // Scrollable content (below the fixed TabBarItems)
        Padding(
          padding: EdgeInsets.only(top: 100), // Leave space for the tab bar
          child: _TabViewItems(),
        ),

        // Fixed TabBarItems at the top
        if (widget._tabBarLocation == TabBarLocation.top ||
            widget._tabBarLocation == null)
          Positioned(
            top: 199,
            left: 0,
            right: 0,
            child: Container(
              color: Colors.white, // Optional: Add background color
              child: _TabBarItems(context),
            ),
          ),

        // Optional TabBarItems at the bottom
        if (widget._tabBarLocation == TabBarLocation.bottom)
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: _TabBarItems(context),
          ),
      ],
    );
  }

  // ignore: non_constant_identifier_names
  SizedBox _TabViewItems() {
    return SizedBox(
      // height:
      //     widget._tabViewItemHeight ?? MediaQuery.of(context).size.height * 0.8,
      width: widget._tabViewItemWidth ?? MediaQuery.of(context).size.width,
      child: PageView(
        controller: _pageController,
        onPageChanged: (value) => _changeSelectedIndex(value),
        children: widget._tabViewItems,
      ),
    );
  }

  // ignore: non_constant_identifier_names
  Padding _TabBarItems(BuildContext context) {
    return Padding(
      padding: widget._padding ??
          EdgeInsets.only(
            bottom: MediaQuery.of(context).size.height * 0.01,
          ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(
            width: widget._tabBarItemWidth ?? MediaQuery.of(context).size.width,
            height: widget._tabBarItemHeight ?? 43,
            child: ListView.builder(
              itemCount: widget._tabBarItems.length,
              scrollDirection: Axis.horizontal,
              itemExtent: widget._tabBarItemExtend ?? 75,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () => setState(() {
                    _changeSelectedIndex(index);
                  }),
                  child: Card(
                    shape: widget._shape ??
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              14.0), // Adjust the radius as needed
                        ),
                    elevation: _selectedIndex == index
                        ? widget._selectedTabElevation ?? 4
                        : 0,
                    shadowColor: Colors.grey.withAlpha(70),
                    color: _selectedIndex == index
                        ? widget._selectedCardColor ?? tClrPrimary(context)
                        : widget._unSelectedCardColor ??
                            tClrPrimaryContainer(context),
                    child: Center(
                      child: Text(
                        widget._tabBarItems[index],
                        textAlign: TextAlign.center,
                        style: widget._titleStyle ??
                            Theme.of(context).textTheme.titleSmall?.copyWith(
                                color: _selectedIndex == index
                                    ? widget._selectedTitleColor ??
                                        _CustomTabBarColor().selectedTitleColor
                                    : widget._unSelectedTitleColor ??
                                        _CustomTabBarColor()
                                            .unSelectedTitleColor),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _CustomTabBarColor {
  final Color unSelectedCardColor = const Color.fromARGB(255, 34, 34, 34);
  final Color selectedCardColor = const Color.fromARGB(255, 13, 172, 158);
  final Color selectedTitleColor = Colors.white;
  final Color unSelectedTitleColor = Colors.white;
}
