import 'package:flutter/foundation.dart';

void kLog(Object? object) {
  if (kDebugMode) {
    print('[KLOG] $object');
  }
}
