import 'package:get/get.dart';
import 'package:karyo/karyo.dart';

class AppInfoController extends GetxController {
  final appVersion = "".obs;
  final buildNumber = "".obs;
  final deviceIdentifier = "".obs;
  final isAppFirstRun = false.obs;

  static AppInfoController get to => Get.find();

  @override
  void onInit() async {
    await _getAppVersion();
    await _getBuildNumber();
    await _getDeviceIdentifier();
    await _getIsAppFirstRun();
    super.onInit();
  }

  _getAppVersion() async {
    appVersion.value = await getAppVersion();
    update();
  }

  _getBuildNumber() async {
    buildNumber.value = await getBuildNumber();
    update();
  }

  _getDeviceIdentifier() async {
    deviceIdentifier.value = await getDeviceIdentifier() ?? '';
    update();
  }

  _getIsAppFirstRun() async {
    isAppFirstRun.value = await IsAppFirstRun();
    update();
  }
}
