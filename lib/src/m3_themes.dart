import 'package:flutter/material.dart';
import 'package:flutter_acrylic/window.dart';
import 'package:karyo/karyo.dart';

class ThemeSuite {
  final String name;
  final String bkUrl;
  final String bkUrl2;
  final String hexColor;
  final bool isVip;
  const ThemeSuite(
      {required this.name,
      required this.bkUrl,
      required this.bkUrl2,
      required this.hexColor,
      required this.isVip});
}

const List<ThemeSuite> allMySuites = [
  ThemeSuite(
      bkUrl: '', bkUrl2: "", hexColor: "fcba03", name: '黑白', isVip: false),
  ThemeSuite(
      bkUrl:
          // 'https://jihulab.com/godly/fger/-/raw/main/images/2023/12/26_23_39_15_202312262339821.png',
          // 'https://jihulab.com/godly/fger/-/raw/main/images/2023/12/26_23_46_26_202312262346165.png',
          // 'https://jihulab.com/godly/fger/-/raw/main/images/2023/12/26_23_48_59_f47b2fdae57e1b8174a7e7e80dd010c7.jpg',
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/12/26_23_50_40_9a7889c78e28335de5e5f5267ddcb47d.jpg',
      bkUrl2:
          "https://i.miji.bid/2024/01/10/91e18ed7c4c7a15a70311666d4f25b6a.png",
      hexColor: "fcba03",
      name: '活波黄橙',
      isVip: false),
  ThemeSuite(
      bkUrl:
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/12/27_23_30_35_202312272330208.png',
      bkUrl2:
          "https://i.miji.bid/2023/12/30/9ea836f24c1f2e26a059334ecf2c6ce7.png",
      hexColor: "e60b6e",
      name: '美少女战士',
      isVip: true),
  ThemeSuite(
      bkUrl:
          'https://i.miji.bid/2023/12/30/31667956ee7cc15138397629117d4457.png',
      bkUrl2:
          "https://i.miji.bid/2023/12/30/31667956ee7cc15138397629117d4457.png",
      hexColor: "e60725",
      name: '2024新年',
      isVip: false),
  ThemeSuite(
      bkUrl:
          'https://i.miji.bid/2024/01/01/d358f7b4265041a00c703cd3dbbe18a2.png',
      bkUrl2:
          "https://i.miji.bid/2024/01/01/c113edfd7182d4d658a66064643113a9.png",
      hexColor: "e60725",
      name: 'HelloKitty',
      isVip: false),
  ThemeSuite(
      bkUrl:
          'https://i.miji.bid/2024/01/10/9b22a9da2a94b8f6398f34ac893217df.png',
      bkUrl2:
          "https://i.miji.bid/2024/01/10/91e18ed7c4c7a15a70311666d4f25b6a.png",
      hexColor: "e60725",
      name: '山野自然',
      isVip: true),
  ThemeSuite(
      bkUrl:
          'https://i.miji.bid/2024/01/10/6d9e5fd775988f90d385d6713a9219a6.png',
      bkUrl2:
          "https://i.miji.bid/2024/01/10/91e18ed7c4c7a15a70311666d4f25b6a.png",
      hexColor: "e60725",
      name: '红女郎',
      isVip: true),
];

const Color m3BaseColor = Color(0xff6750a4);
// const Color m3BaseColor2 = Color.fromARGB(255, 48, 47, 47); // purple
const Color m3BaseColor2 = Color.fromARGB(255, 145, 20, 199);
const List<Color> colorOptions = [
  m3BaseColor,
  Colors.orange,
  Colors.blue,
  Colors.teal,
  Colors.green,
  Colors.yellow,
  Colors.orange,
  Colors.pink,
  Colors.purple,
  Colors.red,
  Colors.lightBlue,
  Colors.cyan,
  Color.fromARGB(255, 15, 15, 15),
  Colors.brown,
  Colors.deepOrange,
  Colors.indigo,
  Colors.lime,
  Colors.pinkAccent,
  Color(0xFF6A0572), // Vivid Purple
  Color(0xFF00A6ED), // Bright Sky Blue
  Color(0xFFFF4081), // Deep Pink
  Color(0xFF9C27B0), // Royal Purple
  Color(0xFFE91E63), // Vibrant Pink
  Color(0xFF795548), // Mocha Brown
  Color(0xFFFF5722), // Neon Orange
  Color(0xFF009688), // Dark Teal
  Color(0xFF607D8B), // Steel Blue
  Color(0xFF4CAF50), // Fresh Green
];
const List<String> colorText = <String>[
  "默认1",
  "默认2",
  "天空蓝",
  "淡雅绿",
  "浅草绿",
  "活泼黄",
  "活力橙",
  "少女粉",
  "高贵紫",
  "深宫红",
  "海水蓝",
  "青漾青",
  "酷炫黑",
  "皮革棕",
  "深橘色",
  "深海蓝",
  "柠檬青",
  "二次粉",
  "鲜艳紫",
  "明亮天蓝",
  "深粉红",
  "皇室紫",
  "活力粉",
  "摩卡棕",
  "霓虹橙",
  "深青绿",
  "钢蓝",
  "清新绿",
];

ThemeData updateThemes(int colorIndex, bool useMaterial3, bool isDark) {
  var themeData = ThemeData(
      colorSchemeSeed: colorOptions[colorIndex],
      useMaterial3: useMaterial3,
      scaffoldBackgroundColor: isDark ? Colors.black : null,
      dialogBackgroundColor:
          isDark ? PlatformTheme.darkSettingSectionColor : null,
      brightness: isDark ? Brightness.dark : Brightness.light);
  return themeData;
}

ThemeData updateThemesByColor(Color c, bool useMaterial3, bool isDark) {
  final ColorScheme colorScheme = ColorScheme.fromSeed(
    seedColor: c,
    // primaryFixed: c,
    primary: c,
    brightness: isDark ? Brightness.dark : Brightness.light,
  );
  if (isOnMacOS()) {
    Window.overrideMacOSBrightness(dark: isDark);
  }

  var themeData = ThemeData(
      colorScheme: colorScheme,
      useMaterial3: useMaterial3,
      scaffoldBackgroundColor: isDark ? Colors.black : Colors.white,
      dialogBackgroundColor:
          isDark ? PlatformTheme.darkSettingSectionColor : null,
      brightness: isDark ? Brightness.dark : Brightness.light);
  return themeData;
}
