import 'dart:ui';

import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:karyo/karyo.dart';

class VipIntroPage extends StatelessWidget {
  int _selectedPlan = 0;

  TextEditingController _accController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBar(context),
        body: CupertinoPageScaffold(
            child: ListView(padding: const EdgeInsets.all(16), children: [
          const SizedBox(
            height: 8,
          ),
          Column(
            children: [
              Container(
                width: 70,
                height: 70,
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                  boxShadow: [
                    BoxShadow(blurRadius: 18, color: Colors.grey.withAlpha(80))
                  ],
                ),
                child: Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      image: const DecorationImage(
                        image: AssetImage("assets/Logo/s2.png"),
                        fit: BoxFit.contain,
                      )),
                ),
              ),
              const SizedBox(
                height: 4,
              ),
              Text(
                "Daybreak",
                style: NORMAL_TXT_STYLE.copyWith(color: ColorBook.yellowVIP),
              ),
              const SizedBox(
                height: 12,
              ),
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.all(16),
                      child: Text(
                        "你的另一个大脑, Daybreak还支持Windows, Linux, macOS, iOS, Android，一个会员账号通用所有平台",
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: SMALL_BOLD_TXT_STYLE.copyWith(
                            color: Colors.grey.withAlpha(60),
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
          const SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.all(2),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    child: Column(children: [
                  Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      color: ColorBook.yellowLight.withAlpha(90),
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 5,
                            color: Color.fromARGB(255, 199, 193, 193)
                                .withAlpha(20))
                      ],
                    ),
                    // max: 100,
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "46.2¥",
                          style: NORMAL_BOLD_TXT_STYLE.copyWith(
                              fontFamily: FONT_FOR_NUMBER,
                              color: ColorBook.yellowLight,
                              fontSize: 28),
                        ),
                        Text(
                          "原价: ￥79",
                          style: NORMAL_BOLD_TXT_STYLE.copyWith(
                            color: Colors.red,
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                      ],
                    ),
                  ),
                  g16,
                  GestureDetector(
                    onTap: () {
                      GoTo(context, VipPayGuide());
                    },
                    child: Container(
                      padding: const EdgeInsets.only(
                          left: 16, right: 16, top: 8, bottom: 8),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: ColorBook.brownVIP.withAlpha(190),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 5,
                              color: const Color.fromARGB(255, 199, 193, 193)
                                  .withAlpha(20))
                        ],
                      ),
                      child: Text(
                        "1年尊享",
                        style: NORMAL_BOLD_TXT_STYLE.copyWith(
                          color: ColorBook.yellowLight,
                        ),
                      ),
                    ),
                  ),
                ])),
                const SizedBox(
                  width: 16,
                ),
                Expanded(
                    child: Column(children: [
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                    // height: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      color: ColorBook.yellowLight.withAlpha(90),
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 5,
                            color: Color.fromARGB(255, 199, 193, 193)
                                .withAlpha(20))
                      ],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "72.4¥",
                          style: NORMAL_BOLD_TXT_STYLE.copyWith(
                              fontFamily: FONT_FOR_NUMBER,
                              color: ColorBook.yellowLight,
                              fontSize: 28),
                        ),
                        Text(
                          "原价: ￥99.2",
                          style: NORMAL_BOLD_TXT_STYLE.copyWith(
                            color: Colors.red,
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                      ],
                    ),
                  ),
                  g16,
                  GestureDetector(
                      onTap: () {
                        GoTo(context, VipPayGuide());
                      },
                      child: Container(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, top: 8, bottom: 8),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: ColorBook.brownVIP.withAlpha(190),
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 5,
                                color: const Color.fromARGB(255, 199, 193, 193)
                                    .withAlpha(20))
                          ],
                        ),
                        child: Text(
                          "2年尊享",
                          style: NORMAL_BOLD_TXT_STYLE.copyWith(
                            color: ColorBook.yellowLight,
                          ),
                        ),
                      ))
                ])),
                g16,
                Expanded(
                  child: Column(children: [
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: ColorBook.yellowLight.withAlpha(90),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 5,
                              color: Color.fromARGB(255, 199, 193, 193)
                                  .withAlpha(20))
                        ],
                      ),
                      // height: 100,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "99¥",
                            style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                fontFamily: FONT_FOR_NUMBER,
                                color: ColorBook.yellowLight,
                                fontSize: 28),
                          ),
                          Text(
                            "原价: ￥168",
                            style: NORMAL_BOLD_TXT_STYLE.copyWith(
                              color: Colors.red,
                              decoration: TextDecoration.lineThrough,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    GestureDetector(
                        onTap: () {
                          GoTo(context, VipPayGuide());
                        },
                        child: Container(
                          padding: EdgeInsets.only(
                              left: 16, right: 16, top: 8, bottom: 8),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: ColorBook.brownVIP.withAlpha(190),
                            boxShadow: [
                              BoxShadow(
                                  blurRadius: 5,
                                  color: Color.fromARGB(255, 199, 193, 193)
                                      .withAlpha(20))
                            ],
                          ),
                          child: Text(
                            "永久尊享",
                            style: NORMAL_BOLD_TXT_STYLE.copyWith(
                              color: ColorBook.yellowLight,
                            ),
                          ),
                        ))
                  ]),
                )
              ],
            ),
          ),
          SizedBox(height: 16),
          Container(
              width: 200,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    '创世白金(氪金)会员专属权益',
                    textAlign: TextAlign.center,
                    style: HERO_TITLE_TXT_BOLD_STYLE.copyWith(
                        color: ColorBook.yellowVIP),
                  ),
                  SizedBox(height: 16),
                  Flexible(
                    child: Container(
                      width: 200,
                      child: Text("* 以上价格为限时特惠，随着产品功能扩张和迭代，价格可能升高，但已开通用户权益保持不变",
                          softWrap: true,
                          textAlign: TextAlign.center,
                          style: EX_SMALL_TXT_STYLE),
                    ),
                  )
                ],
              )),
          GridView.count(
              physics: BouncingScrollPhysics(),
              shrinkWrap: true,
              childAspectRatio: 0.7,
              crossAxisCount: 3,
              mainAxisSpacing: 8.0,
              crossAxisSpacing: 8.0,
              padding: EdgeInsets.symmetric(vertical: 16),
              children: [
                ProfileCardWidget(
                  title: "主题皮肤",
                  desc: "换色换心情",
                  icon: Container(
                    width: 32,
                    child: Icon(
                      FluentIcons.color_24_filled,
                      size: 28,
                      color: ColorBook.brownVIP,
                    ),
                  ),
                  iconColor: RandomColorGen.nextColor(),
                ),
                ProfileCardWidget(
                  title: "身份标识",
                  desc: "在英雄榜,全球可见",
                  icon: Container(
                    width: 32,
                    child: Icon(
                      FontAwesomeIcons.productHunt,
                      size: 32,
                      color: ColorBook.brownVIP,
                    ),
                  ),
                  iconColor: RandomColorGen.nextColor(),
                ),
                ProfileCardWidget(
                  title: "毛玻璃效果",
                  desc: "专属VIP的半透明效果",
                  icon: Container(
                    width: 32,
                    child: Icon(
                      FluentIcons.blur_24_filled,
                      size: 32,
                      color: ColorBook.brownVIP,
                    ),
                  ),
                  iconColor: RandomColorGen.nextColor(),
                ),
                ProfileCardWidget(
                  title: "无限设备同步",
                  desc: "走哪儿用哪儿",
                  icon: Container(
                    width: 32,
                    child: Icon(
                      FontAwesomeIcons.deviantart,
                      size: 32,
                      color: ColorBook.brownVIP,
                    ),
                  ),
                  iconColor: RandomColorGen.nextColor(),
                ),
                ProfileCardWidget(
                  title: "智能统计",
                  desc: "AI统计数据",
                  icon: Container(
                    width: 32,
                    child: Icon(
                      FontAwesomeIcons.intercom,
                      size: 32,
                      color: ColorBook.brownVIP,
                    ),
                  ),
                  iconColor: RandomColorGen.nextColor(),
                ),
                ProfileCardWidget(
                  title: "技术支持",
                  desc: "专人解答",
                  icon: Container(
                    width: 32,
                    child: Icon(
                      FontAwesomeIcons.teamspeak,
                      size: 32,
                      color: ColorBook.brownVIP,
                    ),
                  ),
                  iconColor: RandomColorGen.nextColor(),
                ),
                ProfileCardWidget(
                  title: "UI Pro",
                  desc: "更美观的界面",
                  icon: Container(
                    width: 32,
                    child: Icon(
                      FontAwesomeIcons.uikit,
                      size: 32,
                      color: ColorBook.brownVIP,
                    ),
                  ),
                  iconColor: RandomColorGen.nextColor(),
                ),
                ProfileCardWidget(
                  title: "永久有效",
                  desc: "未来新增特性",
                  icon: Container(
                    width: 32,
                    child: Icon(
                      FontAwesomeIcons.diamond,
                      size: 32,
                      color: ColorBook.brownVIP,
                    ),
                  ),
                  iconColor: RandomColorGen.nextColor(),
                ),
                ProfileCardWidget(
                  title: "柠檬时间",
                  desc: "原子化统计",
                  icon: Container(
                    width: 32,
                    child: Icon(
                      FontAwesomeIcons.atom,
                      size: 28,
                      color: ColorBook.brownVIP,
                    ),
                  ),
                  iconColor: RandomColorGen.nextColor(),
                ),
                ProfileCardWidget(
                  title: "今日时间PRO",
                  desc: "精细化时段安排",
                  icon: Container(
                    width: 32,
                    child: Icon(
                      FluentIcons.weather_sunny_24_filled,
                      size: 32,
                      color: ColorBook.brownVIP,
                    ),
                  ),
                  iconColor: RandomColorGen.nextColor(),
                ),
                ProfileCardWidget(
                  title: "清单列表∞",
                  desc: "不限列表数",
                  icon: Container(
                    width: 32,
                    child: Icon(
                      FluentIcons.expand_up_left_16_filled,
                      size: 32,
                      color: ColorBook.brownVIP,
                    ),
                  ),
                  iconColor: RandomColorGen.nextColor(),
                ),
                ProfileCardWidget(
                  title: "Markdown支持",
                  desc: "md记录重要内容",
                  icon: Container(
                    width: 32,
                    child: Icon(
                      FluentIcons.markdown_20_filled,
                      size: 32,
                      color: ColorBook.brownVIP,
                    ),
                  ),
                  iconColor: RandomColorGen.nextColor(),
                ),
              ]),
          SizedBox(
            height: 32,
          ),
          CupertinoButton(
            color: ColorBook.brownVIP,
            child: Text('立即开通'),
            onPressed: () {
              GoTo(context, VipPayGuide());
            },
          ),
          SizedBox(
            height: 32,
          ),
        ])));
  }

  PreferredSizeWidget appBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size(double.infinity, 74),
      child: ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 12, sigmaY: 12),
          child: Container(
            color: CupertinoTheme.of(context)
                .scaffoldBackgroundColor
                .withOpacity(0.8),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(width: 18),
                      ClipRRect(
                          borderRadius: BorderRadius.circular(4),
                          child: SvgPicture.asset(
                            'assets/icons/vip.svg',
                            fit: BoxFit.cover,
                            height: 32,
                            width: 32,
                          )),
                      SizedBox(width: 16),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Text(
                            '开通Daybreak会员',
                            style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                color: ColorBook.yellowVIP),
                          ),
                          SizedBox(height: 4),
                          GestureDetector(
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.baseline,
                                textBaseline: TextBaseline.alphabetic,
                                children: <Widget>[
                                  Text(
                                    '说明',
                                    style: SMALL_TXT_STYLE,
                                  ),
                                  SizedBox(width: 2),
                                  Icon(
                                    CupertinoIcons.right_chevron,
                                    size: 14,
                                    color: ColorBook.yellowVIP,
                                  )
                                ]),
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) => AlertDialog(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(20.0))),
                                        title: Text(
                                          '说明',
                                          style: NORMAL_BOLD_TXT_STYLE,
                                        ),
                                        content: Text(
                                          '会员支付与售后说明：'
                                          '\n1. 一次性知识付费服务，一旦付费无法退款，请谨慎购买;'
                                          '\n2. 由于税收政策，我们无法为小额支付提供发票开具服务'
                                          '\n3. 开通会员即代表您认可并知晓我们的协议'
                                          '\n4. APP还在不断完善中, 新付费功能会不断为会员用户开放',
                                          style: NORMAL_TXT_STYLE,
                                        ),
                                        actions: <Widget>[
                                          new TextButton(
                                            child: new Text(
                                              "我知道了",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: COLOR_PRIMARY),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      ));
                            },
                          )
                        ],
                      )),
                      GestureDetector(
                        onTap: () => Navigator.of(context).pop(),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            margin: EdgeInsets.only(top: 14),
                            padding: EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.1),
                              shape: BoxShape.circle,
                            ),
                            child: Icon(
                              Icons.close,
                              size: 24,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 14),
                    ],
                  ),
                ),
                Divider(
                  height: 1,
                  color: Colors.transparent,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ProfileCardWidget extends StatelessWidget {
  final String? title;
  final String? desc;
  final Widget? icon;
  final Color? iconColor;

  const ProfileCardWidget(
      {Key? key, this.title, this.desc, this.icon, this.iconColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Theme.of(context).dialogBackgroundColor,
      elevation: 8.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      shadowColor: Colors.grey.withAlpha(20),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            SizedBox(height: 8.0),
            icon!,
            SizedBox(height: 8.0),
            Text(
              '$title',
              textAlign: TextAlign.center,
              style: NORMAL_BOLD_TXT_STYLE.copyWith(color: ColorBook.yellowVIP),
            ),
            Text(
              '$desc',
              textAlign: TextAlign.center,
              style: SMALL_TXT_STYLE.copyWith(color: ColorBook.yellowVIP),
            ),
          ],
        ),
      ),
    );
  }
}
