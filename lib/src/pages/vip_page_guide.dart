import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:karyo/karyo.dart';
import 'package:url_launcher/url_launcher.dart';

class VipPayGuide extends StatelessWidget {
  void _saveImage(BuildContext context) async {
    // ByteData bytes = await rootBundle.load("assets/images/wx.png");
    // final result = await ImageGallerySaver.saveImage(bytes.buffer.asUint8List(),
    //     quality: 100, name: "mana_wxp");
    // print(result);
    // if (result != null) {
    //   showToast(context, "二维码已保存，请前往微信扫码支付", COLOR_PRIMARY);
    // } else {
    //   showToast(context, "请直接截图前往微信扫码支付", COLOR_PRIMARY);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBar(context),
        body: CupertinoPageScaffold(
            child: ListView(padding: const EdgeInsets.all(16), children: [
          Text(
            '1⃣️. 根据您要开通的PRO权限种类，点击下方蓝色按钮，支付宝支付相应金额，发送截图付款成功页面给客服即可（或者扫下面二维码支付)',
            style: NORMAL_TXT_STYLE.copyWith(color: ColorBook.yellowVIP),
          ),
          SizedBox(
            height: 15,
          ),
          Center(
            child: Container(
              width: 150,
              height: 150,
              //超出部分，可裁剪
              clipBehavior: Clip.hardEdge,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(24),
              ),
              child: Image(
                image: CachedNetworkImageProviderEnhanced(
                    'https://jihulab.com/godly/fger/-/raw/main/images/2023/10/20_19_59_56_pay.png'),
                // width: 250,
                // height: 250,
              ),
            ),
          ),
          const SizedBox(height: 32),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: CupertinoButton(
                color: Colors.blue,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      FontAwesomeIcons.alipay,
                      size: 25,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Text(
                      '支付宝支付',
                      // textAlign: TextAlign.center,
                      style:
                          NORMAL_BOLD_TXT_STYLE.copyWith(color: Colors.white),
                    ),
                  ],
                ),
                onPressed: () {
                  launchURL("https://qr.alipay.com/fkx15672dogn5mvh0tmez70");
                  // ClipboardData data = new ClipboardData(text: "jintianiloveu");
                  // Clipboard.setData(data);
                  // launch("weixin://");
                }),
          ),
          // GestureDetector(
          //   child: Container(
          //     width: 200,
          //     height: 200,
          //     child: Image.asset("assets/images/wx.png"),
          //   ),
          //   onLongPress: () {
          //     HapticFeedback.lightImpact();
          //     _saveImage(context);
          //   },
          // ),
          const SizedBox(height: 32),
          Text(
            '2⃣️. 直接点击下方按钮复制客户微信号并跳转微信添加，将支付截图和账号发给客服即可。添加可备注暗号：【DB大玩家】',
            style: NORMAL_TXT_STYLE.copyWith(color: ColorBook.yellowVIP),
          ),
          const SizedBox(height: 32),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: CupertinoButton(
                color: Colors.green,
                child: Text(
                  '复制客服微信(或者扫码)',
                  textAlign: TextAlign.center,
                  style: NORMAL_BOLD_TXT_STYLE.copyWith(color: Colors.white),
                ),
                onPressed: () {
                  ClipboardData data = const ClipboardData(text: "bojuebot");
                  Clipboard.setData(data);
                  launch("weixin://");
                }),
          ),
          const SizedBox(
            height: 32,
          ),
          Center(
            child: Container(
              width: 150,
              height: 150,
              //超出部分，可裁剪
              clipBehavior: Clip.hardEdge,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(24),
              ),
              child: Image(
                image: AssetImage("assets/qrcode/chat2.png"),
                width: 250,
                height: 250,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            '会员须知：\n1. 由于我们致力于精确维护每一位用户的权益，因此采用线下添加微信的方式完成PRO权限开通;\n'
            '2. 一次性知识付费服务, 一旦购买无法退款, 请谨慎购买;\n'
            '3. PRO权限不得转卖、倒卖，一经发现会立即取消PRO权限;\n'
            '4. 对于尊贵的PRO用户，我们的软件还在不断完善中，如果有一些瑕疵还请见谅，我们会持续不断地改善我们的产品;\n'
            '5. 添加客服微信后，还可以让客服邀请你加入我们的官方交流小镇，我们一起探索高效率的工作和生活方式，分享生活中的绝妙idea',
            style: SMALL_TXT_STYLE,
          )
        ])));
  }

  PreferredSizeWidget appBar(BuildContext context) {
    return CupertinoNavigationBar(
        backgroundColor: Colors.transparent,
        border: const Border(bottom: BorderSide(color: Colors.transparent)),
        // leading: CupertinoButton(
        //   padding: EdgeInsets.all(0),
        //   child: Icon(FlutterIcons.chevron_left_ent, color: COLOR_PRIMARY),
        //   onPressed: () {
        //     Navigator.of(context).pop();
        //   },
        // ),
        automaticallyImplyLeading: false,
        middle: Text('开通PRO权限步骤',
            style: NORMAL_BOLD_TXT_STYLE.copyWith(color: ColorBook.yellowVIP)),
        trailing: CupertinoButton(
          padding: const EdgeInsets.all(0),
          child: Text(
            '完成',
            style: TextStyle(
              color: ColorBook.yellowVIP,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ));
  }
}
