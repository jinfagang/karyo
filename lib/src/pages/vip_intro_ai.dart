import 'dart:math';
import 'dart:ui';

import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:iconsax_flutter/iconsax_flutter.dart';
import 'package:karyo/karyo.dart';
import 'package:karyo/src/pages/vip_page_guide_sim.dart';
import 'package:karyo/src/widgets/marqueen_widget.dart';

import '../widgets/ratio_button.dart';

class CommentA {
  final String avatarUrl;
  final String name;
  final String content;

  CommentA(
      {required this.avatarUrl, required this.name, required this.content});
}

// ignore: must_be_immutable
class VipIntroPageAI extends StatefulWidget {
  @override
  State<VipIntroPageAI> createState() => _VipIntroPageAIState();
}

class _VipIntroPageAIState extends State<VipIntroPageAI> {
  int _selectedPlan = 5;

  TextEditingController _accController = TextEditingController();

  final List<CommentA> comments = [
    CommentA(
      avatarUrl:
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/07/23_18_0_11_20230723180010.png',
      name: '柠檬的鱼',
      content: '这个软件太棒了，界面很美很简约，还可以写作文，也不贵',
    ),
    CommentA(
      avatarUrl:
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/07/23_17_59_56_20230723175956.png',
      name: '很有范儿',
      content: '太方便了，每天都可以问他学到很多知识，比搜索引擎好用',
    ),
    CommentA(
      avatarUrl:
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/07/23_17_59_38_20230723175938.png',
      name: '大大铭',
      content: '和AI助手聊天实在是太有趣了',
    ),
  ];

  final List<CommentA> openvipsMembers = [
    CommentA(
      avatarUrl:
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/07/23_17_55_10_20230723175509.png',
      name: 'r***j',
      content: '这个软件太棒了，界面很美很简约，还可以写作文，也不贵',
    ),
    CommentA(
      avatarUrl:
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/07/23_17_55_29_20230723175529.png',
      name: '12***34',
      content: '太方便了，每天都可以问他学到很多知识，比搜索引擎好用',
    ),
    CommentA(
      avatarUrl:
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/07/23_17_58_20_20230723175820.png',
      name: '2***j',
      content: '和AI助手聊天实在是太有趣了',
    ),
    CommentA(
      avatarUrl:
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/07/23_17_57_5_20230723175705.png',
      name: 'z***k',
      content: '和AI助手聊天实在是太有趣了',
    ),
    CommentA(
      avatarUrl:
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/07/23_17_57_20_20230723175719.png',
      name: 'l***t',
      content: '和AI助手聊天实在是太有趣了',
    ),
    CommentA(
      avatarUrl:
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/07/23_17_57_35_20230723175734.png',
      name: '15***2',
      content: '和AI助手聊天实在是太有趣了',
    ),
    CommentA(
      avatarUrl:
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/07/23_17_58_0_20230723175800.png',
      name: 'k***g',
      content: '和AI助手聊天实在是太有趣了',
    ),
    CommentA(
      avatarUrl:
          'https://jihulab.com/godly/fger/-/raw/main/images/2023/07/23_17_58_20_20230723175820.png',
      name: 'l***p',
      content: '和AI助手聊天实在是太有趣了',
    ),
  ];

  String getPriceBySelect(int s) {
    if (s == 0) {
      return "112";
    } else if (s == 2) {
      return "85";
    } else if (s == 3) {
      return "55";
    } else if (s == 5) {
      return "42";
    }
    return "20";
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldWrapperAdapt(
      child: buildMain(context),
    );
  }

  Widget buildMain(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    return GradientScaffold(
        appBar: AppBar(
          actions: [buildCloseButton(context)],
          backgroundColor: Colors.transparent,
          automaticallyImplyLeading: false,
        ),
        child: Stack(children: [
          ListView(padding: const EdgeInsets.all(16), children: [
            const SizedBox(
              height: 8,
            ),
            // Column(
            //   children: [
            //     Container(
            //       width: 70,
            //       height: 70,
            //       padding: const EdgeInsets.all(0),
            //       decoration: BoxDecoration(
            //         color: Colors.white,
            //         borderRadius: BorderRadius.circular(34),
            //         boxShadow: [
            //           BoxShadow(
            //               blurRadius: 18, color: Colors.grey.withAlpha(80))
            //         ],
            //       ),
            //       child: Container(
            //         width: 70,
            //         height: 70,
            //         decoration: BoxDecoration(
            //             color: Colors.transparent,
            //             image: const DecorationImage(
            //               image: AssetImage("assets/images/artwork.png"),
            //               fit: BoxFit.contain,
            //             )),
            //       ),
            //     ),
            //     const SizedBox(
            //       height: 4,
            //     ),
            //     Text(
            //       "AI助手 Plus",
            //       style: HERO_TITLE_TXT_STYLE.copyWith(
            //           color: tClrPrimaryContainer(context)),
            //     ),
            //     const SizedBox(
            //       height: 12,
            //     ),
            //     Row(
            //       children: [
            //         Expanded(
            //           child: Padding(
            //             padding: EdgeInsets.all(16),
            //             child: Text(
            //               "一个你口袋的万能助手，随时恭候你的命令",
            //               softWrap: true,
            //               textAlign: TextAlign.center,
            //               style: SMALL_TXT_STYLE.copyWith(
            //                   color: tClrPrimaryContainer(context),
            //                   fontWeight: FontWeight.bold),
            //             ),
            //           ),
            //         )
            //       ],
            //     )
            //   ],
            // ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "✦ 会员权益 ✦",
                  style: HERO_TITLE_TXT_BOLD_STYLE.copyWith(
                      color: ColorBook.yellowVIP),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: BoxDecoration(
                  gradient: const LinearGradient(
                    colors: [
                      ColorBook.yellowVIP,
                      ColorBook.yellowVIPLight,
                    ],
                  ),
                  borderRadius: BorderRadius.circular(26),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(1.5),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).dialogBackgroundColor,
                      borderRadius: BorderRadius.circular(26),
                    ),
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            const Icon(
                              Iconsax.bubble,
                              color: ColorBook.yellowVIP,
                              size: 19,
                            ),
                            g8,
                            Text(
                              "拥有所有会员权限",
                              style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                  color: ColorBook.yellowVIP),
                            )
                          ],
                        ),
                        Text(
                          "高级外观系统、未来新增的会员功能均可开放",
                          style: NORMAL_TXT_STYLE.copyWith(
                              color: ColorBook.yellowVIP, fontSize: 13),
                        ),
                        g8,
                        Row(
                          children: [
                            const Icon(
                              Iconsax.cloud_sunny_copy,
                              color: ColorBook.yellowVIP,
                              size: 19,
                            ),
                            g8,
                            Text(
                              "无限制项目数目、同步次数等",
                              style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                  color: ColorBook.yellowVIP),
                            )
                          ],
                        ),
                        Text(
                          "应用内项目个数，同步次数等均不限制",
                          style: NORMAL_TXT_STYLE.copyWith(
                              color: ColorBook.yellowVIP, fontSize: 13),
                        ),
                        g8,
                        Row(
                          children: [
                            const Icon(
                              Iconsax.magic_star,
                              color: ColorBook.yellowVIP,
                              size: 19,
                            ),
                            g8,
                            Text(
                              "AI能力",
                              style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                  color: ColorBook.yellowVIP),
                            )
                          ],
                        ),
                        Text(
                          "不限日调用次数、翻译、润色全面开放",
                          style: NORMAL_TXT_STYLE.copyWith(
                              color: ColorBook.yellowVIP, fontSize: 13),
                        ),
                        g8,
                        Row(
                          children: [
                            const Icon(
                              Iconsax.cloud_connection,
                              color: ColorBook.yellowVIP,
                              size: 19,
                            ),
                            g8,
                            Text(
                              "同时在线终端数",
                              style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                  color: ColorBook.yellowVIP),
                            )
                          ],
                        ),
                        Text(
                          "允许同时超过3个终端在线",
                          style: NORMAL_TXT_STYLE.copyWith(
                              color: ColorBook.yellowVIP, fontSize: 13),
                        ),
                        g8,
                        Row(
                          children: [
                            const Icon(
                              Iconsax.diamonds,
                              color: ColorBook.yellowVIP,
                              size: 19,
                            ),
                            g8,
                            Text(
                              "社区身份标识",
                              style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                  color: ColorBook.yellowVIP),
                            )
                          ],
                        ),
                        Text(
                          "会员用户将在社区彰显会员身份, 优先客服、答疑、认证等人工服务",
                          style: NORMAL_TXT_STYLE.copyWith(
                              color: ColorBook.yellowVIP, fontSize: 13),
                        ),
                        g8,
                      ],
                    ),
                  ),
                ),
              ),
            ),

            SizedBox(
              height: 180,
              width: screenWidth,
              child: GridView.count(
                  physics: const BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  childAspectRatio: 1.2,
                  crossAxisCount: 1,
                  mainAxisSpacing: 8.0,
                  crossAxisSpacing: 8.0,
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  children: [
                    ProfileCardWidget2Pay(
                      title: "终身会员",
                      desc: "112",
                      icon: const SizedBox(
                        width: 32,
                        child: const Icon(
                          FluentIcons.align_left_16_filled,
                          size: 28,
                          color: ColorBook.brownVIP,
                        ),
                      ),
                      iconColor: RandomColorGen.nextColor(),
                      isSelected: _selectedPlan == 0,
                      onPressed: () {
                        setState(() {
                          _selectedPlan = 0;
                        });
                      },
                      originPrice: '168',
                      detail: '0.01元/天',
                      isCheap: true,
                    ),
                    ProfileCardWidget2Pay(
                      title: "年会员",
                      desc: "42",
                      icon: const SizedBox(
                        width: 32,
                        child: Icon(
                          FluentIcons.align_left_16_filled,
                          size: 28,
                          color: ColorBook.brownVIP,
                        ),
                      ),
                      iconColor: RandomColorGen.nextColor(),
                      isSelected: _selectedPlan == 5,
                      onPressed: () {
                        setState(() {
                          _selectedPlan = 5;
                        });
                      },
                      originPrice: '128',
                      detail: '0.01元/天',
                      isCheap: true,
                    ),
                    ProfileCardWidget2Pay(
                      title: "2000额度",
                      desc: "85",
                      icon: const SizedBox(
                        width: 32,
                        child: const Icon(
                          FontAwesomeIcons.faceKiss,
                          size: 32,
                          color: ColorBook.brownVIP,
                        ),
                      ),
                      iconColor: RandomColorGen.nextColor(),
                      isSelected: _selectedPlan == 2,
                      onPressed: () {
                        setState(() {
                          _selectedPlan = 2;
                        });
                      },
                      originPrice: '120',
                      detail: '0.02/天',
                      isCheap: true,
                    ),
                    ProfileCardWidget2Pay(
                      title: "1000额度",
                      desc: "55",
                      icon: const SizedBox(
                        width: 32,
                        child: const Icon(
                          FontAwesomeIcons.faceKiss,
                          size: 32,
                          color: ColorBook.brownVIP,
                        ),
                      ),
                      iconColor: RandomColorGen.nextColor(),
                      isSelected: _selectedPlan == 3,
                      onPressed: () {
                        setState(() {
                          _selectedPlan = 3;
                        });
                      },
                      originPrice: '100',
                      detail: '可用长达半年',
                      isCheap: true,
                    ),
                    ProfileCardWidget2Pay(
                      title: "200额度",
                      desc: "20",
                      icon: const SizedBox(
                        width: 32,
                        child: Icon(
                          FluentIcons.layer_diagonal_person_20_filled,
                          size: 32,
                          color: ColorBook.brownVIP,
                        ),
                      ),
                      iconColor: RandomColorGen.nextColor(),
                      isSelected: _selectedPlan == 4,
                      onPressed: () {
                        setState(() {
                          _selectedPlan = 4;
                        });
                      },
                      originPrice: '40',
                      detail: '新手额度',
                    ),
                  ]),
            ),

            Container(
              padding: EdgeInsets.only(left: 8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  gradient: LinearGradient(
                    colors: [
                      ColorBook.yellowLight.withAlpha(40),
                      ColorBook.orangeVIP.withAlpha(50)
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  )),
              child: MarqueeWidget(
                count: openvipsMembers.length,
                itemBuilder: (BuildContext context, int index) {
                  return _buildMarqueeRow(openvipsMembers[index]);
                },
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 5.0, horizontal: 8.0),
              width: 100,
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
                gradient: LinearGradient(
                  colors: [Colors.orange.shade200, Colors.orange.shade500],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: const Text(
                "用户评价",
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
              ),
            ),
            const SizedBox(
              height: 8,
            ),

            Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
                decoration: BoxDecoration(
                    color: ColorBook.yellowLight.withAlpha(20),
                    shape: BoxShape.rectangle,
                    borderRadius: const BorderRadius.all(Radius.circular(12))),
                child: Column(
                  children: comments
                      .map((comment) => buildCommentRaw(comment.avatarUrl,
                          comment.name, true, comment.content))
                      .toList(),
                )),
            const SizedBox(
              height: 132,
            ),
          ]),
          Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: ClipRRect(
                  borderRadius:
                      const BorderRadius.vertical(top: Radius.circular(25)),
                  child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context)
                                .dialogBackgroundColor
                                .withAlpha(30),
                            borderRadius: const BorderRadius.vertical(
                                top: Radius.circular(25))),
                        padding: const EdgeInsets.only(
                            top: 14, left: 16, right: 16, bottom: 32),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Text(
                                  "¥${getPriceBySelect(_selectedPlan)}",
                                  style: HERO_TITLE_TXT_BOLD_STYLE,
                                ),
                                const SizedBox(
                                  width: 12,
                                ),
                                GroupRadioButton(
                                    padding: const EdgeInsets.all(8),
                                    radioRadius: 12,
                                    color: ColorBook.yellowVIP,
                                    onChanged: (v) {},
                                    label: [
                                      Row(children: [
                                        const Icon(
                                          FontAwesomeIcons.weixin,
                                          size: 16,
                                          color: Colors.green,
                                        ),
                                        const SizedBox(
                                          width: 8,
                                        ),
                                        Text(
                                          '微信',
                                          style: NORMAL_TXT_STYLE,
                                        ),
                                        // Ratio(value: false, onChanged: (v) {}),
                                      ]),
                                      Row(
                                        children: [
                                          const Icon(
                                            FontAwesomeIcons.alipay,
                                            size: 16,
                                            color: Colors.blue,
                                          ),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          Text(
                                            '支付宝',
                                            style: NORMAL_TXT_STYLE,
                                          ),
                                        ],
                                      )
                                    ]),
                              ],
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            MyElevatedButton(
                              width: screenWidth * 0.9,
                              height: 55,
                              gradient: const LinearGradient(
                                colors: [
                                  ColorBook.yellowLight,
                                  ColorBook.orangeVIP
                                ],
                              ),
                              onPressed: () {
                                GoTo(context, VipPayGuideSim());
                              },
                              child: Text(
                                '立即开通',
                                style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                    color: tClrPrimaryContainer(context)),
                              ),
                            ),
                          ],
                        ),
                      ))))
        ]));
  }

  Widget _buildMarqueeRow(CommentA a) {
    return Row(
      children: [
        CircleAvatar(
          backgroundImage: NetworkImage(a.avatarUrl),
          radius: 12,
        ),
        const SizedBox(
          width: 8,
        ),
        Text(
          "${a.name} 于${Random().nextInt(60)}分钟前购买了永久会员",
          style: NORMAL_BOLD_TXT_STYLE,
        ),
      ],
    );
  }

  PreferredSizeWidget appBar(BuildContext context) {
    return PreferredSize(
      preferredSize: const Size(double.infinity, 74),
      child: ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 12, sigmaY: 12),
          child: Container(
            color: CupertinoTheme.of(context)
                .scaffoldBackgroundColor
                .withOpacity(0.8),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const SizedBox(width: 18),
                      ClipRRect(
                          borderRadius: BorderRadius.circular(4),
                          child: SvgPicture.asset(
                            'assets/images/vip.svg',
                            fit: BoxFit.cover,
                            height: 32,
                            width: 32,
                          )),
                      const SizedBox(width: 16),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Text(
                            '获取Plus权限',
                            style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                color: ColorBook.yellowVIP),
                          ),
                          SizedBox(height: 4),
                          GestureDetector(
                            child: Text(
                              '说明',
                              style: SMALL_TXT_STYLE.copyWith(
                                  color: ColorBook.yellowLight),
                            ),
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) => AlertDialog(
                                        shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(20.0))),
                                        title: Text(
                                          '说明',
                                          style: NORMAL_BOLD_TXT_STYLE.copyWith(
                                              color: ColorBook.yellowLight),
                                        ),
                                        content: Text(
                                          '会员支付与售后说明：'
                                          '\n1. 一次性知识付费服务，一旦付费无法退款，请谨慎购买;'
                                          '\n2. 由于税收政策，我们无法为小额支付提供发票开具服务'
                                          '\n3. 开通PRO即代表您认可并知晓我们的协议'
                                          '\n4. APP还在不断完善中, 新付费功能会不断为会员用户开放',
                                          style: NORMAL_TXT_STYLE,
                                        ),
                                        actions: <Widget>[
                                          TextButton(
                                            child: const Text(
                                              "我知道了",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: ColorBook.yellowLight),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      ));
                            },
                          )
                        ],
                      )),
                      GestureDetector(
                        onTap: () => Navigator.of(context).pop(),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            margin: const EdgeInsets.only(top: 14),
                            padding: const EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.1),
                              shape: BoxShape.circle,
                            ),
                            child: const Icon(
                              Icons.close,
                              size: 24,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 14),
                    ],
                  ),
                ),
                const Divider(
                  height: 1,
                  color: Colors.transparent,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ProfileCardWidget2 extends StatelessWidget {
  final String? title;
  final String? desc;
  final Widget? icon;
  final Color? iconColor;

  const ProfileCardWidget2(
      {Key? key, this.title, this.desc, this.icon, this.iconColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: tClrPrimaryContainer(context),
      elevation: 0.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      // shadowColor: Colors.grey.withAlpha(10),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            const SizedBox(height: 8.0),
            icon!,
            const SizedBox(height: 8.0),
            Text(
              '$title',
              textAlign: TextAlign.center,
              style: NORMAL_BOLD_TXT_STYLE.copyWith(
                  color: tClrOnPrimaryContainer(context)),
            ),
            Text(
              '$desc',
              textAlign: TextAlign.center,
              style: SMALL_TXT_STYLE.copyWith(
                  color: tClrOnPrimaryContainer(context)),
            ),
          ],
        ),
      ),
    );
  }
}

class ProfileCardWidget2Pay extends StatelessWidget {
  final String? title;
  final String? desc;
  final String? detail;
  final String? originPrice;
  final Widget? icon;
  final Color? iconColor;
  final bool isCheap;
  final bool isSelected;
  final onPressed;

  const ProfileCardWidget2Pay(
      {Key? key,
      this.title,
      this.desc,
      this.originPrice,
      this.icon,
      this.isCheap = false,
      this.isSelected = false,
      this.iconColor,
      this.detail,
      this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Container(
          margin: const EdgeInsets.all(8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: ColorBook.yellowLight,
            gradient: isSelected
                ? const LinearGradient(
                    colors: [ColorBook.yellowLight, ColorBook.yellowVIP],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  )
                : null,
          ),
          width: isSelected ? 250 : 200,
          // height: isSelected ? 200 : 120,
          child: GestureDetector(
            onTap: onPressed,
            child: Padding(
              padding: const EdgeInsets.all(1.50),
              child: AnimatedContainer(
                duration: const Duration(milliseconds: 500),
                curve: Curves.easeInOut,
                decoration: BoxDecoration(
                  color: isSelected
                      ? null
                      : Theme.of(context).dialogBackgroundColor,
                  // gradient: isSelected
                  //     ? LinearGradient(
                  //         colors: [ColorBook.yellowLight, ColorBook.yellowVIP],
                  //         begin: Alignment.topLeft,
                  //         end: Alignment.bottomRight,
                  //       )
                  //     : null,
                  borderRadius: BorderRadius.circular(15),
                ),
                // shadowColor: Colors.grey.withAlpha(10),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      // SizedBox(height: 8.0),
                      // icon!,
                      // SizedBox(height: 8.0),
                      Text(
                        '$title',
                        textAlign: TextAlign.center,
                        style: NORMAL_BOLD_TXT_STYLE,
                      ),
                      Text(
                        '¥$desc',
                        textAlign: TextAlign.center,
                        style: HERO_TITLE_TXT_BOLD_STYLE,
                      ),
                      Text(
                        '原价: $originPrice',
                        textAlign: TextAlign.center,
                        style: SMALL_TXT_STYLE.copyWith(color: Colors.grey),
                      ),
                      Text(
                        '$detail',
                        textAlign: TextAlign.center,
                        style: SMALL_TXT_STYLE.copyWith(color: Colors.grey),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )),
      isCheap
          ? Positioned(
              right: 8,
              top: 0,
              child: Container(
                padding:
                    const EdgeInsets.only(bottom: 0, left: 8, right: 8, top: 0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  gradient: isCheap
                      ? const LinearGradient(
                          colors: [ColorBook.yellowLight, ColorBook.orangeVIP],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        )
                      : const LinearGradient(
                          colors: [ColorBook.yellowLight, ColorBook.orangeVIP],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                ),
                child: const Text(
                  '超值推荐',
                  style: TextStyle(
                      color: ColorBook.yellowVIPLight,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                      fontSize: 12),
                ),
              ),
            )
          : Container()
    ]);
  }
}
