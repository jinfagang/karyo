import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:karyo/karyo.dart';
import 'package:url_launcher/url_launcher.dart';

class VipPayGuideSim extends StatelessWidget {
  void _saveImage(BuildContext context) async {
    // ByteData bytes = await rootBundle.load("assets/images/wx.png");
    // final result = await ImageGallerySaver.saveImage(bytes.buffer.asUint8List(),
    //     quality: 100, name: "mana_wxp");
    // print(result);
    // if (result != null) {
    //   showToast(context, "二维码已保存，请前往微信扫码支付", COLOR_PRIMARY);
    // } else {
    //   showToast(context, "请直接截图前往微信扫码支付", COLOR_PRIMARY);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBar(context),
        body: CupertinoPageScaffold(
            child: ListView(
                shrinkWrap: true,
                padding: const EdgeInsets.all(16),
                children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.zero,
                    margin: EdgeInsets.zero,
                    width: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: ColorBook.orangeVIP.withAlpha(90)),
                    child: Center(
                      child: Text(
                        '1',
                        style: HERO_TITLE_TXT_BOLD_STYLE.copyWith(
                            fontSize: 29,
                            color: ColorBook.yellowVIP,
                            fontStyle: FontStyle.italic),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Text(
                      "点击下方按钮支付成功后截图付款凭据",
                      style: NORMAL_TXT_STYLE.copyWith(
                          color: ColorBook.yellowLight, fontSize: 17),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              const SizedBox(height: 32),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: CupertinoButton(
                    color: Colors.blue,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          FontAwesomeIcons.alipay,
                          size: 25,
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text(
                          '支付宝支付',
                          // textAlign: TextAlign.center,
                          style: NORMAL_BOLD_TXT_STYLE.copyWith(
                              color: Colors.white),
                        ),
                      ],
                    ),
                    onPressed: () {
                      launchURL(
                          "https://qr.alipay.com/fkx15672dogn5mvh0tmez70");
                      // ClipboardData data = new ClipboardData(text: "jintianiloveu");
                      // Clipboard.setData(data);
                      // launch("weixin://");
                    }),
              ),
              isOnMobile()
                  ? Container()
                  : GestureDetector(
                      child: Container(
                        width: 200,
                        height: 200,
                        child: CachedNetworkImageEnhanced(imageUrl: 'https://jihulab.com/godly/fger/-/raw/2fe9c121ffc9f24385c8633ccb4660fb444dbcf6/images/2023/10/20_19_59_56_pay.png'),
                      ),
                      onLongPress: () {
                        HapticFeedback.lightImpact();
                        _saveImage(context);
                      },
                    ),
              const SizedBox(height: 32),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.zero,
                    margin: EdgeInsets.zero,
                    width: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: ColorBook.orangeVIP.withAlpha(90)),
                    child: Center(
                      child: Text(
                        '2',
                        style: HERO_TITLE_TXT_BOLD_STYLE.copyWith(
                            fontSize: 29,
                            color: ColorBook.yellowVIP,
                            fontStyle: FontStyle.italic),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Text(
                      "点击下方按钮添加客服微信，发送付款凭据和账号进行开通 (或输入: davidplus09 添加)",
                      style: NORMAL_TXT_STYLE.copyWith(
                          color: ColorBook.yellowLight, fontSize: 17),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 32),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: CupertinoButton(
                    color: Colors.green,
                    child: Text(
                      '复制客服微信(或者扫码)',
                      textAlign: TextAlign.center,
                      style:
                          NORMAL_BOLD_TXT_STYLE.copyWith(color: Colors.white),
                    ),
                    onPressed: () {
                      ClipboardData data =
                          const ClipboardData(text: "davidplus09");
                      Clipboard.setData(data);
                      launch("weixin://");
                    }),
              ),
              const SizedBox(
                height: 32,
              ),
              isOnMobile()
                  ? Container()
                  : Center(
                      child: Container(
                        width: 150,
                        height: 150,
                        //超出部分，可裁剪
                        clipBehavior: Clip.hardEdge,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(24),
                        ),
                        child: Image(
                          image: CachedNetworkImageProviderEnhanced(
                              "https://jihulab.com/godly/fger/-/raw/main/images/2023/10/20_19_59_4_chat2.jpg"),
                          width: 250,
                          height: 250,
                        ),
                      ),
                    ),
              SizedBox(
                height: 15,
              ),
              Text(
                '会员须知：\n1. 为了确保用户的权益，请务必添加客服微信进行开通，也可以进一步享受我们的答疑服务;\n'
                '2. 一次性知识付费服务, 一旦购买无法退款, 请谨慎购买;\n'
                '3. Plus权限不得转卖、倒卖，一经发现我们有权取消Plus权限;\n'
                '4. 对于尊贵的Plus用户，我们的软件还在不断完善中，如果有一些瑕疵还请见谅，我们会持续不断地改善我们的产品;\n'
                '5. 开通代表您知悉我们的条款和协议\n'
                '6. 您支付成功后，我们会一直为您服务，如遇到任何开通问题，请随时与我们取得联系',
                style:
                    SMALL_TXT_STYLE.copyWith(color: Colors.grey.withAlpha(80)),
              )
            ])));
  }

  PreferredSizeWidget appBar(BuildContext context) {
    return CupertinoNavigationBar(
        backgroundColor: Colors.transparent,
        border: const Border(bottom: BorderSide(color: Colors.transparent)),
        // leading: CupertinoButton(
        //   padding: EdgeInsets.all(0),
        //   child: Icon(FlutterIcons.chevron_left_ent, color: COLOR_PRIMARY),
        //   onPressed: () {
        //     Navigator.of(context).pop();
        //   },
        // ),
        automaticallyImplyLeading: false,
        middle: Text('获取Plus权限指引',
            style: NORMAL_BOLD_TXT_STYLE.copyWith(color: ColorBook.yellowVIP)),
        trailing: CupertinoButton(
          padding: const EdgeInsets.all(0),
          child: Text(
            '完成',
            style: TextStyle(
              color: ColorBook.yellowVIP,
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ));
  }
}
