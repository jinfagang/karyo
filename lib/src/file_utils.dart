import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:file_saver/file_saver.dart';
import 'package:flutter/services.dart';
import 'package:karyo/karyo.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';
import 'package:image_gallery_saver_plus/image_gallery_saver_plus.dart';


enum TempFileType {
  pasteboardImage('tm_paste_board_image', '.png'),
  editImage('image_edit', '.png'),
  voiceRecord('voice_record', '.ogg'),
  logZip('log', '.zip'),
  // video thumbnail
  thumbnail('thumbnail', '.png');

  const TempFileType(this.prefix, this.suffix);

  final String prefix;
  final String suffix;
}

Future<String> saveFileFromUrlToCache(String fileUrl) async {
  var response = await Dio()
      .get(fileUrl, options: Options(responseType: ResponseType.bytes));
  File file = new File(fileUrl);
  String fileName = file.path.split('/').last;

  final cacheFolder = await getApplicationCacheDirectory();
  fileName = p.join(cacheFolder.path, fileName);
  kLog("cache file into target : $fileName");
  File f = await File(fileName).create();
  await f.writeAsBytes(
    Uint8List.fromList(response.data),
  );
  return fileName;
}

Future<String> saveFileFromUrlToTargetName(
    String fileUrl, String targetName) async {
  var response = await Dio()
      .get(fileUrl, options: Options(responseType: ResponseType.bytes));
  File file = new File(fileUrl);

  final cacheFolder = await getApplicationCacheDirectory();
  targetName = p.join(cacheFolder.path, targetName);
  kLog("cache file into target : $targetName");
  File f = await File(targetName).create();
  await f.writeAsBytes(
    Uint8List.fromList(response.data),
  );
  return targetName;
}

Future<String> generateTempFilePath(TempFileType type) async {
  final tempDir = await getTemporaryDirectory();
  return p.join(
    tempDir.path,
    '${type.prefix}_${const Uuid().v4()}${type.suffix}',
  );
}

Future<File?> saveBytesToTempFile(
  Uint8List bytes,
  TempFileType type,
) async {
  try {
    final file = File(await generateTempFilePath(type));
    if (file.existsSync()) {
      await file.delete();
    }
    await file.writeAsBytes(bytes);
    return file;
  } catch (error, stack) {
    kLog('failed to save bytes to temp file. $error $stack');
    return null;
  }
}

void renameFileWithTime(String path, DateTime time) {
  final file = File(path);
  if (!file.existsSync()) return;
  final newName = '${file.path}.${time.toIso8601String()}';
  file.renameSync(newName);
}

Future<String> saveBytesImageToLocal(Uint8List bytes,
    {String suffix = 'jpg'}) async {
  final cacheFolder = await getApplicationCacheDirectory();
  String fileName =
      DateTime.now().millisecondsSinceEpoch.toString() + '.$suffix';
  fileName = p.join(cacheFolder.path, fileName);

  File f = await File(fileName).create();
  await f.writeAsBytes(
    Uint8List.fromList(bytes),
  );
  return fileName;
}

Future<bool> saveBytesImageToAlbum(
  Uint8List bytes,
) async {
  // bytes use uuid
  const _uuid = Uuid();
  String fileName = _uuid.v4();
  if (isOnMobile()) {
    final result =
        await ImageGallerySaverPlus.saveImage(bytes, quality: 100, name: fileName);
    if (result['filePath'] == null) {
      return false;
    }
  } else {
    // desktop save to Downloads?
    // MimeType mt = lookupMimeType(file.path);
    String path = await FileSaver.instance.saveFile(
        name: fileName, bytes: bytes, ext: 'png', mimeType: MimeType.png);
    kLog('save to: $path');
    return true;
  }
  return true;
}

Future<bool> saveNetworkImageToLocal(String imageUrl) async {
  var response = await Dio()
      .get(imageUrl, options: Options(responseType: ResponseType.bytes));
  File file = new File(imageUrl);
  String fileName = file.path.split('/').last;
  if (isOnMobile()) {
    final result = await ImageGallerySaverPlus.saveImage(
        Uint8List.fromList(response.data),
        quality: 100,
        name: fileName);
    if (result['filePath'] == null) {
      return false;
    }
  } else {
    // desktop save to Downloads?
    // MimeType mt = lookupMimeType(file.path);
    String path = await FileSaver.instance.saveFile(
        name: fileName,
        bytes: Uint8List.fromList(response.data),
        ext: 'png',
        mimeType: MimeType.png);
    kLog('save to: $path');
    return true;
  }
  return true;
}

Future<bool> saveFileAs(String fileUrl) async {
  try {
    String? fileName = await FilePicker.platform.saveFile(
      allowedExtensions: null,
      type: FileType.any,
      dialogTitle: '文件另存为',
      fileName: p.basename(fileUrl),
      initialDirectory: getDownloadsDirectory().toString(),
      lockParentWindow: true,
    );

    if (fileName == null || fileName == "") {
      return false;
    } else {
      kLog("saving to path: $fileName");
      // write the file to target path?
      File f = await File(fileName).create();
      var response = await Dio()
          .get(fileUrl, options: Options(responseType: ResponseType.bytes));
      await f.writeAsBytes(
        Uint8List.fromList(response.data),
      );
      return true;
    }
  } on PlatformException catch (e) {
    kLog('Unsupported operation' + e.toString());
    return false;
  } catch (e) {
    kLog('Got exception on save: ${e.toString()}');
    return false;
  }
}
