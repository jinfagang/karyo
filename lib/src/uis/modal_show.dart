import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';
import 'package:wolt_modal_sheet/wolt_modal_sheet.dart';

class WoltModalShowSingleWidget extends StatefulWidget {
  const WoltModalShowSingleWidget({super.key, required this.child});
  final Widget child;

  @override
  State<WoltModalShowSingleWidget> createState() => _WoltModalShowState();

  void show(BuildContext context) {}
}

class _WoltModalShowState extends State<WoltModalShowSingleWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}

List<WoltModalSheetPage> _buildSingleChild(
    BuildContext context, Widget child, EdgeInsets? padding) {
  var page1 = WoltModalSheetPage(
      hasSabGradient: false,
      stickyActionBar: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: [
            EnhancedButton(
              backgroundColor: tClrPrimary(context),
              onPressed: () {
                Navigator.pop(context);
              },
              title: "确定",
            ),
          ],
        ),
      ),
      topBarTitle: Text(
        '编辑小镇名',
        style: NORMAL_BOLD_TXT_STYLE,
      ),
      isTopBarLayerAlwaysVisible: true,
      trailingNavBarWidget: buildCloseButton(context),
      child: Padding(
        padding: padding ?? const EdgeInsets.all(8.0),
        child: child,
      ));
  return [page1];
}

List<SliverWoltModalSheetPage> _buildSliverChild(BuildContext context,
    Widget child, double height, String title, EdgeInsets? padding) {
  var page2 = SliverWoltModalSheetPage(
    hasSabGradient: true,
    hasTopBarLayer: false,
    // backgroundColor: Colors.white,
    // backgroundColor: Colors.transparent,
    topBarTitle: Text(
      title,
      style: NORMAL_BOLD_TXT_STYLE,
    ),
    mainContentSlivers: [
      SliverList(
        delegate: SliverChildBuilderDelegate(
          (_, index) => SizedBox(
            height: height,
            child: Padding(
              padding: padding ?? const EdgeInsets.all(8.0),
              child: child,
            ),
          ),
          childCount: 1,
        ),
      )
    ],
    isTopBarLayerAlwaysVisible: true,
    trailingNavBarWidget: buildCloseButton(context),
  );
  return [page2];
}

void showWoltModal(
  BuildContext context,
  Widget child, {
  EdgeInsets? padding,
  String? title,
  double height = 400,
}) {
  final pageIndexNotifier = ValueNotifier<int>(0);

  WoltModalSheet.show<void>(
    pageIndexNotifier: pageIndexNotifier,
    context: context,
    pageListBuilder: (modalSheetContext) {
      // return _buildSingleChild(modalSheetContext, child, padding);
      return _buildSliverChild(
          modalSheetContext, child, height, title ?? '', padding);
    },
    modalTypeBuilder: (context) {
      final size = SizeHelper.screenW;
      if (size < 500) {
        return WoltModalType.bottomSheet;
      } else {
        return WoltModalType.dialog;
      }
    },
    onModalDismissedWithBarrierTap: () {
      Navigator.of(context).pop();
      pageIndexNotifier.value = 0;
    },
    maxDialogWidth: 560,
    minDialogWidth: height,
    minPageHeight: 0.0,
    maxPageHeight: 0.9,
  );
}

void showMyModalSheet(BuildContext context, Widget child) {
  showModalBottomSheet(
      context: context,
      showDragHandle: true,
      isScrollControlled: false,
      backgroundColor: PlatformTheme.getMainBkColorSection(context),
      isDismissible: true,
      builder: (context) {
        return DecoratedBox(
            decoration: BoxDecoration(
              color: PlatformTheme.getMainBkColorSection(context),
              // color: Colors.white,
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: child);
      });
}

void showMyModalSheetBlurred({
  required BuildContext context,
  required WidgetBuilder builder,
  double borderRadius = 16.0,
}) {
  showModalBottomSheet(
    context: context,
    backgroundColor: Colors.transparent,
    isScrollControlled: true,
    builder: (context) {
      return _AnimatedBlurSheet(
        builder: builder,
        borderRadius: borderRadius,
      );
    },
  );
}

class _AnimatedBlurSheet extends StatefulWidget {
  final WidgetBuilder builder;
  final double borderRadius;

  const _AnimatedBlurSheet({required this.builder, required this.borderRadius});

  @override
  _AnimatedBlurSheetState createState() => _AnimatedBlurSheetState();
}

class _AnimatedBlurSheetState extends State<_AnimatedBlurSheet>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _blurAnimation;
  late Animation<double> _opacityAnimation;
  late Animation<Offset> _slideAnimation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    _blurAnimation = Tween<double>(begin: 0, end: 10).animate(
      CurvedAnimation(parent: _controller, curve: Curves.easeInOut),
    );

    _opacityAnimation = Tween<double>(begin: 0, end: 1).animate(
      CurvedAnimation(parent: _controller, curve: Curves.easeInOut),
    );

    _slideAnimation =
        Tween<Offset>(begin: const Offset(0, 1), end: Offset.zero).animate(
      CurvedAnimation(parent: _controller, curve: Curves.easeOut),
    );

    _controller.forward();
  }

  void _closeModal() {
    _controller.reverse().then((_) {
      if (mounted) {
        Navigator.pop(context);
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _closeModal, // Dismiss on tap outside
      child: Stack(
        children: [
          // Animated Blur Background (No movement, only opacity)
          AnimatedBuilder(
            animation: _opacityAnimation,
            builder: (context, child) {
              return AnimatedOpacity(
                opacity: _opacityAnimation.value,
                duration: const Duration(milliseconds: 300),
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: _blurAnimation.value,
                    sigmaY: _blurAnimation.value,
                  ),
                  child: Container(
                    color: PlatformTheme.getMainBkColorSection(context)
                        .withOpacity(0.2), // Dark overlay
                  ),
                ),
              );
            },
          ),

          // Sliding Modal Sheet
          Align(
            alignment: Alignment.bottomCenter,
            child: SlideTransition(
              position: _slideAnimation,
              child: Container(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height * 0.9,
                ),
                decoration: BoxDecoration(
                  color: PlatformTheme.getMainBkColorSection(context),
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(widget.borderRadius),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.green.withOpacity(0.1),
                      blurRadius: 10,
                      spreadRadius: 2,
                    ),
                  ],
                ),
                child: widget.builder(context),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

void showMyModalSheetPushUp(BuildContext context, Widget child) {
  showModalBottomSheet(
      context: context,
      showDragHandle: false,
      isScrollControlled: true,
      // backgroundColor: Colors.transparent,
      isDismissible: true,
      builder: (context) {
        return Container(
            decoration: BoxDecoration(
              // color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(25.0),
                topRight: const Radius.circular(25.0),
              ),
            ),
            child: child);
      });
}
