import 'dart:ui';

import 'package:flutter/material.dart';

import '../../karyo.dart';

class GlassCard extends StatelessWidget {
  final Widget? child;
  final double? height;
  final double? width;
  final EdgeInsets? padding;

  const GlassCard(
      {Key? key,
      this.padding,
      this.height,
      @required this.width,
      @required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
        child: Container(
          padding: padding,
          height: height,
          width: width,
          child: child,
          decoration: BoxDecoration(
            color: ColorBook.glassColor,
            borderRadius: BorderRadius.circular(20),
            // border: Border.all(width: 1, color: ColorBook.borderColor),
          ),
        ),
      ),
    );
  }
}
