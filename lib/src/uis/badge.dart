import 'package:badges/badges.dart' as badges;
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:karyo/karyo.dart';
import 'package:badges/src/utils/drawing_utils.dart';

Widget buildMyBadege(
    BuildContext context, Widget child, Widget badgeContent, int num) {
  return badges.Badge(
      badgeStyle: badges.BadgeStyle(
        // badgeColor: tClrPrimary(context),
        badgeColor: Colors.red,
        shape: badges.BadgeShape.square,
        padding: num > 9
            ? const EdgeInsets.symmetric(horizontal: 5, vertical: 3)
            : const EdgeInsets.symmetric(horizontal: 8, vertical: 3),
        borderRadius: BorderRadius.circular(17),
        // borderSide: BorderSide(
        //     color: PlatformTheme.getMainBkColor(context), width: 1)
      ),
      badgeAnimation: const badges.BadgeAnimation.scale(),
      badgeContent: badgeContent,
      child: child);
}

Widget buildMyBadgeSimple(BuildContext context, Widget child, int num,
    {Color color = Colors.orange}) {
  return badges.Badge(
      position: badges.BadgePosition.bottomEnd(bottom: -2, end: -4),
      badgeStyle: badges.BadgeStyle(
        shape: badges.BadgeShape.square,
        padding: num > 9
            ? const EdgeInsets.symmetric(horizontal: 5, vertical: 3)
            : const EdgeInsets.symmetric(horizontal: 6, vertical: 1),
        borderRadius: BorderRadius.circular(17),
        badgeColor: color,
        // borderSide: BorderSide(
        //     color: PlatformTheme.getMainBkColor(context), width: 1)
      ),
      badgeAnimation: const badges.BadgeAnimation.scale(),
      badgeContent: Text(
        '$num',
        style: NORMAL_S_TXT_STYLE.copyWith(color: Colors.white),
      ),
      child: child);
}

Widget buildMyBadgeSimpleRightTop(BuildContext context, Widget child, int num,
    {Color color = Colors.red}) {
  return badges.Badge(
      position: badges.BadgePosition.topEnd(top: -2, end: -6),
      badgeStyle: badges.BadgeStyle(
        shape: badges.BadgeShape.square,
        padding: num > 9
            ? const EdgeInsets.symmetric(horizontal: 6, vertical: 1)
            : const EdgeInsets.symmetric(horizontal: 6, vertical: 1),
        borderRadius: BorderRadius.circular(17),
        badgeColor: color,
        // borderSide: BorderSide(
        //     color: PlatformTheme.getMainBkColor(context), width: 1)
      ),
      badgeAnimation: const badges.BadgeAnimation.scale(),
      badgeContent: Text(
        '${num > 99 ? '99+' : num}',
        style: NORMAL_S_TXT_STYLE.copyWith(color: Colors.white),
      ),
      child: child);
}

Widget buildMyBadgeSimpleLeft(BuildContext context, Widget child, int num,
    {Color color = Colors.red}) {
  return badges.Badge(
      position: badges.BadgePosition.bottomStart(bottom: 0, start: -16),
      badgeStyle: badges.BadgeStyle(
        shape: badges.BadgeShape.square,
        padding: num > 9
            ? const EdgeInsets.symmetric(horizontal: 5, vertical: 3)
            : const EdgeInsets.symmetric(horizontal: 6, vertical: 1),
        borderRadius: BorderRadius.circular(17),
        badgeColor: color,
        // borderSide: BorderSide(
        //     color: PlatformTheme.getMainBkColor(context), width: 1)
      ),
      badgeAnimation: const badges.BadgeAnimation.scale(),
      badgeContent: Text(
        '$num',
        style: NORMAL_S_TXT_STYLE.copyWith(color: Colors.white),
      ),
      child: child);
}

Widget buildVerifyBadge(BuildContext context, Widget child,
    {int userLevel = 0}) {
  // Different level has different color.
  return userLevel > 0
      ? badges.Badge(
          position: badges.BadgePosition.bottomEnd(bottom: -2, end: -4),
          badgeContent: Padding(
            padding: const EdgeInsets.all(0.0),
            child:
                Icon(getIconByLevel(userLevel), color: Colors.white, size: 9),
          ),
          badgeStyle: badges.BadgeStyle(
              badgeColor: userLevel < 5 ? Colors.purple : Colors.blue,
              shape: badges.BadgeShape.twitter),
          child: child)
      : child;
}

Widget buildBadgeDots(int num,
    {Color color = Colors.red, bool showIndicatorOnly = false}) {
  if (num == 0) {
    return const SizedBox();
  }
  return showIndicatorOnly
      ? Container(
          decoration: BoxDecoration(
              color: color, borderRadius: BorderRadius.circular(22)),
          child: const SizedBox(width: 4, height: 4))
      : Container(
          decoration: BoxDecoration(
              color: color, borderRadius: BorderRadius.circular(22)),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
            child: Text(
              '$num',
              style: NORMAL_S_BOLD_TXT_STYLE.copyWith(color: Colors.white),
            ),
          ),
        ).animate().scale();
}

Widget getTwitterBadge(
    Color color, int userLevel, double size, void Function()? onTap) {
  final gc = getGradientColors(color);
  return GestureDetector(
    onTap: onTap,
    child: CustomPaint(
      painter: DrawingUtils.drawBadgeShape(
          shape: badges.BadgeShape.twitter,
          color: color,
          badgeGradient: badges.BadgeGradient.linear(colors: [gc[0], gc[1]]),
          borderSide: BorderSide.none),
      child: Padding(
        padding: size > 10 ? const EdgeInsets.all(12) : const EdgeInsets.all(3),
        child: Icon(getIconByLevel(userLevel), color: Colors.white, size: size),
      ),
    ),
  );
}

Widget buildVerifyBadgeCenter(BuildContext context, Widget child,
    {int userLevel = 0}) {
  // Different level has different color.
  return userLevel > 0
      ? badges.Badge(
          onTap: () {},
          position: badges.BadgePosition.custom(end: -17, top: 0, bottom: 0),
          badgeContent: Padding(
            padding: const EdgeInsets.all(0.0),
            child:
                Icon(getIconByLevel(userLevel), color: Colors.white, size: 9),
          ),
          badgeStyle: badges.BadgeStyle(
              padding: EdgeInsets.symmetric(horizontal: 3, vertical: 3),
              badgeColor: userLevel < 5 ? Colors.indigoAccent : Colors.blue,
              shape: badges.BadgeShape.twitter),
          child: child)
      : child;
}

Widget buildBadgeTight(BuildContext context, Widget child, int num,
    {Color color = Colors.red}) {
  if (num == 0) {
    return child;
  }
  return badges.Badge(
      position: badges.BadgePosition.bottomEnd(bottom: 8, end: 4),
      badgeStyle: badges.BadgeStyle(
        shape: badges.BadgeShape.square,
        padding: num > 9
            ? const EdgeInsets.symmetric(horizontal: 5, vertical: 3)
            : const EdgeInsets.symmetric(horizontal: 6, vertical: 1),
        borderRadius: BorderRadius.circular(17),
        badgeColor: color,
        // borderSide: BorderSide(
        //     color: PlatformTheme.getMainBkColor(context), width: 1)
      ),
      badgeAnimation: const badges.BadgeAnimation.scale(),
      badgeContent: Text(
        '$num',
        style: NORMAL_S_TXT_STYLE.copyWith(color: Colors.white),
      ),
      child: child);
}

Widget buildBadgeNormal(BuildContext context, Widget child, int num,
    {Color color = Colors.red}) {
  if (num == 0) {
    return child;
  }
  return badges.Badge(
      position: badges.BadgePosition.topEnd(top: -5, end: -8),
      badgeStyle: badges.BadgeStyle(
        shape: badges.BadgeShape.square,
        padding: num > 9
            ? const EdgeInsets.symmetric(horizontal: 5, vertical: 3)
            : const EdgeInsets.symmetric(horizontal: 6, vertical: 1),
        borderRadius: BorderRadius.circular(17),
        badgeColor: color,
        // borderSide: BorderSide(
        //     color: PlatformTheme.getMainBkColor(context), width: 1)
      ),
      badgeAnimation: const badges.BadgeAnimation.scale(),
      badgeContent: Text(
        '$num',
        style: NORMAL_S_TXT_STYLE.copyWith(color: Colors.white),
      ),
      child: child);
}

IconData getIconByLevel(int lvl) {
  if (lvl < 2) {
    return FontAwesomeIcons.check;
  } else if (lvl < 4) {
    return FontAwesomeIcons.solidStar;
  } else if (lvl < 6) {
    return FontAwesomeIcons.crown;
  } else {
    // return FontAwesomeIcons.crown;
    return FontAwesomeIcons.check;
  }
}
