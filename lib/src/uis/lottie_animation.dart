import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

Widget getLottieKuXiao() {
  return SizedBox(
      width: 88,
      child: Lottie.asset(
        'assets/tgs/😂.gz',
        decoder: LottieComposition.decodeGZip,
        repeat: false,
      ));
}

Widget getLottieWaoo() {
  // final AnimationController  _controller = AnimationController(vsync: );
  return GestureDetector(
      onTap: () {},
      child: SizedBox(
          width: 88,
          child: Lottie.network(
            'https://db.manaai.cn/uploads/tgs/wao.gz',
            decoder: LottieComposition.decodeGZip,
            repeat: false,
          )));
}

Widget getLottieSmile() {
  // final AnimationController  _controller = AnimationController(vsync: );
  return GestureDetector(
      onTap: () {},
      child: SizedBox(
          width: 88,
          child: Lottie.network(
            'https://db.manaai.cn/uploads/tgs/smile.gz',
            decoder: LottieComposition.decodeGZip,
            repeat: false,
          )));
}

Widget getLottieNight() {
  // final AnimationController  _controller = AnimationController(vsync: );
  return GestureDetector(
      onTap: () {},
      child: SizedBox(
          width: 88,
          child: Lottie.network(
            'https://db.manaai.cn/uploads/tgs/sunny-hight.json',
            decoder: LottieComposition.decodeGZip,
            repeat: false,
          )));
}

Widget getLottieEmpty({bool repeat = false, double width = 120}) {
  // final AnimationController  _controller = AnimationController(vsync: );
  return GestureDetector(
      onTap: () {},
      child: SizedBox(
          width: width,
          child: Lottie.network(
            'https://db.manaai.cn/uploads/tgs/fire.json',
            repeat: repeat,
          )));
}

Widget getLottieTime({bool repeat = false, double width = 120}) {
  // final AnimationController  _controller = AnimationController(vsync: );
  return GestureDetector(
      onTap: () {},
      child: SizedBox(
          width: width,
          child: Lottie.network(
            'https://db.manaai.cn/uploads/tgs/catball.json',
            repeat: repeat,
          )));
}

Widget getLottieCatFish({bool repeat = false, double width = 120}) {
  // final AnimationController  _controller = AnimationController(vsync: );
  return GestureDetector(
      onTap: () {},
      child: SizedBox(
          width: width,
          child: Lottie.network(
            'https://db.manaai.cn/uploads/tgs/cat.json',
            repeat: repeat,
          )));
}

Widget getLottieGroupChat({bool repeat = false, double width = 120}) {
  // final AnimationController  _controller = AnimationController(vsync: );
  return GestureDetector(
      onTap: () {},
      child: SizedBox(
          width: width,
          child: Lottie.network(
            'https://lottie.host/049a0532-5de6-4ae6-ae4a-cd9a8e6a6ddd/8DleHi7Fy9.json',
            repeat: repeat,
          )));
}

Widget getLottieSend({bool repeat = false, double width = 120}) {
  // final AnimationController  _controller = AnimationController(vsync: );
  return SizedBox(
      width: width,
      child: Lottie.network(
        'https://db.manaai.cn/uploads/tgs/send.json',
        repeat: repeat,
      ));
}
