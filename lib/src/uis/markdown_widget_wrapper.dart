import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CodeWrapperWidget extends StatefulWidget {
  const CodeWrapperWidget(this.child, this.text, this.language, {super.key});

  final Widget child;
  final String text;
  final String language;

  @override
  State<CodeWrapperWidget> createState() => _PreWrapperState();
}

class _PreWrapperState extends State<CodeWrapperWidget> {
  bool hasCopied = false;
  late Widget _switchWidget;

  @override
  void initState() {
    super.initState();
    _switchWidget = _buildDefaultCopy();
  }

  @override
  Widget build(BuildContext context) => Column(
        children: [
          widget.child,
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  child: SizedBox(
                    height: 14,
                    child: AnimatedSwitcher(
                      duration: const Duration(milliseconds: 200),
                      child: _switchWidget,
                    ),
                  ),
                  onTap: () async {
                    debugPrint('cliked??');
                    if (hasCopied) return;
                    await Clipboard.setData(ClipboardData(text: widget.text));
                    _switchWidget = Row(
                      children: [
                        Icon(
                          Icons.check,
                          key: UniqueKey(),
                          color: Colors.green,
                          size: 15,
                        ),
                        const SizedBox(
                          width: 4,
                        ),
                        const Text(
                          '已复制',
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.green,
                          ),
                        ),
                      ],
                    );
                    refresh();
                    Future.delayed(const Duration(seconds: 3), () {
                      hasCopied = false;
                      _switchWidget = _buildDefaultCopy();
                      refresh();
                    });
                  },
                ),
                Text(
                  widget.language,
                  style: TextStyle(fontSize: 10, color: Colors.grey.shade400),
                ),
              ],
            ),
          ),
        ],
      );

  void refresh() {
    if (mounted) setState(() {});
  }

  Widget _buildDefaultCopy() => SizedBox(
        height: 14,
        child: Row(
          children: [
            Icon(
              FluentIcons.copy_add_20_filled,
              color: Colors.grey.shade400,
              key: UniqueKey(),
              size: 13,
            ),
            const SizedBox(
              width: 4,
            ),
            Text(
              '复制代码',
              style: TextStyle(fontSize: 10, color: Colors.grey.shade500),
            ),
          ],
        ),
      );
}
