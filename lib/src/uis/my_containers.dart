import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class BoxBlockBlur extends StatelessWidget {
  final Widget child;
  final Widget backChild;
  final double? innerPanding;
  final Color? backgroundColor;
  final BoxBorder? border;
  final EdgeInsets? padding;
  final EdgeInsets? margin;
  final double? borderRadius;
  final bool showDivider;
  final bool enableShadow;
  final bool enableGradient;
  final bool enableTransparent;
  final BoxShadow? boxShadow;
  final int? alpha;
  final Function()? onTap;

  const BoxBlockBlur({
    super.key,
    required this.child,
    required this.backChild,
    this.innerPanding,
    this.backgroundColor,
    this.border,
    this.padding,
    this.margin,
    this.borderRadius = 12,
    this.showDivider = true,
    this.enableShadow = true,
    this.enableGradient = false,
    this.enableTransparent = false,
    this.boxShadow,
    this.onTap,
    this.alpha = 255,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        enableTransparent
            ? SizedBox(
                child: child,
              )
            : Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(borderRadius!),
                    child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaX: 15, sigmaY: 15),
                        child: backChild),
                  ),
                  InkWell(
                    onTap: onTap,
                    borderRadius: BorderRadius.circular(borderRadius!),
                    child: Ink(
                        decoration: BoxDecoration(
                          color: backgroundColor ??
                              PlatformTheme.getSectionColor(context)
                                  .withAlpha(alpha!),
                          border: border,
                          borderRadius: BorderRadius.circular(borderRadius!),
                          boxShadow: enableShadow
                              ? [
                                  boxShadow ??
                                      BoxShadow(
                                          color: isDarkModeOnContext(context)
                                              ? const Color.fromARGB(
                                                  68, 27, 27, 27)
                                              : const Color.fromARGB(
                                                  159, 245, 245, 245),
                                          blurRadius: 9,
                                          spreadRadius: 5),
                                ]
                              : null,
                        ),
                        padding: padding ?? EdgeInsets.zero,
                        child: child),
                  ),
                ],
              ),
        if (enableGradient)
          Positioned.fill(
            child: DecoratedBox(
              // width: double.infinity,
              decoration: BoxDecoration(
                border: border,
                borderRadius: BorderRadius.circular(borderRadius!),
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      isDarkModeOnContext(context)
                          ? Colors.black.withAlpha(10)
                          : Colors.white.withAlpha(10),
                      isDarkModeOnContext(context)
                          ? Colors.black.withAlpha(90)
                          : Colors.white.withAlpha(90)
                    ]),
              ),
            ),
          ),
      ],
    );
  }
}
