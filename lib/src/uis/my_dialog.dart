import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class MyDialog extends StatelessWidget {
  final String title;
  final String content;
  final List<Widget> actions;
  final Widget? appendWidget;

  const MyDialog({
    required this.title,
    required this.content,
    required this.actions,
    this.appendWidget,
  });

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.zero,
      // contentPadding: EdgeInsets.zero,
      actionsPadding: const EdgeInsets.only(bottom: 8),
      backgroundColor: PlatformTheme.getMainBkColorSection(context),
      elevation: 8.0,
      surfaceTintColor: PlatformTheme.getMainBkColorSection(context),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      title: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: SizeHelper.screenW * 0.6),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Text(
                title,
                style: NORMAL_BOLD_TXT_STYLE.copyWith(fontSize: 15),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
      content: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: SizeHelper.screenW * 0.4),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Expanded(
                child: Text(
                  content,
                  style: NORMAL_TXT_STYLE,
                  textAlign: TextAlign.center,
                ),
              ),
            ]),
            appendWidget ?? const SizedBox()
          ],
        ),
      ),
      actions: actions,
      actionsAlignment: MainAxisAlignment.center,
    );
  }
}

openMyWechatDialog(
  BuildContext context,
  String title,
  String content,
  Function()? onConfirm, {
  Function()? onCancel,
  String cancelText = '取消',
  Widget? appendWidget = null,
  String confirmText = '确定',
  barrierDismissible = true,
}) {
  showGeneralDialog(
    barrierDismissible: barrierDismissible,
    barrierLabel: '',
    context: context,
    transitionDuration: const Duration(milliseconds: 300),
    barrierColor: PlatformTheme.getMainBkColor(context).withAlpha(10),
    transitionBuilder: (context, animation, secondaryAnimation, child) {
      // Custom animation
      return FadeTransition(
        opacity: animation,
        child: ScaleTransition(
          scale: Tween<double>(begin: 0.9, end: 1.0).animate(
            CurvedAnimation(parent: animation, curve: Curves.easeOutBack),
          ),
          child: child,
        ),
      );
    },
    pageBuilder: (ctx, a1, a2) {
      return Center(
        child: IntrinsicWidth(
          child: IntrinsicHeight(
            child: Container(
              margin: EdgeInsets.symmetric(
                  horizontal: 20), // Add margin to avoid touching screen edges
              decoration: BoxDecoration(
                color: PlatformTheme.getMainBkColor(context),
                borderRadius: BorderRadius.circular(26),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    blurRadius: 20,
                    offset: Offset(0, 10),
                  ),
                ],
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: MyDialog(
                  title: title,
                  content: content,
                  appendWidget: appendWidget,
                  actions: <Widget>[
                    TextButton(
                      style: TextButton.styleFrom(
                        backgroundColor: Colors.red.withAlpha(
                            50), // Background color for the cancel button
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8), // Controlled padding
                        shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(8), // Border radius
                        ),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                        if (onCancel != null) onCancel();
                      },
                      child: Text(
                        cancelText,
                        style: NORMAL_BOLD_TXT_STYLE.copyWith(
                            color: Colors.red), // Text color
                      ),
                    ),
                    if (onConfirm != null)
                      TextButton(
                        style: TextButton.styleFrom(
                          backgroundColor: Colors
                              .green, // Background color for the confirm button
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16,
                              vertical: 8), // Controlled padding
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(8), // Border radius
                          ),
                        ),
                        onPressed: onConfirm,
                        child: Text(
                          confirmText,
                          style: NORMAL_BOLD_TXT_STYLE.copyWith(
                              color: Colors.white), // Text color
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    },
  ).whenComplete(() {});
}
