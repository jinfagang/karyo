import 'package:flutter/cupertino.dart';

Widget buildScaleAnimatedSwitcher(bool condition, Widget a, Widget b,
    {int dur = 550}) {
  return AnimatedSwitcher(
    duration: Duration(milliseconds: dur),
    child: condition ? a : b,
    transitionBuilder: ((child, animation) {
      return ScaleTransition(
        scale: animation,
        child: child,
      );
    }),
  );
}

Widget buildSlideAnimatedSwitcher(bool condition, Widget a, Widget b) {
  return AnimatedSwitcher(
    duration: Duration(milliseconds: 250),
    child: condition ? a : b,
    transitionBuilder: ((child, animation) {
      return SlideTransition(
        position: Tween<Offset>(
          begin: const Offset(-0.8, 0.0), // adjust the position as you need
          end: const Offset(0.0, 0.0),
        ).animate(animation),
        child: child,
      );
    }),
  );
}

Widget buildScaleAnimatedContainer(bool showCondition, Widget toShow,
    {Duration duration = const Duration(milliseconds: 450),
    curve = Curves.decelerate}) {
  return AnimatedScale(
    duration: duration,
    child: toShow,
    scale: showCondition ? 1 : 0,
    curve: curve,
  );
}

Widget buildFadeAnimatedContainer(bool showCondition, Widget toShow,
    {Duration duration = const Duration(milliseconds: 450),
    curve = Curves.decelerate}) {
  return AnimatedCrossFade(
    duration: duration,
    firstChild: toShow,
    secondChild: const SizedBox(
      height: 30,
    ),
    crossFadeState: showCondition
        ? CrossFadeState.showFirst
        : CrossFadeState.showSecond, // child: toShow,
    // scale: showCondition ? 1 : 0,
    // curve: curve,
  );
}

Widget buildAnimatedContainer(bool showCondition, Widget toShow,
    {Duration duration = const Duration(milliseconds: 450),
    curve = Curves.decelerate}) {
  return AnimatedSize(
    duration: duration,
    child: Container(
      height: showCondition ? null : 0.0,
      child: toShow,
    ),
    curve: curve,
  );
}
