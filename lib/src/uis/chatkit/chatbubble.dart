import 'package:flutter/material.dart';
import 'dart:ui' as ui;

const double BUBBLE_RADIUS = 16;

///basic chat bubble type
///
///chat bubble [BorderRadius] can be customized using [bubbleRadius]
///chat bubble color can be customized using [color]
///chat bubble tail can be customized  using [tail]
///chat bubble display message can be changed using [text]
///[text] is the only required parameter
///message sender can be changed using [isSender]
///[sent],[delivered] and [seen] can be used to display the message state
///chat bubble [TextStyle] can be customized using [textStyle]

class BubbleNormal extends StatelessWidget {
  final double bubbleRadius;
  final double bubbleRadius2;
  final bool isSender;
  final Color color;
  final String text;
  final Widget? child;
  final bool tail;
  final bool sent;
  final bool delivered;
  final bool seen;
  final TextStyle textStyle;
  final BoxConstraints? constraints;
  final LinearGradient? gradient;
  final List<Color>? colors;

  BubbleNormal({
    Key? key,
    required this.text,
    this.constraints,
    this.child,
    this.bubbleRadius = BUBBLE_RADIUS,
    this.bubbleRadius2 = BUBBLE_RADIUS,
    this.isSender = true,
    this.color = Colors.white70,
    this.tail = true,
    this.sent = false,
    this.delivered = false,
    this.seen = false,
    this.gradient,
    this.colors,
    this.textStyle = const TextStyle(
      color: Colors.black87,
      fontSize: 16,
    ),
  }) : super(key: key);

  ///chat bubble builder method
  @override
  Widget build(BuildContext context) {
    bool stateTick = false;
    Icon? stateIcon;
    if (sent) {
      stateTick = true;
      stateIcon = const Icon(
        Icons.done,
        size: 18,
        color: Color(0xFF97AD8E),
      );
    }
    if (delivered) {
      stateTick = true;
      stateIcon = const Icon(
        Icons.done_all,
        size: 18,
        color: Color(0xFF97AD8E),
      );
    }
    if (seen) {
      stateTick = true;
      stateIcon = const Icon(
        Icons.done_all,
        size: 18,
        color: Color(0xFF92DEDA),
      );
    }

    return Row(
      children: <Widget>[
        isSender
            ? const Expanded(
                child: SizedBox(
                  width: 1,
                ),
              )
            : Container(),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 0),
          child: ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(bubbleRadius),
                  topRight: Radius.circular(bubbleRadius2),
                  bottomLeft: Radius.circular(bubbleRadius),
                  bottomRight: Radius.circular(bubbleRadius2)),
              child: CustomPaint(
                painter: BubblePainter(
                  scrollable: Scrollable.of(context),
                  bubbleContext: context,
                  // colors: gradient!.colors,
                  colors: colors ??
                      [
                        Color(0xFF19B7FF),
                        Color(0xFF491CCB),
                      ],
                ),
                child: Stack(
                  children: <Widget>[
                    Padding(
                      padding: stateTick
                          ? const EdgeInsets.only(left: 2, right: 20)
                          : const EdgeInsets.only(left: 2, right: 2),
                      child: child ??
                          Text(
                            text,
                            style: textStyle,
                            textAlign: TextAlign.left,
                          ),
                    ),
                    stateIcon != null && stateTick
                        ? Positioned(
                            bottom: 4,
                            right: 6,
                            child: stateIcon,
                          )
                        : const SizedBox(
                            width: 1,
                          ),
                  ],
                ),
              )),
        )
      ],
    );
  }
}

class BubbleNormalNip extends StatelessWidget {
  final double bubbleRadius;
  final double bubbleRadius2;
  final bool isSender;
  final Color color;
  final String text;
  final Widget? child;
  final bool tail;
  final bool sent;
  final bool delivered;
  final bool seen;
  final bool isGrouped;
  final bool isWechatStyle;
  final TextStyle textStyle;
  final BoxConstraints? constraints;
  final BorderRadius? borderRadius;
  final LinearGradient? gradient;
  final List<Color>? colors;

  const BubbleNormalNip({
    Key? key,
    required this.text,
    this.constraints,
    this.child,
    this.bubbleRadius = BUBBLE_RADIUS,
    this.bubbleRadius2 = BUBBLE_RADIUS,
    this.isSender = true,
    this.color = Colors.white70,
    this.tail = true,
    this.sent = false,
    this.delivered = false,
    this.borderRadius,
    this.seen = false,
    this.gradient,
    this.colors,
    this.isGrouped = false,
    this.isWechatStyle = false,
    this.textStyle = const TextStyle(
      color: Colors.black87,
      fontSize: 16,
    ),
  }) : super(key: key);

  ///chat bubble builder method
  @override
  Widget build(BuildContext context) {
    bool stateTick = false;
    Icon? stateIcon;
    if (sent) {
      stateTick = true;
      stateIcon = const Icon(
        Icons.done,
        size: 18,
        color: Color(0xFF97AD8E),
      );
    }
    if (delivered) {
      stateTick = true;
      stateIcon = const Icon(
        Icons.done_all,
        size: 18,
        color: Color(0xFF97AD8E),
      );
    }
    if (seen) {
      stateTick = true;
      stateIcon = const Icon(
        Icons.done_all,
        size: 18,
        color: Color(0xFF92DEDA),
      );
    }

    return Row(
      children: <Widget>[
        isSender
            ? const Expanded(
                child: SizedBox(
                  width: 1,
                ),
              )
            : Container(),
        isWechatStyle
            ? Padding(
                padding: EdgeInsets.only(
                  left: isSender
                      ? 0
                      : isGrouped
                          ? 3 + 6
                          : 3,
                  right: isSender
                      ? isGrouped
                          ? 3 + 6
                          : 3
                      : 0,
                  // horizontal: isSender
                  //     ? isGrouped
                  //         ? 4 + 7
                  //         : 4
                  //     : isGrouped
                  //         ? 4 + 7
                  //         : 4,
                ),
                child: ClipPath(
                    clipper: isSender
                        ? BubbleCliperRightTop(
                            radius: 11, nipSize: isGrouped ? 0 : 6)
                        : BubbleCliperLeftTop(
                            radius: 11, nipSize: isGrouped ? 0 : 6),
                    child: _buildCustomPainter(context, stateTick)))
            : Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                child: ClipRRect(
                  borderRadius: borderRadius ??
                      BorderRadius.only(
                          topLeft: Radius.circular(bubbleRadius),
                          topRight: Radius.circular(bubbleRadius2),
                          bottomLeft: Radius.circular(bubbleRadius2),
                          bottomRight: Radius.circular(bubbleRadius2)),
                  child: _buildCustomPainter(context, stateTick),
                ))
      ],
    );
  }

  Widget _buildCustomPainter(BuildContext context, bool stateTick) {
    return CustomPaint(
      painter: BubblePainter(
        scrollable: Scrollable.of(context),
        bubbleContext: context,
        // colors: gradient!.colors,
        colors: colors ??
            [
              const Color(0xFF19B7FF),
              const Color(0xFF491CCB),
            ],
      ),
      child: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
              left: isWechatStyle && !isGrouped
                  ? isSender
                      ? 0
                      : 7
                  : 2,
              right: isWechatStyle && !isGrouped
                  ? isSender
                      ? 7
                      : 0
                  : 2,
            ),
            child: child ??
                Text(
                  text,
                  style: textStyle,
                  textAlign: TextAlign.left,
                ),
          ),
        ],
      ),
    );
  }
}

///iMessage's chat bubble type
///
///chat bubble color can be customized using [color]
///chat bubble tail can be customized  using [tail]
///chat bubble display message can be changed using [text]
///[text] is the only required parameter
///message sender can be changed using [isSender]
///chat bubble [TextStyle] can be customized using [textStyle]

class BubbleSpecialThree extends StatelessWidget {
  final bool isSender;
  final String text;
  final Widget? child;
  final bool tail;
  final Color color;
  final bool sent;
  final bool delivered;
  final bool seen;
  final TextStyle textStyle;
  final BoxConstraints? constraints;

  const BubbleSpecialThree({
    Key? key,
    this.isSender = true,
    this.constraints,
    required this.text,
    this.child,
    this.color = Colors.white70,
    this.tail = true,
    this.sent = false,
    this.delivered = false,
    this.seen = false,
    this.textStyle = const TextStyle(
      color: Colors.black87,
      fontSize: 16,
    ),
  }) : super(key: key);

  ///chat bubble builder method
  @override
  Widget build(BuildContext context) {
    bool stateTick = false;
    Icon? stateIcon;
    if (sent) {
      stateTick = true;
      stateIcon = const Icon(
        Icons.done,
        size: 18,
        color: Color(0xFF97AD8E),
      );
    }
    if (delivered) {
      stateTick = true;
      stateIcon = const Icon(
        Icons.done_all,
        size: 18,
        color: Color(0xFF97AD8E),
      );
    }
    if (seen) {
      stateTick = true;
      stateIcon = const Icon(
        Icons.done_all,
        size: 18,
        color: Color(0xFF92DEDA),
      );
    }

    return Row(
      children: [
        isSender
            ? Expanded(
                child: SizedBox(
                  width: 1,
                ),
              )
            : Container(),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 0),
          child: CustomPaint(
            painter: SpecialChatBubbleThree(
                color: color,
                alignment: isSender ? Alignment.topRight : Alignment.topLeft,
                tail: tail),
            child: Container(
              padding: EdgeInsets.zero,
              margin: isSender
                  ? stateTick
                      ? const EdgeInsets.fromLTRB(0, 0, 8, 0)
                      : const EdgeInsets.fromLTRB(0, 0, 8, 0)
                  : const EdgeInsets.fromLTRB(8, 0, 0, 0),
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: stateTick
                        ? const EdgeInsets.only(left: 2, right: 20)
                        : const EdgeInsets.only(left: 2, right: 2),
                    child: child ??
                        Text(
                          text,
                          style: textStyle,
                          textAlign: TextAlign.left,
                        ),
                  ),
                  stateIcon != null && stateTick
                      ? Positioned(
                          bottom: 0,
                          right: 0,
                          child: stateIcon,
                        )
                      : const SizedBox(
                          width: 1,
                        ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

///custom painter use to create the shape of the chat bubble
///
/// [color],[alignment] and [tail] can be changed

class SpecialChatBubbleThree extends CustomPainter {
  final Color color;
  final Alignment alignment;
  final bool tail;
  final double nipSize;
  final bool drawShadow;

  SpecialChatBubbleThree({
    required this.color,
    required this.alignment,
    required this.tail,
    this.nipSize = 7.0,
    this.drawShadow = false,
  });

  double _radius = 10.0;

  @override
  void paint(Canvas canvas, Size size) {
    var h = size.height;
    var w = size.width;
    if (alignment == Alignment.topRight) {
      if (tail) {
        var path = Path();

        path.moveTo(_radius * 2, 0);

        /// top-left corner
        path.quadraticBezierTo(0, 0, 0, _radius * 1.8);

        /// left line
        path.lineTo(0, h - _radius * 1.8);

        /// bottom-left corner
        path.quadraticBezierTo(0, h, _radius * 1.8, h);

        /// bottom line
        path.lineTo(w - _radius * 3, h);

        /// bottom-right bubble curve
        path.quadraticBezierTo(
            w - _radius * 1.5, h, w - _radius * 1.5, h - _radius * 0.6);

        /// bottom-right tail curve 1
        path.quadraticBezierTo(w - _radius * 1, h, w, h);

        /// bottom-right tail curve 2
        path.quadraticBezierTo(
            w - _radius * 0.8, h, w - _radius, h - _radius * 1.5);

        /// right line
        path.lineTo(w - _radius, _radius * 1.5);

        /// top-right curve
        path.quadraticBezierTo(w - _radius, 0, w - _radius * 3, 0);

        // _radius = 16;
        // path.moveTo(_radius, 0);
        // path.lineTo(size.width - _radius - nipSize, 0);
        // path.arcToPoint(Offset(size.width - nipSize, _radius),
        //     radius: Radius.circular(_radius));

        // path.lineTo(size.width - nipSize, size.height - nipSize);

        // path.arcToPoint(Offset(size.width, size.height),
        //     radius: Radius.circular(nipSize), clockwise: false);

        // path.arcToPoint(Offset(size.width - 2 * nipSize, size.height - nipSize),
        //     radius: Radius.circular(2 * nipSize));

        // path.arcToPoint(Offset(size.width - 4 * nipSize, size.height),
        //     radius: Radius.circular(2 * nipSize));

        // path.lineTo(_radius, size.height);
        // path.arcToPoint(Offset(0, size.height - _radius),
        //     radius: Radius.circular(_radius));
        // path.lineTo(0, _radius);
        // path.arcToPoint(Offset(_radius, 0), radius: Radius.circular(_radius));

        canvas.clipPath(path);
        if (drawShadow)
          canvas.drawShadow(path, Colors.grey.withAlpha(50), 4.0, true);

        canvas.drawRRect(
            RRect.fromLTRBR(0, 0, w, h, Radius.zero),
            Paint()
              ..color = color
              ..style = PaintingStyle.fill);
      } else {
        var path = Path();

        /// starting point
        path.moveTo(_radius * 2, 0);

        /// top-left corner
        path.quadraticBezierTo(0, 0, 0, _radius * 1.8);

        /// left line
        path.lineTo(0, h - _radius * 1.8);

        /// bottom-left corner
        path.quadraticBezierTo(0, h, _radius * 2, h);

        /// bottom line
        path.lineTo(w - _radius * 3, h);

        /// bottom-right curve
        path.quadraticBezierTo(w - _radius, h, w - _radius, h - _radius * 1.5);

        /// right line
        path.lineTo(w - _radius, _radius * 1.8);

        /// top-right curve
        path.quadraticBezierTo(w - _radius, 0, w - _radius * 3, 0);
        if (drawShadow)
          canvas.drawShadow(path, Colors.grey.withAlpha(50), 4.0, true);
        canvas.clipPath(path);
        canvas.drawRRect(
            RRect.fromLTRBR(0, 0, w, h, Radius.zero),
            Paint()
              ..color = color
              ..style = PaintingStyle.fill);
      }
    } else {
      if (tail) {
        var path = Path();

        /// starting point
        path.moveTo(_radius * 3, 0);

        /// top-left corner
        path.quadraticBezierTo(_radius, 0, _radius, _radius * 1.8);

        /// left line
        path.lineTo(_radius, h - _radius * 1.8);
        // bottom-right tail curve 1
        path.quadraticBezierTo(_radius * .8, h, 0, h);

        /// bottom-right tail curve 2
        path.quadraticBezierTo(
            _radius * 1, h, _radius * 1.8, h - _radius * 0.6);

        /// bottom-left bubble curve
        path.quadraticBezierTo(_radius * 1.5, h, _radius * 3, h);

        /// bottom line
        path.lineTo(w - _radius * 2, h);

        /// bottom-right curve
        path.quadraticBezierTo(w, h, w, h - _radius * 1.5);

        /// right line
        path.lineTo(w, _radius * 1.5);

        /// top-right curve
        path.quadraticBezierTo(w, 0, w - _radius * 2, 0);
        if (drawShadow)
          canvas.drawShadow(
              path.shift(Offset(0, -8)), Colors.grey.withAlpha(35), 8.0, false);
        canvas.clipPath(path);
        canvas.drawRRect(
            RRect.fromLTRBR(0, 0, w, h, Radius.zero),
            Paint()
              ..color = color
              ..style = PaintingStyle.fill);
      } else {
        var path = Path();

        /// starting point
        path.moveTo(_radius * 3, 0);

        /// top-left corner
        path.quadraticBezierTo(_radius, 0, _radius, _radius * 1.5);

        /// left line
        path.lineTo(_radius, h - _radius * 1.5);

        /// bottom-left curve
        path.quadraticBezierTo(_radius, h, _radius * 3, h);

        /// bottom line
        path.lineTo(w - _radius * 2, h);

        /// bottom-right curve
        path.quadraticBezierTo(w, h, w, h - _radius * 1.5);

        /// right line
        path.lineTo(w, _radius * 1.5);

        /// top-right curve
        path.quadraticBezierTo(w, 0, w - _radius * 2, 0);
        canvas.clipPath(path);
        if (drawShadow)
          canvas.drawShadow(path, Colors.grey.withAlpha(50), 4.0, false);
        canvas.drawRRect(
            RRect.fromLTRBR(0, 0, w, h, Radius.zero),
            Paint()
              ..color = color
              ..style = PaintingStyle.fill);
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class BubblePainter extends CustomPainter {
  BubblePainter({
    required ScrollableState scrollable,
    required BuildContext bubbleContext,
    required List<Color> colors,
  })  : _scrollable = scrollable,
        _bubbleContext = bubbleContext,
        _colors = colors,
        super(repaint: scrollable.position);

  final ScrollableState _scrollable;
  final BuildContext _bubbleContext;
  final List<Color> _colors;

  @override
  void paint(Canvas canvas, Size size) {
    final scrollableBox = _scrollable.context.findRenderObject() as RenderBox;
    final scrollableRect = Offset.zero & scrollableBox.size;
    final bubbleBox = _bubbleContext.findRenderObject() as RenderBox;

    final origin =
        bubbleBox.localToGlobal(Offset.zero, ancestor: scrollableBox);
    final paint = Paint()
      ..shader = ui.Gradient.linear(
        scrollableRect.topCenter,
        scrollableRect.bottomCenter,
        _colors,
        [0.0, 1.0],
        TileMode.clamp,
        Matrix4.translationValues(-origin.dx, -origin.dy, 0.0).storage,
      );
    canvas.drawRect(Offset.zero & size, paint);
  }

  @override
  bool shouldRepaint(BubblePainter oldDelegate) {
    return oldDelegate._scrollable != _scrollable ||
        oldDelegate._bubbleContext != _bubbleContext ||
        oldDelegate._colors != _colors;
  }
}

// Custom clippers.
class BubbleCliperLeft extends CustomClipper<Path> {
  final double radius;
  final double nipSize;
  final double offset;

  BubbleCliperLeft({this.radius = 5, this.offset = 10, this.nipSize = 7});

  @override
  Path getClip(Size size) {
    var path = Path();
    path.addRRect(RRect.fromLTRBR(
        nipSize, 0, size.width, size.height, Radius.circular(radius)));

    var path2 = Path();
    path2.lineTo(0, 2 * nipSize);
    path2.lineTo(-nipSize, nipSize);
    path2.lineTo(0, 0);

    path.addPath(path2, Offset(nipSize, size.height - offset - 2 * nipSize));
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class BubbleCliperLeftTop extends CustomClipper<Path> {
  final double radius;
  final double nipSize;
  final double offset;

  BubbleCliperLeftTop({this.radius = 6, this.offset = 0, this.nipSize = 7});

  @override
  Path getClip(Size size) {
    var path = Path();
    path.addRRect(RRect.fromLTRBR(
        nipSize, 0, size.width, size.height, Radius.circular(radius)));

    var path2 = Path();
    path2.lineTo(0, 2 * nipSize);
    path2.lineTo(-nipSize, nipSize);
    path2.lineTo(0, 0);

    // path.addPath(path2, Offset(nipSize, size.height - offset - 2 * nipSize));
    path.addPath(path2, Offset(nipSize, offset + 2.0 * nipSize));
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class BubbleCliperRightTop extends CustomClipper<Path> {
  final double radius;
  final double nipSize;
  final double offset;

  BubbleCliperRightTop({this.radius = 6, this.offset = 0, this.nipSize = 7});

  @override
  Path getClip(Size size) {
    var path = Path();
    path.addRRect(RRect.fromLTRBR(
        0, 0, size.width - nipSize, size.height, Radius.circular(radius)));

    var path2 = Path();
    path2.lineTo(0, nipSize);
    path2.lineTo(nipSize, 0);
    path2.lineTo(0, -nipSize);

    path.addPath(path2, Offset(size.width - nipSize, offset + 3.0 * nipSize));
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class BubbleCliperRightCenter extends CustomClipper<Path> {
  final double radius;
  final double nipSize;
  final double offset;

  BubbleCliperRightCenter(
      {this.radius = 4, this.offset = 10, this.nipSize = 7});

  @override
  Path getClip(Size size) {
    var path = Path();
    path.addRRect(RRect.fromLTRBR(
        0, 0, size.width - nipSize, size.height, Radius.circular(radius)));

    var path2 = Path();
    path2.lineTo(0, nipSize);
    path2.lineTo(nipSize, 0);
    path2.lineTo(0, -nipSize);

    path.addPath(path2, Offset(size.width - nipSize, size.height / 2));
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class BubbleCliperNipBottom extends CustomClipper<Path> {
  final double radius;
  final double nipSize;
  final double offset;

  BubbleCliperNipBottom({this.radius = 12, this.offset = 10, this.nipSize = 8});

  @override
  Path getClip(Size size) {
    var path = Path();
    path.addRRect(RRect.fromLTRBR(
        0, 0, size.width, size.height - nipSize, Radius.circular(radius)));

    var nipCenterX = size.width / 2;
    var path2 = Path();
    path2.lineTo(-nipSize, 0);
    path2.lineTo(0, nipSize);
    path2.lineTo(nipSize, 0);

    path.addPath(path2, Offset(nipCenterX, size.height - nipSize));
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
