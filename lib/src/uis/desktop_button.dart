import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';
import 'package:badges/badges.dart' as badges;

class HoverableButton extends StatefulWidget {
  final Color? accentColor;
  final Widget child;
  final double minWidth;
  final double? height;
  final BorderRadius? borderRadius;
  final Function()? onPressed;
  final EdgeInsets? padding;
  final bool isBlur;
  final bool isShadow;
  final bool isBorder;

  const HoverableButton({
    Key? key,
    this.accentColor,
    this.minWidth = 34,
    this.height,
    this.onPressed,
    this.borderRadius,
    this.padding,
    this.isBlur = false,
    this.isShadow = false,
    this.isBorder = false,
    required this.child,
  }) : super(key: key);

  @override
  State<HoverableButton> createState() => _HoverableButtonState();
}

class _HoverableButtonState extends State<HoverableButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding ?? const EdgeInsets.all(3),
      child: widget.isBlur
          ? ClipRRect(
              borderRadius: widget.borderRadius ?? BorderRadius.circular(8.0),
              child: BackdropFilter(
                  filter: ImageFilter.blur(
                      sigmaX: 10.0, sigmaY: 10.0), // Adjust blur strength
                  child: _buildMain()),
            )
          : _buildMain(),
    );
  }

  Widget _buildMain() {
    return Material(
      shape: RoundedRectangleBorder(
          side: widget.isBorder
              ? BorderSide(
                  color: Colors.grey.withAlpha(20), // Border color
                  width: 1.0, // Border width
                )
              : BorderSide.none,
          borderRadius: widget.borderRadius ?? BorderRadius.circular(8.0)),
      elevation: widget.isShadow ? 4 : 0.0,
      shadowColor: Colors.grey.withAlpha(20),
      // color: Colors.transparent,
      color: widget.accentColor ?? Colors.transparent,
      clipBehavior: Clip.antiAlias, // Add This
      child: ConstrainedBox(
        constraints: BoxConstraints(
            maxHeight: widget.height != null
                ? widget.height!
                : isOnMobile()
                    ? 34
                    : 28),
        child: MaterialButton(
          splashColor: isOnMobile() ? null : Colors.transparent,
          visualDensity: VisualDensity.compact,
          shape: RoundedRectangleBorder(
              borderRadius: widget.borderRadius ?? BorderRadius.circular(8.0)),
          // minWidth: isOnMobile() ? min(widget.minWidth, 12) : widget.minWidth,
          minWidth: widget.minWidth,
          // color: widget.accentColor,
          height: widget.height ?? 34,
          padding: widget.padding ?? const EdgeInsets.all(4),
          onPressed: widget.onPressed,
          hoverColor: Colors.grey.withAlpha(40),
          child: Center(child: widget.child),
        ),
      ),
    );
  }
}

class HoverableCircleButton extends StatefulWidget {
  final Color? accentColor;
  final Widget child;
  final double minWidth;
  final double? height;
  final BorderRadius? borderRadius;
  final Function()? onPressed;
  final EdgeInsets? padding;

  const HoverableCircleButton({
    Key? key,
    this.accentColor,
    this.minWidth = 34,
    this.height,
    this.onPressed,
    this.borderRadius,
    this.padding,
    required this.child,
  }) : super(key: key);

  @override
  State<HoverableCircleButton> createState() => _HoverableButtonCircleState();
}

class _HoverableButtonCircleState extends State<HoverableCircleButton> {
  @override
  Widget build(BuildContext context) {
    return Material(
      shape: RoundedRectangleBorder(
          borderRadius: widget.borderRadius ?? BorderRadius.circular(8.0)),
      elevation: 0.0,
      // color: Colors.transparent,
      color: widget.accentColor ?? Colors.transparent,
      clipBehavior: Clip.antiAlias, // Add This
      child: ConstrainedBox(
        constraints: BoxConstraints(
            maxWidth: widget.minWidth,
            maxHeight: widget.height != null
                ? widget.height!
                : isOnMobile()
                    ? 34
                    : 28),
        child: MaterialButton(
          visualDensity: VisualDensity.adaptivePlatformDensity,
          shape: RoundedRectangleBorder(
              borderRadius: widget.borderRadius ?? BorderRadius.circular(15.0)),
          // minWidth: isOnMobile() ? min(widget.minWidth, 12) : widget.minWidth,
          minWidth: widget.minWidth,
          // color: widget.accentColor,
          height: widget.height ?? 34,
          padding: widget.padding ?? const EdgeInsets.all(0),
          onPressed: widget.onPressed,
          hoverColor: Colors.grey.withAlpha(80),
          child: Center(child: widget.child),
        ),
      ),
    );
  }
}

class SideBarButton extends StatefulWidget {
  // we just need color params here
  final Color accentColor;
  final Widget child;
  final Function()? onPressed;

  const SideBarButton({
    Key? key,
    this.accentColor = Colors.blue,
    this.onPressed,
    required this.child,
  }) : super(key: key);

  @override
  State<SideBarButton> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<SideBarButton> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return Material(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        elevation: 0.0,
        color: selected ? widget.accentColor : Colors.transparent,
        clipBehavior: Clip.antiAlias, // Add This
        child: MaterialButton(
          child: widget.child,
          onPressed: () {
            setState(() {
              if (!selected) {
                // toggle
                selected = true;
              } else {
                selected = false;
              }
            });
            widget.onPressed;
          },
          hoverColor: widget.accentColor,
        ));
  }
}

const Duration _kExpand = Duration(milliseconds: 200);
const ShapeBorder _defaultShape = RoundedRectangleBorder(
  borderRadius: BorderRadius.all(Radius.circular(5.0)),
);

enum SidebarItemSize {
  /// A small [SidebarItem]. Has a [height] of 24 and an [iconSize] of 12.
  small(24.0, 12.0),

  /// A medium [SidebarItem]. Has a [height] of 28 and an [iconSize] of 16.
  medium(29.0, 16.0),

  /// A large [SidebarItem]. Has a [height] of 32 and an [iconSize] of 20.0.
  large(50.0, 30.0);

  /// {@macro sidebarItemSize}
  const SidebarItemSize(
    this.height,
    this.iconSize,
  );

  /// The height of the [SidebarItem].
  final double height;

  /// The maximum size of the [SidebarItem]'s leading icon.
  final double iconSize;
}

class SidebarItem with Diagnosticable {
  /// Creates a sidebar item.
  const SidebarItem(
      {this.leading,
      required this.label,
      this.selectedColor,
      this.unselectedColor,
      this.shape,
      this.focusNode,
      this.semanticLabel,
      this.disclosureItems,
      this.trailing,
      this.unreadNum = 0,
      this.showLabel = false});

  final Widget? leading;
  final Widget label;
  final Color? selectedColor;
  final Color? unselectedColor;
  final ShapeBorder? shape;

  final FocusNode? focusNode;
  final bool showLabel;

  final String? semanticLabel;
  final List<SidebarItem>? disclosureItems;
  final Widget? trailing;
  final int? unreadNum;

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(ColorProperty('selectedColor', selectedColor));
    properties.add(ColorProperty('unselectedColor', unselectedColor));
    properties.add(StringProperty('semanticLabel', semanticLabel));
    properties.add(DiagnosticsProperty<ShapeBorder>('shape', shape));
    properties.add(DiagnosticsProperty<FocusNode>('focusNode', focusNode));
    properties.add(IterableProperty<SidebarItem>(
      'disclosure items',
      disclosureItems,
    ));
    properties.add(DiagnosticsProperty<Widget?>('trailing', trailing));
  }
}

class SidebarItems extends StatefulWidget {
  const SidebarItems({
    super.key,
    required this.items,
    required this.currentIndex,
    required this.onChanged,
    this.itemSize = SidebarItemSize.medium,
    this.scrollController,
    this.selectedColor,
    this.iconSize,
    this.unselectedColor,
    this.padding,
    this.spacing,
    this.shape,
    this.cursor = SystemMouseCursors.basic,
    this.showLabel = false,
  }) : assert(currentIndex >= 0);

  /// The [SidebarItem]s used by the sidebar. If no items are provided,
  /// the sidebar is not rendered.
  final List<SidebarItem> items;

  /// The current selected index. It must be in the range of 0 to
  /// [items.length]
  final int currentIndex;
  final double? spacing;
  final double? iconSize;
  final bool showLabel;
  final EdgeInsets? padding;

  /// Called when the current selected index should be changed.
  final ValueChanged<int> onChanged;

  /// The size specifications for all [items].
  ///
  /// Defaults to [SidebarItemSize.medium].
  final SidebarItemSize itemSize;

  /// The scroll controller used by this sidebar. If null, a local scroll
  /// controller is created.
  final ScrollController? scrollController;

  /// The color to paint the item when it's selected.
  ///
  /// If null, [MacosThemeData.primaryColor] is used.
  final Color? selectedColor;

  /// The color to paint the item when it's unselected.
  ///
  /// Defaults to transparent.
  final Color? unselectedColor;

  /// The [shape] property specifies the outline (border) of the
  /// decoration. The shape must not be null. It's used alongside
  /// [selectedColor].
  final ShapeBorder? shape;

  /// Specifies the kind of cursor to use for all sidebar items.
  ///
  /// Defaults to [SystemMouseCursors.basic].
  final MouseCursor? cursor;

  @override
  State<SidebarItems> createState() => _SidebarItemsState();
}

class _SidebarItemsState extends State<SidebarItems> {
  bool _isHovered = false;

  List<SidebarItem> get _allItems {
    List<SidebarItem> result = [];
    for (var element in widget.items) {
      if (element.disclosureItems != null) {
        result.addAll(element.disclosureItems!);
      } else {
        result.add(element);
      }
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.items.isEmpty) return const SizedBox.shrink();
    assert(widget.currentIndex < _allItems.length);
    final theme = Theme.of(context);
    return IconTheme.merge(
      data: IconThemeData(size: widget.iconSize ?? 22),
      child: _SidebarItemsConfiguration(
          // selectedColor: widget.selectedColor ?? theme.primaryColor,
          selectedColor: widget.selectedColor ?? theme.primaryColor,
          unselectedColor: widget.unselectedColor ?? Colors.grey.shade200,
          shape: widget.shape ?? _defaultShape,
          itemSize: widget.itemSize,
          child: Padding(
            padding: widget.padding ?? EdgeInsets.zero,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: List.generate(widget.items.length, (index) {
                final item = widget.items[index];

                return MouseRegion(
                  cursor: widget.cursor!,
                  child: _SidebarItem(
                    item: item,
                    showLabel: widget.showLabel,
                    unreadNum: item.unreadNum,
                    selected: _allItems[widget.currentIndex] == item,
                    onClick: () => widget.onChanged(_allItems.indexOf(item)),
                  ),
                );
              }),
            ),
          )),
    );
  }
}

class _SidebarItemsConfiguration extends InheritedWidget {
  // ignore: use_super_parameters
  const _SidebarItemsConfiguration({
    Key? key,
    required super.child,
    this.selectedColor = Colors.transparent,
    this.unselectedColor = Colors.grey,
    this.shape = _defaultShape,
    this.itemSize = SidebarItemSize.medium,
  }) : super(key: key);

  final Color selectedColor;
  final Color unselectedColor;
  final ShapeBorder shape;
  final SidebarItemSize itemSize;

  static _SidebarItemsConfiguration of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<_SidebarItemsConfiguration>()!;
  }

  @override
  bool updateShouldNotify(_SidebarItemsConfiguration oldWidget) {
    return true;
  }
}

/// A macOS style navigation-list item intended for use in a [Sidebar]
class _SidebarItem extends StatelessWidget {
  /// Builds a [_SidebarItem].
  // ignore: use_super_parameters
  const _SidebarItem({
    Key? key,
    required this.item,
    required this.onClick,
    required this.selected,
    this.showLabel = false,
    this.unreadNum = 0,
  }) : super(key: key);

  /// The widget to lay out first.
  ///
  /// Typically an [Icon]
  final SidebarItem item;

  /// Whether the item is selected or not
  final bool selected;
  final VoidCallback? onClick;
  final bool showLabel;
  final int? unreadNum;

  void _handleActionTap() async {
    onClick?.call();
  }

  Map<Type, Action<Intent>> get _actionMap => <Type, Action<Intent>>{
        ActivateIntent: CallbackAction<ActivateIntent>(
          onInvoke: (ActivateIntent intent) => _handleActionTap(),
        ),
        ButtonActivateIntent: CallbackAction<ButtonActivateIntent>(
          onInvoke: (ButtonActivateIntent intent) => _handleActionTap(),
        ),
      };

  bool get hasLeading => item.leading != null;
  bool get hasTrailing => item.trailing != null;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final selectedColor = item.selectedColor;
    final unselectedColor = item.unselectedColor;

    final double spacing = 10.0 + theme.visualDensity.horizontal;
    final itemSize = _SidebarItemsConfiguration.of(context).itemSize;
    TextStyle? labelStyle;
    switch (itemSize) {
      case SidebarItemSize.small:
        labelStyle = theme.textTheme.bodySmall;
        break;
      case SidebarItemSize.medium:
        labelStyle = theme.textTheme.bodyMedium;
        break;
      case SidebarItemSize.large:
        labelStyle = theme.textTheme.bodyLarge;
        break;
    }

    return Semantics(
      label: item.semanticLabel,
      button: true,
      focusable: true,
      focused: item.focusNode?.hasFocus,
      enabled: onClick != null,
      selected: selected,
      child: FocusableActionDetector(
        focusNode: item.focusNode,
        descendantsAreFocusable: false,
        enabled: onClick != null,
        //mouseCursor: SystemMouseCursors.basic,
        actions: _actionMap,
        child: Padding(
          padding: const EdgeInsets.only(top: 7),
          child: badges.Badge(
            showBadge: unreadNum! > 0,
            // showBadge: true,
            position: badges.BadgePosition.topEnd(top: -4, end: 2),
            badgeAnimation: const badges.BadgeAnimation.scale(),
            badgeContent: Text(
              '${unreadNum!}',
              style: NORMAL_S_TXT_STYLE.copyWith(color: Colors.white),
            ),
            child: Material(
              shape: RoundedRectangleBorder(
                  borderRadius: showLabel
                      ? BorderRadius.circular(12.0)
                      : BorderRadius.circular(10.0)),
              elevation: 0.0,
              color: Colors.transparent,
              clipBehavior: Clip.antiAlias, // Add This
              child: SizedBox(
                height: 44,
                child: Center(
                  child: HoverableButton(
                    borderRadius: showLabel
                        ? BorderRadius.circular(12.0)
                        : BorderRadius.circular(10.0),
                    minWidth: itemSize.height,
                    // height: itemSize.height,
                    height: 44,
                    // visualDensity:
                    //     showLabel ? VisualDensity.comfortable : VisualDensity.compact,
                    // // color: selected ? selectedColor : Colors.grey.withAlpha(30),
                    // accentColor: selected ? selectedColor : Colors.transparent,
                    accentColor: selected
                        ? isDarkModeOnContext(context)
                            ? const Color.fromARGB(255, 48, 48, 48)
                            : const Color.fromARGB(255, 220, 220, 220)
                        : Colors.transparent,
                    onPressed: onClick,
                    // elevation: 0,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 0),
                      child: Row(
                        mainAxisAlignment: showLabel
                            ? MainAxisAlignment.center
                            : MainAxisAlignment.center,
                        children: [
                          if (hasLeading)
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: EdgeInsets.zero,
                                  child: IconTheme(
                                    data: IconThemeData(
                                        color: selected
                                            ? selectedColor
                                            : isDarkModeOnContext(context)
                                                ? Colors.grey.shade600
                                                : Colors.grey.shade400),
                                    child: item.leading!,
                                  ),
                                ),
                                if (showLabel)
                                  const SizedBox(
                                    height: 2,
                                  ),
                                if (showLabel)
                                  DefaultTextStyle(
                                    style: SMALL_TXT_STYLE.copyWith(
                                      color: Colors.grey.withAlpha(20),
                                    ),
                                    child: item.label,
                                  ),
                              ],
                            ),
                          if (hasTrailing) ...[
                            const Spacer(),
                            DefaultTextStyle(
                              style: NORMAL_TXT_STYLE,
                              child: item.trailing!,
                            ),
                          ],
                        ],
                      ),
                    ),
                    // hoverColor: Colors.grey.shade100,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _DisclosureSidebarItem extends StatefulWidget {
  // ignore: use_super_parameters
  _DisclosureSidebarItem({
    Key? key,
    // ignore: unused_element
    required this.item,
    this.selectedItem,
    this.onChanged,
  })  : assert(item.disclosureItems != null),
        super(key: key);

  final SidebarItem item;
  final SidebarItem? selectedItem;
  final ValueChanged<SidebarItem>? onChanged;

  @override
  __DisclosureSidebarItemState createState() => __DisclosureSidebarItemState();
}

class __DisclosureSidebarItemState extends State<_DisclosureSidebarItem>
    with SingleTickerProviderStateMixin {
  static final Animatable<double> _easeInTween =
      CurveTween(curve: Curves.easeIn);
  static final Animatable<double> _halfTween =
      Tween<double>(begin: 0.0, end: 0.25);

  late AnimationController _controller;
  late Animation<double> _iconTurns;
  late Animation<double> _heightFactor;

  bool _isExpanded = false;

  bool get hasLeading => widget.item.leading != null;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(duration: _kExpand, vsync: this);
    _heightFactor = _controller.drive(_easeInTween);
    _iconTurns = _controller.drive(_halfTween.chain(_easeInTween));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _handleTap() {
    setState(() {
      _isExpanded = !_isExpanded;
      if (_isExpanded) {
        _controller.forward();
      } else {
        _controller.reverse().then<void>((void value) {
          if (!mounted) return;
          setState(() {
            // Rebuild without widget.children.
          });
        });
      }
      PageStorage.of(context).writeState(context, _isExpanded);
    });
    // widget.onExpansionChanged?.call(_isExpanded);
  }

  Widget _buildChildren(BuildContext context, Widget? child) {
    final theme = Theme.of(context);
    final double spacing = 10.0 + theme.visualDensity.horizontal;

    final itemSize = _SidebarItemsConfiguration.of(context).itemSize;
    TextStyle? labelStyle;
    switch (itemSize) {
      case SidebarItemSize.small:
        labelStyle = theme.textTheme.bodySmall;
        break;
      case SidebarItemSize.medium:
        labelStyle = theme.textTheme.bodyMedium;
        break;
      case SidebarItemSize.large:
        labelStyle = theme.textTheme.bodyLarge;
        break;
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          width: double.infinity,
          child: _SidebarItem(
            item: SidebarItem(
              label: widget.item.label,
              leading: Row(
                children: [
                  RotationTransition(
                    turns: _iconTurns,
                    child: Icon(
                      CupertinoIcons.chevron_right,
                      size: 12.0,
                      color: theme.brightness == Brightness.light
                          ? Colors.black
                          : Colors.white,
                    ),
                  ),
                  if (hasLeading)
                    Padding(
                      padding: EdgeInsets.only(left: spacing),
                      child: widget.item.leading!,
                    ),
                ],
              ),
              unselectedColor: Colors.transparent,
              focusNode: widget.item.focusNode,
              semanticLabel: widget.item.semanticLabel,
              shape: widget.item.shape,
              trailing: widget.item.trailing,
            ),
            onClick: _handleTap,
            selected: false,
          ),
        ),
        ClipRect(
          child: DefaultTextStyle(
            style: labelStyle!,
            child: Align(
              alignment: Alignment.centerLeft,
              heightFactor: _heightFactor.value,
              child: child,
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final bool closed = !_isExpanded && _controller.isDismissed;

    final Widget result = Offstage(
      offstage: closed,
      child: TickerMode(
        enabled: !closed,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: widget.item.disclosureItems!.map((item) {
            return Padding(
              padding: EdgeInsets.only(
                left: 24.0 + theme.visualDensity.horizontal,
              ),
              child: SizedBox(
                // width: double.infinity,
                width: 48,
                child: _SidebarItem(
                  item: item,
                  onClick: () => widget.onChanged?.call(item),
                  selected: widget.selectedItem == item,
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );

    return AnimatedBuilder(
      animation: _controller.view,
      builder: _buildChildren,
      child: closed ? null : result,
    );
  }
}
