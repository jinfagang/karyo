import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:karyo/karyo.dart';

class BottomNavigationBarItemCustom extends StatelessWidget {
  final Widget icon;
  final Widget? activeIcon;
  final String? label;
  final int? badge;
  final TextStyle? labelStyle;
  final bool isActive;
  final VoidCallback? onTap;

  const BottomNavigationBarItemCustom({
    super.key,
    required this.icon,
    this.activeIcon,
    this.label,
    this.badge,
    this.labelStyle,
    this.isActive = false,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          isActive ? (activeIcon ?? icon) : icon,
          if (label != null)
            Text(
              label!,
              style: labelStyle ??
                  TextStyle(
                    color: isActive ? Colors.blue : Colors.grey,
                    fontSize: 12,
                  ),
            ),
        ],
      ),
    );
  }
}

class BottomNavigationBarBlur extends StatelessWidget {
  final bool showSelectedLabels;
  final bool showUnselectedLabels;
  final VoidCallback? onClick;
  final double filterX;
  final double filterY;
  final double opacity;
  final Color backgroundColor;
  final BottomNavigationBarType bottomNavigationBarType;
  final int currentIndex;
  final List<BottomNavigationBarItemCustom> items;
  final Color selectedItemColor;
  final Color unselectedItemColor;
  final Function(int) onTap;
  final Function(int)? onTapUp;
  final Function(int)? onDoubleTap;
  final Function(int)? onLongPress;
  final Function()? onEmptyDoubleTap;
  final int? num;

  const BottomNavigationBarBlur(
      {Key? key,
      this.showSelectedLabels = false,
      this.showUnselectedLabels = false,
      this.onClick,
      this.filterX = 5.0,
      this.filterY = 10.0,
      this.opacity = 0.8,
      this.backgroundColor = Colors.black,
      this.bottomNavigationBarType = BottomNavigationBarType.fixed,
      this.currentIndex = 0,
      this.num,
      required this.onTap,
      required this.items,
      this.onDoubleTap,
      this.onTapUp,
      this.onLongPress,
      this.onEmptyDoubleTap,
      this.selectedItemColor = Colors.blue,
      this.unselectedItemColor = Colors.white})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.bottomCenter,
        child: Stack(
          children: [
            Positioned(
              left: 8,
              right: 8,
              bottom: 16,
              top: 0, // 或者根据你的实际需求调整区域大小
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onDoubleTap: onEmptyDoubleTap,
                child: Container(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 90, right: 90, bottom: 42),
              child: CustomBottomNavigationBar(
                showSelectedLabels: showSelectedLabels,
                showUnselectedLabels: showUnselectedLabels,
                // backgroundColor: Colors.white,
                // elevation: 12,
                bottomNavigationBarType: bottomNavigationBarType,
                items: items,
                currentIndex: currentIndex,
                selectedItemColor: selectedItemColor,
                unselectedItemColor: unselectedItemColor,
                num: num,
                onTap: (index) {
                  // HapticFeedback.lightImpact();
                  onTap(index);
                },
                // onDoubleTap: onDoubleTap == null
                //     ? null
                //     : (index) {
                //         // HapticFeedback.lightImpact();
                //         onDoubleTap!(index);
                //       },
                // onLongPress: (index) {
                //   // HapticFeedback.lightImpact();
                //   if (onLongPress == null) return;
                //   onLongPress!(index);
                // },
              ),
            )
          ],
        ));
  }
}

class CustomBottomNavigationBar extends StatelessWidget {
  final List<BottomNavigationBarItemCustom> items;
  final Function(int) onTap;
  final Function(int)? onDoubleTap;
  final Function(int)? onTapUp;
  final Function(int)? onLongPress;
  final int currentIndex;
  final bool showSelectedLabels;
  final bool showUnselectedLabels;
  final Color selectedItemColor;
  final Color unselectedItemColor;
  final BottomNavigationBarType bottomNavigationBarType;
  final double height; // Custom height for the Bottom Navigation Bar
  final int? num;

  const CustomBottomNavigationBar({
    Key? key,
    required this.items,
    required this.onTap,
    required this.currentIndex,
    required this.showSelectedLabels,
    required this.showUnselectedLabels,
    required this.selectedItemColor,
    required this.unselectedItemColor,
    required this.bottomNavigationBarType,
    this.onDoubleTap,
    this.onTapUp,
    this.onLongPress,
    this.height = 58, // Default height
    this.num,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      decoration: BoxDecoration(
        color: PlatformTheme.getMainBkColorSection4(context),
        borderRadius: BorderRadius.circular(44),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.04),
            blurRadius: 12,
            spreadRadius: 5,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: List.generate(items.length, (index) {
          return Expanded(
            child: GestureDetector(
              onTap: () => onTap(index),
              // onTapDown: (v) => onTap(index),
              onDoubleTap: onDoubleTap != null
                  ? () {
                      onDoubleTap!(index);
                    }
                  : null,
              // onLongPress: () {
              //   if (onLongPress != null) onLongPress!(index);
              // },
              behavior: HitTestBehavior.translucent,
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    buildBadgeNormal(
                        context,
                        AnimatedSwitcher(
                          duration: Duration(milliseconds: 200),
                          child: ShaderMask(
                            shaderCallback: (bounds) {
                              // Apply a gradient shader if selected, else a solid color
                              return LinearGradient(
                                colors: [
                                  currentIndex == index
                                      ? selectedItemColor
                                      : unselectedItemColor,
                                  currentIndex == index
                                      ? selectedItemColor.withOpacity(0.7)
                                      : unselectedItemColor.withOpacity(0.7),
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                              ).createShader(bounds);
                            },
                            child: Icon(
                              (items[index].icon as Icon).icon,
                              color: currentIndex == index
                                  ? selectedItemColor
                                  : unselectedItemColor,
                              size: 26,
                            ),
                          ),
                        ),
                        items[index].badge ?? 0,
                        color: Colors.red),
                    if (showSelectedLabels || showUnselectedLabels)
                      Text(
                        // items[index].label ?? '',
                        '',
                        style: TextStyle(
                          color: currentIndex == index
                              ? selectedItemColor
                              : unselectedItemColor,
                          fontSize: 12,
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
