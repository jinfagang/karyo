import 'package:fluentui_system_icons/fluentui_system_icons.dart';
import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class MarkdownField extends StatelessWidget {
  final int maxLines;
  final TextEditingController? controller;
  final TextInputAction? textInputAction;
  final TextStyle? textStyle;
  final Function(String)? onChanged;
  final bool autofocus;
  final bool isVIP;
  final FocusNode? focusNode;

  const MarkdownField({
    Key? key,
    this.maxLines = 999,
    this.controller,
    this.onChanged,
    this.textStyle,
    this.textInputAction,
    this.autofocus = true,
    this.focusNode,
    this.isVIP = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        cursorColor: Colors.amber,
        controller: controller,
        autofocus: (autofocus != null) ? autofocus : true,
        textInputAction: (textInputAction != null)
            ? textInputAction
            : TextInputAction.newline,
        decoration: null,
        focusNode: focusNode,
        // decoration: InputDecoration(
        //     counterText: "💗已输入字数 ", errorText: "超出最大字数，会员畅想无限字数备注详情"),
        style: this.textStyle,
        keyboardType: TextInputType.multiline,
        // maxLength: isVIP ? 9999999 : 500,
        maxLines: (maxLines != null) ? maxLines : 99999,
        onChanged: onChanged,
      ),
    );
  }
}

class MyMarkdownEdtior extends StatefulWidget {
  static const TextStyle NORMAL_TXT_STYLE_ = TextStyle(
      fontSize: 14,
      // color: Color(0xff2d3645),
      fontFamily: FONT_NAME,
      fontFamilyFallback: [FALL_BACK_FONT_NAME, FALL_BACK_FONT_NAME_EMOJI]);

  const MyMarkdownEdtior(
      {Key? key,
      this.controller,
      this.onChanged,
      this.textStyle = NORMAL_TXT_STYLE_,
      this.maxLines = 999,
      this.autofocus = true,
      this.isVIP = false,
      this.focusNode,
      this.saveButton,
      this.textInputAction = TextInputAction.newline})
      : super(key: key);

  final int maxLines;
  final FocusNode? focusNode;
  final TextEditingController? controller;
  final TextInputAction? textInputAction;
  final TextStyle? textStyle;
  final Function(String)? onChanged;
  final bool autofocus;
  final bool isVIP;
  final Widget? saveButton;

  @override
  State<MyMarkdownEdtior> createState() => _MyMarkdownEdtiorState();
}

class _MyMarkdownEdtiorState extends State<MyMarkdownEdtior> {
  final ScrollController _scrollController = ScrollController();
  late MarkdownField markdownField;

  @override
  void initState() {
    markdownField = new MarkdownField(
      controller: widget.controller,
      textStyle: widget.textStyle,
      maxLines: widget.maxLines,
      textInputAction: widget.textInputAction,
      onChanged: widget.onChanged,
      focusNode: widget.focusNode,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints(minHeight: 40, maxHeight: isOnMobile() ? 150 : 300),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          isOnMobile() ? markdownEditer() : Container(),
          Expanded(child: markdownField),
          isOnMobile() ? Container() : markdownEditer(),
        ],
      ),
    );
  }

  Widget markdownEditer() {
    return Row(
      children: [
        Expanded(
          flex: 4,
          child: Transform.translate(
            offset: Offset(0, -1 * MediaQuery.of(context).viewInsets.bottom),
            child: Container(
              color: Colors.transparent,
              child: SizedBox(
                height: 32,
                child: Center(
                  child: Scrollbar(
                    // scrollbarOrientation: ScrollbarOrientation.left,
                    controller: _scrollController,
                    // thumbVisibility: true,
                    child: markdownEdititems(),
                  ),
                ),
              ),
            ),
          ),
        ),
        Expanded(flex: 1, child: widget.saveButton ?? Container())
      ],
    );
  }

  void wrapWith({@required String? leftSide, String? rightSide}) {
    final text = widget.controller!.value.text;
    final selection = widget.controller!.selection;
    final middle = selection.textInside(text);
    final newText = selection.textBefore(text) +
        '$leftSide$middle$rightSide' +
        selection.textAfter(text);

    widget.controller!.value = widget.controller!.value.copyWith(
      text: newText,
      selection: TextSelection.collapsed(
        offset: selection.baseOffset + leftSide!.length + middle.length,
      ),
    );
  }

  void showPrompt() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return buildMyDialog(
              context,
              Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          FluentIcons.premium_person_20_filled,
                          color: ColorBook.yellowVIP,
                          size: 52,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Text(
                          "Markdown编辑器可通过开通白金会员使用，一次开通多端畅想，终生升级迭代，感谢您的支持🙏",
                          textAlign: TextAlign.center,
                          style: NORMAL_TXT_STYLE.copyWith(
                              color: ColorBook.yellowVIP),
                        )
                      ])),
              height: 260);
        });
  }

  ListView markdownEdititems() => ListView(
        shrinkWrap: true,
        controller: _scrollController,
        scrollDirection: Axis.horizontal,
        children: [
          HoverableButton(
            child: Icon(
              Icons.format_size_rounded,
              size: 18,
              color: Colors.grey.withAlpha(200),
            ),
            onPressed: () {
              if (widget.isVIP) {
                wrapWith(leftSide: '# ', rightSide: '');
              } else {
                showPrompt();
              }
            },
          ),
          SizedBox(width: 2),
          HoverableButton(
              child: Icon(
                Icons.format_bold,
                size: 18,
                color: Colors.grey.withAlpha(200),
              ),
              onPressed: () {
                if (widget.isVIP) {
                  wrapWith(
                    leftSide: '**',
                    rightSide: '**',
                  );
                } else {
                  showPrompt();
                }
              }),
          SizedBox(width: 2),
          HoverableButton(
              child: Icon(
                Icons.check_box,
                size: 18,
                color: Colors.grey.withAlpha(200),
              ),
              onPressed: () {
                if (widget.isVIP) {
                  wrapWith(
                    leftSide: '- [ ] ',
                    rightSide: '',
                  );
                } else {
                  showPrompt();
                }
              }),
          SizedBox(width: 2),
          HoverableButton(
              child: Icon(
                Icons.list,
                size: 18,
                color: Colors.grey.withAlpha(200),
              ),
              onPressed: () {
                if (widget.isVIP) {
                  wrapWith(
                    leftSide: '- ',
                    rightSide: '',
                  );
                } else {
                  showPrompt();
                }
              }),
          SizedBox(width: 2),
          HoverableButton(
              child: Icon(
                Icons.format_italic,
                size: 18,
                color: Colors.grey.withAlpha(200),
              ),
              onPressed: () {
                if (widget.isVIP) {
                  wrapWith(
                    leftSide: '*',
                    rightSide: '*',
                  );
                } else {
                  showPrompt();
                }
              }),
          SizedBox(width: 2),
          HoverableButton(
              child: Icon(
                Icons.code,
                size: 18,
                color: Colors.grey.withAlpha(200),
              ),
              onPressed: () {
                if (widget.isVIP) {
                  wrapWith(
                    leftSide: '```',
                    rightSide: '```',
                  );
                } else {
                  showPrompt();
                }
              }),
          SizedBox(width: 2),
          HoverableButton(
              child: Icon(
                Icons.strikethrough_s_rounded,
                size: 18,
                color: Colors.grey.withAlpha(200),
              ),
              onPressed: () {
                if (widget.isVIP) {
                  wrapWith(
                    leftSide: '~~',
                    rightSide: '~~',
                  );
                } else {
                  showPrompt();
                }
              }),
          SizedBox(width: 2),
          HoverableButton(
              child: Icon(
                Icons.link_sharp,
                size: 18,
                color: Colors.grey.withAlpha(200),
              ),
              onPressed: () {
                if (widget.isVIP) {
                  wrapWith(
                    leftSide: '[',
                    rightSide: ']()',
                  );
                } else {
                  showPrompt();
                }
              }),
          SizedBox(width: 2),
          HoverableButton(
              child: Icon(
                Icons.image,
                size: 18,
                color: Colors.grey.withAlpha(200),
              ),
              onPressed: () {
                if (widget.isVIP) {
                  wrapWith(
                    leftSide: '![](https://',
                    rightSide: ')',
                  );
                } else {
                  showPrompt();
                }
              }),
        ],
      );
}


