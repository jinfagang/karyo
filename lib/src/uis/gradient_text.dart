import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

enum GradientType {
  linear,
  radial,
}

/// Direction to apply in the gradient.
enum GradientDirection {
  /// Bottom to top.
  btt,

  /// Left to right.
  ltr,

  /// Right to left.
  rtl,

  /// Top to bottom.
  ttb,
}

/// A gradient text.
class GradientText extends StatelessWidget {
  /// Colors used to show the gradient.
  final List<Color> colors;

  /// Direction in which the gradient will be displayed.
  final GradientDirection? gradientDirection;

  /// The type of gradient to apply.
  final GradientType gradientType;

  /// How visual overflow should be handled.
  final TextOverflow? overflow;

  /// The radius of the gradient, as a fraction of the shortest side
  /// of the paint box.
  final double radius;

  /// If non-null, the style to use for this text.
  final TextStyle? style;

  /// The text to display.
  final String text;

  /// How the text should be aligned horizontally.
  final TextAlign? textAlign;

  /// How the text should be scaled.
  final double? textScaleFactor;

  /// Maximum number of lines for the text to span.
  final int? maxLines;

  /// Gradient stops
  final List<double>? stops;

  const GradientText(
    this.text, {
    required this.colors,
    this.gradientDirection = GradientDirection.ltr,
    this.gradientType = GradientType.linear,
    super.key,
    this.overflow,
    this.radius = 1.0,
    this.style,
    this.textAlign,
    this.stops,
    this.textScaleFactor,
    this.maxLines,
  }) : assert(
          colors.length >= 2,
          'Colors list must have at least two colors',
        );

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (Rect bounds) {
        switch (gradientType) {
          case GradientType.linear:
            final Map<String, Alignment> map = {};
            switch (gradientDirection) {
              case GradientDirection.rtl:
                map['begin'] = Alignment.centerRight;
                map['end'] = Alignment.centerLeft;
                break;
              case GradientDirection.ttb:
                map['begin'] = Alignment.topCenter;
                map['end'] = Alignment.bottomCenter;
                break;
              case GradientDirection.btt:
                map['begin'] = Alignment.bottomCenter;
                map['end'] = Alignment.topCenter;
                break;
              default:
                map['begin'] = Alignment.centerLeft;
                map['end'] = Alignment.centerRight;
            }
            return LinearGradient(
              begin: map['begin']!,
              colors: colors,
              stops: stops,
              end: map['end']!,
            ).createShader(bounds);
          case GradientType.radial:
            return RadialGradient(
              colors: colors,
              radius: radius,
            ).createShader(bounds);
        }
      },
      child: Text(
        text,
        overflow: overflow,
        style: style != null
            ? style?.copyWith(color: Colors.white)
            : const TextStyle(color: Colors.white),
        textScaleFactor: textScaleFactor,
        textAlign: textAlign,
        maxLines: maxLines,
      ),
    );
  }
}

class GradientTextField extends StatelessWidget {
  final List<Color>? colors;
  final GradientDirection? gradientDirection;
  final GradientType gradientType;
  final double radius;
  final int? maxLines;
  final int? minLines;
  final TextStyle? style;
  final TextAlign? textAlign;
  final TextInputAction? textInputAction;
  final double? textScaleFactor;
  final List<double>? stops;
  final TextEditingController controller;
  final ValueChanged<String>? onSubmitted;

  const GradientTextField({
    this.colors,
    this.gradientDirection = GradientDirection.ltr,
    this.gradientType = GradientType.linear,
    this.radius = 1.0,
    this.maxLines = 1,
    this.minLines = 1,
    this.style,
    this.textAlign,
    this.stops,
    this.textInputAction,
    this.textScaleFactor,
    required this.controller,
    this.onSubmitted,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return colors != null
        ? ShaderMask(
            shaderCallback: (Rect bounds) {
              switch (gradientType) {
                case GradientType.linear:
                  final Map<String, Alignment> map = {};
                  switch (gradientDirection) {
                    case GradientDirection.rtl:
                      map['begin'] = Alignment.centerRight;
                      map['end'] = Alignment.centerLeft;
                      break;
                    case GradientDirection.ttb:
                      map['begin'] = Alignment.topCenter;
                      map['end'] = Alignment.bottomCenter;
                      break;
                    case GradientDirection.btt:
                      map['begin'] = Alignment.bottomCenter;
                      map['end'] = Alignment.topCenter;
                      break;
                    default:
                      map['begin'] = Alignment.centerLeft;
                      map['end'] = Alignment.centerRight;
                  }
                  return LinearGradient(
                    begin: map['begin']!,
                    colors: colors!,
                    stops: stops,
                    end: map['end']!,
                  ).createShader(bounds);
                case GradientType.radial:
                  return RadialGradient(
                    colors: colors!,
                    radius: radius,
                  ).createShader(bounds);
              }
            },
            child: _buildTextField(context),
          )
        : _buildTextField(context);
  }

  Widget _buildTextField(BuildContext context) {
    return TextField(
      controller: controller,
      onSubmitted: onSubmitted,
      style: style != null
          ? style?.copyWith(color: Colors.white)
          : const TextStyle(color: Colors.white),
      textAlign: textAlign ?? TextAlign.start,
      maxLines: maxLines,
      minLines: minLines,
      textInputAction: textInputAction,
      cursorColor:
          isDarkModeOnContext(context) ? Colors.amber : tClrPrimary(context),
      decoration: const InputDecoration(
        border: InputBorder.none,
        isDense: true,
        contentPadding: EdgeInsets.zero,
      ),
    );
  }
}

class GradientWidget extends StatelessWidget {
  /// Colors used to show the gradient.
  final List<Color> colors;

  /// Direction in which the gradient will be displayed.
  final GradientDirection? gradientDirection;

  /// The type of gradient to apply.
  final GradientType gradientType;

  /// How visual overflow should be handled.
  final TextOverflow? overflow;

  final double radius;
  final Widget child;

  /// Gradient stops
  final List<double>? stops;

  const GradientWidget({
    required this.child,
    required this.colors,
    this.gradientDirection = GradientDirection.ltr,
    this.gradientType = GradientType.linear,
    super.key,
    this.overflow,
    this.radius = 1.0,
    this.stops,
  }) : assert(
          colors.length >= 2,
          'Colors list must have at least two colors',
        );

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (Rect bounds) {
        switch (gradientType) {
          case GradientType.linear:
            final Map<String, Alignment> map = {};
            switch (gradientDirection) {
              case GradientDirection.rtl:
                map['begin'] = Alignment.centerRight;
                map['end'] = Alignment.centerLeft;
                break;
              case GradientDirection.ttb:
                map['begin'] = Alignment.topCenter;
                map['end'] = Alignment.bottomCenter;
                break;
              case GradientDirection.btt:
                map['begin'] = Alignment.bottomCenter;
                map['end'] = Alignment.topCenter;
                break;
              default:
                map['begin'] = Alignment.centerLeft;
                map['end'] = Alignment.centerRight;
            }
            return LinearGradient(
              begin: map['begin']!,
              colors: colors,
              stops: stops,
              end: map['end']!,
            ).createShader(bounds);
          case GradientType.radial:
            return RadialGradient(
              colors: colors,
              radius: radius,
            ).createShader(bounds);
        }
      },
      child: child,
    );
  }
}
