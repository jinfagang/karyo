import 'dart:math';

import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';
// import 'package:dice_bear/dice_bear.dart';

const String aiAvatarBaseUrl = 'http://db.manaai.cn/uploads/ai_avatars/';
List<String> fakeFemaleAvatarUrls = [
  '${aiAvatarBaseUrl}female/ai_1.jpg',
  '${aiAvatarBaseUrl}female/ai_2.jpg',
  '${aiAvatarBaseUrl}female/ai_3.jpg',
  '${aiAvatarBaseUrl}female/ai_4.jpg',
  '${aiAvatarBaseUrl}female/ai_5.jpg',
  '${aiAvatarBaseUrl}female/ai_6.jpg',
  '${aiAvatarBaseUrl}female/ai_7.jpg',
  '${aiAvatarBaseUrl}female/ai_8.jpg',
  '${aiAvatarBaseUrl}female/ai_9.jpg',
  '${aiAvatarBaseUrl}female/ai_10.jpg',
  '${aiAvatarBaseUrl}female/ai_11.jpg',
  '${aiAvatarBaseUrl}female/ai_12.jpg',
  '${aiAvatarBaseUrl}female/ai_10.jpg',
  '${aiAvatarBaseUrl}female/ai_10.jpg',
  '${aiAvatarBaseUrl}female/ai_10.jpg',
  '${aiAvatarBaseUrl}female/ai_10.jpg',
  '${aiAvatarBaseUrl}female/ai_10.jpg',
];

Widget getRandomAvatar(String seed) {
  // Avatar _avatar = DiceBearBuilder(
  //   sprite: DiceBearSprite.adventurer,
  //   seed: seed,
  //   size: 44,
  // ).build();
  // return _avatar.toImage(width: 88, height: 88);
  return Container();
}

String getRandomAvatarUrl(String seed, String gender) {
  int seedI = 230;
  try {
    seedI = int.parse(seed);
  } catch (e) {
    kLog("$e");
  }

  if (seedI < 0 || seedI > 34) {
    seedI = Random(seedI).nextInt(34);
  }
  String aUrl = '';
  if (gender == '1') {
    aUrl = '${aiAvatarBaseUrl}female/ai_$seedI.jpg';
  } else {
    aUrl = '${aiAvatarBaseUrl}male/ai_$seedI.jpg';
  }
  // return CachedNetworkImageEnhanced(imageUrl: aUrl);
  return aUrl;
}
