import 'package:flutter/material.dart';
import 'package:karyo/karyo.dart';

class TextFieldContainer extends StatelessWidget {
  final Widget? child;
  const TextFieldContainer({
    Key? key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      width: size.width * 0.82,
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
        borderRadius: BorderRadius.circular(22),
        boxShadow: [
          BoxShadow(
              blurRadius: 1,
              spreadRadius: 1,
              color: isDarkModeOnContext(context)
                  ? const Color.fromARGB(255, 22, 22, 22)
                  : const Color.fromARGB(193, 245, 245, 245))
        ],
      ),
      child: child,
    );
  }
}
